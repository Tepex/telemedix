#!/bin/sh
#---------------------------------------------------------------
# Given an xxhdpi image or an App Icon (launcher), this script
# creates different dpis resources
#
# Using: ImageMagick
#
# Example:
# ./resize_image.sh ic_launcher.png
# OR
# ./resize_image.sh my_cool_xxhdpi_image.png
#---------------------------------------------------------------

BASE="app/src/main"
ASSETS_SRC="$BASE/assets"
RES_SRC="$BASE/res";

echo " Creating different dimensions (dips) of "$1" ..."

if [ $1 = "ic_launcher.png" ]; then
    echo "  App icon detected"

    convert ic_launcher.png -resize 144x144 "$RES_SRC/drawable-xxhdpi/ic_launcher.png"
    convert ic_launcher.png -resize 96x96 "$RES_SRC/drawable-xhdpi/ic_launcher.png"
    convert ic_launcher.png -resize 72x72 "$RES_SRC/drawable-hdpi/ic_launcher.png"
    convert ic_launcher.png -resize 48x48 "$RES_SRC/drawable-mdpi/ic_launcher.png"
#    rm -i ic_launcher.png
else

    convert "$ASSETS_SRC/$1" -resize 67% "$RES_SRC/drawable-xhdpi/$1"
    convert "$ASSETS_SRC/$1" -resize 50% "$RES_SRC/drawable-hdpi/$1"
    convert "$ASSETS_SRC/$1" -resize 33% "$RES_SRC/drawable-mdpi/$1"
    cp "$ASSETS_SRC/$1" "$RES_SRC/drawable-xxhdpi/$1"

fi

echo " Done"
