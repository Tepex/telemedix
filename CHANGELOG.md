# Change Log

## Версия 59 *(01.02.2017)*

* Регистрация/авторизация/редактирование пользователя
* UI список консультаций

## Version 58 *(31.01.2017)*

* Исправление текста в заголовке ApplicationFragmentStep8.java:36
* Создание Activity завершения консультации с отправкой отзыва

### v62.6.x (05.04.2017)

* Удаление из NavigationView разделы «Платежи» и «Мой профиль» для незарегистрированных пользвателей.
* Проверка заполнености полей Contact Data

### v62.9.2 (11.04.2017)
* NPE Календаря для даты рождения в «метриках».  
* Убраны лишние кнопки на морде и красные кружки у «консультаций» и «назначений».  
* Поправлено сохранение хронических заболеваний. 

### v62.9.3 (11.04.2017)
* Добавить удаление полей «аллергии» и «хроники» 
-- Поправить permission для Android 6

### v62.9.4 (12.04.2017)
* Сохранять данные в презентере при отключении view

### v62.10.1 (12.04.2017)
* Новая модель данных с copy-on-write

### v62.10.2 (12.04.2017)
* Валидация почты и телефона в контактах

### v63.4.2 (29.04.2017) 
-- Список врачей по категориям
