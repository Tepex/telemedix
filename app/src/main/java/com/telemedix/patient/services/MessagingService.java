package com.telemedix.patient.services;

import android.content.Intent;

import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import static com.telemedix.patient.view.home.SplashActivity.TOKEN_INTENT_FILTER_NAME;
import static com.telemedix.patient.view.home.SplashActivity.EXTRAS_REGISTERED;

/**
 * Обрабтка нотификации о начале консультации.
 * За 10 мин. до начала консультации должно прийти GCM-уведомление о запланированной консльтации
 * с параметрами видеосвязи для Twilio. В ответ необходимо подтвердить готовность, сделав запрос
 * Api.confirmConsultation
 */
public class MessagingService extends FirebaseMessagingService
{
	@Override
	public void onMessageReceived(RemoteMessage remoteMessage)
	{
		LOG.d("MessagingService. New message.");
		String message = remoteMessage.getData().get("message");
		if(message == null)
		{
			LOG.w("Notification message is null!");
			return;
		}
		
		if(GREETING_MESSAGE.equals(message))
		{
			LOG.d("MessagingService. Registration app OK. Notify Splash fragment.");
		
			Intent intent = new Intent(TOKEN_INTENT_FILTER_NAME);
			intent.putExtra(EXTRAS_REGISTERED, true);
			LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
			return;
		}
	}
	
	private static final String GREETING_MESSAGE = "Hello patient!";
	
	private static final Logger LOG = LoggerManager.getLogger();
}
