package com.telemedix.patient.services;

import android.content.Intent;

import android.support.annotation.CallSuper;

import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import static com.telemedix.patient.view.home.SplashActivity.TOKEN_INTENT_FILTER_NAME;
import static com.telemedix.patient.view.home.SplashActivity.EXTRAS_TOKEN;

public class TelemedixInstanceIDService extends FirebaseInstanceIdService 
{
	@Override
	@CallSuper
	public void onCreate()
	{
		super.onCreate();
		LOG.d("TelemedixInstanceIDService.onCreate");
	}
	
	@Override
	public void onTokenRefresh()
	{
		LOG.d("TelemedixInstanceIDService.onTokenRefresh");
		String token = FirebaseInstanceId.getInstance().getToken();
		LOG.d("FCM token received: %s", token);
		/* Уведомление */
		Intent intent = new Intent(TOKEN_INTENT_FILTER_NAME);
		intent.putExtra(EXTRAS_TOKEN, token);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
	
	@Override
	@CallSuper
	public void onDestroy()
	{
		LOG.d("TelemedixInstanceIDService.onDestroy");
		super.onDestroy();
	}
	
	protected static final Logger LOG = LoggerManager.getLogger();
}
