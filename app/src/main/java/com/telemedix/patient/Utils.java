package com.telemedix.patient;

import android.content.Context;

import android.content.pm.PackageManager;

import android.database.Cursor;

import android.Manifest;

import android.net.Uri;

import android.provider.MediaStore;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

import android.util.TypedValue;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.signature.StringSignature;

import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import com.orhanobut.hawk.Hawk;

import com.telemedix.core.model.pojo.User;

import static com.telemedix.core.model.Repository.API_KEY;

public class Utils
{
	public static boolean isEmpty(TextView field)
	{
		return (getTrimText(field) == null);
	}
	
	public static String getTrimText(TextView field)
	{
		if(field == null) return null;
		String ret = field.getText().toString().trim();
		if(ret.length() == 0) return null;
		return ret;
	}
	
	public static GlideUrl getGlideUrl(String uri)
	{
		return new GlideUrl(
			App.getApp().getApiHost()+uri,
			new LazyHeaders.Builder().addHeader(API_KEY, App.getApp().getApiKey()).build());
	}
	
	public static String getAvatarPath(Context context, Uri uri)
	{
		String path;
		Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
		// Source is Dropbox or other similar local file path
		if(cursor == null) path = uri.getPath();
		else
		{
			cursor.moveToFirst();
			int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			path = cursor.getString(idx);
			cursor.close();
		}
		return path;
	}
	
	public static void setAvatar(Context context, ImageView imageView, User user, boolean customHash)
	{
		int avatarSize = context.getResources().getDimensionPixelSize(R.dimen.avatar_size);
		GlideUrl url = getGlideUrl(user.getPhoto(avatarSize, avatarSize));
		
		DrawableRequestBuilder builder = Glide.with(context).load(url);
		if(customHash)
		{
			String avatarHash = Hawk.get(PREFS_AVATAR_HASH_STRING, "no_hash");
			if(avatarHash != null) LOG.d("Avatar hash: %d", avatarHash.hashCode());
			else LOG.d("Avatar hash is NULL");
			builder.signature(new StringSignature(avatarHash));
		}
		builder.placeholder(R.drawable.no_photo_placeholder)
			.error(R.drawable.no_photo_placeholder)
			.into(imageView);
	}
	
	public static void setAvatarHashString(String avatarHashString)
	{
		LOG.d("Set avatar hash: %d", avatarHashString.hashCode());
		Hawk.put(PREFS_AVATAR_HASH_STRING, avatarHashString);
		Hawk.put(PREFS_FULL_AVATAR_HASH_STRING, "full_"+avatarHashString);
	}
	
	/** Спрятать виртуальную клаву */
	public static void hideSoftKeyboard(Context context, View view)
	{
		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	public static final String PREFS_AVATAR_HASH_STRING = "avatar_hash_string";
	public static final String PREFS_FULL_AVATAR_HASH_STRING = "full_avatar_hash_string";
	
	protected static final Logger LOG = LoggerManager.getLogger();
}
