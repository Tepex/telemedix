package com.telemedix.patient.view.doctors;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.support.v4.content.ContextCompat;

import android.support.v7.widget.LinearLayoutManager;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import com.telemedix.core.model.DoctorsListModel;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.pojo.Doctor;
import com.telemedix.core.model.pojo.Scope;

import com.telemedix.core.presenter.DoctorsListPresenter;

import com.telemedix.core.view.DoctorsListView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;

import com.telemedix.patient.view.BaseActivity;

public class DoctorsListActivity extends BaseActivity<DoctorsListPresenter, DoctorsListModel> implements DoctorsListView, DoctorsListFilterAdapter.AdapterListener
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setToolbarTitle(R.string.doctors_list_title);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		/*
		if(currAction == FilterActivity.EXTRA_ACTION_FIND_ALL) searchString = getString(R.string.all_doctors);
		if(searchString != null && !searchString.isEmpty()) setToolbarTitle(searchString);
		*/
		
		listView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		listView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.list_divider), 1));
		
		Scope scope = (Scope)getIntent().getSerializableExtra(EXTRAS_SCOPE);
		getPresenter().loadDoctorsList(scope);
	}
	
	@CallSuper
	@Override
	protected void onStart()
	{
		super.onStart();
		LOG.d("onStart search open: %b", searchView.isSearchOpen());
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode != RESULT_FILTER) return;
		/*
		if(resultCode == FilterActivity.RESULT_OK) init(data);
		else init(null);
		*/
	}
	
	@Override
	protected @LayoutRes int getLayout()
	{
		return R.layout.activity_doctors_list;
	}
	
	@Override
	public DoctorsListPresenter createPresenter()
	{
		DoctorsListPresenter ret = new DoctorsListPresenter();
		ret.onCreate(new DoctorsListModel(), App.getApp().getDoctorsInterceptor());
		return ret;
	}
	
	@Override
	@CallSuper
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.find_doctors, menu);
		MenuItem item = menu.findItem(R.id.action_search);
		if(item != null)
		{
			searchView.setMenuItem(item);
			searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener()
			{
				@Override
				public boolean onQueryTextSubmit(final String query)
				{
					/* Backdoor for developers */
					/*
					if(Utils.processCommand(query))
					{
						Toast.makeText(FindDoctorsActivity.this, "Developer mode: "+App.getApp().isDevMode(), Toast.LENGTH_LONG).show();
						return true;
					}
					*/
					return false;
				}
				
				@Override
				public boolean onQueryTextChange(final String filter)
				{
					LOG.d("filter %s", filter);
					/*
					if(newText.isEmpty()) loadData();
					else loadData(DoctorsPool.get.searchDoctors(newText));
					*/
					/*
					getPresenter().setFilter(newText);
					listView.getAdapter().notifyDataSetChanged();
					*/
					return true;
				}
			});
		}
		return super.onCreateOptionsMenu(menu);
	}
    
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() ==  R.id.action_sort_price)
		{
			/*
			if(lastSort != R.id.action_sort_price) ordering = true;
			else ordering = !ordering;
			lastSort = R.id.action_sort_price;
			DoctorsPool.get
				.getDoctorsObservable(currAction, searchString, DoctorsPool.SORT_PRICE, ordering)
				.subscribe(this::loadData);
			*/
		}
		else if(item.getItemId() == R.id.action_sort_by_rating)
		{
			/*
			if(lastSort != R.id.action_sort_by_rating) ordering = false;
			else ordering = !ordering;
			lastSort = R.id.action_sort_by_rating;
			DoctorsPool.get
				.getDoctorsObservable(currAction, searchString, DoctorsPool.SORT_RATING, ordering)
				.subscribe(this::loadData);
				*/
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	@CallSuper
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		if(menu.findItem(R.id.action_sort) != null)
		{
			//menu.findItem(R.id.action_sort).setVisible(currAction != FilterActivity.EXTRA_ACTION_FIND_ALL);
			menu.findItem(R.id.action_sort).getSubMenu().getItem(0).setChecked(true);
		}
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public void showProgress(boolean show)
	{
		if(show) listView.showProgress();
		else listView.hideProgress();
	}

	
	@Override
	@CallSuper
	public void onBackPressed()
	{
		if(searchView.isSearchOpen()) searchView.closeSearch();
		else super.onBackPressed();
	}
	
	@Override
	@CallSuper
	protected void onDestroy()
	{
		searchView.setOnQueryTextListener(null);
		listView.clear();
		super.onDestroy();
	}
	
	/* View */
	
	@Override
	public void onNew()
	{
		showModel();
	}
	
	@Override
	public void onSuccess()
	{
		finishView();
	}
	
	@Override
	@CallSuper
	public void showModel()
	{
		super.showModel();
		adapter = new DoctorsListFilterAdapter(getPresenter(), this);
		listView.setAdapter(adapter);
	}
	
	/* DoctorsListFilterAdapter.AdapterListener */
	
	@Override
	public void selectDoctor(Doctor doctor)
	{
		LOG.d("Select doctor: %s", doctor);
	}
	
	@Override
	public void openFilter(Scope.Type type)
	{
		/*
		Intent intent = new Intent(this, FilterActivity.class);
		intent.putExtra(FilterActivity.EXTRA_ACTION, scopeType);
		startActivityForResult(intent, RESULT_FILTER);
		*/
	}
	
	private DoctorsListFilterAdapter adapter;
	
	@BindView(R.id.list_view) SuperRecyclerView listView;
	@BindView(R.id.search_view) MaterialSearchView searchView;
	
	public static final String EXTRAS_SCOPE = "scope";
	
	private static final int RESULT_FILTER = 0x1;
}
