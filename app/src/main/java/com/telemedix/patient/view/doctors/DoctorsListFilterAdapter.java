package com.telemedix.patient.view.doctors;

import android.content.Context;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.core.model.pojo.Doctor;
import com.telemedix.core.model.pojo.Scope;

import com.telemedix.core.presenter.DoctorsListPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropTransformation;

public class DoctorsListFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	public DoctorsListFilterAdapter(DoctorsListPresenter presenter, AdapterListener adapterListener)
	{
		this.presenter = presenter;
		this.adapterListener = adapterListener;
		this.isFilterable = true;
	}
	
	@Override
	public int getItemViewType(int position)
	{
		/* Для фильтруемого списка (isFilterable) первый элемент — особенный: отдельное View с кнопками фильтра */
		if(position == 0 && isFilterable) return TYPE_HEADER;
		return TYPE_ITEM;
	}
	
	@Override
	public int getItemCount()
	{
		int size = presenter.getModel().getDoctorsList().size();
		/* Для фильтруемого списка (isFilterable) первый элемент — особенный: отдельное View с кнопками фильтра */
		if(isFilterable && size > 0) ++size;
		return size;
	}
	
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		if(viewType == TYPE_HEADER)
			return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.find_doctor_filter, null, false), presenter.getModel().getScope(), adapterListener);
		return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_list_item, null, false));
	}
	
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder adapterHolder, int position)
	{
		if(!(adapterHolder instanceof ItemHolder)) return;
		
		/* Первый элемент списка — заголовок */
		if(isFilterable) --position;
		final Doctor doctor = presenter.getModel().getDoctorsList().get(position);
		final ItemHolder holder = (ItemHolder)adapterHolder;
			
		StringBuilder sb = new StringBuilder(doctor.getShortName());
		if(doctor.getSpecialization() != null) sb.append(", ").append(doctor.getSpecialization().getName());
		holder.title.setText(sb.toString());
			
		sb = new StringBuilder();
		if(doctor.getClinic() != null) sb.append(doctor.getClinic().getName());
		if(doctor.getPrice() != Doctor.NO_PRICE)
			sb.append(", ").append(holder.getContext().getString(R.string.price_format, doctor.getPrice()));
		holder.subtitle.setText(sb.toString());
			
		if(doctor.getUser() != null) Utils.setAvatar(holder.getContext(), holder.avatar, doctor.getUser(), false);
			
		holder.actionIndicator.setImageResource((doctor.isVideoEnabled()) ? R.drawable.videocam : R.drawable.audiocall);
		holder.actionIndicator.setEnabled(doctor.isOnline());
			
		if(doctor.isOnline())
		{
			holder.online.setVisibility(View.VISIBLE);
			holder.offlineInfo.setVisibility(View.GONE);
		}
		else
		{
			holder.online.setVisibility(View.GONE);
			holder.offlineInfo.setVisibility(View.VISIBLE);
		}
			
		if(adapterListener != null) holder.itemView.setOnClickListener(view->adapterListener.selectDoctor(doctor));
		holder.rate.setRating(doctor.getRating());
	}
	
	private static final int TYPE_HEADER = 1;
	private static final int TYPE_ITEM = 0;
	
	private DoctorsListPresenter presenter;
	private AdapterListener adapterListener;
	private boolean isFilterable;
	
	protected static final Logger LOG = LoggerManager.getLogger();
	
	public interface AdapterListener
	{
		void openFilter(Scope.Type type);
		void selectDoctor(Doctor doctor);
	}
	
	static class HeaderHolder extends RecyclerView.ViewHolder
	{
		public HeaderHolder(View itemView, Scope scope, AdapterListener listener)
		{
			super(itemView);
			ButterKnife.bind(this, itemView);
			if(scope != null)
			{
				if(scope.getType() == Scope.Type.SPEC) filterSpecialisationBtn.setText(scope.getEntity().getName());
				else if(scope.getType() == Scope.Type.CITY) filterCityBtn.setText(scope.getEntity().getName());
				else if(scope.getType() == Scope.Type.CLINIC) filterClinicBtn.setText(scope.getEntity().getName());
			}
			filterSpecialisationBtn.setOnClickListener(view->listener.openFilter(scope.getType()));
			filterCityBtn.setOnClickListener(view->listener.openFilter(scope.getType()));
			filterClinicBtn.setOnClickListener(view->listener.openFilter(scope.getType()));
		}
		
		@BindView(R.id.filter_spec_btn) Button filterSpecialisationBtn;
		@BindView(R.id.filter_city_btn) Button filterCityBtn;
		@BindView(R.id.filter_clinic_btn) Button filterClinicBtn;
	}
	
	static class ItemHolder extends RecyclerView.ViewHolder
	{
		public ItemHolder(View itemView)
		{
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
		
		public Context getContext()
		{
			return title.getContext();
		}
		
		@BindView(R.id.title) TextView title;
		@BindView(R.id.subtitle) TextView subtitle;
		@BindView(R.id.avatar) ImageView avatar;
		@BindView(R.id.online) TextView online;
		@BindView(R.id.offline_info) TextView offlineInfo;
		@BindView(R.id.action_indicator) ImageView actionIndicator;
		@BindView(R.id.rate) RatingBar rate;
    }
}
