package com.telemedix.patient.view.user;

import android.content.Context;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.inputmethod.InputMethodManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.orhanobut.hawk.Hawk;

import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.pojo.Address;
import com.telemedix.core.model.pojo.ClosestRelative;
import com.telemedix.core.model.pojo.User;

import com.telemedix.core.model.PatientContactsModel;

import com.telemedix.core.model.Model.State;

import com.telemedix.core.presenter.PatientContactsPresenter;

import static com.telemedix.core.presenter.PatientContactsPresenter.ERR_EMAIL;
import static com.telemedix.core.presenter.PatientContactsPresenter.ERR_PHONE;

import com.telemedix.core.view.PatientContactsView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseFragment;

/**
 *
 */
public class ContactDataFragment extends BaseFragment<PatientContactsView, PatientContactsModel, PatientInterceptor, PatientContactsPresenter> implements PatientContactsView
{
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_contact_data);
		
		getBaseActivity().setToolbarTitle(R.string.action_contact_data);
		if(getBaseActivity() instanceof RegistrationActivity) getBaseActivity().setToolbarNextButton(true);
		
		nextButton.setText(R.string.action_fill_later);
		return view;
	}
	
	@Override
	@CallSuper
	public void onResume()
	{
		super.onResume();
		editFirstName.addTextChangedListener(textWatcher);
		editMiddleName.addTextChangedListener(textWatcher);
		editLastName.addTextChangedListener(textWatcher);
		editEmail.addTextChangedListener(textWatcher);
		
		editCountry.addTextChangedListener(textWatcher);
		editCity.addTextChangedListener(textWatcher);
		editAddress.addTextChangedListener(textWatcher);
		
		editClosestRelativeFirstName.addTextChangedListener(textWatcher);
		editClosestRelativeLastName.addTextChangedListener(textWatcher);
		editClosestRelativePhone.addTextChangedListener(textWatcher);
	}
	
	@Override
	public void onPause()
	{
		editFirstName.removeTextChangedListener(textWatcher);
		editMiddleName.removeTextChangedListener(textWatcher);
		editLastName.removeTextChangedListener(textWatcher);
		editEmail.removeTextChangedListener(textWatcher);
		
		editCountry.removeTextChangedListener(textWatcher);
		editCity.removeTextChangedListener(textWatcher);
		editAddress.removeTextChangedListener(textWatcher);
		
		editClosestRelativeFirstName.removeTextChangedListener(textWatcher);
		editClosestRelativeLastName.removeTextChangedListener(textWatcher);
		editClosestRelativePhone.removeTextChangedListener(textWatcher);
		super.onPause();
	}
	
	/* Base */
	
	@Override
	public PatientContactsPresenter createPresenter()
	{
		PatientContactsPresenter ret = new PatientContactsPresenter();
		ret.onCreate(new PatientContactsModel(), App.getApp().getPatientInterceptor());
		return ret;
	}
	
	/* View */

	@Override
	public void onError(State state)
	{
		if(state != null)
		{
			if((state.getErrorCode() & ERR_EMAIL) != 0) tilEmail.setError(getStringRes(R.string.err_contacts_email));
			else tilEmail.setError(null);
			
			if((state.getErrorCode() & ERR_PHONE) != 0) tilClosestRelativePhone.setError(getStringRes(R.string.err_contacts_phone));
			else tilClosestRelativePhone.setError(null);
		}
	}
	
	@Override
	@CallSuper
	public void showModel()
	{
		super.showModel();
		blockListeners = true;
		
		if(getModel().getUser() != null)
		{
			editFirstName.setText(getModel().getUser().getFirstName());
			editMiddleName.setText(getModel().getUser().getPatronymic());
			editLastName.setText(getModel().getUser().getLastName());
			editEmail.setText(getModel().getUser().getEmail());
		}
			
		// грязный хак!
		editCountry.setText("Россия");
			
		if(getModel().getAddress() != null)
		{
			editCity.setText(getModel().getAddress().getCity());
			editAddress.setText(getModel().getAddress().getAddress());
		}
			
		if(getModel().getClosestRelative() != null)
		{
			editClosestRelativeFirstName.setText(getModel().getClosestRelative().getFirstName());
			editClosestRelativeLastName.setText(getModel().getClosestRelative().getLastName());
			editClosestRelativePhone.setText(getModel().getClosestRelative().getPhone());
		}
		
		blockListeners = false;
		
		updateNextButton(getPresenter().copyOnWrite());
	}
	
	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		getModel().setState(State.NEW);
		if(getBaseActivity() instanceof RegistrationActivity) getBaseActivity().goFragment(new MetricsFragment(), true);
		else getBaseActivity().finish();
	}
	
	@Override
	public String getFirstName()
	{
		return Utils.getTrimText(editFirstName);
	}
	
	@Override
	public String getMiddleName()
	{
		return Utils.getTrimText(editMiddleName);
	}
	
	@Override
	public String getLastName()
	{
		return Utils.getTrimText(editLastName);
	}
	
	@Override
	public String getEmail()
	{
		return Utils.getTrimText(editEmail);
	}
	
	@Override
	public String getCity()
	{
		return Utils.getTrimText(editCity);
	}
	
	@Override
	public String getAddress()
	{
		return Utils.getTrimText(editAddress);
	}
	
	@Override
	public String getClosestRelativeFirstName()
	{
		return Utils.getTrimText(editClosestRelativeFirstName);
	}
	
	@Override
	public String getClosestRelativeLastName()
	{
		return Utils.getTrimText(editClosestRelativeLastName);
	}
	
	@Override
	public String getClosestRelativePhone()
	{
		return Utils.getTrimText(editClosestRelativePhone);
	}
	
	@Override
	@OnClick(R.id.next_btn)
	public void nextStep()
	{
		LOG.d("nextStep");
		getPresenter().saveData();
	}
	
	@Override
	public void updateNextButton(boolean isChanged)
	{
		if(isChanged) nextButton.setText(R.string.action_save);
		else nextButton.setText(R.string.action_fill_later);
	}
	
	@BindView(R.id.first_name_input) EditText editFirstName;
	@BindView(R.id.middle_name_input) EditText editMiddleName;
	@BindView(R.id.last_name_input) EditText editLastName;
	@BindView(R.id.email_input) EditText editEmail;
	@BindView(R.id.email_til) TextInputLayout tilEmail;
	
	@BindView(R.id.country_input) EditText editCountry;
	@BindView(R.id.city_input) EditText editCity;
	@BindView(R.id.address_input) EditText editAddress;
	
	@BindView(R.id.emergency_first_name_input) EditText editClosestRelativeFirstName;
	@BindView(R.id.emergency_last_name_input) EditText editClosestRelativeLastName;
	@BindView(R.id.emergency_phone_input) EditText editClosestRelativePhone;
	@BindView(R.id.emergency_phone_til) TextInputLayout tilClosestRelativePhone;
	
	@BindView(R.id.next_btn) Button nextButton;
	
	private boolean blockListeners = false;
		
	private TextWatcher textWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
		{
			if(!blockListeners) updateNextButton(getPresenter().copyOnWrite());
		}
			
		@Override
		public void beforeTextChanged(final CharSequence s, final int start, final int count, final int end) {}
		@Override
		public void afterTextChanged(final Editable pEditable) {}
	};
}
