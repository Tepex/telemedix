package com.telemedix.patient.view.user;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.Bundle;

import android.provider.Telephony.Sms.Intents;

import android.support.annotation.CallSuper;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import android.support.v7.app.AlertDialog;

import android.telephony.SmsMessage;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.orhanobut.hawk.Hawk;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.ValidateCodeModel;

import com.telemedix.core.presenter.ValidateCodePresenter;

import com.telemedix.core.view.ValidateCodeView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseFragment;
import com.telemedix.patient.view.MenuActivity;

public class ValidateCodeFragment extends BaseFragment<ValidateCodeView, ValidateCodeModel, RegisterInterceptor, ValidateCodePresenter> implements ValidateCodeView
{
	@Override
	@CallSuper
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		intentFilter.addAction(Intents.SMS_RECEIVED_ACTION);
	}
	
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_validate_code);
		getBaseActivity().setToolbarNextButton(false);
		
		String phoneNumber = getArguments().getString(ARGS_USER_PHONE);
		getModel().setPhoneNumber(phoneNumber);
		caption.setText(getBaseActivity().getString(R.string.sms_was_send, "+"+phoneNumber));
		
		if(getModel().getState() == State.ERROR) setSmsCode("");
		return view;
	}

	@Override
	@CallSuper
	public void onResume()
	{
		super.onResume();
		codeInput.addTextChangedListener(textWatcher);
		getBaseActivity().registerReceiver(broadcastReceiver, intentFilter);
	}

	@Override
	@CallSuper
	public void onPause()
	{
		if(textWatcher != null) codeInput.removeTextChangedListener(textWatcher);
		if(broadcastReceiver != null) getBaseActivity().unregisterReceiver(broadcastReceiver);
		super.onPause();
	}
	
	@Override
	@CallSuper
	public void onDestroyView()
	{
		textWatcher = null;
		broadcastReceiver = null;
		super.onDestroyView();
	}
	
	/* Base */
	
	@Override
	public ValidateCodePresenter createPresenter()
	{
		ValidateCodePresenter ret = new ValidateCodePresenter();
		ret.onCreate(new ValidateCodeModel(), App.getApp().getRegisterInterceptor());
		return ret;
	}
	
	/* ValidateCodeView */
	
	@Override
	public void onError(State state)
	{
		if(state != null)
		{
			codeInputLayout.setError(" ");
			getBaseActivity().showAlertDialog(R.string.error_title, R.string.invalid_code);
		}
		else codeInputLayout.setError(null);
	}

	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		LOG.d("savePatientInfo: %s", getModel());
		App.getApp().setPatientInfo(getModel().getPhoneNumber(), getModel().getPatientId());
		getModel().setState(State.NEW);
		if(getBaseActivity() instanceof RegistrationActivity)
		{
			Bundle args = new Bundle();
			args.putString(ARGS_USER_PHONE, getModel().getPhoneNumber());
			ChangePinCodeFragment fragment = new ChangePinCodeFragment();
			fragment.setArguments(args);
			getBaseActivity().goFragment(fragment, true);
		}
		else getBaseActivity().showAlertDialog(R.string.attention_title, R.string.change_number_ok, (di, i)->
		{
			di.dismiss();
			getBaseActivity().goActivityFragment(MenuActivity.class, SecurityFragment.class, true);
		});
	}

	@Override
	@OnClick(R.id.change_phone_btn)
	public void onChangeButtonClick()
	{
		getBaseActivity().getSupportFragmentManager().popBackStack();
	}
	
	@Override
	@OnClick(R.id.send_code_again_btn)
	public void onSendAgainClick()
	{
		getPresenter().resendSms();
	}
	
	@Override
	public void setSmsCode(String code)
	{
		codeInput.setText(code);
	}
	
	public static final String ARGS_USER_PHONE = "phoneNumber";
	
	@BindView(R.id.til_code_input) TextInputLayout codeInputLayout;
	@BindView(R.id.code_input) TextInputEditText codeInput;
	@BindView(R.id.validate_code_caption) TextView caption;
	
	private IntentFilter intentFilter = new IntentFilter();
	
	/** SMS event receiver */
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(final Context context, final Intent intent)
		{
			SmsMessage[] messages = Intents.getMessagesFromIntent(intent);
			for(int i = 0; i < messages.length; ++i) getPresenter().onReceiveSms(messages[i].getMessageBody());
		}
	};

	private TextWatcher textWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
		{
			getPresenter().onSmsCodeChanged(s.toString().trim());
		}
			
		@Override
		public void beforeTextChanged(final CharSequence s, final int start, final int count, final int end) {}
		@Override
		public void afterTextChanged(final Editable pEditable) {}
	};
}
