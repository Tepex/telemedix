package com.telemedix.patient.view;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import com.telemedix.core.interceptor.Interceptor;
import com.telemedix.core.model.Model;
import com.telemedix.core.model.Model.State;
import com.telemedix.core.presenter.Presenter;
import com.telemedix.core.presenter.PresenterCache;
import com.telemedix.core.view.View;

import com.telemedix.patient.R;

import java.util.HashMap;

public abstract class BaseFragment<V extends View, M extends Model, I extends Interceptor, P extends Presenter<V, M, I>> extends Fragment implements View
{
	@Override
	@CallSuper
	public void onAttach(Context context)
	{
		super.onAttach(context);
		if(!(context instanceof BaseActivity))
			throw new RuntimeException(getClass().getSimpleName()+" must be attached to BaseActivity");
		presenterCache = ((BaseActivity)context).getPresenterCache();
	}
	
	protected android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState, @LayoutRes int layout)
	{
		android.view.View root = inflater.inflate(layout, container, false);
		unbinder = ButterKnife.bind(this, root);
		return root;
	}
	
	/** Достаем презентер из кеша или создаем новый если в кеше его нет. */
	@Override
	@CallSuper
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(savedInstanceState == null) presenterId = presenterCache.generateId();
		else presenterId = savedInstanceState.getLong(PRESENTER_INDEX_KEY);
		presenter = presenterCache.get(presenterId);
		if(presenter == null)
		{
			presenter = createPresenter();
			if(presenter != null) presenterCache.put(presenterId, presenter);
		}
	}
	
	@Override
	@CallSuper
	public void onViewCreated(android.view.View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
//		presenter.bindView(getPresenterView());
	}
	
	@Override
	@CallSuper
	public void onResume()
	{
		super.onResume();
		presenter.bindView(getPresenterView());
		isDestroyedBySystem = false;
	}
	
	@Override
	@CallSuper
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		isDestroyedBySystem = true;
		outState.putLong(PRESENTER_INDEX_KEY, presenterId);
	}
	
	@Override
	@CallSuper
	public void onDestroyView()
	{
		presenter.unbindView();
		unbinder.unbind();
		super.onDestroyView();
	}

	@Override
	@CallSuper
	public void onDestroy()
	{
		// убит пользователем
		if(!isDestroyedBySystem)
		{
			LOG.d("%s: Destroyed by user. Clearing presenter", getClass().getSimpleName());
			presenterCache.remove(presenterId);
			presenter.onDestroy();
		}
		presenter = null;
		unbinder = null;
		super.onDestroy();
	}
	
	public BaseActivity getBaseActivity()
	{
		return (BaseActivity)getActivity();
	}
	
	protected String getStringRes(@StringRes int id)
	{
		return getActivity().getString(id);
	}
	
	/**
	 * Признак «одноразового» фрагмента.
	 *
	 * @return true — перепрыгиваем через этот фрагмент при обратной навигации.
	 */
	public boolean isSkipBackNavigation()
	{
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public V getPresenterView()
	{
		return (V)this;
	}
	
	public M getModel()
	{
		return presenter.getModel();
	}
	
	/** Правая кнопка в Toolbar */
	public void nextStep() {}
	public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {}
	
	/* View */
	
	@Override
	public void networkError(Throwable thr)
	{
		getBaseActivity().networkError(thr);
	}
	
	@Override
	public void serverError(int code)
	{
		getBaseActivity().serverError(code);
	}

	@Override
	public void setState(Model.State state)
	{
		LOG.i("%s.setState %s", getClass().getSimpleName(), state);
		getBaseActivity().showProgress(state == Model.State.LOADING);
		if(state == Model.State.ERROR) onError(state);
		else onError(null);
		
		if(state == Model.State.NEW) onNew();
		else if(state == Model.State.READY) onReady(); 
		else if(state == Model.State.SUCCESS) onSuccess();
	}
	
	@Override
	public void onError(Model.State state) {}
	
	@Override
	public void onNew()
	{
		showModel();
	}
	
	@Override
	public void onReady()
	{
		showModel();
	}
	
	@Override
	public void onSuccess()
	{
		finishView();
	}

	@Override
	public P getPresenter()
	{
		return presenter;
	}
	
	@Override
	public void finishView() {}
	@Override
	public void showModel() {}
	
	protected abstract P createPresenter();
	
	protected static final Logger LOG = LoggerManager.getLogger();
	
	public static final String PRESENTER_INDEX_KEY = "presenter-id";
	
	private PresenterCache presenterCache;
	private long presenterId;
	private P presenter;
	/** Флаг определяет каким образом фрагмент уничтожается — по воле пользователя или системой */
	private boolean isDestroyedBySystem;
	
	private Unbinder unbinder;
}
