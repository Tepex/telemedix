package com.telemedix.patient.view;

import android.content.Intent;

import android.content.res.Configuration;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import android.support.design.widget.NavigationView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;

import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.UserModel;
import com.telemedix.core.model.pojo.User;

import com.telemedix.core.presenter.UserPresenter;

import com.telemedix.core.view.UserView;

import com.telemedix.patient.App;
import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.home.HomeActivity;

import com.telemedix.patient.view.user.ContactDataFragment;
import com.telemedix.patient.view.user.MetricsFragment;
import com.telemedix.patient.view.user.RegistrationActivity;
import com.telemedix.patient.view.user.SecurityFragment;

import static com.telemedix.patient.view.BaseFragment.PRESENTER_INDEX_KEY;

public class MenuActivity extends BaseActivity<UserPresenter, UserModel> implements UserView
{
	@CallSuper
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(navView == null) return;
		
		//getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		setNavigationViewEnabled(true);
		
		navView.setNavigationItemSelectedListener(selectionListener);
		navView.getMenu().findItem(R.id.app_version).setTitle(BuildConfig.VERSION_NAME);
		
		View headerView = navView.getHeaderView(0);
		if(App.getApp().isUserRegistered())
		{
			headerView.findViewById(R.id.nav_register_btn).setVisibility(View.GONE);
			headerView.findViewById(R.id.nav_user_info).setVisibility(View.VISIBLE);
		}
		else
		{
			headerView.findViewById(R.id.nav_register_btn).setVisibility(View.VISIBLE);
			headerView.findViewById(R.id.nav_user_info).setVisibility(View.GONE);
		}
		
		drawerLayout.closeDrawers();
		actionBarDrawerToggle.syncState();
		
		if(savedInstanceState == null)
		{
			BaseFragment startFragment;
			Class fragmentClass = (Class)getIntent().getSerializableExtra(EXTRAS_START_FRAGMENT);
			if(fragmentClass != null)
			{
				try
				{
					startFragment = (BaseFragment)fragmentClass.newInstance();
				}
				catch(ReflectiveOperationException e)
				{
					LOG.e("Can\'t create fragment %s", fragmentClass.getSimpleName(), e);
					throw new RuntimeException(e);
				}
				goFragment(startFragment, true);
			}
		}
		
			/*
			
			
					Fragment fr = getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT);
		if(fr != null && (fr instanceof ContactDataFragment)) navView.getMenu().findItem(R.id.action_contact_data).setChecked(true);

			
			else if(this instanceof PaymentsListActivity) nav_view.getMenu().findItem(R.id.action_payment_methods).setChecked(true);
			else if(this instanceof PaymentsHistoryListActivity) nav_view.getMenu().findItem(R.id.action_payment_history).setChecked(true);
			else if(this instanceof MyDoctorsActivity) nav_view.getMenu().findItem(R.id.action_my_doctors).setChecked(true);
			else if(this instanceof ContactActivity) nav_view.getMenu().findItem(R.id.action_contact_data).setChecked(true);
			else if(this instanceof MyClinicsActivity) nav_view.getMenu().findItem(R.id.action_my_clinics).setChecked(true);
			else if(this instanceof SecurityActivity) nav_view.getMenu().findItem(R.id.action_safety).setChecked(true);
			else if(this instanceof InsuranceListActivity) nav_view.getMenu().findItem(R.id.action_my_documents).setChecked(true);
			else if(this instanceof MyInfoActivity) nav_view.getMenu().findItem(R.id.action_my_information).setChecked(true);
			*/
	}
	
	@Override
	public UserPresenter createPresenter()
	{
		UserPresenter ret = new UserPresenter();
		ret.onCreate(new UserModel(), App.getApp().getPatientInterceptor());
		return ret;
	}
	
	protected @LayoutRes int getLayout()
	{
		return R.layout.activity_base_navigation;
	}
	
	@Override
	@CallSuper
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		actionBarDrawerToggle.syncState();
	}
	
	@Override
	@CallSuper
	protected void onResume()
	{
		super.onResume();
		LOG.d("%s.onResume", getClass().getSimpleName());
		if(this instanceof HomeActivity) navView.getMenu().findItem(R.id.action_home_screen).setChecked(true);
	}
	
	@Override
	@CallSuper
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	@CallSuper
	@Override
	protected void onDestroy()
	{
		navView.setNavigationItemSelectedListener(null);
		selectionListener = null;
		super.onDestroy();
	}

	@CallSuper
	@Override
	public boolean onOptionsItemSelected(final MenuItem item)
	{
		/*
		if(item.getItemId() == android.R.id.home)
		{
			if(actionBarDrawerToggle != null && actionBarDrawerToggle.isDrawerIndicatorEnabled()) drawerLayout.openDrawer(GravityCompat.START);
			else
			{
				LOG.d("onBackPressed. stack count: %s", getSupportFragmentManager().getBackStackEntryCount());
				onBackPressed();
			}
			return true;
		}
		*/
		LOG.d("options item. navview: %b, item: %s", item);
		if(navView.isEnabled())
		{
			if(actionBarDrawerToggle.onOptionsItemSelected(item)) return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/* UserView */
	
	@Override
	public void onNew()
	{
		updateMenuHeader(getPresenter().getModel().getUser());
	}
	
	@Override
	public void updateMenuHeader(User user)
	{
		View headerView = navView.getHeaderView(0);
		ImageView navAvatar = (ImageView)headerView.findViewById(R.id.nav_avatar);
		TextView userNameTextView = (TextView)headerView.findViewById(R.id.nav_username);
		TextView emailTextView = (TextView)headerView.findViewById(R.id.nav_email);
		if(user != null)
		{
			Utils.setAvatar(this, navAvatar, user, true);
			StringBuilder sb = new StringBuilder();
			if(user.getLastName() != null) sb.append(user.getLastName()).append('\n');
			if(user.getFirstName() != null) sb.append(user.getFirstName()).append('\n');
			if(user.getPatronymic() != null) sb.append(user.getPatronymic());
			userNameTextView.setText(sb);
			if(user.getEmail() != null) emailTextView.setText(user.getEmail());
		}
		else
		{
			navAvatar.setImageResource(R.drawable.no_photo_placeholder);
			userNameTextView.setText(null);
			emailTextView.setText(null);
		}
		navView.invalidate();
	}
	
	public void setNavigationViewEnabled(boolean isEnabled)
	{
		LOG.d("%s.setNavigationViewEnabled: %b", getClass().getSimpleName(), isEnabled);
		actionBarDrawerToggle.setDrawerIndicatorEnabled(isEnabled);
		navView.setEnabled(isEnabled);
		if(isEnabled)
		{
			navView.setVisibility(View.VISIBLE);
			drawerLayout.addDrawerListener(actionBarDrawerToggle);
			drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
			actionBarDrawerToggle.setToolbarNavigationClickListener(null);
		}
		else
		{
			navView.setVisibility(View.GONE);
			drawerLayout.removeDrawerListener(actionBarDrawerToggle);
			drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
			// Хак для ситуации, когда гамбургер заменяется на стрелку
			actionBarDrawerToggle.setToolbarNavigationClickListener(v->onBackPressed());
		}
		drawerLayout.closeDrawers();
		actionBarDrawerToggle.syncState();
	}
	
	public void goRegistration(View view)
	{
		drawerLayout.closeDrawers();
		actionBarDrawerToggle.syncState();
		goActivity(RegistrationActivity.class, null, !(this instanceof HomeActivity));
	}
	
	private void showNeedRegistrationDialog()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this)
			.setTitle(R.string.attention_title)
			.setMessage(R.string.need_register_text)
			.setPositiveButton(R.string.action_register, (di, i)->goRegistration(null))
			.setNegativeButton(R.string.action_cancel, (di, i)->di.dismiss())
			.setCancelable(true);
		builder.show();
	}
	
	@Nullable
	@BindView(R.id.nav_view) NavigationView navView;
	@BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
	
	private ActionBarDrawerToggle actionBarDrawerToggle;
	
	private NavigationView.OnNavigationItemSelectedListener selectionListener = item->
	{
		LOG.d("selectionListener");
		drawerLayout.closeDrawers();
		actionBarDrawerToggle.syncState();
		if(item.isChecked()) return true;
		
		/* Если Activity запускается не из HomeActivity, то финишировать текущее. */
		if(item.getItemId() == R.id.action_home_screen) goActivity(HomeActivity.class, null, true);
		else if(item.getItemId() == R.id.action_payment_history)
		{
			if(!App.getApp().isUserRegistered()) showNeedRegistrationDialog();
			else showAlertDialog("Заглушка", "Go payment history");
		}
		else if(item.getItemId() == R.id.action_contact_data)
		{
			if(!App.getApp().isUserRegistered()) showNeedRegistrationDialog();
			else goActivityFragment(MenuActivity.class, ContactDataFragment.class, !(this instanceof HomeActivity));
		}
		else if(item.getItemId() == R.id.action_safety)
		{
			if(!App.getApp().isUserRegistered()) showNeedRegistrationDialog();
			else goActivityFragment(MenuActivity.class, SecurityFragment.class, !(this instanceof HomeActivity));
		}
		else if(item.getItemId() == R.id.action_my_information)
		{
			if(!App.getApp().isUserRegistered()) showNeedRegistrationDialog();
			else goActivityFragment(MenuActivity.class, MetricsFragment.class, !(this instanceof HomeActivity));
		}
		else if(item.getItemId() == R.id.action_my_doctors)
		{
			if(!App.getApp().isUserRegistered()) showNeedRegistrationDialog();
			else showAlertDialog("Заглушка", "My doctors");
		}
		else if(item.getItemId() == R.id.action_contact_support) showAlertDialog("Заглушка", "Support");
		else if(item.getItemId() == R.id.action_faq) showAlertDialog("Заглушка", "FAQ");
		else if(item.getItemId() == R.id.action_terms) showAlertDialog("Заглушка", "Terms");
		else return false;
		return true;
	};
}
