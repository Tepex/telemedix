package com.telemedix.patient.view.user;

import android.app.DatePickerDialog;
import android.app.Dialog;

import android.content.Context;
import android.content.Intent;

import android.database.Cursor;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import android.support.v4.app.DialogFragment;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.inputmethod.InputMethodManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.orhanobut.hawk.Hawk;

import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.pojo.BloodType;
import com.telemedix.core.model.pojo.User;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.PatientMetricsModel;

import com.telemedix.core.presenter.PatientMetricsPresenter;

import static com.telemedix.core.presenter.PatientMetricsPresenter.ERR_BIRTHDAY;
import static com.telemedix.core.presenter.PatientMetricsPresenter.ERR_HEIGHT;
import static com.telemedix.core.presenter.PatientMetricsPresenter.ERR_WEIGHT;

import com.telemedix.core.view.PatientMetricsView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseFragment;
import com.telemedix.patient.view.MenuActivity;

import org.joda.time.LocalDate;

import static android.app.Activity.RESULT_OK;

/**
 *
 */
public class MetricsFragment extends BaseFragment<PatientMetricsView, PatientMetricsModel, PatientInterceptor, PatientMetricsPresenter> implements PatientMetricsView
{
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_metrics);
		
		getBaseActivity().setToolbarTitle(R.string.action_my_information);
		getBaseActivity().setToolbarNextButton(true);
		
		if(getBaseActivity() instanceof MenuActivity)
		{
			getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
			((MenuActivity)getBaseActivity()).setNavigationViewEnabled(true);
		}
				
		nextButton.setText(R.string.action_fill_later);
		return view;
	}
	
	@Override
	@CallSuper
	public void onResume()
	{
		super.onResume();
		editBirthday.addTextChangedListener(textWatcher);
		editHeight.addTextChangedListener(textWatcher);
		editWeight.addTextChangedListener(textWatcher);
		
		editBirthday.setOnFocusChangeListener((view, hasFocus)->{if(hasFocus) setBirthDate(view);});
		
		spinnerBloodType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id)
			{
				if(!blockListeners) updateNextButton(getPresenter().copyOnWrite());
			}
			
			@Override
			public void onNothingSelected(final AdapterView<?> parent) {}
		});
		spinnerSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id)
			{
				if(!blockListeners) updateNextButton(getPresenter().copyOnWrite());
			}
			
			@Override
			public void onNothingSelected(final AdapterView<?> parent) {}
		});
	}
	
	@Override
	@CallSuper
	public void onPause()
	{
		editBirthday.removeTextChangedListener(textWatcher);
		editHeight.removeTextChangedListener(textWatcher);
		editWeight.removeTextChangedListener(textWatcher);
		editBirthday.setOnFocusChangeListener(null);
		spinnerBloodType.setOnItemSelectedListener(null);
		spinnerSex.setOnItemSelectedListener(null);
		super.onPause();
	}
	
	/* Base */
	
	@Override
	public PatientMetricsPresenter createPresenter()
	{
		PatientMetricsPresenter ret = new PatientMetricsPresenter();
		ret.onCreate(new PatientMetricsModel(), App.getApp().getPatientInterceptor());
		return ret;
	}
	
	/* View */
	
	@Override
	public void onError(State state)
	{
		if(state != null)
		{
			if((state.getErrorCode() & ERR_BIRTHDAY) != 0) tilBirthday.setError(getStringRes(R.string.err_birthday));
			else tilBirthday.setError(null);
			
			if((state.getErrorCode() & ERR_HEIGHT) != 0) tilHeight.setError(getStringRes(R.string.err_height));
			else tilHeight.setError(null);

			if((state.getErrorCode() & ERR_WEIGHT) != 0) tilWeight.setError(getStringRes(R.string.err_weight));
			else tilWeight.setError(null);
		}
	}
	
	@Override
	@CallSuper
	public void showModel()
	{
		super.showModel();
		blockListeners = true;
		
		if(getModel().getUser() != null)
		{
			String birthday = getModel().getUser().getFormattedBirthday();
			if(birthday != null) editBirthday.setText(birthday);
			if(spinnerSex.getCount() == 0) spinnerSex.setAdapter(new ArrayAdapter<Sex>(getBaseActivity(), android.R.layout.simple_spinner_item, Sex.values(getActivity())));
			spinnerSex.setSelection(Sex.getIndexOf(getModel().getUser().getSex()));
		}
		
		editHeight.setText(getModel().getHeightStr());
		editWeight.setText(getModel().getWeightStr());
		if(spinnerBloodType.getCount() == 0) spinnerBloodType.setAdapter(new ArrayAdapter<BloodType>(getBaseActivity(), android.R.layout.simple_spinner_item, BloodType.values()));
		spinnerBloodType.setSelection(getModel().getBloodType().ordinal());
		
		Utils.setAvatar(getActivity(), avatar, getModel().getUser(), true);
		blockListeners = false;
		updateNextButton(getPresenter().copyOnWrite());
	}
	
	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		getModel().setState(State.NEW);
		getBaseActivity().goFragment(new AllergiesChronicDiseasesFragment(), true);	
	}
	
	@Override
	public String getBirthday()
	{
		return Utils.getTrimText(editBirthday); 
	}
	
	@Override
	public String getHeight()
	{
		return Utils.getTrimText(editHeight); 
	}
	
	@Override
	public String getWeight()
	{
		return Utils.getTrimText(editWeight); 
	}
	
	@Override
	public BloodType getBloodType()
	{
		return (BloodType)spinnerBloodType.getSelectedItem();
	}
	
	@Override
	public String getSex()
	{
		return ((Sex)spinnerSex.getSelectedItem()).getValue();
	}
	
	@Override
	public void updateNextButton(boolean isChanged)
	{
		if(isChanged) nextButton.setText(R.string.action_save);
		else nextButton.setText(R.string.action_fill_later);
	}
	
	@Override
	@OnClick(R.id.next_btn)
	public void nextStep()
	{
		LOG.d("nextStep");
		getPresenter().saveData();
	}
	
	@OnClick(R.id.upload_photo_btn)
	public void openAvatarChooser()
	{
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType("image/*");
		getBaseActivity().startActivityForResult(intent, SELECT_PHOTO);
    }
    
	@Override
	public void onActivityResult(final int requestCode, final int resultCode, final Intent data)
	{
		if(requestCode != SELECT_PHOTO) return;
		if(resultCode != RESULT_OK) return; 
		
		String path = Utils.getAvatarPath(getActivity(), data.getData());
		/* Состоит из id пользователя и локального пути к картинке в телефоне. Таким образом эта строка уникальна для
		 * пользователя и его аватарки. id пользователя нужно чтобы различать разных пользователей приложения на
		 * телефоне которые используют одну и туже картинку. Путь к картинке нужен чтобы различать разные аватарки
		 * одного и того же пользователя. */
		String avatarHashString = getModel().getUser().getId()+path;
		Utils.setAvatarHashString(avatarHashString);
		LOG.d("Upload photo path: %s, %d", path, avatarHashString.hashCode());
		
		getPresenter().uploadPhoto(path);
	}
	
	@OnClick(R.id.my_info_birthday)
	public void setBirthDate(View v)
	{
		DatePickerFragment fragment = new DatePickerFragment();
		Bundle args = new Bundle();
		args.putString(ARGS_BIRTHDAY, Utils.getTrimText(editBirthday));
		fragment.setArguments(args);
		fragment.setCallback(date->
			{
				LOG.d("set date: %s", date);
				editBirthday.setText(date);
			});
		fragment.show(getActivity().getSupportFragmentManager(), "datePicker");
	}
	
	@OnClick(R.id.myInfoAvatar)
	public void openAvatarFullScreen()
	{
		String photoUri = null;
		if(getPresenter().getModel().getUser() != null) photoUri = getPresenter().getModel().getUser().getPhoto(0, 0);
		LOG.d("Origin avatar uri: %s", photoUri);
		if(photoUri != null)
		{
			Bundle extras = new Bundle();
			extras.putString(FullScreenPhotoActivity.EXTRA_PHOTO_URL, photoUri);
			getBaseActivity().goActivity(FullScreenPhotoActivity.class, extras, false);
		}
	}
		
	@BindView(R.id.my_info_birthday) TextView editBirthday;
	@BindView(R.id.my_info_birthday_til) TextInputLayout tilBirthday;
	@BindView(R.id.my_info_height) EditText editHeight;
	@BindView(R.id.my_info_height_til) TextInputLayout tilHeight;
	@BindView(R.id.my_info_weight) EditText editWeight;
	@BindView(R.id.my_info_weight_til) TextInputLayout tilWeight;
	@BindView(R.id.my_info_blood_type) Spinner spinnerBloodType;
	@BindView(R.id.my_info_sex) Spinner spinnerSex;
	@BindView(R.id.myInfoAvatar) ImageView avatar;
	
	@BindView(R.id.next_btn) Button nextButton;
	
	private boolean blockListeners = false;
	
	private static final String ARGS_BIRTHDAY = "birthday";
	private static final int SELECT_PHOTO = 1;
	
	private TextWatcher textWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
		{
			if(!blockListeners) updateNextButton(getPresenter().copyOnWrite());
		}
			
		@Override
		public void beforeTextChanged(final CharSequence s, final int start, final int count, final int end) {}
		@Override
		public void afterTextChanged(final Editable pEditable) {}
	};
	
	interface OnBirthDateSet
	{
		void onSet(String date);
	}
	
	public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
	{
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState)
		{
			String dateString = getArguments().getString(ARGS_BIRTHDAY);
			LocalDate date = null;
			if(dateString != null)
			{
				try
				{
					date = com.telemedix.core.Utils.DATE_FORMATTER.parseLocalDate(dateString);
				}
				catch(Exception e) {}
			}
			if(date == null) date = LocalDate.now().minusYears(30);
			return new DatePickerDialog(getActivity(), this, date.getYear(), date.getMonthOfYear()-1, date.getDayOfMonth());
		}
		
		public void setCallback(OnBirthDateSet callback)
		{
			this.callback = callback;
		}
		
		@Override
		public void onDateSet(DatePicker view, int year, int month, int day)
		{
			if(callback != null) callback.onSet(day+"."+(month+1)+"."+year);
		}
		
		@Override
		@CallSuper
		public void onDestroy()
		{
			callback = null;
			super.onDestroy();
		}
		
		private OnBirthDateSet callback;
	}
	
	public static class Sex
	{
		private Sex(String value, String name)
		{
			this.value = value;
			this.name = name;
		}
		
		@Override
		public String toString()
		{
			return name;
		}
		
		public String getValue()
		{
			return value;
		}
		
		public static int getIndexOf(String search)
		{
			if(User.FEMALE.equals(search)) return 1;
			return 0;
		}
		
		public static Sex[] values(Context context)
		{
			if(values == null) values = new Sex[] {new Sex(User.MALE, context.getString(R.string.sex_male)), new Sex(User.FEMALE, context.getString(R.string.sex_female))};
			return values;
		}

		private String value;
		private String name;
		
		private static Sex[] values;
	}
}
