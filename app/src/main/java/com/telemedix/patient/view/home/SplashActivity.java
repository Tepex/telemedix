package com.telemedix.patient.view.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;

import android.view.View;

import com.telemedix.core.model.AppModel;
import com.telemedix.core.model.Model.State;
import com.telemedix.core.presenter.AppPresenter;
import com.telemedix.core.view.AppView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseActivity;

public class SplashActivity extends BaseActivity<AppPresenter, AppModel> implements AppView
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		LOG.i("Check is App regstered…");
		LOG.d("ApiKey: %s", App.getApp().getApiKey());
		
		/* При запуске отображать ввод пин-кода если надо */
		App.getApp().pinCodePassed(false);
		
		if(App.getApp().isAppRegistered())
		{
			goActivity(HomeActivity.class, null, true);
			return;
		}
		
		// Приложение еще не зарегистрировано.
		LOG.d("App is not registered yet. Initializing presenter, model, interceptor");
		
		// Подписываемся на получение FCM токена.
		LOG.d("Registering broadcat receiver for FCM-token and Hello-notification");
		broadcastManager = LocalBroadcastManager.getInstance(this);
		intentFilter = new IntentFilter(TOKEN_INTENT_FILTER_NAME);
		broadcastManager.registerReceiver(tokenReceiver, intentFilter);
		
		// Запрашиваем токен. По первому разу вернет null
		
		LOG.d("Requesting FCM-token…");
		String token = App.getApp().getFcmToken();
		if(token != null) registerApp(token);
		// Ждем сообщения от FCM
	}
	
	@Override
	public AppPresenter createPresenter()
	{
		AppPresenter ret = new AppPresenter();
		ret.onCreate(new AppModel(App.getApp().getUUID(), App.getApp().getIMEI()), App.getApp().getAppInterceptor());
		return ret;
	}
	
	@Override
	protected @LayoutRes int getLayout()
	{
		return NO_LAYOUT;
	}
	
	@Override
	@CallSuper
	protected void onDestroy()
	{
		if(broadcastManager != null && tokenReceiver != null) broadcastManager.unregisterReceiver(tokenReceiver);
		tokenReceiver = null;
		broadcastManager = null;
		super.onDestroy();
	}
	
	/* AppView */
	
	// Вынесено в отдельный метод для юнит-теста
	@Override
	public void registerApp(String token)
	{
		LOG.d("registering App…");
		getPresenter().getModel().setToken(token);
		getPresenter().registerApp(token);
	}

	@Override
	public void onError(State state)
	{
		LOG.e("response error: %s", state.getError());
	}
	
	@Override
	public void onNew()
	{
		// OK от нашего сервера
		// Регистрация приложения завершена!
		finishView();
	}
	
	@Override
	public void onReady()
	{
		showModel();
	}

	@Override
	public void showModel()
	{
		LOG.d("Received token and API key from our server. Waiting Hello-notification. Thread: %s", Thread.currentThread().getName());
		LOG.d("token: %s", getPresenter().getModel().getToken());
		LOG.d("API key: %s", getPresenter().getModel().getApiKey());
	}
	
	@Override
	public void finishView()
	{
		LOG.d("Received Hello-notification. Saving token, API key and start App.");
		App.getApp().saveFcmToken(getPresenter().getModel().getToken());
		App.getApp().saveApiKey(getPresenter().getModel().getApiKey());
		goActivity(HomeActivity.class, null, true);
	}
	
	private boolean isRegistered;
	private IntentFilter intentFilter;
	private LocalBroadcastManager broadcastManager;
	/** Приемник сообщений от FCM  и от нашего сервера */
	private BroadcastReceiver tokenReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			// Пришла Hello-нотификация от нашего сервера.
			if(intent.getBooleanExtra(EXTRAS_REGISTERED, false)) setState(State.READY);
			// Пришел токен от FCM. Регистрируем приложение на нашем сервере, получаем API key
			// и ждем Hello-нотификации от сервера для завершения регистрации.
			else
			{
				String token = intent.getStringExtra(EXTRAS_TOKEN);
				if(token != null) registerApp(token);
			}
		}
	};
	
	public static final String TOKEN_INTENT_FILTER_NAME = "token_intent_filter";
	public static final String EXTRAS_TOKEN = "token";
	public static final String EXTRAS_REGISTERED = "registered";
}
