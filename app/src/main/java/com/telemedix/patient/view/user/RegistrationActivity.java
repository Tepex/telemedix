package com.telemedix.patient.view.user;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import com.telemedix.core.presenter.Presenter;

import com.telemedix.patient.R;

import com.telemedix.patient.view.BaseActivity;

public class RegistrationActivity extends BaseActivity
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setToolbarTitle(R.string.title_activity_register);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		if(savedInstanceState == null) goFragment(new RegistrationFragment(), true);
	}
	
	@Override
	public Presenter createPresenter()
	{
		return null;
	}
}
