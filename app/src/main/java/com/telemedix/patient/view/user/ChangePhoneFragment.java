package com.telemedix.patient.view.user;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.ChangePhoneModel;

import com.telemedix.core.model.pojo.Locales;
import com.telemedix.core.model.pojo.PhoneLocale;

import com.telemedix.core.model.Model.State;

import com.telemedix.core.presenter.ChangePhonePresenter;

import com.telemedix.core.view.ChangePhoneView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseFragment;
import com.telemedix.patient.view.MenuActivity;

import static com.telemedix.core.presenter.ChangePhonePresenter.ERR_OLD_INVALID;
import static com.telemedix.core.presenter.ChangePhonePresenter.ERR_NEW_INVALID;
import static com.telemedix.core.presenter.ChangePhonePresenter.ERR_OLD_NOT_MATCH;
import static com.telemedix.core.presenter.ChangePhonePresenter.ERR_EQUALS;

public class ChangePhoneFragment extends BaseFragment<ChangePhoneView, ChangePhoneModel, RegisterInterceptor, ChangePhonePresenter> implements ChangePhoneView
{
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_change_phone);
		
		getBaseActivity().setToolbarTitle(R.string.change_number_title);
		((MenuActivity)getBaseActivity()).setNavigationViewEnabled(false);
		getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		return view;
	}
	
	@Override
	@CallSuper
	public void onResume()
	{
		super.onResume();
		oldPhoneText.addTextChangedListener(textWatcher);
		newPhoneText.addTextChangedListener(textWatcher);
		oldCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if(!blockListeners) getPresenter().copyOnWrite();
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
			
		});
		newCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if(!blockListeners) getPresenter().copyOnWrite();
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
			
		});
	}
	
	@Override
	@CallSuper
	public void onPause()
	{
		oldPhoneText.removeTextChangedListener(textWatcher);
		newPhoneText.removeTextChangedListener(textWatcher);
		oldCountryCode.setOnItemSelectedListener(null);
		newCountryCode.setOnItemSelectedListener(null);
		super.onPause();
	}
	
	@Override
	@CallSuper
	public void onDestroyView()
	{
		textWatcher = null;
		super.onDestroyView();
	}
	
	/* Base */
	
	@Override
	public ChangePhonePresenter createPresenter()
	{
		ChangePhonePresenter ret = new ChangePhonePresenter(App.getApp().getPhoneNumber());
		ret.onCreate(new ChangePhoneModel(), App.getApp().getRegisterInterceptor());
		return ret;
	}
	
	/* ChangePhoneView */
	
	@Override
	public void onError(State state)
	{
		if(state != null)
		{
			if(((state.getErrorCode() & ERR_OLD_INVALID) != 0) || ((state.getErrorCode() & ERR_OLD_NOT_MATCH) != 0)) oldPhoneLayout.setError(getStringRes(R.string.err_old_number));
			else oldPhoneLayout.setError(null);

			if((state.getErrorCode() & ERR_NEW_INVALID) != 0) newPhoneLayout.setError(getStringRes(R.string.err_new_number));
			else newPhoneLayout.setError(null);
			
			errText.setVisibility(((state.getErrorCode() & ERR_EQUALS) != 0) ? View.VISIBLE : View.GONE); 
		}
		else
		{
			oldPhoneLayout.setError(null);
			newPhoneLayout.setError(null);
			errText.setVisibility(View.GONE);
		}
	}
	
	@Override
	@CallSuper
	public void onNew()
	{
		updateNextButton(false);
		super.onNew();
	}
	
	@Override
	public void onReady()
	{
		updateNextButton(true);
	}
	
	@Override
	@CallSuper
	public void showModel()
	{
		super.showModel();
		blockListeners = true;
		if(spinnerAdapter == null)
		{
			spinnerAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Locales.localies); 
			spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			oldCountryCode.setAdapter(spinnerAdapter);
			newCountryCode.setAdapter(spinnerAdapter);
		}
		
		int pos = spinnerAdapter.getPosition(getModel().getOldPhoneLocale());
		oldCountryCode.setSelection(pos);
		pos = spinnerAdapter.getPosition(getModel().getNewPhoneLocale());
		newCountryCode.setSelection(pos);
		
		oldPhoneText.setText(getModel().getOldPhoneNumber());
		newPhoneText.setText(getModel().getNewPhoneNumber());
		
		blockListeners = false;
		
		getPresenter().copyOnWrite();
	}
	
	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		getModel().setState(State.NEW);
		Bundle args = new Bundle();
		args.putString(ValidateCodeFragment.ARGS_USER_PHONE, getModel().getNewPhoneNumberWithCode());
		ValidateCodeFragment fragment = new ValidateCodeFragment();
		fragment.setArguments(args);
		getBaseActivity().goFragment(fragment, true);
	}
	
	@Override
	public PhoneLocale getOldPhoneLocale()
	{
		return (PhoneLocale)oldCountryCode.getSelectedItem();
	}
	
	@Override
	public String getOldPhoneNumber()
	{
		String text = Utils.getTrimText(oldPhoneText);
		if(text != null) text = text.replaceAll("\\D", "");
		return text;
	}
	
	@Override
	public PhoneLocale getNewPhoneLocale()
	{
		return (PhoneLocale)newCountryCode.getSelectedItem();
	}
	
	@Override
	public String getNewPhoneNumber()
	{
		String text = Utils.getTrimText(newPhoneText);
		if(text != null) text = text.replaceAll("\\D", "");
		return text;
	}
	
	@Override
	public void updateNextButton(boolean isEnabled)
	{
		sendBtn.setEnabled(isEnabled);
	}
	
	@Override
	@OnClick(R.id.send)
	public void nextStep()
	{
		Utils.hideSoftKeyboard(getActivity(), sendBtn);
		getPresenter().changePhone();
	}
	
	@BindView(R.id.old_country_code) Spinner oldCountryCode;
	@BindView(R.id.new_country_code) Spinner newCountryCode;
	@BindView(R.id.til_old_tel) TextInputLayout oldPhoneLayout;
	@BindView(R.id.til_new_tel) TextInputLayout newPhoneLayout;
	@BindView(R.id.old_tel) TextInputEditText oldPhoneText;
	@BindView(R.id.new_tel) TextInputEditText newPhoneText;
	@BindView(R.id.err_text) TextView errText;
	@BindView(R.id.send) Button sendBtn;
	
	private ArrayAdapter<PhoneLocale> spinnerAdapter;
	private boolean blockListeners = false;

	private TextWatcher textWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
		{
			if(!blockListeners) getPresenter().copyOnWrite();
		}
			
		@Override
		public void beforeTextChanged(final CharSequence s, final int start, final int count, final int end) {}
		@Override
		public void afterTextChanged(final Editable pEditable) {}
	};
}
