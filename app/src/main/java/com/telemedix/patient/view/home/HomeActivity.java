package com.telemedix.patient.view.home;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.support.v4.app.Fragment;

import android.view.View;

import android.widget.Button;

import com.telemedix.patient.App;
import com.telemedix.patient.R;

import com.telemedix.patient.view.MenuActivity;
import com.telemedix.patient.view.BaseFragment;

public class HomeActivity extends MenuActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		BaseFragment fragment;
		
		if(App.getApp().isPinCodeNeeded()) fragment = new InputPinCodeFragment();
		else fragment = new HomeFragment();

		LOG.i("Starting fragment %s", fragment.getClass().getSimpleName());
		
		getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.fragment_container, fragment, CURRENT_FRAGMENT)
			.commit();

		int action = getIntent().getIntExtra(EXTRAS_ACTION, 0);
		if(action == EXTRAS_ACTION_REGISTRATION_OK && !App.getApp().isRegistrationPassed())
		{
			App.getApp().setRegistrationPassed();
			showAlertDialog(null, getString(R.string.registration_ok));
		}
	}
		
	/* Для InputPinCodeFragment — собственная виртуальная нумерная клавиатура для ввода PIN-кода */
	public void numPressed(View view)
	{
		Fragment fr = getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT);
		if(fr != null && (fr instanceof InputPinCodeFragment))
			((InputPinCodeFragment)fr).numPressed(((Button)view).getText().toString());
	}
	
	public static final String EXTRAS_ACTION = "action";
	public static final int EXTRAS_ACTION_REGISTRATION_OK = 1;
}
