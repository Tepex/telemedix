package com.telemedix.patient.view.user;

import android.content.Context;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.inputmethod.InputMethodManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.core.interceptor.Interceptor;

import com.telemedix.core.model.ChangePinCodeModel;
import com.telemedix.core.model.Model.State;

import com.telemedix.core.presenter.ChangePinCodePresenter;

import com.telemedix.core.view.ChangePinCodeView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseFragment;
import com.telemedix.patient.view.MenuActivity;

import static com.telemedix.core.presenter.ChangePinCodePresenter.ERR_INVALID_CURRENT_PIN_CODE;
import static com.telemedix.core.presenter.ChangePinCodePresenter.ERR_EQUALS;

public class ChangePinCodeFragment extends BaseFragment<ChangePinCodeView, ChangePinCodeModel, Interceptor, ChangePinCodePresenter> implements ChangePinCodeView
{
	@Override
	@CallSuper
	public void onAttach(Context context)
	{
		super.onAttach(context);
		isRegistration = (context instanceof RegistrationActivity);
	}
	
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_change_pin);

		@StringRes int resId;
		if(isRegistration)
		{
			currentPinEdit.setVisibility(View.GONE);
			resId = R.string.set_pin_code_title; 
			getBaseActivity().setToolbarNextButton(true);
			phoneValidated.setVisibility(View.VISIBLE);
			pinEnterText.setVisibility(View.VISIBLE);
			phoneValidated.setText(getBaseActivity().getString(R.string.pin_enter_caption,
                getArguments().getString(ValidateCodeFragment.ARGS_USER_PHONE)));
		}
		else
		{
			phoneValidated.setVisibility(View.GONE);
			pinEnterText.setVisibility(View.GONE);
			if(currentPinCode == null)
			{
				currentPinEdit.setVisibility(View.GONE);
				resId = R.string.set_pin_code_title;
			}
			else
			{
				currentPinEdit.setVisibility(View.VISIBLE);
				resId = R.string.change_pin_code_title;
			}
			
			getBaseActivity().setToolbarNextButton(false);
			((MenuActivity)getBaseActivity()).setNavigationViewEnabled(false);
			getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		}
		getBaseActivity().setToolbarTitle(resId);
		
		return view;
	}

	@Override
	@CallSuper
	public void onResume()
	{
		super.onResume();
		currentPinEdit.addTextChangedListener(textWatcher);
		pin1Edit.addTextChangedListener(textWatcher);
		pin2Edit.addTextChangedListener(textWatcher);
	}
	
	@Override
	@CallSuper
	public void onPause()
	{
		currentPinEdit.removeTextChangedListener(textWatcher);
		pin1Edit.removeTextChangedListener(textWatcher);
		pin2Edit.removeTextChangedListener(textWatcher);
		super.onPause();
	}
	
	@Override
	@CallSuper
	public void onDestroyView()
	{
		textWatcher = null;
		super.onDestroyView();
	}

	/* Base */
	
	@Override
	public boolean isSkipBackNavigation()
	{
		return isRegistration;
	}
	
	@Override
	public ChangePinCodePresenter createPresenter()
	{
		ChangePinCodePresenter ret = new ChangePinCodePresenter(currentPinCode);
		ret.onCreate(new ChangePinCodeModel(), new Interceptor());
		return ret;
	}
	
	/* View */
	
	@Override
	@CallSuper
	public void setState(State state)
	{
		updateNextButton((isRegistration && state == null) || state == State.READY || state == State.SUCCESS);
		super.setState(state);
	}
	
	@Override
	public void onError(State state)
	{
		if(state != null)
		{
			if((state.getErrorCode() & ERR_INVALID_CURRENT_PIN_CODE) != 0) currentPinLayout.setError(getStringRes(R.string.pin_invalid));
			else currentPinLayout.setError(null);
			errText.setVisibility(((state.getErrorCode() & ERR_EQUALS) != 0) ? View.VISIBLE : View.GONE); 
		}
		else
		{
			currentPinLayout.setError(null);
			errText.setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onReady()
	{
	}

	@Override
	@CallSuper
	public void showModel()
	{
		super.showModel();
		blockListeners = true;
		
		currentPinEdit.setText(getModel().getCurrentPinCode());
		pin1Edit.setText(getModel().getPinCode1());
		pin2Edit.setText(getModel().getPinCode2());
		
		blockListeners = false;
		getPresenter().copyOnWrite();
	}
	
	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		getModel().setState(State.NEW);
		
		if(isRegistration) getBaseActivity().goFragment(new ContactDataFragment(), true);
		else
		{
			@StringRes int resId;
			if(currentPinCode == null) resId = R.string.set_pin_code_ok;
			else resId = R.string.change_pin_code_ok;
			getBaseActivity().showAlertDialog(R.string.attention_title, resId, (di, i)->
			{
				di.dismiss();
				getBaseActivity().goActivityFragment(MenuActivity.class, SecurityFragment.class, true);
			});
		}
	}

	@Override
	public String getCurrentPinCode()
	{
		return Utils.getTrimText(currentPinEdit);
	}
	
	@Override
	public String getNewPinCode1()
	{
		return Utils.getTrimText(pin1Edit);
	}
	
	@Override
	public String getNewPinCode2()
	{
		return Utils.getTrimText(pin2Edit);
	}
	
	
	@Override
	public void updateNextButton(boolean isEnabled)
	{
		nextButton.setEnabled(isEnabled);
	}
	
	@Override
	@OnClick(R.id.pin_next_btn)
	public void nextStep()
	{
		LOG.d("Change PIN nextStep");
		Utils.hideSoftKeyboard(getActivity(), nextButton);
		
		/* Сохранение ПИН-кода */
		App.getApp().setPinCode(getModel().getPinCode1());
		finishView();
	}
	
	@BindView(R.id.pin_enter_caption) TextView phoneValidated;
	@BindView(R.id.pin_enter_text) TextView pinEnterText;
	@BindView(R.id.til_current_pin) TextInputLayout currentPinLayout;
	@BindView(R.id.current_pin) TextInputEditText currentPinEdit;
	@BindView(R.id.new_pin1) TextInputEditText pin1Edit;
	@BindView(R.id.new_pin2) TextInputEditText pin2Edit;
	@BindView(R.id.err_text) TextView errText;
	
	@BindView(R.id.pin_next_btn) Button nextButton;
	
	private String currentPinCode = App.getApp().getPinCode();
	private boolean blockListeners = false;
	private boolean isRegistration;

	private TextWatcher textWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
		{
			if(!blockListeners) getPresenter().copyOnWrite();
		}
			
		@Override
		public void beforeTextChanged(final CharSequence s, final int start, final int count, final int end) {}
		@Override
		public void afterTextChanged(final Editable pEditable) {}
	};
}
