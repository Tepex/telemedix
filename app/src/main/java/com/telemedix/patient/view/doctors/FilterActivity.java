package com.telemedix.patient.view.doctors;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.v4.content.ContextCompat;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import com.telemedix.patient.view.BaseActivity;

//import com.telemedix.patient.models.FilterItem;

import com.telemedix.core.presenter.Presenter;

import com.telemedix.patient.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Список специализаций, городов, клиник.
 */
public class FilterActivity extends BaseActivity
{
	@Override
	public Presenter createPresenter()
	{
		return null;
	}
	
	/*
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_find_doctors_filter);
		int currAction = getIntent().getIntExtra(EXTRA_ACTION, EXTRA_ACTION_FIND_ALL);
		
		switch(currAction)
		{
			case EXTRA_ACTION_FIND_BY_SPEC:
				searchView.setHint(getString(R.string.search_by_specialisation));
				break;
			case EXTRA_ACTION_FIND_BY_CITY:
				searchView.setHint(getString(R.string.search_by_city));
				break;
			case EXTRA_ACTION_FIND_BY_CLINIC:
				searchView.setHint(getString(R.string.search_by_clinic));
				break;
		}
		
		searchView.showSearch(false);
		searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener()
		{
			@Override
			public boolean onQueryTextSubmit(final String query)
			{
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(final String newText)
			{
				List<FilterItem> newList = new LinkedList<>();
				for(FilterItem item: defaultList) if(item.getName().toLowerCase().contains(newText.toLowerCase())) newList.add(item);
				filterAdapter.setList(newList);
				return true;
			}
		});
		
		listView.setLayoutManager(new LinearLayoutManager(this));
		listView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.list_divider)));
		
		listView.setAdapter(filterAdapter);
		filterAdapter.setAdapterListener((filterItem)->
		{
			Intent intent = new Intent();
			intent.putExtra(EXTRA_ACTION, filterItem.getId());
			intent.putExtra(EXTRA_ACTION_ARG, filterItem.getName());
			setResult(RESULT_OK, intent);
			finish();
		});
		
		if(currAction == EXTRA_ACTION_FIND_BY_SPEC)
		{
			defaultList.add(new FilterItem("Терапевт", EXTRA_ACTION_FIND_BY_SPEC));
			defaultList.add(new FilterItem("Педиатр", EXTRA_ACTION_FIND_BY_SPEC));
			defaultList.add(new FilterItem("Гинеколог", EXTRA_ACTION_FIND_BY_SPEC));
			defaultList.add(new FilterItem("Диетолог", EXTRA_ACTION_FIND_BY_SPEC));
			defaultList.add(new FilterItem("Психолог", EXTRA_ACTION_FIND_BY_SPEC));
			defaultList.add(new FilterItem("Все специализации", EXTRA_ACTION_FIND_ALL));
		}
		else if(currAction == EXTRA_ACTION_FIND_BY_CITY)
		{
			defaultList.add(new FilterItem("Москва", EXTRA_ACTION_FIND_BY_CITY));
			defaultList.add(new FilterItem("Санкт-Петербург", EXTRA_ACTION_FIND_BY_CITY));
		}
		filterAdapter.setList(defaultList);
	}
	
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(final MenuItem item)
	{
		if(item.getItemId() == android.R.id.home)
		{
			setResult(RESULT_CANCELED, new Intent());
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@BindView(R.id.list_view) SuperRecyclerView listView;
	@BindView(R.id.search_view) MaterialSearchView searchView;
	
	private List<FilterItem> defaultList = new LinkedList<>();
	private FilterAdapter filterAdapter = new FilterAdapter();
	*/
	public static final String EXTRA_ACTION     = "extra_action";
	public static final String EXTRA_ACTION_ARG = "extra_action_arg";
	
	public static final int EXTRA_ACTION_FIND_ALL       = -1;
	public static final int EXTRA_ACTION_FIND_BY_SPEC   = 1;
	public static final int EXTRA_ACTION_FIND_BY_CITY   = 2;
	public static final int EXTRA_ACTION_FIND_BY_CLINIC = 3;
	
	public static final int RESULT_CANCELED = 0;
	public static final int RESULT_OK = 1;
	
	/*
	static class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ItemHolder>
	{
		@Override
		public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType)
		{
			return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item, parent, false));
		}
		
		@Override
		public void onBindViewHolder(ItemHolder holder, int position)
		{
			final FilterItem filterItem = list.get(position);
			holder.bind(filterItem);
			holder.itemView.setOnClickListener(view->
			{
				if(adapterListener != null) adapterListener.onItemSelected(filterItem);
			});
		}
		
		@Override
		public int getItemCount()
		{
			return list.size();
		}
		
		public void setList(List<FilterItem> list)
		{
			this.list = list;
			notifyDataSetChanged();
		}
		
		public void setAdapterListener(AdapterListener listener)
		{
			adapterListener = listener;
		}
		
		public static class ItemHolder extends RecyclerView.ViewHolder
		{
			public ItemHolder(View itemView)
			{
				super(itemView);
				ButterKnife.bind(this, itemView);
			}
			
			public void bind(FilterItem filterItem)
			{
				title.setText(filterItem.getName());
			}
			
			@BindView(R.id.title) TextView title;
		}
		
		public interface AdapterListener
		{
			void onItemSelected(FilterItem filterItem);
		}
		
		private List<FilterItem> list = new ArrayList<>();
		private AdapterListener adapterListener;
	}
	*/
}
