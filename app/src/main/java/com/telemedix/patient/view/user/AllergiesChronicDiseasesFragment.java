package com.telemedix.patient.view.user;

import android.content.Context;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.inputmethod.InputMethodManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.PatientAllergiesChronicDiseasesModel;
import com.telemedix.core.model.pojo.Entity;
import com.telemedix.core.model.pojo.User;

import com.telemedix.core.presenter.PatientAllergiesChronicDiseasesPresenter;
import com.telemedix.core.view.PatientAllergiesChronicDiseasesView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseActivity;
import com.telemedix.patient.view.BaseFragment;
import com.telemedix.patient.view.MenuActivity;

import com.telemedix.patient.view.home.HomeActivity;

import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class AllergiesChronicDiseasesFragment extends BaseFragment<PatientAllergiesChronicDiseasesView, PatientAllergiesChronicDiseasesModel, PatientInterceptor, PatientAllergiesChronicDiseasesPresenter> implements PatientAllergiesChronicDiseasesView
{
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_allergies_chronics);
		
		BaseActivity activity = getBaseActivity();
		activity.setToolbarTitle(R.string.action_my_information);
		activity.setToolbarNextButton(false);
		
		if(activity instanceof MenuActivity)
		{
			((MenuActivity)activity).setNavigationViewEnabled(false);
			activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
		
		return view;
	}
	
	@Override
	@CallSuper
	public void onDestroyView()
	{
		removeListeners(allergyLayout);
		removeListeners(chronicLayout);
		textWatcher = null;
		super.onDestroyView();
	}
	
	/* Base */
	
	@Override
	public PatientAllergiesChronicDiseasesPresenter createPresenter()
	{
		PatientAllergiesChronicDiseasesPresenter ret = new PatientAllergiesChronicDiseasesPresenter();
		ret.onCreate(new PatientAllergiesChronicDiseasesModel(), App.getApp().getPatientInterceptor());
		return ret;
	}
	
	/* View */
	
	@Override
	@CallSuper
	public void showModel()
	{
		blockListeners = true;
		
		if(getModel().getAllergies() != null)
			for(Entity allergy: getModel().getAllergies()) addRow(allergyLayout, allergy.getName());
		if(getModel().getChronicDiseases() != null)
			for(Entity chronicDisease: getModel().getChronicDiseases()) addRow(chronicLayout, chronicDisease.getName());
		
		blockListeners = false;
	}
	
	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		if(getBaseActivity() instanceof RegistrationActivity)
		{
			Bundle extras = new Bundle();
			extras.putInt(HomeActivity.EXTRAS_ACTION, HomeActivity.EXTRAS_ACTION_REGISTRATION_OK);
			getBaseActivity().goActivity(HomeActivity.class, extras, true);
		}
		else getBaseActivity().finish();
	}
	
	@Override
	public Set<Entity> getAllergies()
	{
		Set<Entity> set = new HashSet<Entity>();
		for(int i = 0; i < allergyLayout.getChildCount(); ++i)
		{
			View view = allergyLayout.getChildAt(i);
			if(!(view instanceof LinearLayout)) continue;
			String name = getTextFromDynamicRow((ViewGroup)view);
			if(name != null) set.add(new Entity(0, name));
		}
		return set;
	}
	
	@Override
	public Set<Entity> getChronicDiseases()
	{
		Set<Entity> set = new HashSet<Entity>();
		for(int i = 0; i < chronicLayout.getChildCount(); ++i)
		{
			View view = chronicLayout.getChildAt(i);
			if(!(view instanceof LinearLayout)) continue;
			String name = getTextFromDynamicRow((ViewGroup)view);
			if(name != null) set.add(new Entity(0, name));
		}
		return set;
	}
	 
	@Override
	public void updateNextButton(boolean isChanged)
	{
		if(isChanged) nextButton.setText(R.string.action_save);
		else nextButton.setText(R.string.action_fill_later);
	}
	
	@Override
	@OnClick(R.id.next_btn)
	public void nextStep()
	{
		LOG.d("nextStep");
		getPresenter().saveData();
	}
	
	@OnClick(R.id.addAllergyBtn)
	public void addAllergy()
	{
		addRow(allergyLayout, null);
	}
	
	@OnClick(R.id.addChronicsBtn)
	public void addChronics()
	{
		addRow(chronicLayout, null);
	}
	
	private void addRow(LinearLayout parent, String name)
	{
		/*
		LayoutInflater inflater = (LayoutInflater)getBaseActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout row = (LinearLayout)inflater.inflate(R.layout.patient_allergy_layout, parent, false);
		*/
		LinearLayout row = (LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.patient_allergy_layout, parent, false);
		for(int i = 0; i < row.getChildCount(); ++i)
		{
			View view = row.getChildAt(i);
			if(view instanceof EditText)
			{
				if(name != null) ((EditText)view).setText(name);
				((EditText)view).addTextChangedListener(textWatcher);
			}
			else if(view instanceof Button) view.setOnClickListener(removeListener);
		}
		parent.addView(row);
	}
	
	private static String getTextFromDynamicRow(ViewGroup row)
	{
		for(int i = 0; i < row.getChildCount(); ++i)
		{
			View view = row.getChildAt(i);
			if(view instanceof EditText) return Utils.getTrimText((EditText)view);
		}
		return null;
	}
	
	private void removeListeners(ViewGroup row)
	{
		for(int i = 0; i < row.getChildCount(); ++i)
		{
			View view = row.getChildAt(i);
			if(view instanceof EditText) ((EditText)view).removeTextChangedListener(textWatcher);
			else if(view instanceof Button) view.setOnClickListener(null);
		}
	}
	
	@BindView(R.id.myInfoSecond_allergies_layout) LinearLayout allergyLayout;
	@BindView(R.id.myInfoSecond_chronics_layout) LinearLayout chronicLayout;
	@BindView(R.id.next_btn) Button nextButton;
	
	private boolean blockListeners = false;
	
	private TextWatcher textWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
		{
			if(!blockListeners) updateNextButton(getPresenter().copyOnWrite());
		}
			
		@Override
		public void beforeTextChanged(final CharSequence s, final int start, final int count, final int end) {}
		@Override
		public void afterTextChanged(final Editable pEditable) {}
	};
	
	private View.OnClickListener removeListener = view->
	{
		ViewParent row = view.getParent();
		if(!(row instanceof ViewGroup)) return;
		/* Если удаляем непустую строку, то помечаем данные как измененные */
		String text = getTextFromDynamicRow((ViewGroup)row);
		ViewParent container = ((ViewGroup)row).getParent();
		if(!(container instanceof ViewGroup)) return;
		view.setOnClickListener(null);
		((ViewGroup)container).removeView((View)row);
		if(text != null) updateNextButton(getPresenter().copyOnWrite());
	};
}
