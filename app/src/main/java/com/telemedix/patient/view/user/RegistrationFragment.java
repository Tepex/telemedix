package com.telemedix.patient.view.user;

import android.content.Context;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.inputmethod.InputMethodManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.pojo.Locales;
import com.telemedix.core.model.pojo.PhoneLocale;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.PhoneModel;

import com.telemedix.core.presenter.PhonePresenter;

import com.telemedix.core.view.PhoneView;

import com.telemedix.patient.App;
import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import com.telemedix.patient.view.BaseFragment;

import java.util.Arrays;
import java.util.Locale;

public class RegistrationFragment extends BaseFragment<PhoneView, PhoneModel, RegisterInterceptor, PhonePresenter> implements PhoneView
{
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_register);
		
		spinnerAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Locales.localies); 
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		countryCode.setAdapter(spinnerAdapter);
		
		int pos = Arrays.binarySearch(Locales.localies, new PhoneLocale(0, Locale.getDefault().getCountry()));
		if(pos > -1) countryCode.setSelection(pos);
		else if (Locales.localies.length > 0) countryCode.setSelection(0);
		
		return view;
	}
	
	@Override
	@CallSuper
	public void onResume()
	{
		super.onResume();
		phoneText.addTextChangedListener(textWatcher);
		countryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				getPresenter().onPhoneLocaleChanged(spinnerAdapter.getItem(pos));
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
			
		});
	}
	
	@Override
	@CallSuper
	public void onPause()
	{
		phoneText.removeTextChangedListener(textWatcher);
		countryCode.setOnItemSelectedListener(null);
		super.onPause();
	}
	
	@Override
	@CallSuper
	public void onDestroyView()
	{
		countryCode.setAdapter(null);
		/*
		spinnerAdapter = null;
		
		textWatcher = null;
		*/
		super.onDestroyView();
	}
	
	/* Base */
	
	@Override
	public PhonePresenter createPresenter()
	{
		PhonePresenter ret = new PhonePresenter();
		ret.onCreate(new PhoneModel(), App.getApp().getRegisterInterceptor());
		return ret;
	}
	
	/* PhoneView */
	
	@Override
	@CallSuper
	public void setState(State state)
	{
		sendBtn.setEnabled(state == State.READY || state == State.SUCCESS);
		super.setState(state);
	}
	
	@Override
	public void onError(State state)
	{
		if(state != null) phoneLayout.setError(state.getError());
		else phoneLayout.setError(null);
	}
	
	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		Bundle args = new Bundle();
		args.putString(ValidateCodeFragment.ARGS_USER_PHONE, getModel().getPhoneNumberWithCode());
		ValidateCodeFragment fragment = new ValidateCodeFragment();
		fragment.setArguments(args);
		getBaseActivity().goFragment(fragment, true);
	}

	@Override
	public void setPhoneNumber(String number)
	{
		phoneText.setText(number);
	}
	
	@Override
	public void setCode(PhoneLocale item)
	{
		int pos = spinnerAdapter.getPosition(item);
		countryCode.setSelection(pos);
	}
	
	@Override
	public void registerPhone()
	{
		getPresenter().registerPhone();
	}
	
	@OnClick(R.id.send)
	public void nextButton(View view)
	{
		/* спрятать виртуальную клаву */
		InputMethodManager imm = (InputMethodManager)getBaseActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		registerPhone();
	}
	
	// TODO: Выделить PhoneView в отдельный класс
	@BindView(R.id.countryCode) Spinner countryCode;
	@BindView(R.id.til_tel) TextInputLayout phoneLayout;
	@BindView(R.id.tel) TextInputEditText phoneText;
	@BindView(R.id.send) Button sendBtn;

	
	private ArrayAdapter<PhoneLocale> spinnerAdapter;
	
	private TextWatcher textWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
		{
			getPresenter().onPhoneNumberChanged(s.toString().trim());
		}
			
		@Override
		public void beforeTextChanged(final CharSequence s, final int start, final int count, final int end) {}
		@Override
		public void afterTextChanged(final Editable pEditable) {}
	};
}
