package com.telemedix.patient.view.user;

import android.support.v7.app.AppCompatActivity;
import android.support.annotation.CallSuper;

import android.os.Bundle;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

public class FullScreenPhotoActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_screen_photo);
		unbinder = ButterKnife.bind(this);
		
		Glide.with(this)
			.load(Utils.getGlideUrl(getIntent().getStringExtra(EXTRA_PHOTO_URL)))
			.signature(new StringSignature(Hawk.get(Utils.PREFS_FULL_AVATAR_HASH_STRING, "no_hash")))
			.crossFade()
			.into(imageView);
	}
	
	@CallSuper
	@Override
	protected void onDestroy()
	{
		if(unbinder != null) unbinder.unbind();
		unbinder = null;
		super.onDestroy();
	}
	
	@BindView(R.id.image) ImageView imageView;
	
	private Unbinder unbinder;
	public static final String EXTRA_PHOTO_URL = "extra_photo_url";
	public static final String EXTRA_USER_ID = "user_id";
}
