package com.telemedix.patient.view.user;

import android.content.Context;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.SecurityModel;
import com.telemedix.core.model.Model.State;

import com.telemedix.core.presenter.SecurityPresenter;

import com.telemedix.core.view.SecurityView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseFragment;
import com.telemedix.patient.view.MenuActivity;
import com.telemedix.patient.view.home.SplashActivity;

public class SecurityFragment extends BaseFragment<SecurityView, SecurityModel, RegisterInterceptor, SecurityPresenter> implements SecurityView
{
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_security);

		getBaseActivity().setToolbarTitle(R.string.security_title);
		
		if(App.getApp().getPinCode() == null) pinText.setText(R.string.set_pin_code);
		else pinText.setText(R.string.change_pin_code);
		return view;
	}
	
	/* Base */
	
	@Override
	public SecurityPresenter createPresenter()
	{
		SecurityPresenter ret = new SecurityPresenter();
		ret.onCreate(new SecurityModel(), App.getApp().getRegisterInterceptor());
		return ret;
	}
	
	/* View */
	
	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		LOG.d("delete from prefs");
		/* Удалить все из Preferences */
		App.getApp().deleteProfile();
		getBaseActivity().showAlertDialog(R.string.attention_title, R.string.profile_deleted, (di, i)->
		{
			di.dismiss();
//			((MenuActivity)getBaseActivity()).goRegistration(null);
			getBaseActivity().goActivity(SplashActivity.class, null, true);
		});
	}
	
	
	@OnClick(R.id.change_phone_btn)
	@Override
	public void changePhone()
	{
		getBaseActivity().goFragment(new ChangePhoneFragment(), true);
	}
	
	@OnClick(R.id.change_pin_btn)
	@Override
	public void changePin()
	{
		getBaseActivity().goFragment(new ChangePinCodeFragment(), true);
	}
	
	@OnClick(R.id.del_account_btn)
	@Override
	public void delAccount()
	{
		LOG.d("del account");
		getBaseActivity().showConfirmDialog(R.string.attention_title, R.string.confirm_delete_profile, (di, i)->
		{
			di.dismiss();
			getPresenter().delAccount();
		});
	}
	
	@BindView(R.id.change_pin_txt) TextView pinText;
}
