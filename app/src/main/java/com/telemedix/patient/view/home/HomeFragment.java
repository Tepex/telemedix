package com.telemedix.patient.view.home;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.core.interceptor.DoctorsInterceptor;

import com.telemedix.core.model.HomeModel;
import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.pojo.Entity;
import com.telemedix.core.model.pojo.Scope;

import com.telemedix.core.presenter.HomePresenter;

import com.telemedix.core.view.HomeView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseFragment;
import com.telemedix.patient.view.MenuActivity;

import com.telemedix.patient.view.doctors.DoctorsListActivity;

import io.reactivex.Observable;

import java.util.List;

public class HomeFragment extends BaseFragment<HomeView, HomeModel, DoctorsInterceptor, HomePresenter> implements HomeView
{
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_home);
		
		((MenuActivity)getBaseActivity()).setNavigationViewEnabled(true);
		getBaseActivity().setToolbarLogoEnabled(true);

		return view;
	}
	
	@Override
	@CallSuper
	public void onStart()
	{
		super.onStart();
		if(App.getApp().isUserRegistered())
		{
			LOG.d("HomeFragment. User logged in");
			register.setVisibility(View.GONE);
			perscriptions.setVisibility(View.VISIBLE);
		}
		else
		{
			LOG.d("HomeFragment. User is not logged in");
			register.setVisibility(View.VISIBLE);
			perscriptions.setVisibility(View.GONE);
		}
	}
	
	@Override
	@CallSuper
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_home, menu);
	}
	
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == R.id.action_search)
		{
			startDoctorsList(null);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	/* Base */
	
	@Override
	public HomePresenter createPresenter()
	{
		HomePresenter ret = new HomePresenter();
		ret.onCreate(new HomeModel(), App.getApp().getDoctorsInterceptor());
		return ret;
	}
	
	/* PhoneView */
	
	/** Динамическое создание 6-ти кнопок специальностей врачей в 2 колонки. */
	@Override
	@CallSuper
	public void showModel()
	{
		super.showModel();
		buttonContainer.removeAllViews();
		LayoutInflater inflater = LayoutInflater.from(getActivity());
		List<Entity> list = getModel().getDoctorsSpec();
		int size = list.size() / 2;
		if(list.size() % 2 == 1) ++size;
		if(size > 3) size = 3;
		for(int i = 0; i < size; ++i)
		{
			LinearLayout row = (LinearLayout)inflater.inflate(R.layout.home_buttons_container, buttonContainer, false);
			initSpecButton((Button)row.findViewById(R.id.first_btn), list.get(i*2));
			if((i*2+1) < list.size()) initSpecButton((Button)row.findViewById(R.id.second_btn), list.get(i*2+1));
			buttonContainer.addView(row);
		}
	}
	
	private void initSpecButton(Button btn, Entity item)
	{
		btn.setTag(item);
		btn.setText(item.getName());
		btn.setOnClickListener(view->startDoctorsList((Entity)view.getTag()));
	}

	@Override
	@OnClick(R.id.consultations_card)
	public void startConsultationsListActiviy()
	{
		/*
		Intent intent = new Intent(getActivity(), ConsultationsListActivity.class);
		startActivity(intent);
		*/
	}
	
	@Override
	@OnClick(R.id.naz_card)
	public void startMyPerescriptionsActivity()
	{
		/*
		Intent intent = new Intent(getActivity(), MyPrescriptionsActivity.class);
		startActivity(intent);
		*/
	}
	
	@Override
	@OnClick(R.id.register_card)
	public void startRegisterActivity()
	{
		((MenuActivity)getBaseActivity()).goRegistration(null);
	}
	
	@Override
	@OnClick(R.id.support_card)
	public void startSupport()
	{
		/*
		Intent intent = new Intent(getActivity(), RegisterActivity.class);
		startActivity(intent);
		*/
	}
	
	@Override
	public void startDoctorsList(Entity spec)
	{
		Bundle extras = null;
		if(spec != null)
		{
			extras = new Bundle();
			extras.putSerializable(DoctorsListActivity.EXTRAS_SCOPE, new Scope(Scope.Type.SPEC, spec));
		}
		getBaseActivity().goActivity(DoctorsListActivity.class, extras, false);
	}
	
	@BindView(R.id.register_card) View register;
	@BindView(R.id.naz_card) View perscriptions;
	@BindView(R.id.support_card) View support;
	@BindView(R.id.consultations_card) View consultations;
	@BindView(R.id.button_container) ViewGroup buttonContainer;
}
