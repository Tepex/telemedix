package com.telemedix.patient.view.home;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.core.interceptor.Interceptor;
import com.telemedix.core.model.InputPinCodeModel;
import com.telemedix.core.model.Model.State;
import com.telemedix.core.presenter.InputPinCodePresenter;
import com.telemedix.core.view.InputPinCodeView;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.view.BaseFragment;
import com.telemedix.patient.view.MenuActivity;

import com.telemedix.patient.view.user.RegistrationActivity;

import java.lang.reflect.Field;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputPinCodeFragment extends BaseFragment<InputPinCodeView, InputPinCodeModel, Interceptor, InputPinCodePresenter> implements InputPinCodeView
{
	@Override
	@CallSuper
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_input_pin);
		
		getBaseActivity().setToolbarLogoEnabled(true);
		getBaseActivity().setToolbarNextButton(false);
		((MenuActivity)getBaseActivity()).setNavigationViewEnabled(false);
		getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(false);

		/* Динамически инициализируем кнопки NumPad */
		Class idClass = R.id.class;
		Field[] fields = idClass.getFields();
		for(Field field: fields)
		{
			Matcher matcher = NUM_PATTERN.matcher(field.getName());
			if(!matcher.matches()) continue;
			
			Button numButton = null;
			try
			{
				numButton = (Button)view.findViewById(field.getInt(null));
			}
			catch(IllegalAccessException e)
			{
				LOG.w("Can\'t get button %s", field.getName(), e);
			}
			if(numButton == null) continue;
			numButton.setText(matcher.group(1));
		}
		
		return view;
	}
	
	/* Base */
	
	@Override
	public InputPinCodePresenter createPresenter()
	{
		InputPinCodePresenter ret = new InputPinCodePresenter();
		ret.onCreate(new InputPinCodeModel(), new Interceptor());
		return ret;
	}
	
	/* PresenterView */
	
	@Override
	@CallSuper
	public void finishView()
	{
		super.finishView();
		getBaseActivity().goFragment(new HomeFragment(), false);
	}

	void numPressed(String buttonValue)
	{
		String pinCode = pin.getText()+buttonValue;
		pin.setText(pinCode);
		LOG.d("pin: %s", pin.getText());
	}
	
	@OnClick(R.id.forgot_pin)
	public void forgotPin()
	{
		getBaseActivity().goActivity(RegistrationActivity.class, null, true);
	}
	
	@OnClick(R.id.pin_key_bckspc)
	public void onBckSpcsClick()
	{
		if(pin.getText().length() > 0) pin.setText(pin.getText().toString().substring(0, pin.getText().length()-1));
	}
	
	@OnClick(R.id.pin_enter)
	public void checkPin()
	{
		String pinCode = App.getApp().getPinCode();
		if(pinCode != null)
		{
			if(pinCode.equals(pin.getText().toString()))
			{
				App.getApp().pinCodePassed(true);
				finishView();
			}
			else
			{
				getBaseActivity().showError(R.string.pin_invalid);
				pin.setText("");
			}
		}
		else finishView();
    }
    
    @BindView(R.id.forgot_pin) TextView forgotPin;
	@BindView(R.id.pin_edit) TextView pin;
	
	private static final Pattern NUM_PATTERN = Pattern.compile("^pin_key_(\\d)$");
}
