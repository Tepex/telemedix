package com.telemedix.patient.view;

import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import com.telemedix.core.model.Model;
import com.telemedix.core.presenter.Presenter;
import com.telemedix.core.presenter.PresenterCache;

import com.telemedix.patient.R;
import com.telemedix.patient.view.home.HomeActivity;
import com.telemedix.patient.view.user.ChangePinCodeFragment;

import static com.telemedix.patient.view.BaseFragment.PRESENTER_INDEX_KEY;

public abstract class BaseActivity<P extends Presenter, M extends Model> extends AppCompatActivity implements com.telemedix.core.view.View
{
	@CallSuper
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		LOG.i("Start activity %s", getClass().getSimpleName());
		presenterCache = (PresenterCache)getLastCustomNonConfigurationInstance();
		if(presenterCache == null)
		{
			long seed;
			if(savedInstanceState == null) seed = 0;
			else seed = savedInstanceState.getLong(NEXT_ID_KEY);
			presenterCache = PresenterCache.getInstance(seed);
		}
		
		// Инициализация презентера как в BaseFragment
		if(savedInstanceState == null) presenterId = presenterCache.generateId();
		else presenterId = savedInstanceState.getLong(PRESENTER_INDEX_KEY);
		presenter = presenterCache.get(presenterId);
		if(presenter == null)
		{
			presenter = createPresenter();
			if(presenter != null) presenterCache.put(presenterId, presenter);
		}
		
		super.onCreate(savedInstanceState);
		if(getLayout() == NO_LAYOUT) return;
		
		setContentView(getLayout());
		unbinder = ButterKnife.bind(this);
		if(toolbar != null) setSupportActionBar(toolbar);
	}
	
	protected @LayoutRes int getLayout()
	{
		return R.layout.activity_base;
	}
	
	@Override
	@CallSuper
	@SuppressWarnings("unchecked")
	protected void onResume()
	{
		super.onResume();
		if(presenter != null && (this instanceof com.telemedix.core.view.View)) presenter.bindView((com.telemedix.core.view.View)this);
		isDestroyedBySystem = false;
	}
	
	@Override
	@CallSuper
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		isDestroyedBySystem = true;
		if(presenter != null) outState.putLong(PRESENTER_INDEX_KEY, presenterId);
	}
	
	@CallSuper
	@Override
	protected void onDestroy()
	{
		LOG.d("%s.onDestroy()", getClass().getSimpleName());
		if(unbinder != null) unbinder.unbind();
		unbinder = null;
		if(presenter != null)
		{
			presenter.unbindView();
			// убит пользователем
			if(!isDestroyedBySystem)
			{
				presenterCache.remove(presenterId);
				presenter.onDestroy();
			}
			presenter = null;
		}
		presenterCache = null;
		super.onDestroy();
	}

	@Override
	@CallSuper
	public void onBackPressed()
	{
		/*
		Fragment fragment = getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT);
		if(fragment != null)
		{
			LOG.d("onBackPressed. current fragment: %s", fragment.getClass().getSimpleName());
			getSupportFragmentManager()
				.beginTransaction()
				.remove(fragment)
				.commit();
		}
		*/
		FragmentManager fm = getSupportFragmentManager();
		BaseFragment fragment = (BaseFragment)fm.findFragmentByTag(CURRENT_FRAGMENT);
		String cl = "null";
		if(fragment != null) cl = fragment.getClass().getSimpleName();
		int stackCount = fm.getBackStackEntryCount();
		LOG.d("%s: stack size before: %d. Current: %s", getClass().getSimpleName(), stackCount, cl);
		/* Если на вершине стека (выход из Activity) или нужно перепрыгнуть фрагмент */
		if(fragment != null && (stackCount == 1 || fragment.isSkipBackNavigation())) fm.popBackStackImmediate();
		super.onBackPressed();
	}
	
	
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == android.R.id.home)
		{
			//getSupportFragmentManager().popBackStack();
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Optional
	@OnClick(R.id.skip_step)
	public void nextStep()
	{
		Fragment fr = getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT);
		if(fr != null && (fr instanceof BaseFragment)) ((BaseFragment)fr).nextStep();
	}
	
	@Override
	@CallSuper
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		Fragment fr = getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT);
		if(fr != null && (fr instanceof BaseFragment)) ((BaseFragment)fr).onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public Object onRetainCustomNonConfigurationInstance()
	{
		return presenterCache;
	}
	
	public void setToolbarVisible(boolean visible)
	{
		if(toolbar == null) return;
		if(visible) getSupportActionBar().show();
		else getSupportActionBar().hide();
	}
	
	public void setToolbarTitle(@StringRes int titleId)
	{
		if(toolbar != null) getSupportActionBar().setTitle(titleId);
	}
	
	public void setToolbarTitle(String title)
	{
		if(toolbar != null) getSupportActionBar().setTitle(title);
	}
	
	public void setToolbarNextButton(boolean hasNext)
	{
		if(nextButton == null) return;
		nextButton.setVisibility(hasNext ? View.VISIBLE : View.GONE);
	}
	
	public void setToolbarHamburger(boolean set)
	{
//        getSupportActionBar().setIcon(R.drawable.icon_telemedix_white);
		getSupportActionBar().setDisplayUseLogoEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}
	
	public void setToolbarLogoEnabled(boolean enabled)
	{
		if(toolbarLogoImage != null) toolbarLogoImage.setVisibility(enabled ? View.VISIBLE : View.GONE);
	}
	
	public void setToolbarBackEnabled(boolean enabled)
	{
		getSupportActionBar().setDisplayHomeAsUpEnabled(enabled);
	}
	
	/** Переход к фрагменту в текущем Activity */
	public void goFragment(BaseFragment fragment, boolean addToBackStack)
	{
		LOG.d("%s.goFragment(%s, %b)", getClass().getSimpleName(), fragment.getClass().getSimpleName(), addToBackStack);
		showProgress(false);
		FragmentTransaction ft = getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.fragment_container, fragment, CURRENT_FRAGMENT);
		if(addToBackStack) ft.addToBackStack(null);
		//ft.addToBackStack(null);
		ft.commit();
	}
	
	public void goActivityFragment(Class activityClass, Class fragmentClass, boolean isTop)
	{
		LOG.d("%s.goActivityFragment(%s, %s, %b)", getClass().getSimpleName(), activityClass.getSimpleName(), fragmentClass.getSimpleName(), isTop);
		Bundle extras = new Bundle();
		extras.putSerializable(EXTRAS_START_FRAGMENT, fragmentClass);
		goActivity(activityClass, extras, isTop);
	}
	
	public void goActivity(Class activityClass, Bundle extras, boolean isTop)
	{
		Intent intent = new Intent(this, activityClass);
		if(extras != null) intent.putExtras(extras);
		if(isTop) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		if(isTop) finish();
	}
	
	public PresenterCache getPresenterCache()
	{
		return presenterCache;
	}
	
	public void showProgress(boolean show)
	{
		if(progress != null) progress.setVisibility(show ? View.VISIBLE : View.GONE);
	}
	
	public void showError(@StringRes int msgId)
	{
		Snackbar.make(getCurrentFocus(), getString(msgId), Snackbar.LENGTH_LONG).show();
	}
		
	public void showAlertDialog(@StringRes int titleId, @StringRes int messageId)
	{
		showAlertDialog(titleId, getString(messageId)); 
	}
	
	public void showAlertDialog(@StringRes int titleId, String msg)
	{
		showAlertDialog(getString(titleId), msg);
	}
	
	public void showAlertDialog(String title, String msg)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this)
			.setTitle(title)
			.setMessage(msg)
			.setPositiveButton(R.string.action_ok, (di, i)->di.dismiss())
			.setCancelable(true);
		builder.show();
	}
	
	public void showAlertDialog(@StringRes int titleId, @StringRes int messageId, DialogInterface.OnClickListener listener)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this)
			.setTitle(titleId)
			.setMessage(messageId)
			.setPositiveButton(R.string.action_ok, listener)
			.setCancelable(true);
		builder.show();
	}
	
	public void showConfirmDialog(@StringRes int titleId, @StringRes int messageId, DialogInterface.OnClickListener listener)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this)
			.setTitle(titleId)
			.setMessage(messageId)
			.setNegativeButton(R.string.action_cancel, (di, i)->di.dismiss())
			.setPositiveButton(R.string.action_next, listener)
			.setCancelable(true);
		builder.show();
	}
	
	/* Veiw */
	
	@Override
	public void serverError(int code)
	{
		Snackbar.make(getCurrentFocus(), getString(R.string.server_error, code), Snackbar.LENGTH_LONG).show();
	}
	
	@Override
	public void networkError(Throwable thr)
	{
		LOG.w("Network error:", thr);
		showAlertDialog(R.string.network_error_title, thr.getMessage());
	}

	@Override
	public void setState(Model.State state)
	{
		LOG.i("%s.setState %s", getClass().getSimpleName(), state);
		showProgress(state == Model.State.LOADING);
		if(state == Model.State.ERROR) onError(state);
		else if(state == Model.State.NEW) onNew();
		else if(state == Model.State.READY) onReady(); 
		else if(state == Model.State.SUCCESS) onSuccess();
	}
	
	@Override
	public void onError(Model.State state) {}
	@Override
	public void onNew()	{}
	@Override
	public void onReady() {}
	@Override
	public void onSuccess() {}
	
	@Override
	public P getPresenter()
	{
		return presenter;
	}
	
	@Override
	public void finishView() {}
	@Override
	public void showModel()	{}
	
	protected abstract P createPresenter();
	
	@Nullable 
	@BindView(R.id.toolbar)
	protected Toolbar toolbar;
	@Nullable
	@BindView(R.id.toolbar_logo)
	protected View toolbarLogoImage;
	@Nullable
	@BindView(R.id.skip_step)
	protected View nextButton;
	@Nullable
	@BindView(R.id.progressLayout)
	protected View progress;
	
	public static final String CURRENT_FRAGMENT = "current";
	public static final String EXTRAS_START_FRAGMENT = "start_fragment";
	public static final int NO_LAYOUT = -1;
	
	private static final String NEXT_ID_KEY = "nextId";
	
	private PresenterCache presenterCache;
	private P presenter;
	private Unbinder unbinder;
	private long presenterId;
	private boolean isDestroyedBySystem;
	
	protected static final Logger LOG = LoggerManager.getLogger();
}

// emergency fix