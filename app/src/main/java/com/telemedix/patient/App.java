package com.telemedix.patient;

import android.annotation.SuppressLint;

import android.app.Application;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.telephony.TelephonyManager;

import com.google.firebase.iid.FirebaseInstanceId;

import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;

import com.telemedix.core.interceptor.AppInterceptor;
import com.telemedix.core.interceptor.DoctorsInterceptor;
import com.telemedix.core.interceptor.RegisterInterceptor;
import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.Repository;

import com.telemedix.core.model.pojo.Entity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.util.Arrays;
import java.util.UUID;

public class App extends Application
{
	@Override
	@CallSuper
	public void onCreate()
	{
		super.onCreate();
		
		/* This process is dedicated to LeakCanary for heap analysis.
		You should not init your app in this process. 
		if (LeakCanary.isInAnalyzerProcess(this)) return;
		LeakCanary.install(this);
		*/
		
		app = this;
		Hawk.init(app).build();
		
		//Utils.getUUID(),
		apiHost = getString(R.string.API_HOST);
		
		repository = Repository.getInstance(apiHost+API_PREFIX, CONNECTION_TIMEOUT, BuildConfig.DEBUG);
		
		apiKey = Hawk.get(PREFS_API_KEY);
		if(apiKey != null)
		{
			LOG.i("apikey: %s", apiKey);
			repository.setApiKey(apiKey);
		}
		
		fcmToken = Hawk.get(PREFS_FCM_TOKEN, null);
		
		LOG.i("Start Telemedix patient. API_KEY: %s", apiKey);
		
		phoneNumber = Hawk.get(PREFS_USER_PHONE);
		patientId = Hawk.get(PREFS_PATIENT_ID);
		devMode = Hawk.get(PREFS_DEV_MODE, false);
		
		LOG.d("Start Telemedix patient.");
		LOG.d("\tAPI_KEY: %s", apiKey);
		LOG.d("\tPatiend ID: %s", patientId);
		LOG.d("\tPhone number: %s", phoneNumber);

		//if(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS && !Hawk.contains(Utils.PREFS_TOKEN))
		/*
		if(!Hawk.contains(Utils.PREFS_TOKEN))
		{
			LOG.d("start RegistrationIntentService");
			startService(new Intent(this, RegistrationIntentService.class));
		}
		*/
	}
	
	public static App getApp()
	{
		return app;
	}
	
	public String getApiHost()
	{
		return apiHost;
	}
	
	public boolean isAppRegistered()
	{
		return (fcmToken != null && apiKey != null);
	}
	
	public String getUUID()
	{
		if(Hawk.contains(PREFS_APP_ID)) return Hawk.get(PREFS_APP_ID);
		else
		{
			String appId = UUID.randomUUID().toString();
			Hawk.put(PREFS_APP_ID, appId);
			return appId;
		}
	}
	
	@SuppressLint("HardwareIds")
	public String getIMEI()
	{
		/* http://stackoverflow.com/questions/33078003/android-6-0-permission-error */
		/*
		if(Buildconfig.VERSION < 22 ||
			ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
				return ((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		else
		{
			
		}
		*/
		
		return ((TelephonyManager)getSystemService(TELEPHONY_SERVICE)).getDeviceId();
	}
	
	/** 
	 * Проверка регистрации приложения на сервере. 
	 * Для регисрации приложения необходимо получить FCM токен и зарегистрировать его на сервере приложения.
	 * После успешной регистрации придет SMS с подтверждением.
	 */
	public String getFcmToken()
	{
		if(fcmToken == null)
		{
			LOG.d("Local FCM token is NULL. Reading from Prefs");
			fcmToken = Hawk.get(PREFS_FCM_TOKEN, null);
			LOG.d("Prefs FCM token: %s", fcmToken);
			if(fcmToken == null)
			{
				LOG.d("Prefs FCM token is NULL. Asking Firebase");
				fcmToken = FirebaseInstanceId.getInstance().getToken();
				if(fcmToken != null)
				{
					LOG.d("Saving FCM token");
					Hawk.put(PREFS_FCM_TOKEN, fcmToken);
				}
			}
		}
		LOG.d("FCM token: %s", fcmToken);
		return fcmToken;
	}
	
	public void saveFcmToken(String fcmToken) 
	{
		LOG.d("App. Saving received token: %s", fcmToken);
		this.fcmToken = fcmToken;
		Hawk.put(PREFS_FCM_TOKEN, fcmToken);
	}
	
	public String getApiKey()
	{
		return apiKey;
	}
	
	public void saveApiKey(String apiKey)
	{
		this.apiKey = apiKey;
		repository.setApiKey(apiKey);
		Hawk.put(PREFS_API_KEY, apiKey);
	}
	
	public Repository getRepository()
	{
		return repository;
	}
	
	public boolean isDevMode()
	{
		return devMode;
	}
	
	public void setDevMode(boolean devMode)
	{
		this.devMode = devMode;
		if(devMode) Hawk.put(PREFS_DEV_MODE, true);
		else Hawk.delete(PREFS_DEV_MODE);
	}
	
	public String getPhoneNumber()
	{
		if(phoneNumber == null) phoneNumber = Hawk.get(PREFS_USER_PHONE);
		return phoneNumber;
	}
	
	public String getPatientId()
	{
		if(patientId == null) patientId = Hawk.get(PREFS_PATIENT_ID);
		return patientId;
	}
	
	public void setPatientInfo(String phoneNumber, String patientId)
	{
		this.phoneNumber = phoneNumber;
		this.patientId = patientId;
		Hawk.put(PREFS_USER_PHONE, phoneNumber);
		Hawk.put(PREFS_PATIENT_ID, patientId);
		
		/* удаляем из кеша аватарку */
		Hawk.delete(Utils.PREFS_AVATAR_HASH_STRING);
		Hawk.delete(Utils.PREFS_FULL_AVATAR_HASH_STRING);
	}
	
	public boolean isUserRegistered()
	{
		return (getPatientId() != null);
	}
	
	public AppInterceptor getAppInterceptor()
	{
		if(appInterceptor == null)
		{
			appInterceptor = new AppInterceptor();
			appInterceptor.setApi(repository.getApi());
			appInterceptor.setSubscribeOn(Schedulers.io());
			appInterceptor.setObserveOn(AndroidSchedulers.mainThread());
		}
		return appInterceptor;
	}
	
	public RegisterInterceptor getRegisterInterceptor()
	{
		if(registerInterceptor == null)
		{
			registerInterceptor = new RegisterInterceptor();
			registerInterceptor.setApi(repository.getApi());
			registerInterceptor.setSubscribeOn(Schedulers.io());
			registerInterceptor.setObserveOn(AndroidSchedulers.mainThread());
		}
		return registerInterceptor;
	}
	
	public PatientInterceptor getPatientInterceptor()
	{
		if(patientInterceptor == null)
		{
			patientInterceptor = new PatientInterceptor();
			patientInterceptor.setApi(repository.getApi());
			patientInterceptor.setSubscribeOn(Schedulers.io());
			patientInterceptor.setObserveOn(AndroidSchedulers.mainThread());
		}
		return patientInterceptor;
	}
	
	public DoctorsInterceptor getDoctorsInterceptor()
	{
		if(doctorsInterceptor == null)
		{
			/* Грязный хак! Прибиваем гвоздями интерфейс к (id) специальностям врача на сервере */
			doctorsInterceptor = new DoctorsInterceptor(Arrays.asList(
				new Entity(1, getString(R.string.therapists)),
				new Entity(5, getString(R.string.pregnancy)),
				new Entity(3, getString(R.string.pediatricians)),
				new Entity(2, getString(R.string.psychologists)),
				new Entity(0, getString(R.string.all_doctors)),
				new Entity(4, getString(R.string.weight_loss))));
			doctorsInterceptor.setApi(repository.getApi());
			doctorsInterceptor.setSubscribeOn(Schedulers.io());
			doctorsInterceptor.setObserveOn(AndroidSchedulers.mainThread());
		}
		return doctorsInterceptor;
	}
	
	public boolean isPinCodeNeeded()
	{
		LOG.d("User PIN: [%s]", Hawk.get(PREFS_USER_PIN,"----"));
		return (pinCodeNeeded && Hawk.contains(PREFS_USER_PIN));
	}
	
	public String getPinCode()
	{
		return Hawk.get(PREFS_USER_PIN);
	}
	
	public void setPinCode(String pinCode)
	{
		if(pinCode == null || pinCode.trim().length() == 0) Hawk.delete(PREFS_USER_PIN);
		else Hawk.put(PREFS_USER_PIN, pinCode);
		pinCodeNeeded = false;
	}
	
	public void pinCodePassed(boolean passed)
	{
		pinCodeNeeded = !passed;
	}
	
	public boolean isRegistrationPassed()
	{
		return registrationPassed;
	}
	
	public void setRegistrationPassed()
	{
		registrationPassed = true;
	}
	
	public void deleteProfile()
	{
		Hawk.delete(PREFS_API_KEY);
		Hawk.delete(PREFS_FCM_TOKEN);
		Hawk.delete(PREFS_APP_ID);
		Hawk.delete(PREFS_USER_PHONE);
		Hawk.delete(PREFS_PATIENT_ID);
		Hawk.delete(PREFS_USER_PIN);
		Hawk.delete(PREFS_DEV_MODE);
		
		Hawk.delete(Utils.PREFS_AVATAR_HASH_STRING);
		Hawk.delete(Utils.PREFS_FULL_AVATAR_HASH_STRING);
		
		apiHost = null;
		apiKey = null;
		fcmToken = null;
		phoneNumber = null;
		patientId = null;
		devMode = false;
		
		pinCodeNeeded = true;
		registrationPassed = false;
		
		patientInterceptor.clearCache();
	}

	private String apiHost;
	private String apiKey;
	private String fcmToken;
	private String phoneNumber;
	private String patientId;
	
	private Repository repository;
	private boolean devMode;
	
	private boolean pinCodeNeeded = true;
	private boolean registrationPassed = false;
	
	private AppInterceptor appInterceptor;
	private RegisterInterceptor registerInterceptor;
	private PatientInterceptor patientInterceptor;
	private DoctorsInterceptor doctorsInterceptor;
	
	private static App app;
	
	private static final int CONNECTION_TIMEOUT = 60;
	
	private static final String API_PREFIX = "/api/p/";
	
	private static final String PREFS_API_KEY = "api_key";
	private static final String PREFS_FCM_TOKEN = "fcm_token";
	private static final String PREFS_APP_ID = "app_id";
	private static final String PREFS_USER_PHONE = "phone";
	private static final String PREFS_PATIENT_ID = "patient_id";
	private static final String PREFS_USER_PIN = "pin";
	private static final String PREFS_DEV_MODE = "devMode";
	
	private static final Logger LOG = LoggerManager.getLogger();
}
