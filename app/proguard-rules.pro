# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-dontpreverify
-allowaccessmodification
-optimizations !code/simplification/arithmetic

-keepattributes *Annotation*
-keepattributes EnclosingMethod
-keepattributes InnerClasses

#
# Note that you cannot just include these flags in your own
# configuration file; if you are including this file, optimization
# will be turned off. You'll need to either edit this file, or
# duplicate the contents of this file and remove the include of this
# file from your project's proguard.config path property.

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgent
-keep public class * extends android.preference.Preference
-keep public class * extends android.support.v7.app.AppCompatActivity
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.support.v4.app.DialogFragment
-keep public class * extends android.app.Fragment
-keep public class com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
	native <methods>;
}

-keep public class * extends android.view.View {
	public <init>(android.content.Context);
	public <init>(android.content.Context, android.util.AttributeSet);
	public <init>(android.content.Context, android.util.AttributeSet, int);
	public void set*(...);
}

-keepclasseswithmembers class * {
	public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
	public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.support.v7.app.AppCompatActivity {
	public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
	public static **[] values();
	public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
	public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class **.R$* {
	public static <fields>;
}

-keep public class android.support.v4.app.** { *; }
-keep public interface android.support.v4.app.** { *; }

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontwarn android.support.**

# HAWK
-keep public class com.orhanobut.hawk.** { *; }
-dontnote com.orhanobut.hawk.**

# SLF4J
-keep public class org.slf4j.Logger
-keep public class org.slf4j.LoggerFactory
-keep public class org.slf4j.** { *; }
-keep public class com.noveogroup.android.log.** { *; }
-keep public class com.noveogroup.android.log.Pattern$1
-keep public class org.slf4j.MDC$1
-dontnote com.noveogroup.android.log.Pattern$1
-dontnote org.slf4j.MDC$1
-dontwarn org.slf4j.**


# RxJava2
-keep public class io.reactivex.disposables.CompositeDisposable
-keep public class io.reactivex.disposables.Disposable
-keep public class io.reactivex.Observable
-keep public class io.reactivex.Scheduler
-keep public class io.reactivex.ObservableTransformer

# Retrofit2
-keep public class retrofit2.HttpException
-keep public class retrofit2.http.** { *; }

# OkHttp3
-keep public class okhttp3.** { *; }
-dontnote okhttp3.**

# ALSO REMEMBER KEEPING YOUR MODEL CLASSES
-dontwarn com.telemedix.**
-keep class com.telemedix.** { *; }

# http://stackoverflow.com/questions/29679177/cardview-shadow-not-appearing-in-lollipop-after-obfuscate-with-proguard/29698051
-keep public class android.support.v7.widget.RoundRectDrawable { *; }

-dontnote android.net.http.**
-dontnote org.apache.http.**
-dontnote sun.misc.Unsafe

-dontnote org.apache.harmony.xnet.provider.jsse.NativeCrypto
-dontnote org.apache.harmony.xnet.provider.jsse.SSLParametersImpl
-dontnote com.android.org.conscrypt.SSLParametersImpl
-dontnote sun.security.ssl.SSLContextImpl

-dontnote android.os.SystemProperties
-dontnote dalvik.system.CloseGuard
-dontnote org.joda.time.tz.Provider
-dontnote org.joda.time.tz.NameProvider
-dontnote okio.**
-dontnote org.webrtc.**
-dontnote com.malinskiy.superrecyclerview.**
-dontnote com.miguelcatalan.materialsearchview.**
-dontnote com.wdullaer.materialdatetimepicker.**
-dontnote com.twilio.conversations.**
-dontnote com.google.android.gms.**
-dontnote com.google.firebase.FirebaseApp

-dontwarn java.nio.file.**
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn org.joda.convert.FromString
-dontwarn org.joda.convert.ToString
-dontwarn java.lang.invoke.**

