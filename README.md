# Telemedix

## структура приложения



## Режим отладки
В строке поиска доктора ввести:
$dev on
Выключение оладочного режима:
$dev off

В отладочном режиме:
- в левом меню в конце появляется пункт 'test' который запускает тестовый REST-запрос - принудительная
отправка GCM-нотификации о скором начале консультации.
- оплата консультации пропускается


## Версификация
### Формат версии `versionName`:
`major.minor.patch-build-flavor-type`  
`mm.ii.pp-bbbb-flavor-type`

Пример:  
`1.2.3.451-stage-debug`	— передовая версия разработки  
`1.2.3.456-prod-debug`	— отладочная основная версия   
`1.2.3.456-prod-release`	— релизная основная версия

`versionCode` — 6-ти значное число `mmiipp`  
`mm` — старший номер версии  
`ii` — младший номет версии  
`pp` — номер патча

Ветки `stage` и `prod` существуют как два разных независимых приложения в одном проекте —
с общим кодом, но разными манифестами и ресурсами.

### Сценарии сборки

#### Рабочая сборка `trivial build`
Сборка без комита с инкрементом `build`:

1. Выдергивание versionName из манифеста
2. Парсинг versionName
3. Инкремент `build`
4. Запись новой версии в манифест
5. Сборка:  
`$ ./gradlew installDebug`

#### Сборка патча `patch build`  
Сборка с коммитом в репозиторий

#### Сборка релиза для тестирования
Сборка с коммитом, инкрементом `minor` и выкладыванием для 





API KEY
45589192
SECRET
0bf09e1a020af264f1ae287bd0b62d6bfe1971ae


Session id:
2_MX40NTU4OTE5Mn5-MTQ2MzEzMTY2ODA3OX52UHlxVXlTNzFJL0g5b0hMQWRhL2lqSVd-fg

Publisher token:

T1==cGFydG5lcl9pZD00NTU4OTE5MiZzaWc9NTRiY2Y0NzJhOTFhZGM1NmJlNmY1NDdhMWIwMTQ0NmFmYmE0YjRjMDpzZXNzaW9uX2lkPTJfTVg0ME5UVTRPVEU1TW41LU1UUTJNekV6TVRZMk9EQTNPWDUyVUhseFZYbFROekZKTDBnNWIwaE1RV1JoTDJscVNWZC1mZyZjcmVhdGVfdGltZT0xNDYzMTMxNzMxJm5vbmNlPTAuMjQ3MjU0OTA4MzE3NzAwMDMmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTQ2NTcyMzczMQ==


Subscriber token:

T1==cGFydG5lcl9pZD00NTU4OTE5MiZzaWc9YzJiOGQyZGE5NmRhNzRmNjRkNDIzMGZiOWFkZmRmMTA3NjE1OGQ3MjpzZXNzaW9uX2lkPTJfTVg0ME5UVTRPVEU1TW41LU1UUTJNekV6TVRZMk9EQTNPWDUyVUhseFZYbFROekZKTDBnNWIwaE1RV1JoTDJscVNWZC1mZyZjcmVhdGVfdGltZT0xNDYzMTMxNzY3Jm5vbmNlPTAuNzk5MjI0ODQwMjQ0Mjc4MyZyb2xlPXN1YnNjcmliZXImZXhwaXJlX3RpbWU9MTQ2NTcyMzc2Ng==

## Библиотеки, фреймворки, инструменты
* Проверка утечки памяти [LeakCanary](https://github.com/dlew/leakcanary)
* Логгер для Android []
* Счетчик кол-ва методов в DEX [dex-method-counts](http://frogermcs.github.io/MultiDex-solution-for-64k-limit-in-Dalvik/)
* Редактор MD-файлов [Macdown](http://macdown.uranusjr.com)
* Тестирование [Gradle TestSets plugin](https://github.com/unbroken-dome/gradle-testsets-plugin)
* Покрытие тестами [JaCoCo](https://github.com/jacoco/jacoco)

## Используемая информация
* [Интеграционные тесты](https://mdswanson.com/blog/2014/02/24/integration-testing-rest-apis-for-android.html)
* [MVP](http://blog.bradcampbell.nz/mvp-presenters-that-survive-configuration-changes-part-2/)
* [MVP в Mail.Ru Group (слайды)](https://www.slideshare.net/MailRuGroup/androidmvp-abbyy)
* [Grokking RxJava 4 (перевод)](https://habrahabr.ru/post/265997/)
* [MVP от Рамблера](https://github.com/andrey7mel/android-step-by-step)
* [К вопросу о применении Transformer](http://blog.danlew.net/2015/03/02/dont-break-the-chain/)
* [MultiDex](http://frogermcs.github.io/MultiDex-solution-for-64k-limit-in-Dalvik/)
* [RxJava многопоточность](https://habrahabr.ru/company/rambler-co/blog/280388/)
* [Ресурсы и стили — как правильно](http://blog.danlew.net/2014/11/19/styles-on-android/)
* [Ловля MemoryLeaks](https://habrahabr.ru/company/sebbia/blog/243537/)
* [Merging different flavor AndroidManifest](https://developer.android.com/studio/build/manifest-merge.html)
* [Тестирование (с.м. его проекты на GitHub)](https://medium.com/@Miqubel/testing-android-mvp-aa0de6e165e4)
* [Unit tests for RxJava 2](https://www.infoq.com/articles/Testing-RxJava2)