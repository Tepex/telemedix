package com.telemedix.patient.dialogs;

import android.content.Context;
import android.content.DialogInterface;

import android.media.MediaPlayer;
import android.media.RingtoneManager;

import android.os.Bundle;
import android.os.Vibrator;

import android.support.annotation.CallSuper;

import android.support.v4.content.res.ResourcesCompat;

import android.support.v7.app.AlertDialog;

import android.util.Log;

import android.widget.Button;
import android.widget.TextView;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import org.joda.time.Period;

import static com.telemedix.patient.Constants.TAG;

public class ConsultationNotifyDialog 
{
	public static AlertDialog showDialog(final BaseActivity activity, final int delay)
	{
		final MediaPlayer player = MediaPlayer.create(activity, RingtoneManager.getActualDefaultRingtoneUri(activity, RingtoneManager.TYPE_RINGTONE));
		final Vibrator vibrator = (Vibrator)activity.getSystemService(Context.VIBRATOR_SERVICE);

		Period period = new Period(delay * 1000);
		String str;
		if(period.getMinutes() != 0) str = activity.getString(R.string.consultation_will_start_min_sec, period.getMinutes(), period.getSeconds());
		else str = activity.getString(R.string.consultation_will_start_sec, period.getSeconds());

		final AlertDialog dialog = new AlertDialog.Builder(activity)
			.setTitle(R.string.confirm_consultation_title)
			.setCancelable(true)
			.setMessage(str)
			.setPositiveButton("OK", (di, i)->{di.cancel();})
			.setOnCancelListener(di->
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "AlertDialog.onCancel");
				player.stop();
				player.release();
				vibrator.cancel();
			})
			.create();
		dialog.show();
		
		player.start();
		vibrator.vibrate(pattern, 0);
		return dialog;
	}
	
	private static long pattern[] = {0, 100, 200, 300, 400};
}
