package com.telemedix.patient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import com.telemedix.patient.R;

/**
 * Created by Aleksey on 24.11.2016.
 */

public class DrugSchemeDialog {
    public static Dialog getDialog(Context context, String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.drug_scheme_dialog_title)
                .setMessage(text)
                .setPositiveButton(R.string.action_continue, null);

        return builder.create();
    }
}
