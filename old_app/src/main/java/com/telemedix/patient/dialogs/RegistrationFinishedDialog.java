package com.telemedix.patient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import com.telemedix.patient.R;

/**
 * Created by Aleksey on 19.08.2016.
 */
public class RegistrationFinishedDialog {
    public static Dialog getDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("")
                .setMessage(R.string.register_finished_dialog_text)
                .setPositiveButton(R.string.action_continue, null);

        return builder.create();
    }
}
