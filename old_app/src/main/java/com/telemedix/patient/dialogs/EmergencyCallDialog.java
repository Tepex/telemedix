package com.telemedix.patient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import com.telemedix.patient.R;

/**
 * Created by Aleksey on 19.08.2016.
 */
public class EmergencyCallDialog {
    public static Dialog getDialog(Context context, final DialogClickListener pCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.emergency_dialog_title)
                .setMessage(R.string.emergency_dialog_description)
                .setPositiveButton(R.string.action_call, (pDialogInterface, pI) -> pCallback.onEmergencyCallClick())
                .setNegativeButton(R.string.action_cancel, (pDialogInterface, pI) -> pCallback.onEmergencyCancelClick());

        return builder.create();

    }


    public interface DialogClickListener {
        void onEmergencyCallClick();

        void onEmergencyCancelClick();
    }
}
