package com.telemedix.patient.dialogs;

import android.content.Context;
import android.content.DialogInterface;

import android.media.MediaPlayer;
import android.media.RingtoneManager;

import android.os.Bundle;
import android.os.Vibrator;

import android.support.annotation.CallSuper;

import android.support.design.widget.FloatingActionButton;

import android.support.v4.content.res.ResourcesCompat;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import com.telemedix.patient.models.Doctor;

import static com.telemedix.patient.Constants.TAG;

public class IncomingCallDialog 
{
	public static void showDialog(AppCompatActivity activity, Doctor doctor, final DialogClickListener callback)
	{
		LayoutInflater inflater = activity.getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_incoming_call, null);		
		
		final MediaPlayer player = MediaPlayer.create(activity, RingtoneManager.getActualDefaultRingtoneUri(activity, RingtoneManager.TYPE_RINGTONE));
		final Vibrator vibrator = (Vibrator)activity.getSystemService(Context.VIBRATOR_SERVICE);

		final AlertDialog dialog = new AlertDialog.Builder(activity)
			.setTitle(R.string.incoming_call_title)
			.setCancelable(true)
			.setView(dialogView)
			.setOnCancelListener(di->
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "AlertDialog.onCancel");
				player.stop();
				player.release();
				vibrator.cancel();
			})
			.create();

		if(doctor != null)
		{
			TextView doctorName = (TextView)dialogView.findViewById(R.id.doctor_name);
			ImageView avatar = (ImageView)dialogView.findViewById(R.id.avatar);
			doctorName.setText(doctor.getShortName());
			if(doctor.getPhoto() != null) Glide.with(activity).load(doctor.getPhoto()).crossFade().centerCrop().into(avatar);
		}
		
		((FloatingActionButton)dialogView.findViewById(R.id.btn_video)).setOnClickListener(view->
		{
			dialog.cancel();
			callback.withVideo();
		});
		((FloatingActionButton)dialogView.findViewById(R.id.btn_audio)).setOnClickListener(view->
		{
			dialog.cancel();
			callback.withAudio();
		});
		((FloatingActionButton)dialogView.findViewById(R.id.btn_cancel)).setOnClickListener(view->
		{
			dialog.cancel();
			callback.onCancel();
		});
		dialog.show();
		
		player.start();
		vibrator.vibrate(pattern, 0);
	}
	
	private static long pattern[] = {0, 100, 200, 300, 400};
	
	public static interface DialogClickListener
	{
		void withVideo();
		void withAudio();
        void onCancel();
    }
}
