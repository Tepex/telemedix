package com.telemedix.patient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import com.telemedix.patient.R;

public class ConsultationDialog
{
	public static Dialog getCancelConsultationDialog(Context context, final DialogClickListener pCallback)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context)
			.setTitle(R.string.cancel_consultation_title)
			.setMessage(R.string.cancel_consultation_description)
			.setPositiveButton(R.string.action_approve, (pDialogInterface, pI)->pCallback.onApprove())
			.setNegativeButton(R.string.action_cancel, (pDialogInterface, pI)->pCallback.onCancel());
		return builder.create();
	}
	
	public interface DialogClickListener
	{
		void onApprove();
        void onCancel();
    }
}
