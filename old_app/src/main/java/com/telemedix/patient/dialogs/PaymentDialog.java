package com.telemedix.patient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import com.telemedix.patient.R;

/**
 * Created by Aleksey on 19.08.2016.
 */
public class PaymentDialog {
    public static Dialog getSuccessDialog(Context context, final DialogClickListener pCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.payment_dialog_title)
                .setMessage(R.string.payment_dialog_success)
                .setPositiveButton(R.string.action_continue, (pDialogInterface, pI) -> pCallback.onSuccessClick());

        return builder.create();

    }

    public static Dialog getFailedDialog(Context context, final DialogClickListener pCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.payment_dialog_title)
                .setMessage(R.string.payment_dialog_failed)
                .setPositiveButton(R.string.action_continue, (pDialogInterface, pI) -> pCallback.onFailClick());

        return builder.create();

    }

    public interface DialogClickListener {
        void onSuccessClick();

        void onFailClick();
    }
}
