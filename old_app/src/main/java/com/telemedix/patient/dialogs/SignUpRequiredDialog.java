package com.telemedix.patient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import com.telemedix.patient.R;

public class SignUpRequiredDialog
{
	public static Dialog getRequiredAccessDialog(Context context, final DialogClickListener pCallback)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context)
			.setTitle(R.string.emergency_dialog_title)
			.setMessage(R.string.required_signup_to_access_description)
			.setPositiveButton(R.string.action_register, (pDialogInterface, pI)->pCallback.onRegisterClick())
			.setNegativeButton(R.string.action_cancel, (pDialogInterface, pI)->pCallback.onRegisterCancelClick());
		return builder.create();
	}
	
	public static Dialog getRequiredSignupForHistoryDialog(Context context, final DialogClickListener pCallback)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context)
			.setTitle(R.string.emergency_dialog_title)
			.setMessage(R.string.required_signup_to_view_consultation_history_description)
			.setPositiveButton(R.string.action_register, (pDialogInterface, pI)->pCallback.onRegisterClick())
			.setNegativeButton(R.string.action_cancel, (pDialogInterface, pI)->pCallback.onRegisterCancelClick());
		return builder.create();
	}
	
	public interface DialogClickListener
	{
		void onRegisterClick();
        void onRegisterCancelClick();
    }
}
