package com.telemedix.patient;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;

import android.database.Cursor;

import android.net.Uri;

import android.provider.MediaStore;

import android.support.annotation.StringRes;

import android.support.v4.app.Fragment;

import android.util.Log;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.App;;

import com.telemedix.patient.models.Schedule;
import com.telemedix.patient.models.Slot;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import java.net.URLEncoder;

import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import static com.telemedix.patient.Constants.TAG;

/**
 * Created by Aleksey on 24.10.2016.
 */
public class Utils
{
	public static boolean isUserLoggedIn()
	{
		return (Hawk.contains(Constants.PREF_USER_PHONE) && !((String)Hawk.get(Constants.PREF_USER_PHONE)).isEmpty());
	}
	
	public static Intent viewOnMap(String address)
	{
		return new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("geo:0,0?q=%s", URLEncoder.encode(address))));
	}
	
	public static Intent viewOnMap(String lat, String lng)
	{
		return new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("geo:%s,%s", lat, lng)));
	}
	
	public static String getRealPathFromURI(Context context, Uri contentURI)
	{
		String result;
		Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
		// Source is Dropbox or other similar local file path
		if(cursor == null) result = contentURI.getPath();
		else
		{
			cursor.moveToFirst();
			int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			result = cursor.getString(idx);
			cursor.close();
		}
		return result;
	}
	
	/**
	 * Для тестовых целей разработки. В поле поиска можно ввести спец команды.
	 * Спец команды должны нчинаться с символа '$'.
	 */
	public static boolean processCommand(String cmd)
	{
		cmd = cmd.trim();
		if(!cmd.startsWith("$")) return false;
		cmd = cmd.substring(1).toLowerCase();
		String[] sp = cmd.split("\\s");
		if(sp.length > 1)
		{
			if(sp[0].equals(CMD_DEV))
			{
				Boolean on = null;
				if(sp[1].equals(CMD_DEV_ON)) on = Boolean.TRUE;
				else if(sp[1].equals(CMD_DEV_OFF)) on = Boolean.FALSE;
				if(on != null)
				{
					App.getApp().setDevMode(on);
					return true;
				}
			}
		}
		return false;
	}
	
	// Date and Time methods
	
	public static Schedule createToday(Fragment fragment, List<Slot> slots)
	{
		return createSchedule(fragment, R.string.schedule_today, slots, 0);
	}
	
	public static Schedule createTomorrow(Fragment fragment, List<Slot> slots)
	{
		return createSchedule(fragment, R.string.schedule_tomorrow, slots, 1);
	}
	
	public static Schedule createAfterTomorrow(Fragment fragment, List<Slot> slots)
	{
		
		return createSchedule(fragment, R.string.schedule_after_tomorrow, slots, 2);
	}
	
	public static Schedule createSchedule(Fragment fragment, @StringRes int nameId, List<Slot> slots, int period)
	{
		int startSlot = -1, endSlot = -1;
		LocalDate compareDate = NOW.plusDays(period);
		List<Slot> times = new LinkedList<>();
		String name = fragment.getString(nameId);
		
		for(int i = 0; i < slots.size(); ++i)
		{
			Slot slot = slots.get(i);
			if(slot.getDate().equals(compareDate))
			{
				if(startSlot == -1) startSlot = i;
				times.add(slot);
			}
			else if(startSlot != -1)
			{
				endSlot = i-1;
				break;
			}
		}
		
		// Нет приема
		if(startSlot == -1) return new Schedule(name, fragment.getString(R.string.schedule_no_reception), null, null);
		
		// может быть и такое :) когда только один слот на некоторую дату
		if(endSlot == -1) endSlot = startSlot;
		if(times.size() == 0) times.add(slots.get(startSlot));
		return new Schedule(name, slots.get(startSlot).getTimeString(), slots.get(endSlot).getTimeString(), times);
	}
	
	public static Calendar[] getSeparateDays(List<Slot> slots)
	{
		Set<Calendar> result = new HashSet<>();
		for(Slot slot: slots)
		{
			Calendar c = Calendar.getInstance();
			c.setTime(slot.getDate().toDate());
			result.add(c);
		}
		return result.toArray(new Calendar[result.size()]);
	}
	
	public static final DateTimeFormatter ISO_FORMATTER = ISODateTimeFormat.dateTimeNoMillis();
	public static final LocalDate NOW = LocalDate.now();
	
	public static final String CMD_DEV = "dev";
	public static final String CMD_DEV_ON = "on";
	public static final String CMD_DEV_OFF = "off";
}
