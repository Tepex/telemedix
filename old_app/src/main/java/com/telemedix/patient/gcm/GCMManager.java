package com.telemedix.patient.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Aleksey on 13.09.2016.
 */
public class GCMManager {
    private GCMManager() {
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(GcmPreferences.TOKEN, "");
    }
}
