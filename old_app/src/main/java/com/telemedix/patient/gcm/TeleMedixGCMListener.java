package com.telemedix.patient.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.Context;
import android.content.Intent;

import android.media.RingtoneManager;

import android.net.Uri;

import android.os.Bundle;

import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;

import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.telemedix.patient.App;
import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.activities.HomeActivity;
import com.telemedix.patient.activities.call.ConversationV2Activity;
import com.telemedix.patient.activities.application.ConsultApplicationActivity;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.ConsultationNotification;
import com.telemedix.patient.models.TwilioSession;

import com.telemedix.patient.network.ConsultationApiManager;
import com.telemedix.patient.network.response.ConfirmConsultationResponse;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import org.joda.time.Period;

import static com.telemedix.patient.Constants.TAG;

/**
 * Обрабтка нотификации о начале консультации.
 * За 10 мин. до начала консультации должно прийти GCM-уведомление о запланированной консльтации
 * с параметрами видеосвязи для Twilio. В ответ необходимо подтвердить готовность, сделав запрос
 * Api.confirmConsultation
 */
public class TeleMedixGCMListener extends GcmListenerService
{
	/**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
	@Override
	public void onMessageReceived(String from, Bundle data)
	{
		String message = data.getString("message");
		if(BuildConfig.DEBUG)
		{
			Log.d(TAG, "From: "+from);
			Log.d(TAG, "Message: "+message);
		}
		if(from.startsWith("/topics/"))
		{
			// message received from some topic.
			return;
		}
		try
		{
			cn = GSON.fromJson(message, ConsultationNotification.class);
			if(BuildConfig.DEBUG) Log.d(TAG, "consultationNotification: "+cn);
		}
		catch(JsonSyntaxException e)
		{
			if(BuildConfig.DEBUG) Log.w(TAG, "Consultation notification parse error");
			if(App.getApp().isDevMode()) showNotification((int)System.currentTimeMillis(), data.getString("title", "TeleMedix"), message);
			return;
		}
		
		if(BuildConfig.DEBUG) Log.d(TAG, "Notification event: "+cn.getEvent());
		
		if(cn.getEvent() == ConsultationNotification.Event.START_ONLINE)
		{
			twilioSession = cn.getBody();
			if(BuildConfig.DEBUG) Log.d(TAG, "save session. room:"+twilioSession.getRoom());
			ConsultationApiManager.confirmConsultation(cn.getBody().getId()).subscribe(TeleMedixGCMListener.this::getConsultation);
		}
		else if(cn.getEvent() == ConsultationNotification.Event.CONCLUSION)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "conclusion id:"+cn.getBody().getId());
		}
		else if(cn.getEvent() == ConsultationNotification.Event.MOVE)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "consultation move. id:"+cn.getBody().getId());
		}
		else if(cn.getEvent() == ConsultationNotification.Event.CANCEL)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "Here we cancel waiting dialog for consultation:"+cn.getBody().getId());
			/* Ищем текущее Activity. */
			BaseActivity currentActivity = App.getApp().getCurrentActivity();
			if(currentActivity != null) currentActivity.cancelConsultationByDoctor();
		}
	}
	
	private void getConsultation(final ConfirmConsultationResponse response)
	{
		ConsultationApiManager.getConsultation(cn.getBody().getId()).subscribe(consultation->
		{
			Period period = new Period(response.getDelay() * 1000);
			String str;
			if(period.getMinutes() != 0) str = TeleMedixGCMListener.this.getString(R.string.consultation_will_start_min_sec, period.getMinutes(), period.getSeconds());
			else str = TeleMedixGCMListener.this.getString(R.string.consultation_will_start_sec, period.getSeconds());
			
			str += "\n"+TeleMedixGCMListener.this.getString(R.string.emergency_info_doctor_caption)+" "+consultation.getDoctorName("");
			showNotification(cn.getBody().getId(), "TeleMedix", str);
			
			/* Ищем текущее Activity. Если не найдено, то запускаем морду. */
			BaseActivity currentActivity = App.getApp().getCurrentActivity();
			if(currentActivity == null)
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "current activity is null. Start HomeActivity");
				/* Запускаем Морду с диалоговым окном подтверждения консультации */
				Intent intent = new Intent(this, HomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra(ConsultApplicationActivity.EXTRA_CONSULTATION, consultation);
				intent.putExtra(ConversationV2Activity.EXTRA_CONSULTATION_RESPONSE, twilioSession);
				intent.putExtra(BaseActivity.EXTRA_CONSULTATION_DELAY, response.getDelay());
				startActivity(intent);
			}
			else
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "current activity: "+currentActivity.getClass().getSimpleName());
				currentActivity.consultationNotifyDialog(consultation, twilioSession, response.getDelay());
			}
		});
	}
	
	/**
	 * Нотификация с автозакрытием: http://stackoverflow.com/a/27083833
	 */
	private void showNotification(int id, String title, String message)
	{
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
			.setSmallIcon(R.mipmap.ic_launcher)
			.setContentTitle(title)
			.setContentText(message)
			.setAutoCancel(true)
			.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			
		//if(consultation != null) notificationBuilder = notificationBuilder.setContentIntent(createTwilioIntent(consultation));
		NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(id, notificationBuilder.build());
	}
	
	public static Gson GSON = new Gson();
	
	private ConsultationNotification cn;
	private TwilioSession twilioSession;
}
