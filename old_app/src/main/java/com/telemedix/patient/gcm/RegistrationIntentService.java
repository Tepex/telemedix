package com.telemedix.patient.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.App;;
import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;

import com.telemedix.patient.network.AppManager;
import com.telemedix.patient.utils.PhoneData;

import static com.telemedix.patient.Constants.TAG;

public class RegistrationIntentService extends IntentService
{
	public RegistrationIntentService()
	{
		super(TAG);
	}
	
	@Override
	protected void onHandleIntent(Intent intent)
	{
		try
		{
			InstanceID instanceID = InstanceID.getInstance(this);
			final String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
			Log.i(TAG, "GCM Registration Token: "+token);
			AppManager.registerApp(PhoneData.get.getIMEI(App.getApp()), token)
				.subscribe(appRegisterResponse->
				{
					if(BuildConfig.DEBUG) Log.d(TAG, "Save token and patientId: "+appRegisterResponse.getPatientId());
					Hawk.put(GcmPreferences.TOKEN, token);
					Hawk.put(Constants.PREF_PATIENT_ID, appRegisterResponse.getPatientId());
				});
		}
		catch(Exception e)
		{
			Log.e(TAG, "Failed to complete token refresh", e);
			Hawk.delete(GcmPreferences.TOKEN);
		}
	}
}
