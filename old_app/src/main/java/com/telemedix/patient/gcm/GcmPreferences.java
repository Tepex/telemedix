package com.telemedix.patient.gcm;

public class GcmPreferences
{
	public static final String REGISTRATION_COMPLETE = "registrationComplete";
	public static final String TOKEN                 = "gcm_token";
}
