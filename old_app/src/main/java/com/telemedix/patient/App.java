package com.telemedix.patient;

import android.app.Activity;
import android.app.Application;

import android.app.Application.ActivityLifecycleCallbacks;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.multidex.MultiDexApplication;

import com.karumi.dexter.Dexter;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;

import java.util.HashSet;
import java.util.Set;

import timber.log.Timber;

/**
 * Created by baneizalfe on 5/13/16.
 */
public class App extends MultiDexApplication implements ActivityLifecycleCallbacks
{
	@Override
	@CallSuper
	public void onCreate()
	{
		super.onCreate();
		registerActivityLifecycleCallbacks(this);
		app = this;

		if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());

		Hawk.init(app)
//			.setEncryptionMethod(HawkBuilder.EncryptionMethod.NO_ENCRYPTION)
//			.setStorage(HawkBuilder.newSharedPrefStorage(app))
//			.setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
			.build();
		Dexter.initialize(this);

//        Hawk.put(Constants.PREF_USER_ID, "2297A6F2-4E6F-E9E6-240D-C30362E462C9");
//        Hawk.put(Constants.PREF_USER_ID, "6A0DCD15-B571-B2D7-F55D-35BCA96DFC41");
		devMode = Hawk.get(PREFS_DEV_MODE, false);
		Timber.d("devMode: "+devMode);
	}
	
	public static App getApp()
	{
		return app;
	}
	
	public String getAvatarUrl()
	{
		return avatarUrl;
	}

	public boolean isDevMode()
	{
		return devMode;
	}
	
	public void setDevMode(boolean devMode)
	{
		Timber.d("setDevMode:"+devMode);
		this.devMode = devMode;
		if(devMode) Hawk.put(PREFS_DEV_MODE, true);
		else Hawk.delete(PREFS_DEV_MODE);
	}
	
	@Override
	public void onActivityStarted(Activity activity)
	{
		if(activity instanceof BaseActivity) activities.add((BaseActivity)activity);
	}
	
	@Override
	public void onActivityStopped(Activity activity)
	{
		if(activity instanceof BaseActivity) activities.remove((BaseActivity)activity);
	}
	
	public BaseActivity getCurrentActivity()
	{
		if(activities.isEmpty()) return null;
		return activities.iterator().next();
	}

	@Override public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}
	@Override public void onActivityDestroyed(Activity activity) {}
	@Override public void onActivityPaused(Activity activity) {}
	@Override public void onActivityResumed(Activity activity) {}
	@Override public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

	private boolean devMode;
	private Set<BaseActivity> activities = new HashSet<>();
	
	private static App app;
	public static final String PREFS_DEV_MODE = "devMode";
	private static final String avatarUrl = "https://thumbs.dreamstime.com/z/joyful-man-face-24417511.jpg";
}
