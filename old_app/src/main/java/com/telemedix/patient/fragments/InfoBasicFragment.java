package com.telemedix.patient.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;

import com.telemedix.patient.App;
import com.telemedix.patient.R;
import com.telemedix.patient.activities.InfoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by baneizalfe on 8/10/16.
 */
public class InfoBasicFragment extends BaseFragment {

    @BindView(R.id.height_input)
    Spinner height_input;

    @BindView(R.id.weight_input)
    Spinner weight_input;

    @BindView(R.id.blood_type_input)
    Spinner blood_type_input;

    @BindView(R.id.gender_input)
    Spinner gender_input;

    @BindView(R.id.avatar)
    ImageView avatar;

    public static InfoBasicFragment newInstance() {
        InfoBasicFragment fragment = new InfoBasicFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_basic, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupContent();
    }

    private void setupContent() {

        int heightArrSize = 10;
        String[] heightArr = new String[heightArrSize];
        for (int i = 0; i < heightArrSize; i++) {
            heightArr[i] = Integer.toString(175 + i);
        }
        height_input.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, heightArr));

        int weightArrSize = 10;
        String[] weightArr = new String[weightArrSize];
        for (int i = 0; i < weightArrSize; i++) {
            weightArr[i] = Integer.toString(75 + i);
        }
        weight_input.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, weightArr));

        String[] bloodTypeArr = {"I(O)"};
        blood_type_input.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, bloodTypeArr));

        String[] genderArr = {"Муж", "Жен"};
        gender_input.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, genderArr));

        Glide.with(getContext()).load(App.getApp().getAvatarUrl()).placeholder(R.drawable.avatar_bg).centerCrop().into(avatar);
    }

    @OnClick(R.id.submit_btn)
    void onSubmitClick() {
        if (getActivity() instanceof InfoActivity)
            ((InfoActivity) getActivity()).showMoreInfo();
    }
}
