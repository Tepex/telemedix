package com.telemedix.patient.fragments;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by baneizalfe on 7/5/16.
 */
public class BaseFragment extends Fragment{

    protected ActionBar getSupportActionBar(){
        if(getActivity() instanceof AppCompatActivity){
            return ((AppCompatActivity) getActivity()).getSupportActionBar();
        }

        return null;
    }

}
