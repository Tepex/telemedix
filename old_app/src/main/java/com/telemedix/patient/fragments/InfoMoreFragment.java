package com.telemedix.patient.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.InfoActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by baneizalfe on 8/10/16.
 */
public class InfoMoreFragment extends BaseFragment {


    public static InfoMoreFragment newInstance() {
        InfoMoreFragment fragment = new InfoMoreFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_more, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @OnClick(R.id.submit_btn)
    void onSubmitClick() {
        if (getActivity() instanceof InfoActivity)
            ((InfoActivity) getActivity()).saveData();
    }
}
