package com.telemedix.patient.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.activities.ConsultationRequestActivity;
import com.telemedix.patient.activities.ConsultationsListActivity;
import com.telemedix.patient.activities.MyPrescriptionsActivity;

import com.telemedix.patient.activities.analysis.AnalysisActivity;
import com.telemedix.patient.activities.diet.DietMainActivity;
import com.telemedix.patient.activities.drugs.DrugsActivity;

//import com.telemedix.patient.activities.registration.RegisterActivity;
import com.telemedix.patient.view.PhoneActivity;

import com.telemedix.patient.activities.doctor.FilterActivity;
import com.telemedix.patient.activities.doctor.FindDoctorsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by baneizalfe on 7/5/16.
 */
public class HomeFragment extends BaseFragment {

    View root;

    @BindView(R.id.register_card)
    View register;
    @BindView(R.id.support_card)
    View support;
    @BindView(R.id.naz_card)
    View perscriptions;
    @BindView(R.id.consultations_card)
    View consultations;

    @OnClick(R.id.order)
    public void openOrderActivity(View v) {
        //Snackbar.make(root, "TBD", Snackbar.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), DrugsActivity.class));
    }

    @OnClick(R.id.analysis)
    public void openAnalysisActivity(View v) {
//        Snackbar.make(root, "TBD", Snackbar.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), AnalysisActivity.class));
    }

    @OnClick(R.id.signing)
    public void openSigningActivity(View v) {
//        Snackbar.make(root, "TBD", Snackbar.LENGTH_SHORT).show();
        startFindDoctorsActivity(FilterActivity.EXTRA_ACTION_FIND_ALL, "");
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Utils.isUserLoggedIn()) {
            setUIforRegistered();
        } else {
            setUIforUnregistered();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_home, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                startFindDoctorsActivity(FilterActivity.EXTRA_ACTION_FIND_ALL, "");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    public void setUIforUnregistered() {
//        consultations.setVisibility(View.VISIBLE);
        register.setVisibility(View.VISIBLE);
//        support.setVisibility(View.VISIBLE);
        perscriptions.setVisibility(View.GONE);
    }

    public void setUIforRegistered() {
        register.setVisibility(View.GONE);
//        support.setVisibility(View.GONE);
        perscriptions.setVisibility(View.VISIBLE);
//        consultations.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupActionBar();
    }

    @OnClick(R.id.consultations_card)
    void startConsultationsListActiviy() {
        Intent intent = new Intent(getActivity(), ConsultationsListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.naz_card)
    void startMyPerescriptionsActivity() {
        Intent intent = new Intent(getActivity(), MyPrescriptionsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.register_card)
    void startRegisterActivity() {
        Intent intent = new Intent(getActivity(), PhoneActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.support_card)
    void startSupport() {
//        Intent intent = new Intent(getActivity(), RegisterActivity.class);
//        startActivity(intent);
    }


    private void startFindDoctorsActivity(int action, String searchString) {
        Intent intent = new Intent(getActivity(), FindDoctorsActivity.class);
        intent.putExtra(FilterActivity.EXTRA_ACTION, action);
        intent.putExtra(FilterActivity.EXTRA_ACTION_ARG, searchString);
        startActivity(intent);
    }

    @OnClick(R.id.all_doctors_btn)
    void startAllDoctors() {
        startFindDoctorsActivity(FilterActivity.EXTRA_ACTION_FIND_ALL, "");
    }

    @OnClick(R.id.therapists_btn)
    void startTherapists() {
        startFindDoctorsActivity(FilterActivity.EXTRA_ACTION_FIND_BY_SPEC, "Терапевт");
    }

    @OnClick(R.id.pediatricians_btn)
    void startPediatricians() {
        startFindDoctorsActivity(FilterActivity.EXTRA_ACTION_FIND_BY_SPEC, "Педиатр");
    }

    @OnClick(R.id.pregnancy_btn)
    void startPregnancy() {
        startFindDoctorsActivity(FilterActivity.EXTRA_ACTION_FIND_BY_SPEC, "Гинеколог");
    }

    @OnClick(R.id.btn_weight_loss)
    void startWeightLoss() {
        startActivity(new Intent(getActivity(), DietMainActivity.class));
    }

    @OnClick(R.id.btn_psychologists)
    void startPsychologists() {
        startFindDoctorsActivity(FilterActivity.EXTRA_ACTION_FIND_BY_SPEC, "Психолог");
    }

    @OnClick(R.id.ask_question_btn)
    void startAskQuestion() {
    	/*
        Intent intent = new Intent(getActivity(), ChatListActivity.class);
        startActivity(intent);
        */
    }

    @OnClick(R.id.urgent_consultation_btn)
    void startEmergencyHelp() {
        Intent intent = new Intent(getActivity(), ConsultationRequestActivity.class);
        startActivity(intent);
    }

    private void setupActionBar() {
//        getSupportActionBar().setIcon(R.drawable.icon_telemedix_white);
//        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
}
