package com.telemedix.patient.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.prescriptions.MyReceptionsActivity;
import com.telemedix.patient.activities.prescriptions.MyResearchesActivity;
import com.telemedix.patient.activities.prescriptions.MyTestAssignmentsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by baneizalfe on 7/5/16.
 */
public class MyPrescriptionsFragment extends BaseFragment {

    private Unbinder mUnbinder;

    @BindView(R.id.prescriptions_clinic_counter)
    TextView clinicCounter;
    @BindView(R.id.prescriptions_extra_counter)
    TextView extraCounter;
    @BindView(R.id.prescriptions_receipt_counter)
    TextView receiptCounter;
    @BindView(R.id.prescriptions_research_counter)
    TextView researchCounter;
    @BindView(R.id.prescriptions_test_counter)
    TextView testCounter;

    @OnClick(R.id.recipes_button)
    public void openMyRecipes(View v) {
        startActivity(new Intent(getActivity(), MyReceptionsActivity.class));
    }

    @OnClick(R.id.test_assignments_button)
    public void openMyTestAssignments(View v) {
        startActivity(new Intent(getActivity(), MyTestAssignmentsActivity.class));
    }

    @OnClick(R.id.my_researches_button)
    public void openMyResearches(View v) {
        startActivity(new Intent(getActivity(), MyResearchesActivity.class));
    }

    public static MyPrescriptionsFragment newInstance() {
        MyPrescriptionsFragment fragment = new MyPrescriptionsFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void setupCounter(TextView tv, int counter) {
        String caption;
        if (counter == 0) {
            caption = getResources().getString(R.string.no_new_items);
        } else if (counter % 10 == 1 && counter != 11) {
            caption = String.format(getResources().getString(R.string.new_item), counter);
        } else {
            caption = String.format(getResources().getString(R.string.new_items), counter);
        }

        tv.setText(caption);

        if (counter == 0) {
            tv.setTextColor(getResources().getColor(android.R.color.black));
            tv.setBackground(getResources().getDrawable(R.drawable.grey_bullet));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_prescriptions, null, false);
        mUnbinder = ButterKnife.bind(this, view);

        setupCounter(clinicCounter, 1);
        setupCounter(receiptCounter, 0);
        setupCounter(testCounter, 2);
        setupCounter(extraCounter, 0);
        setupCounter(researchCounter, 5);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}
