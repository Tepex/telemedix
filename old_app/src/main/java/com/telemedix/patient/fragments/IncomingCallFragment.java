package com.telemedix.patient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telemedix.patient.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class IncomingCallFragment extends Fragment {


    private OnCallInteractionListener mListener;
    private Unbinder mUnbinder;

    public IncomingCallFragment() {
        // Required empty public constructor
    }

    @OnClick(R.id.video_call_btn)
    public void getVideo(View v) {
        mListener.onTake();
    }

    @OnClick(R.id.voice_call_btn)
    public void getCall(View v) {
        mListener.onTake();
    }

    @OnClick(R.id.hang_call_btn)
    public void hangCall(View v) {
        mListener.onHangUp();
    }

    public static IncomingCallFragment newInstance() {
        IncomingCallFragment fragment = new IncomingCallFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCallInteractionListener) {
            mListener = (OnCallInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCallInterationListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_incoming_call, container, false);
        mUnbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnCallInteractionListener {
        void onVideo();

        void onHangUp();

        void onTake();
    }
}
