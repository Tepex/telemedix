package com.telemedix.patient.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import com.telemedix.patient.base.DividerItemDecoration;
import com.telemedix.patient.R;
import com.telemedix.patient.activities.payment.PaymentInfoActivity;
import com.telemedix.patient.adapters.PaymentHistoryListAdapter;
import com.telemedix.patient.models.PaymentHistory;
import com.telemedix.patient.network.PaymentManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by baneizalfe on 8/1/16.
 */
public class PaymentHistoryListFragment extends BaseFragment {

    public interface TYPE {
        int ALL = 0;
        int CREDIT_CARDS = 1;
        int GOOGLE_PAY = 2;
    }

    private int type;

    @BindView(R.id.list_view)
    SuperRecyclerView list_view;

    private PaymentHistoryListAdapter paymentHistoryListAdapter;

    public static PaymentHistoryListFragment newInstance(Bundle args) {
        PaymentHistoryListFragment fragment = new PaymentHistoryListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt("type", TYPE.ALL);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_history_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        list_view.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        list_view.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getContext(), R.drawable.list_divider)));

        paymentHistoryListAdapter = new PaymentHistoryListAdapter(getContext());
        paymentHistoryListAdapter.setHasStableIds(true);
        list_view.setAdapter(paymentHistoryListAdapter);

        /*
        Fake list
         */
//        List<PaymentHistory> paymentList = new ArrayList<>();
//
//        if (type == TYPE.ALL || type == TYPE.CREDIT_CARDS)
//            paymentList.add(new PaymentHistory(
//                    R.drawable.visa_icon,
//                    "29.05.2016 15:35",
//                    String.format(getResources().getString(R.string.bank_card), " \nVISA *********** 1021"),
//                    1000.0f,
//                    getResources().getString(R.string.online_consultation)
//            ));
//
//        if (type == TYPE.ALL || type == TYPE.CREDIT_CARDS)
//            paymentList.add(new PaymentHistory(
//                    R.drawable.visa_icon,
//                    "23.05.2016 13:35",
//                    String.format(getResources().getString(R.string.bank_card), " \nVISA *********** 1021"),
//                    1000.0f,
//                    getResources().getString(R.string.drugs_order)
//            ));
//
//        if (type == TYPE.ALL || type == TYPE.GOOGLE_PAY)
//            paymentList.add(new PaymentHistory(
//                    R.drawable.google_play_icon,
//                    "21.5.2016 23:12",
//                    getResources().getString(R.string.account) + " Google Market \ni.ivanov@gmail.com",
//                    1000.0f,
//                    getResources().getString(R.string.clinic_assesment)
//            ));
//
//        if (type == TYPE.ALL || type == TYPE.CREDIT_CARDS)
//            paymentList.add(new PaymentHistory(
//                    R.drawable.visa_icon,
//                    "16.04.2016 14:35",
//                    String.format(getResources().getString(R.string.bank_card), " \nVISA *********** 1021"),
//                    100.0f,
//                    getResources().getString(R.string.drugs_order)
//            ));
//        if (type == TYPE.ALL || type == TYPE.CREDIT_CARDS)
//            paymentList.add(new PaymentHistory(
//                    R.drawable.payment_promocode,
//                    "16.04.2016 14:35",
//                    getResources().getString(R.string.promo_code),
//                    0.0f,
//                    getResources().getString(R.string.online_consultation)
//            ));


        PaymentManager.getPayments()
                .subscribe(pPaymentInfos -> {
                    this.paymentHistoryListAdapter.setList(pPaymentInfos);
                });

        paymentHistoryListAdapter.setAdapterListener(payment -> {
            Intent intent = new Intent(getActivity(), PaymentInfoActivity.class);
            intent.putExtra(PaymentInfoActivity.EXTRA_PAYMENT_ID, payment.getId());
            startActivity(intent);
        });

    }
}
