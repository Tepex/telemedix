package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.network.response.IdNamePair;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Aleksey on 13.10.2016.
 */

public class Symptoms implements Serializable {
    private List<IdNamePair> items;


    public List<IdNamePair> getItems() {
        return items;
    }

    public void setItems(List<IdNamePair> pItems) {
        items = pItems;
    }
}
