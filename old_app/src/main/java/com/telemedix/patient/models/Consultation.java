package com.telemedix.patient.models;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import com.telemedix.patient.BuildConfig;

import static com.telemedix.patient.Constants.TAG;

public class Consultation extends BaseModel
{
	public static Consultation createInvalidConsultation()
	{
		Consultation res = new Consultation();
		res.isValid = false;
		return res;
	}
	
	public String getStateString()
	{
		return stateStr;
	}
	
	public State getState()
	{
		if(state == null) state = State.fromValue(getStateString());
		return state;
	}
    
	public DoctorService getDoctorService()
	{
		return service;
	}
	
	public Doctor getDoctor()
	{
		return doctor;
	}
	
	public void setDoctor(Doctor doctor)
	{
		this.doctor = doctor;
		doctorName = null;
	}
	
	public String getDoctorName(String def)
	{
		if(doctorName == null)
		{
			if(doctor == null) doctorName = def;
			else doctorName = doctor.getName();
		}
		return doctorName;
	}
	
	public String getDoctorShortName(String def)
	{
		if(doctorShortName == null)
		{
			if(doctor == null) doctorShortName = def;
			else doctorShortName = doctor.getShortName();
		}
		return doctorShortName;
	}

	public Slot getSlot()
	{
		return slot;
	}
	
	public String getCreatedAt()
	{
		return createdAt;
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public boolean isValid()
	{
		return isValid;
	}
	
	public boolean isPending()
	{
		return true;
	}
	
	public ViewType getViewType()
	{
		return viewType;
	}
	
	public boolean isInstant()
	{
		return (getSlot() == null);
	}
	
	@Override
	public String toString()
	{
		if(cacheString == null)
		{
			cacheString = "[id:"+getId()+", ";
			if(isInstant()) cacheString += "Instant";
			else
			{
				/* TODO: проверить парсинг date. Лишняя Z */
				try
				{
					cacheString += "starts: "+getSlot();
				}
				catch(Exception e)
				{
					if(BuildConfig.DEBUG) Log.d(TAG, "ZZ error", e);
				}
					
			}
			cacheString += ", state:"+getState().name()+", doctor:"+getDoctor()+"]";
		}
		return cacheString;
	}
	
	public enum ViewType
	{
		CONSULTATION, LABEL;
	}


	@SerializedName("state")
	private String stateStr;
	@SerializedName("service")
	private DoctorService service;
	@SerializedName("doctor")
	private Doctor doctor;
	@SerializedName("receptionSlot")
	private Slot slot;
	@SerializedName("createdAt")
	private String createdAt;
	@SerializedName("message")
	private String message;
	
	private transient boolean isValid = true;
	private transient ViewType viewType;
	private transient String doctorName;
	private transient String doctorShortName;
	private transient State state;
	
	public static enum State
	{
		CREATING("creating"),
		QUEUE("queue"),
		STARTING("starting"),
		FINISHED("finished"),
		CANCELED("canceled"),
		TIMEOUT("timeout");
		
		State(String state)
		{
			this.state = state;
		}
		
		
		@Override
		public String toString()
		{
			return state;
		}
		
		public static State fromValue(String s)
		{
			for(State st: State.values()) if(st.state.equals(s)) return st;
			return null;
		}
		
		private final String state;
	}
}
