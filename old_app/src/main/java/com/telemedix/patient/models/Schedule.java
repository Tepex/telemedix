package com.telemedix.patient.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.ArrayList;

public class Schedule implements Parcelable
{
	public Schedule(String title, String startTime, String endTime, List<Slot> intervals)
	{
		this.title = title;
		this.startTime = startTime;
		this.endTime = endTime;
		this.intervals = intervals;
    }
    
	public Schedule(String title, List<Slot> intervals)
	{
		this.title = title;
		this.intervals = intervals;
    }
    
	protected Schedule(Parcel in)
	{
		title = in.readString();
		startTime = in.readString();
		endTime = in.readString();
		intervals = new ArrayList<>();
		in.readList(intervals, null);
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(title);
		dest.writeString(startTime);
		dest.writeString(endTime);
		dest.writeList(intervals);
	}
	
	@Override
	public String toString()
	{
		return "[name: "+getTitle()+", startTime: "+getStartTime()+", endTime: "+getEndTime()+", intervals.length: "+intervals.size()+"]";
	}
		
	public String getTitle()
	{
		return title;
	}
	
	public String getStartTime()
	{
		return startTime;
	}
	
	public String getEndTime()
	{
		return endTime;
	}
	
	public List<Slot> getIntervals()
	{
		return intervals;
	}
	
	public CharSequence[] getIntervalsAsString()
	{
		if(intervalsAsString == null)
		{
			intervalsAsString = new String[intervals.size()];
			for(int i = 0; i < intervals.size(); ++i) intervalsAsString[i] = title+" "+intervals.get(i).getTimeString();
		}
		return intervalsAsString;
	}
	
	private String title;
	private String startTime;
	private String endTime;
	private List<Slot> intervals;
	private CharSequence[] intervalsAsString;
	
	public static final Parcelable.Creator<Schedule> CREATOR = new Parcelable.Creator<Schedule>()
	{
		@Override
		public Schedule createFromParcel(Parcel source)
		{
			return new Schedule(source);
		}
		
		@Override
		public Schedule[] newArray(int size)
		{
			return new Schedule[size];
		}
	};
}
