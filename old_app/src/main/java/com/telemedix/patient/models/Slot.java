package com.telemedix.patient.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import com.telemedix.patient.Utils;

import java.lang.Comparable;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

public class Slot extends BaseModel implements Parcelable, Comparable<Slot>
{
	protected Slot(Parcel in)
	{
		dateStr = in.readString();
		timestampStr = in.readString();
		state = in.readString();
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(dateStr);
		dest.writeString(timestampStr);
		dest.writeString(state);
	}
	
	public LocalDate getDate()
	{
		if(date == null)
		{
			int i = dateStr.indexOf('+');
			if(i != -1) dateStr = dateStr.substring(0, i);
			dateStr += "Z";
			date = Utils.ISO_FORMATTER.parseLocalDate(dateStr);
		}
		return date;
	}
	
	public LocalTime getTime()
	{
		if(time == null)
		{
			int i = timestampStr.indexOf('+');
			if(i != -1) timestampStr = timestampStr.substring(0, i);
			timestampStr += "Z";
			time = Utils.ISO_FORMATTER.parseLocalTime(timestampStr);
		}
		return time;
	}
	
	public LocalDateTime getDateTime()
	{
		if(dateTime == null)
		{
			String str = timestampStr;
			int i = str.indexOf('+');
			if(i != -1) str = str.substring(0, i);
			str += "Z";
			dateTime = Utils.ISO_FORMATTER.parseLocalDateTime(str);
		}
		return dateTime;
	}
	
	public String getTimeString()
	{
		if(timeCache == null) timeCache = getTime().toString("HH:mm");
		return timeCache;
	}
	
	public String getDateString()
	{
		if(dateCache == null) dateCache = getDate().toString("dd.MM.yy");
		return dateCache;
	}
	
	public String getDateTimeString()
	{
		if(dateTimeCache == null) dateTimeCache = getDateTime().toString("dd.MM.yyyy HH:mm");
		return dateTimeCache;
	}
	
	public String getState()
	{
		return state;
	}
	
	@Override
	public String toString()
	{
		if(cacheString == null) cacheString = "[id:"+id+", date:"+getDateString()+", time:"+getTimeString()+"]";
		return cacheString;
	}
	
	@Override
	public int compareTo(Slot other)
	{
		return getDateTime().compareTo(other.getDateTime());
	}

	@SerializedName("date")
	private String dateStr;
	@SerializedName("timestamp")
	private String timestampStr;
	@SerializedName("state")
	private String state;
	
	private transient LocalDate date;
	private transient LocalTime time;
	private transient LocalDateTime dateTime;
	
	private transient String timeCache;
	private transient String dateCache;
	private transient String dateTimeCache;
	
	public static final Parcelable.Creator<Slot> CREATOR = new Parcelable.Creator<Slot>()
	{
		@Override
		public Slot createFromParcel(Parcel source)
		{
			return new Slot(source);
		}
		
		@Override
		public Slot[] newArray(int size)
		{
			return new Slot[size];
		}
	};
}
