package com.telemedix.patient.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class Insurance implements Parcelable {

    public String text;

    public String subtitle;

    public int icon;

    public Insurance(String text, int icon) {
        this.text = text;
        this.icon = icon;
    }

    public Insurance() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeInt(this.icon);
    }

    protected Insurance(Parcel in) {
        this.text = in.readString();
        this.icon = in.readInt();
    }

    public static final Creator<Insurance> CREATOR = new Creator<Insurance>() {
        @Override
        public Insurance createFromParcel(Parcel source) {
            return new Insurance(source);
        }

        @Override
        public Insurance[] newArray(int size) {
            return new Insurance[size];
        }
    };
}
