package com.telemedix.patient.models;

/**
 * Created by Aleksey on 19.08.2016.
 */
public enum PaymentType {

    NEW_CARD,
    EXISTING_CARD,
    GOOGLE_ACCOUNT,
    PROMO;
}
