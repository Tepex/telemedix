package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.Constants;
import com.telemedix.patient.network.response.IdNamePair;
import com.telemedix.patient.network.response.StringId;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Aleksey on 29.09.2016.
 */

public class Clinic implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("id")
    private int            id;
    @SerializedName("name")
    private String         name;
    @SerializedName("address")
    private String         address;
//    @SerializedName("parent")
//    private List<Void>     parent;
    @SerializedName("doctors")
    private List<StringId> doctors;
    @SerializedName("icon")
    private String icon;
    @SerializedName("branch")
    private List<StringId> branches;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return name + ", " + address;
    }

//    public List<Void> getParent() {
//        return parent;
//    }

    public List<StringId> getDoctors() {
        return doctors;
    }

    public String getIcon() {
        return Constants.BACKOFFICE_HOST + icon;
    }

    public List<StringId> getBranches() {
        return branches;
    }
}
