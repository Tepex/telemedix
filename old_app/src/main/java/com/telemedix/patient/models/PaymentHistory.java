package com.telemedix.patient.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class PaymentHistory implements Parcelable {

    public String date;

    public String type;

    public float amount;

    public String description;

    public int icon;

    public PaymentHistory(int icon, String date, String type, float amount, String description) {
        this.icon = icon;
        this.date = date;
        this.type = type;
        this.amount = amount;
        this.description = description;
    }

    public PaymentHistory() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeString(this.type);
        dest.writeFloat(this.amount);
        dest.writeString(this.description);
        dest.writeInt(this.icon);
    }

    protected PaymentHistory(Parcel in) {
        this.date = in.readString();
        this.type = in.readString();
        this.amount = in.readFloat();
        this.description = in.readString();
        this.icon = in.readInt();
    }

    public static final Creator<PaymentHistory> CREATOR = new Creator<PaymentHistory>() {
        @Override
        public PaymentHistory createFromParcel(Parcel source) {
            return new PaymentHistory(source);
        }

        @Override
        public PaymentHistory[] newArray(int size) {
            return new PaymentHistory[size];
        }
    };
}
