package com.telemedix.patient.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by baneizalfe on 7/7/16.
 */
public class ChatMessage implements Parcelable {

    public String text;

    public long userId;

    public Date time;

    public ChatMessage(String text, long userId, String timeStr) {
        this.text = text;
        this.userId = userId;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
            this.time = sdf.parse(timeStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeLong(this.userId);
        dest.writeLong(this.time != null ? this.time.getTime() : -1);
    }

    public ChatMessage() {
    }

    protected ChatMessage(Parcel in) {
        this.text = in.readString();
        this.userId = in.readLong();
        long tmpTime = in.readLong();
        this.time = tmpTime == -1 ? null : new Date(tmpTime);
    }

    public static final Parcelable.Creator<ChatMessage> CREATOR = new Parcelable.Creator<ChatMessage>() {
        @Override
        public ChatMessage createFromParcel(Parcel source) {
            return new ChatMessage(source);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };
}
