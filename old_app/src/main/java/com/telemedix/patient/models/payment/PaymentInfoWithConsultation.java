package com.telemedix.patient.models.payment;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.Doctor;

import java.io.Serializable;

/**
 * Created by Aleksey on 24.10.2016.
 */

public class PaymentInfoWithConsultation implements Serializable {
    private PaymentInfo mPaymentInfo;
    private Consultation mConsultation;

    public PaymentInfoWithConsultation() {}

    public PaymentInfoWithConsultation(PaymentInfo pPaymentInfo, Consultation pConsultation) {
        mPaymentInfo = pPaymentInfo;
        mConsultation = pConsultation;
    }

    public PaymentInfo getPaymentInfo() {
        return mPaymentInfo;
    }

    public void setPaymentInfo(PaymentInfo pPaymentInfo) {
        mPaymentInfo = pPaymentInfo;
    }

    public Consultation getConsultation() {
        return mConsultation;
    }

    public void setConsultation(Consultation pConsultation) {
        mConsultation = pConsultation;
    }
}
