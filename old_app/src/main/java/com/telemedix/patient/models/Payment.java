package com.telemedix.patient.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class Payment implements Parcelable {

    public String title;

    public String subtitle;

    public int icon;

    public PaymentType type;
    public int cardId;

    public Payment(String title, String subtitle, int icon, PaymentType pType, int pCardId) {
        this.title = title;
        this.subtitle = subtitle;
        this.icon = icon;
        this.type = pType;
        this.cardId = pCardId;
    }

    public Payment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.subtitle);
        dest.writeInt(this.icon);
        dest.writeSerializable(this.type);
    }

    protected Payment(Parcel in) {
        this.title = in.readString();
        this.subtitle = in.readString();
        this.icon = in.readInt();
        this.type = (PaymentType) in.readSerializable();
    }

    public static final Creator<Payment> CREATOR = new Creator<Payment>() {
        @Override
        public Payment createFromParcel(Parcel source) {
            return new Payment(source);
        }

        @Override
        public Payment[] newArray(int size) {
            return new Payment[size];
        }
    };
}
