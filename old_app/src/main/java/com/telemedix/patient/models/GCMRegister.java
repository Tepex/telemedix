package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aleksey on 13.09.2016.
 */
public class GCMRegister {
    @SerializedName("id")
    private String id;
    @SerializedName("gcm")
    private String gcmToken;
    @SerializedName("imei")
    private String imei;

    public GCMRegister() {}

    public GCMRegister(String pGcmToken, String pImei) {
        setGcmToken(pGcmToken);
        setImei(pImei);
    }

    public String getId() {
        return id;
    }

    public void setId(String pId) {
        id = pId;
    }

    public String getGcmToken() {
        return gcmToken;
    }

    public void setGcmToken(String pGcmToken) {
        gcmToken = pGcmToken;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String pImei) {
        imei = pImei;
    }
}
