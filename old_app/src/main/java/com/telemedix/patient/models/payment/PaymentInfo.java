package com.telemedix.patient.models.payment;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.User;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Aleksey on 21.10.2016.
 */

public class PaymentInfo implements Serializable {
    @SerializedName("id")
    private int          id;
    @SerializedName("title")
    private String       title;
    @SerializedName("price")
    private String       price;
    @SerializedName("currency")
    private String       currency;
    @SerializedName("status")
    private int          status;
    @SerializedName("paymentFormUrl")
    private String       paymentUrl;
    @SerializedName("createdAt")
    private String       createdAt;
    @SerializedName("processedAt")
    private String       processedAt;
    @SerializedName("expiresAt")
    private String       expiresAt;
    @SerializedName("user")
    private User user;
    @SerializedName("consultation")
    private Consultation consultation;
    @SerializedName("cardInfo")
    private CardInfo cardInfo;

    private boolean isValid = true;

    public PaymentInfo() {}

    public static PaymentInfo getInvalidPaymentInfo() {
        PaymentInfo pi = new PaymentInfo();
        pi.isValid = false;
        return pi;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public int getStatus() {
        return status;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getProcessedAt() {
        return processedAt;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public User getUser() {
        return user;
    }

    public Consultation getConsultation() {
        return consultation;
    }

    public boolean isValid() {
        return isValid;
    }

    public CardInfo getCardInfo() {
        return cardInfo;
    }
}
