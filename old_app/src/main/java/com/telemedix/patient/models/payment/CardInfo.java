package com.telemedix.patient.models.payment;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.models.Consultation;

import java.io.Serializable;

/**
 * Created by Alex on 03.11.2016.
 */

public class CardInfo implements Serializable {
    @SerializedName("id")
    private int          id;
    @SerializedName("cardholder")
    private String       cardholder;
    @SerializedName("expiration")
    private int       expiration;
    @SerializedName("pan")
    private String       pan;

    public CardInfo(int id, String cardholder, int expiration, String pan) {
        this.id=id;
        this.cardholder=cardholder;
        this.expiration=expiration;
        this.pan=pan;
    }

    public String getExpiration(){
        //201808 -> 08/18
        String expSrt=String.valueOf(expiration);
        if(expSrt.length()!=6) return "??/??";
        return expSrt.substring(4,6)+"/"+expSrt.substring(2,4);
    }

    public String getCardholder() {
        return cardholder;
    }

    public String getPan() {
        return pan;
    }

    public int getId() {
        return id;
    }
}
