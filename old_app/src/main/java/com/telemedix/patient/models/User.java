package com.telemedix.patient.models;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import java.net.URLDecoder;

import static com.telemedix.patient.Constants.TAG;

/**
 * Created by Aleksey on 06.10.2016.
 */
public class User implements Serializable
{
	public User(String firstName, String lastName, String patronymic, String email)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.patronymic = patronymic;
		this.email = email;
	}
	
	public String getId()
	{
		return id;
	}
	
	public String getFirstName()
	{
		return (firstName == null ? "" : firstName);
	}
	
	public String getLastName()
	{
		return (lastName == null ? "" : lastName);
	}
    
	public String getPatronymic()
	{
		return (patronymic == null ? "" : patronymic);
	}
	
	public String getEmail()
	{
		return (email == null ? "" : email);
	}
	
	public String getSex()
	{
		return sex;
	}
	
	public String getBirthDate()
	{
		return birthDate;
	}
	
	public String getPhone()
	{
		return phone;
	}
    
	public String getPhoto()
	{
		if(photo == null || photo.isEmpty()) return "";
		try
		{
			return Constants.BACKOFFICE_HOST+URLDecoder.decode(photo, "UTF-8");
		}
		catch(UnsupportedEncodingException e)
		{
			Log.e(TAG, "User.getPhoto URL encoding error", e);
			return  null;
		}
	}
	
	public String getFullPhoto()
	{
		String ret = getPhoto();
		if(photo == null) return null;
		return ret+"/0/0";
	}
	
	@SerializedName("id")
	private String id;
	@SerializedName("firstName")
	private String firstName;
	@SerializedName("lastName")
	private String lastName;
	@SerializedName("patronymic")
	private String patronymic;
	@SerializedName("email")
	private String email;
	@SerializedName("sex")
	private String sex;
	@SerializedName("birthDate")
	private String birthDate;
	@SerializedName("phone")
	private String phone;
	@SerializedName("photo")
	private String photo;
}
