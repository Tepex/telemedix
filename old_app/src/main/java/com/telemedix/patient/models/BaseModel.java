package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class BaseModel implements Serializable
{
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	@Override
	public String toString()
	{
		if(cacheString == null) cacheString = "[id:"+id+", name:"+name+"]";
		return cacheString;
	}
	
	@Override
	public int hashCode()
	{
		return id;
	}
	
	protected transient String cacheString;
	
	@SerializedName("id")
	protected int id;
	@SerializedName("name")
	protected String name;
	
	private static final long serialVersionUID = 1L;
}
