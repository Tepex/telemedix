package com.telemedix.patient.models;

import android.util.Log;

import com.telemedix.patient.activities.doctor.FilterActivity;
import com.telemedix.patient.network.Api;
import com.telemedix.patient.network.DoctorManager;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Aleksey on 22.08.2016.
 */
public enum DoctorsPool {

    get;

    private List<Doctor> mDoctors;

    DoctorsPool() {
    }

    public Observable<List<Doctor>> getAllDoctorsObservable() {
        return getDoctorsObservable(0, "");
    }

    public static final int SORT_RATING = 1;
    public static final int SORT_PRICE = 2;

    private static Comparator<Doctor> COMPARATOR_RATING = (pDoctor, pT1) -> {
        double diff = Math.ceil((pDoctor.getRating() - pT1.getRating()) * 100);
//        Log.e("COMPARE", "" + pDoctor.getRating() + ", " + pT1.getRating() + ", diff: " + diff);
        return (int)diff;
    };

    private static Comparator<Doctor> COMPARATOR_PRICE = (pDoctor, pT1) -> {
        double diff = Math.ceil(pDoctor.getServices().get(0).getPrice() -
                                     pT1.getServices().get(0).getPrice());

//        Log.e("COMPARE", "" + diff);
        return (int)diff;

    };

    private List<Doctor> sortDoctors(List<Doctor> doctors, int sortKey, boolean asc) {
        switch (sortKey) {
            case SORT_RATING:
                Collections.sort(doctors,COMPARATOR_RATING);
                break;
            case SORT_PRICE:
                Collections.sort(doctors,COMPARATOR_PRICE);
                break;
        }
        if (!asc) {
            Collections.reverse(doctors);
        }
        return doctors;
    }

    public Observable<List<Doctor>> getDoctorsObservable(int type, String searchString, int sortingKey, boolean asc) {
        return getDoctorsObservable(type, searchString)
                .map(pDoctors -> sortDoctors(pDoctors, sortingKey, asc));
    }

    public Observable<List<Doctor>> getDoctorsObservable(int type, String searchString) {
        Observable<List<Doctor>> obs = Observable.create(
                new ObservableOnSubscribe<List<Doctor>>()
                {
                    @Override
                    public void subscribe(ObservableEmitter<List<Doctor>> subscriber)
                    {
                    	if(mDoctors == null || mDoctors.size() == 0)
                    	{
                    		DoctorManager
                    			.getDoctors()
//                    			.toBlocking()
                    			.subscribe(pDoctors->mDoctors = pDoctors);
                    	}
                        subscriber.onNext(getDoctors(type, searchString));
                        subscriber.onComplete();
                    }
                }
        );

        return obs.subscribeOn(Schedulers.io())
                  .observeOn(AndroidSchedulers.mainThread())
                  .unsubscribeOn(Schedulers.io());
    }

    private List<Doctor> getAllDoctors() {
        return getDoctors(0, "");
    }

    private List<Doctor> getDoctors(int type, String searchString) {
        if (type <= 0 || type == FilterActivity.EXTRA_ACTION_FIND_ALL) {
            return mDoctors;
        }

        List<Doctor> result = new LinkedList<>();

        switch (type) {
            case FilterActivity.EXTRA_ACTION_FIND_BY_SPEC:
                for (Doctor doc : mDoctors) {
                    if (doc.getSpecialization().getName().equals(searchString)) {
                        result.add(doc);
                    }
                }
                break;
            case FilterActivity.EXTRA_ACTION_FIND_BY_CITY:
                for (Doctor doc : mDoctors) {
                    if (doc.getCity().equals(searchString)) {
                        result.add(doc);
                    }
                }
                break;
            case FilterActivity.EXTRA_ACTION_FIND_BY_CLINIC:
                for (Doctor doc : mDoctors) {
                    if (doc.getClinic().getName().equals(searchString) || doc.getClinic().getAddress().equals(searchString)) {
                        result.add(doc);
                    }
                }
                break;
        }

        return result;
    }

    private void initDoctorList() {
        mDoctors = new LinkedList<>();

        DoctorManager.getDoctors()
               .subscribe(pDoctors -> mDoctors = pDoctors);
    }

    public List<Doctor> searchDoctors(String searchString) {
        List<Doctor> result = new LinkedList<>();
        for (Doctor doc : mDoctors) {
            if (doc.getName().toLowerCase().contains(searchString.toLowerCase())) {
                result.add(doc);
                continue;
            }

            if (doc.getCity().toLowerCase().contains(searchString.toLowerCase())) {
                result.add(doc);
                continue;
            }

            if (doc.getClinicString().toLowerCase().contains(searchString.toLowerCase())) {
                result.add(doc);
                continue;
            }

            if (doc.getCategory().name.toLowerCase().contains(searchString.toLowerCase())) {
                result.add(doc);
                continue;
            }
        }

        return result;
    }
}
