package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;

public class Service extends BaseModel
{
	public boolean isOnlineConsultation()
	{
		return onlineConsultation;
	}
	
	@SerializedName("isOnlineConsultation")
	private boolean onlineConsultation;
}
