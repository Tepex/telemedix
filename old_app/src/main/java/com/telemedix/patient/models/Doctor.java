package com.telemedix.patient.models;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import com.telemedix.patient.Constants;
import com.telemedix.patient.Utils;

import java.io.Serializable;

import java.util.List;

public class Doctor implements Serializable
{
	public Doctor(DoctorInfo user)
	{
		this.user = user;
	}
	
	public String getId()
	{
		return id;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public Specialization getSpecialization()
	{
		return specialization;
	}
    
	public float getRating()
	{
		return Float.parseFloat(rating);
	}
	
	public String getClinicString()
	{
		return clinic.toString();
	}
	
	public Clinic getClinic()
	{
		return clinic;
	}
	
	public DoctorCategory getCategory()
	{
		return new DoctorCategory(specialization);
	}
	
	public String getPhoto()
	{
		return user.getPhotoUrl();
	}
	
	public String getPriceString(Context context)
	{
		for(DoctorService service: services) if(service.isOnlineConsultation()) return service.getPriceString(context);
		return "";
	}
	
	public String getName()
	{
		return user.getName();
	}
	
	public String getShortName()
	{
		return user.getShortName();
	}
	
	public boolean isVideoEnabled()
	{
		return consultationChannel.contains("video");
	}
	
	public boolean isOnline()
	{
		return sessionStatus.contains("online");
	}
	
	public List<DoctorService> getServices()
	{
		return services;
	}
	
	@Override
	public String toString()
	{
		return getShortName();
	}
	
	private static final long serialVersionUID = 1L;
	
	@SerializedName("id")
	private String id;
	@SerializedName("city")
	private String city;
	@SerializedName("consultationChannel")
	private String consultationChannel;
	@SerializedName("rating")
	private String rating;
	@SerializedName("sessionStatus")
	private String sessionStatus;
	@SerializedName("closestConsultation")
	private String closestConsultation;
	@SerializedName("user")
	private DoctorInfo user;
	@SerializedName("specialization")
	private Specialization specialization;
	@SerializedName("clinic")
	private Clinic clinic;
	@SerializedName("services")
	private List<DoctorService> services;
	
	public static class DoctorInfo implements Serializable
	{
		public DoctorInfo(String id, String firstName, String lastName, String patronymic, String photoUrl)
		{
			this.id = id;
			this.firstName = firstName;
			this.lastName = lastName;
			this.patronymic = patronymic;
			this.photoUrl = photoUrl;
		}
		
		public String getId()
		{
			return id;
		}
		
		public String getFirstName()
		{
			return firstName;
		}
		
		public String getLastName()
		{
			return lastName;
		}
		
		public String getPatronymic()
		{
			return patronymic;
		}
		
		public String getPhotoUrl()
		{
			if(photoUrl == null) return null;
			return Constants.BACKOFFICE_HOST+photoUrl;
		}
		
		public String getName()
		{
			if(nameCache == null) nameCache = getLastName()+" "+getFirstName()+" "+getPatronymic();
			return nameCache;
		}
		
		public String getShortName()
		{
			if(shortNameCache == null) shortNameCache = getLastName()+" "+getFirstName().substring(0, 1)+". "+getPatronymic().substring(0, 1)+".";
			return shortNameCache;
		}
		
		private static final long serialVersionUID = 1L;
		
		@SerializedName("id")
		private String id;
		@SerializedName("firstName")
		private String firstName;
		@SerializedName("lastName")
		private String lastName;
		@SerializedName("patronymic")
		private String patronymic;
		@SerializedName("photo")
		private String photoUrl;
		
		private transient String nameCache;
		private transient String shortNameCache;
	}
}
