package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 29.09.2016.
 */

public class Specialization implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
