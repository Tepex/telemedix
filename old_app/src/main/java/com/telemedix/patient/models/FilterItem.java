package com.telemedix.patient.models;

public class FilterItem extends BaseModel
{
	public FilterItem(String name, int id)
	{
		this.id = id;
		this.name = name;
	}
}
