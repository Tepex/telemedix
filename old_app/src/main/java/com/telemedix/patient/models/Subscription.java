package com.telemedix.patient.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class Subscription implements Parcelable {

    public String code;

    public String description;

    public String includes;

    public String issued_at;

    public String valid_until;

    public int icon;
    public int image;

    public Subscription() {
    }

    public Subscription(String code, String description, String includes, String issued_at, String valid_until, int icon, int image) {
        this.code = code;
        this.description = description;
        this.includes = includes;
        this.issued_at = issued_at;
        this.valid_until = valid_until;
        this.icon = icon;
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.description);
        dest.writeString(this.includes);
        dest.writeString(this.issued_at);
        dest.writeString(this.valid_until);
        dest.writeInt(this.icon);
        dest.writeInt(this.image);
    }

    protected Subscription(Parcel in) {
        this.code = in.readString();
        this.description = in.readString();
        this.includes = in.readString();
        this.issued_at = in.readString();
        this.valid_until = in.readString();
        this.icon = in.readInt();
        this.image = in.readInt();
    }

    public static final Creator<Subscription> CREATOR = new Creator<Subscription>() {
        @Override
        public Subscription createFromParcel(Parcel source) {
            return new Subscription(source);
        }

        @Override
        public Subscription[] newArray(int size) {
            return new Subscription[size];
        }
    };
}
