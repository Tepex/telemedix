package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ClinicService implements Serializable
{
	public Service getService()
	{
		return service;
	}
	
	public boolean isOnlineConsultation()
	{
		return getService().isOnlineConsultation();
	}
	
	@SerializedName("service")
	private Service service;
	
	private static final long serialVersionUID = 1L;
}
