package com.telemedix.patient.models;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import com.telemedix.patient.R;

public class DoctorService extends BaseModel
{
	public ClinicService getClinicService()
	{
		return service;
	}
	
	public int getPrice()
	{
		return price;
	}
	
	public String getPriceString(Context context)
	{
		if(priceStringCache == null) priceStringCache = context.getString(R.string.price_format, price);
		return priceStringCache;
	}
	
	public boolean isOnlineConsultation()
	{
		return getClinicService().isOnlineConsultation();
	}
	
	@SerializedName("service")
	private ClinicService service;
	@SerializedName("price")
	private int price;
	
	private transient String priceStringCache;
}
