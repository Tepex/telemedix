package com.telemedix.patient.models;

/**
 * Created by Aleksey on 06.10.2016.
 */

public enum BloodType {
    ONE_MINUS("I(-)"),
    ONE_PLUS("I(+)"),
    TWO_MINUS("II(-)"),
    TWO_PLUS("II(+)"),
    THREE_MINUS("III(-)"),
    THREE_PLUS("III(+)"),
    FOUR_MINUS("IV(-)"),
    FOUR_PLUS("IV(+)");

    public String text;

    BloodType(String str) {
        text = str;
    }


}
