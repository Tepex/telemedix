package com.telemedix.patient.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by baneizalfe on 8/4/16.
 */
public class DoctorCategory implements Parcelable {

    public static final int TERAPEVT = 1;
    public static final int PEDIATR = 2;
    public static final int PREGNANCY = 3;
    public static final int WEIGHT_LOSS = 4;
    public static final int PSYCHOLOGY = 5;

    public long id;
    public String name;

    public DoctorCategory(Specialization pSpecialization) {
        this.id = pSpecialization.getId();
        this.name = pSpecialization.getName();
    }

    public DoctorCategory(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
    }

    protected DoctorCategory(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<DoctorCategory> CREATOR = new Parcelable.Creator<DoctorCategory>() {
        @Override
        public DoctorCategory createFromParcel(Parcel source) {
            return new DoctorCategory(source);
        }

        @Override
        public DoctorCategory[] newArray(int size) {
            return new DoctorCategory[size];
        }
    };

    @Override
    public String toString() {
        return name;
    }
}
