package com.telemedix.patient.models;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class TwilioSession implements Serializable
{
	public TwilioSession() {}
	
	public TwilioSession(String identity, String chatToken, String videoToken, String room)
	{
		this.identity = identity;
		this.chatToken = chatToken;
		this.videoToken = videoToken;
		this.room = room;
	}
	
	public String getIdentity()
	{
		return identity;
	}
	
	public String getChatToken()
	{
		return chatToken;
	}
	
	public String getVideoToken()
	{
		return videoToken;
	}
	
	public String getRoom()
	{
		return room;
	}
	
	@Override
	public String toString()
	{
		if(cacheString == null) cacheString = "[identity:"+identity+", room:"+room+"]";
		return cacheString;
	}
	
	@NonNull
	@SerializedName("identity")
	protected String identity;
	@SerializedName("chatToken")
	protected String chatToken;
	@NonNull
	@SerializedName("videoToken")
	protected String videoToken;
	@NonNull
	@SerializedName("room")
	protected String room;
	
	protected transient String cacheString;
}
