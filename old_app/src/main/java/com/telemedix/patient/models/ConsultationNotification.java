package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.Utils;
import java.io.Serializable;

import org.joda.time.DateTime;

public class ConsultationNotification implements Serializable
{
	public String getType()
	{
		return type;
	}
	
	public String getEventString()
	{
		return eventStr;
	}
	
	public Event getEvent()
	{
		if(event == null) event = Event.fromValue(getEventString());
		return event;
	}
	
	public String getAck()
	{
		return ack;
	}
	
	public Body getBody()
	{
		return body;
	}

	@Override
	public String toString()
	{
		if(cacheString == null) cacheString = "[type:"+type+", event:"+getEvent()+", body:"+getBody()+"]";
		return cacheString;
	}
	
	@SerializedName("type")
	private String type;
	@SerializedName("event")
	private String eventStr;
	@SerializedName("ack")
	private String ack;
	@SerializedName("body")
	private Body body;
	
	protected transient String cacheString;
	protected transient Event event;
	
	private static final long serialVersionUID = 1L;
	
	public static class Body extends TwilioSession
	{
		/* consultation ID */
		public int getId()
		{
			return id;
		}
		
		/*
		public DateTime getStartsAt()
		{
			if(startsAt == null) startsAt = Utils.ISO_FORMATTER.parseDateTime(startsAtStr);
			return startsAt;
		}
		*/
		
		@Override
		public String toString()
		{
			if(cacheString == null) cacheString = "[id:"+id+"]";
			return cacheString;
		}
		
		@SerializedName("id")
		private int id;
		/*
		@Nullable
		@SerializedName("startsAt")
		private String startsAtStr;
		
		private transient DateTime startsAt;
		*/
	}
	
	public static enum Event
	{
		START_ONLINE("start-online-consultation"),
		CANCEL("consultation.cancel"),
		MOVE("consultation.move_request"),
		CONCLUSION("consultation_conclusion.income");
		
		Event(final String event)
		{
			this.event = event;
		}
		
		@Override
		public String toString()
		{
			return event;
		}
		
		public static Event fromValue(String s)
		{
			for(Event e: Event.values()) if(e.toString().equals(s)) return e;
			return null;
		}
		
		private final String event;
	}
}
