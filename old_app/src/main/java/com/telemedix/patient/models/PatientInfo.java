package com.telemedix.patient.models;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.network.response.IdNamePair;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Aleksey on 06.10.2016.
 */
public class PatientInfo implements Serializable
{
	public static PatientInfo getFailedPatientInfo()
	{
		PatientInfo patientInfo = new PatientInfo();
		patientInfo.valid = false;
		return patientInfo;
	}
	
	public String getId()
	{
		return id;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public Address getAddress()
	{
		return address;
	}
	
	public ClosestRelative getClosestRelative()
	{
		return closestRelative;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public int getWeight()
	{
		return weight;
	}
	
	public String getBloodType()
	{
		return bloodType;
	}
	
	public boolean isValid()
	{
		return valid;
	}
	
	public List<IdNamePair> getAllergies()
	{
		return allergies;
	}
	
	public List<IdNamePair> getChronicDiseases()
	{
		return chronicDiseases;
	}
	
	@SerializedName("id")
	private String id;
	@SerializedName("height")
	private int height;
	@SerializedName("weight")
	private int weight;
	@SerializedName("bloodType")
	private String bloodType;
	@SerializedName("user")
	private User user;
	@SerializedName("address")
	private Address address;
	@SerializedName("allergies")
	private List<IdNamePair> allergies;
	@SerializedName("chronicDiseases")
	private List<IdNamePair> chronicDiseases;
	@SerializedName("closestRelative")
	private ClosestRelative closestRelative;
	
	private boolean valid = true;
	
	public static class ClosestRelative implements Serializable
	{
		public ClosestRelative(String firstName, String lastName, String phone)
		{
			this.firstName = firstName;
			this.lastName = lastName;
			this.phone = phone;
		}
	
		public String getFirstName()
		{
			return firstName;
		}
	
		public String getLastName()
		{
			return lastName;
		}
	
		public String getPhone()
		{
			return phone;
		}
	
		@SerializedName("firstName")
		private String firstName;
		@SerializedName("lastName")
		private String lastName;
		@SerializedName("phone")
		private String phone;
	}
	
	public static class Address implements Serializable
	{
		public String getCity()
		{
			return city;
		}
	
		public String getAddress()
		{
			return address;
		}
	
		public Country getCountry()
		{
			return country;
		}
	
		@SerializedName("country")
		private Country country;
		@SerializedName("city")
		private String city;
		@SerializedName("address")
		private String address;

		public static class Country extends BaseModel
		{
			public String getTitle()
			{
				return title;
			}
	
			@SerializedName("title")
			private String title;
		}
	}
}
