package com.telemedix.patient.view;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import android.support.design.widget.Snackbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.telemedix.core.presenter.Presenter;
import com.telemedix.core.presenter.PresenterCache;

import com.telemedix.patient.R;

import timber.log.Timber;

public class BaseActivity<P extends Presenter> extends AppCompatActivity
{
	@CallSuper
	protected void onCreate(@Nullable Bundle savedInstanceState, @LayoutRes int layout)
	{
		presenterCache = (PresenterCache)getLastCustomNonConfigurationInstance();
		if(presenterCache == null)
		{
			long seed;
			if(savedInstanceState == null) seed = 0;
			else seed = savedInstanceState.getLong(NEXT_ID_KEY);
			presenterCache = PresenterCache.getInstance(seed);
		}
		
		super.onCreate(savedInstanceState);
		setContentView(layout);
		unbinder = ButterKnife.bind(this);
		if(toolbar != null)
		{
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}
	
	@CallSuper
	@Override
	protected void onDestroy()
	{
		Timber.d("onDestroy");
		if(unbinder != null) unbinder.unbind();
		super.onDestroy();
	}

	@CallSuper
	@Override
	public boolean onOptionsItemSelected(final MenuItem item)
	{
		if(item.getItemId() == android.R.id.home)
		{
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@OnClick(R.id.skip_step)
	public void nextStep()
	{
		Fragment fr = getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT);
		if(fr != null && (fr instanceof BaseFragment))
		{
			Timber.d("find fragment "+fr.getClass().getSimpleName());
			((BaseFragment)fr).nextStep();
		}
		else Timber.d("fragment not found");
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		//getFragmentManager().putFragment(outState, "currentFrag", fragment);
		long id = presenterCache.getId();
		outState.putLong(NEXT_ID_KEY, id);
		super.onSaveInstanceState(outState);
	}
	
	@Override
	public Object onRetainCustomNonConfigurationInstance()
	{
		return presenterCache;
	}
	
	public void setToolbarTitle(@StringRes int titleId)
	{
		if(toolbar != null) getSupportActionBar().setTitle(titleId);
	}
	
	public void setToolbarTitle(String title)
	{
		if(toolbar != null) getSupportActionBar().setTitle(title);
	}
	
	public void setToolbarNextButton(boolean hasNext)
	{
		if(nextButton == null) return;
		nextButton.setVisibility(hasNext ? View.VISIBLE : View.GONE);
	}
	
	public void showProgress(boolean show)
	{
		progress.setVisibility(show ? View.VISIBLE : View.GONE);
	}
	
	public void goFragment(BaseFragment fragment, boolean addToBackStack)
	{
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.fragment_container, fragment, CURRENT_FRAGMENT);
		if(addToBackStack) ft.addToBackStack(null);
		ft.commit();
	}
	
	public void goActivity(Class activityClass, Bundle extras, boolean isTop)
	{
		Intent intent = new Intent(this, activityClass);
		intent.putExtras(extras);
		if(isTop) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
	}
	
	public PresenterCache getPresenterCache()
	{
		return presenterCache;
	}
	
	public void serverError(int code)
	{
		Snackbar.make(getCurrentFocus(), getString(R.string.server_error, code), Snackbar.LENGTH_LONG).show();
	}
	
	private static final String NEXT_ID_KEY = "nextId";
	
	protected P presenter;
	private Unbinder unbinder;
	private PresenterCache presenterCache;
	
	@Nullable
	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.skip_step) View nextButton;
	@BindView(R.id.progressLayout) View progress;
	
	public static final String CURRENT_FRAGMENT = "current";
}
