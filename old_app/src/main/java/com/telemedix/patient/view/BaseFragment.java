package com.telemedix.patient.view;

import android.content.Context;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;

import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.telemedix.core.interceptor.Interceptor;

import com.telemedix.core.model.Model;
import com.telemedix.core.model.Model.State;

import com.telemedix.core.presenter.Presenter;
import com.telemedix.core.presenter.PresenterCache;
import com.telemedix.core.presenter.PresenterFactory;

import com.telemedix.core.view.PresenterView;

import com.telemedix.patient.R;

import java.util.HashMap;

import timber.log.Timber;

public abstract class BaseFragment<V extends PresenterView, M extends Model, I extends Interceptor, P extends Presenter<V, M, I>> extends Fragment implements PresenterView
{
	@Override
	@CallSuper
	public void onAttach(Context context)
	{
		super.onAttach(context);
		if(!(context instanceof BaseActivity))
			throw new RuntimeException(getClass().getSimpleName()+" must be attached to BaseActivity");
		presenterCache = ((BaseActivity)context).getPresenterCache();
		Timber.d("onAttach fragment. Get presenterCache "+presenterCache);
	}
	
	protected View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState, @LayoutRes int layout)
	{
		Timber.i("onCreateView "+savedInstanceState);
		View root = inflater.inflate(layout, container, false);
		unbinder = ButterKnife.bind(this, root);
		return root;
	}
	
	/** Достаем презентер из кеша или создаем новый если в кеше его нет. */
	@Override
	@CallSuper
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Timber.d("onCreate. savedInstanceState="+savedInstanceState);
		if(savedInstanceState == null) presenterId = presenterCache.generateId();
		else presenterId = savedInstanceState.getLong(PRESENTER_INDEX_KEY);
		Timber.d("onCreate().presenterId="+presenterId);
		presenter = presenterCache.get(presenterId);
		Timber.d("onCreate().presenter "+presenter);
		if(presenter == null)
		{
			presenter = presenterFactory.create();
			Timber.d("onCreate().presenter created");
			presenterCache.put(presenterId, presenter);
			presenter.onCreate(createModel());
		}
	}
	
	@Override
	@CallSuper
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		presenter.bindView(getPresenterView());
	}
	
	@Override
	@CallSuper
	public void onResume()
	{
		super.onResume();
		isDestroyedBySystem = false;
	}
	
	@Override
	@CallSuper
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		isDestroyedBySystem = true;
		Timber.d("onSaveInstanceState. presenterId="+presenterId);
		outState.putLong(PRESENTER_INDEX_KEY, presenterId);
	}

	@Override
	@CallSuper
	public void onDestroyView()
	{
		presenter.unbindView();
		unbinder.unbind();
		super.onDestroyView();
	}

	@Override
	@CallSuper
	public void onDestroy()
	{
		Timber.d("onDestroy by system "+isDestroyedBySystem);
		// убит пользователем
		if(!isDestroyedBySystem)
		{
			presenterCache.remove(presenterId);
			presenter.onDestroy();
		}
		presenter = null;
		super.onDestroy();
	}
	
	public BaseActivity getBaseActivity()
	{
		return (BaseActivity)getActivity();
	}
	
	@SuppressWarnings("unchecked")
	public V getPresenterView()
	{
		return (V)this;
	}
	
	public P getPresenter()
	{
		return presenter;
	}
	
	public M getModel()
	{
		return presenter.getModel();
	}
	
	public void nextStep() {}
	
	protected abstract P createPresenter();
	protected abstract M createModel();
	protected abstract I createInterceptor();
	
	/* PresenterView */
	@Override
	public abstract void setState(State state);
	
	@Override
	public void serverError(int code)
	{
		getBaseActivity().serverError(code);
	}
	
	private static final String PRESENTER_INDEX_KEY = "presenter-id";
	
	private PresenterCache presenterCache;
	private long presenterId;
	private P presenter;
	/** Флаг определяет каким образом фрагмент уничтожается — по воле пользователя или системой */
	private boolean isDestroyedBySystem;
	
	private Unbinder unbinder;
	
	public PresenterFactory<P, I> presenterFactory = new PresenterFactory<P, I>()
	{
		@Override
		public P create()
		{
			I interceptor = createInterceptor();
			P presenter = createPresenter();
			presenter.setInterceptor(interceptor);
			return presenter;
		}
	};
}
