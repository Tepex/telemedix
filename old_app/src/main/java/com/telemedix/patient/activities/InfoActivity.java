package com.telemedix.patient.activities;

import android.os.Bundle;
import android.view.MenuItem;

import android.support.annotation.CallSuper;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.R;

import com.telemedix.patient.fragments.InfoBasicFragment;
import com.telemedix.patient.fragments.InfoMoreFragment;

public class InfoActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_info_activity);
		if(savedInstanceState == null) getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.main_content, InfoBasicFragment.newInstance())
			.commit();
	}
	
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == android.R.id.home)
		{
			if(getSupportFragmentManager().getBackStackEntryCount() > 0)
			{
				getSupportFragmentManager().popBackStack();
				return true;
			}
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void showMoreInfo()
	{
		getSupportFragmentManager().beginTransaction().replace(R.id.main_content, InfoMoreFragment.newInstance()).addToBackStack("INFO_MORE_FRAGMENT").commit();
	}
	
	public void saveData()
	{
		finish();
	}
}
