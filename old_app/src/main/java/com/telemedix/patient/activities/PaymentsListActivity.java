package com.telemedix.patient.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.base.MenuActivity;
import com.telemedix.patient.base.DividerItemDecoration;

import com.telemedix.patient.Constants;
import com.telemedix.patient.R;
import com.telemedix.patient.adapters.PaymentListAdapter;
import com.telemedix.patient.models.Payment;
import com.telemedix.patient.models.PaymentType;
import com.telemedix.patient.models.payment.CardInfo;
import com.telemedix.patient.network.Api;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentsListActivity extends MenuActivity {

    @BindView(R.id.list_view)
    SuperRecyclerView list_view;

    private PaymentListAdapter paymentListAdapter;

    private List<Payment> convertToPayments(List<CardInfo> cCardInfo) {
        List<Payment> paymentList = new ArrayList<>();
        for (int i = 0; i < cCardInfo.size(); i++) {
            paymentList.add(new Payment(String.format(getResources().getString(R.string.bank_card), cCardInfo.get(i).getPan()),
                    String.format(getResources().getString(R.string.bank_card_valid_until), cCardInfo.get(i).getExpiration()),
                    R.drawable.visa_icon,
                    PaymentType.NEW_CARD, 0));
        }
        return paymentList;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_payment_methods_list;
    }

    @Override
    protected void initViews() {
        list_view.setLayoutManager(new LinearLayoutManager(this));
        list_view.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.list_divider)));
        paymentListAdapter = new PaymentListAdapter(this, false);
        paymentListAdapter.setHasStableIds(true);
        list_view.setAdapter(paymentListAdapter);
        //String TEST_UUID = "6A0DCD15-B571-B2D7-F55D-35BCA96DFC41";
        Api.get.getUserCards(Hawk.get(Constants.PREF_USER_ID))
                .map(this::convertToPayments)
                .subscribe(pList -> {
                            this.paymentListAdapter.setList(pList);
                            paymentListAdapter.notifyDataSetChanged();
                        }
                 );
        /*
        Fake list
         */
        /*
        List<Payment> paymentList = new ArrayList<>();

        paymentList.add(new Payment(String.format(getResources().getString(R.string.bank_card), " *********** 1021"),
                String.format(getResources().getString(R.string.bank_card_valid_until), " 11/18"),
                R.drawable.visa_icon,
                PaymentType.NEW_CARD));
        paymentList.add(new Payment(getResources().getString(R.string.account) + " Google Market",
                "i.ivanov@gmail.com",
                R.drawable.google_play_icon,
                PaymentType.GOOGLE_ACCOUNT));
        */

    }
}
