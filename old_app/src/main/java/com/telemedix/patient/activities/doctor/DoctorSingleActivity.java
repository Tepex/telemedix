package com.telemedix.patient.activities.doctor;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

import android.support.design.widget.TabLayout;

import android.support.v4.view.ViewPager;

import android.util.Log;

import android.view.View;

import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import butterknife.BindView;

import com.bumptech.glide.Glide;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import static com.telemedix.patient.Constants.TAG;

public class DoctorSingleActivity extends BaseActivity
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_doctor_single);
		Doctor doctor = (Doctor)getIntent().getSerializableExtra(ARG_DOCTOR);
		getSupportActionBar().setTitle(doctor.getName());
		
		String titleText = "";
		if(doctor.getCategory() != null) titleText += doctor.getCategory();
		if(doctor.getClinicString() != null) titleText += ", "+doctor.getClinicString();
		titleText += "\n"+doctor.getPriceString(this);
		title.setText(titleText);
		
		rate.setRating(doctor.getRating());
		if(doctor.getPhoto() != null) Glide.with(this).load(doctor.getPhoto()).crossFade().centerCrop().into(avatar);
		if(doctor.isOnline()) online.setVisibility(View.VISIBLE);
		else online.setVisibility(View.INVISIBLE);
		
		action_indicator.setImageResource((doctor.isVideoEnabled()) ? R.drawable.videocam : R.drawable.audiocall);
		action_indicator.setEnabled(doctor.isOnline());
		
		tabs.setupWithViewPager(viewPager);
		viewPager.setAdapter(new TabsAdapter(this, doctor, getSupportFragmentManager()));
	}
		
	public final static String ARG_DOCTOR = "doctor";
	
	@BindView(R.id.avatar) ImageView avatar;
	@BindView(R.id.title) TextView title;
	@BindView(R.id.rate) RatingBar rate;
	@BindView(R.id.online) TextView online;
	@BindView(R.id.action_indicator) ImageView action_indicator;
	@BindView(R.id.tabs) TabLayout tabs;
	@BindView(R.id.viewPager) ViewPager viewPager;
}
