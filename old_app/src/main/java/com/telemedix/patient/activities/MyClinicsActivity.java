package com.telemedix.patient.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.telemedix.patient.base.MenuActivity;
import com.telemedix.patient.base.DividerItemDecoration;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.clinic.ClinicActivity;
import com.telemedix.patient.adapters.ClinicsAdapter;
import com.telemedix.patient.models.Clinic;
import com.telemedix.patient.network.ClinicsManager;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyClinicsActivity extends MenuActivity {

    @BindView(R.id.list_view)
    RecyclerView list_view;

    @BindView(R.id.progressLayout)
    View progress;

    private ClinicsAdapter clinicsAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_clinics;
    }

    @Override
    protected void initViews() {
        list_view.setLayoutManager(new LinearLayoutManager(this));
        list_view.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.list_divider)));
        clinicsAdapter = new ClinicsAdapter(this);
        clinicsAdapter.setHasStableIds(true);
        clinicsAdapter.setAdapterListener(this::selectClinic);
        list_view.setAdapter(clinicsAdapter);

        progress.setVisibility(View.VISIBLE);
        ClinicsManager.getClinics()
                .subscribe(pClinics -> {
                    clinicsAdapter.setList(pClinics);
                    progress.setVisibility(View.GONE);
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void selectClinic(Clinic pClinic) {
        Bundle args = new Bundle();
        args.putSerializable("clinic", pClinic);

        Intent intent = new Intent(this, ClinicActivity.class);
        intent.putExtra(ClinicActivity.EXTRA_CLINIC_ID, pClinic.getId());
        intent.putExtras(args);

        startActivity(intent);
    }
}
