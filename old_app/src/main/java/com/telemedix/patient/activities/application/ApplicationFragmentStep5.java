package com.telemedix.patient.activities.application;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.BaseButterKnifeFragment;
import com.telemedix.patient.network.Api;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApplicationFragmentStep5 extends BaseButterKnifeFragment<ApplicationStepsCallback> {

    @BindView(R.id.application_5_allergy_layout)
    ViewGroup allergyContainer;

    @OnClick(R.id.application_5_add_allergy)
    public void onAddAllergyClick() {
        addAllergyRow(null);
    }

    @OnClick(R.id.application_5_continue)
    public void onNext(View v) {
        HashMap<String,ArrayList<String>> data = new HashMap<>();

        ArrayList<String> allergyTo = new ArrayList<>();
        for (int i = 0; i < allergyContainer.getChildCount(); i++) {
            View view = allergyContainer.getChildAt(i);
            EditText title = (EditText) view.findViewById(R.id.medicineName);
            String titleString = title.getText().toString();
            if (TextUtils.isEmpty(titleString)) {
                continue;
            }
            allergyTo.add(titleString);
        }
        data.put("allergies", allergyTo);

        Api.get.setConsultationStep(mCallback.getConsultationId(), ConsultApplicationActivity.STEP_FIVE, data)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o != null) {
                        mCallback.onNextStep(ConsultApplicationActivity.STEP_FIVE);
                    }
                });
    }

    public ApplicationFragmentStep5() {
        mRootViewResource = R.layout.fragment_appilcation_step5;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void initView() {
        super.initView();
        ArrayList<String> allergyTo = ConsultationManager.instance().getAllergyTo();
        if (allergyTo == null) return;
        for (int i = 0; i < allergyTo.size(); i++) {
            addAllergyRow(allergyTo.get(i));
        }
    }

    private void addAllergyRow(String allergy) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.application_medicine_layout, allergyContainer, false);
        allergyContainer.addView(view);
        EditText title = (EditText) view.findViewById(R.id.medicineName);
        Spinner spinner = (Spinner) view.findViewById(R.id.medicine_time);

        title.setText(allergy);
        spinner.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_application_steps, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_continue: {
                onNext(null);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        ArrayList<String> allergyTo = new ArrayList<>();
        for (int i = 0; i < allergyContainer.getChildCount(); i++) {
            View view = allergyContainer.getChildAt(i);
            EditText title = (EditText) view.findViewById(R.id.medicineName);
            String titleString = title.getText().toString();
            if (TextUtils.isEmpty(titleString)) {
                continue;
            }
            allergyTo.add(titleString);
            title.setText(null);
        }
        allergyContainer.removeAllViews();

        ConsultationManager.instance().setAllergyTo(allergyTo);
    }
}
