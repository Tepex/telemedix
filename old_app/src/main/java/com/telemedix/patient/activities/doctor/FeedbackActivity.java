package com.telemedix.patient.activities.doctor;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.util.Log;

import android.view.View;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.bumptech.glide.Glide;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.activities.ConsultationsListActivity;
import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.network.ConsultationApiManager;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import org.joda.time.Period;

import static com.telemedix.patient.Constants.TAG;

public class FeedbackActivity extends BaseActivity
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_doctor_feedback);
		consultation = (Consultation)getIntent().getSerializableExtra(EXTRA_CONSULTATION);
		long elapsed = getIntent().getLongExtra(EXTRA_ELAPSED_CONSULTATION_TIME, -1);
		getSupportActionBar().setTitle(R.string.feedback_title);
		
		Period period = new Period(elapsed);
		String periodStr = getString(R.string.consultation_complete_seconds, period.getSeconds());
		if(period.getMinutes() != 0 && period.getHours() != 0) periodStr = getString(R.string.consultation_complete_minutes, period.getMinutes())+" "+periodStr;
		if(period.getHours() != 0) periodStr = getString(R.string.consultation_complete_hours, period.getHours())+" "+periodStr;
		
		title.setText(getString(
			R.string.consultation_complete,
			consultation.getDoctorShortName(getString(R.string.emergency_info_doctor_name)),
			periodStr));
		
		rate.setRating(consultation.getDoctor().getRating());
		
		if(consultation.getDoctor().getPhoto() != null) Glide.with(this).load(consultation.getDoctor().getPhoto()).crossFade().centerCrop().into(avatar);
	}
	
	@OnClick(R.id.send_feedback)
	public void sendFeedback(View view)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "send feedback");
		String text = editText.getText().toString();
		if(text.isEmpty()) return;
		ConsultationApiManager.consultationFeedback(consultation.getId(), text, Math.round(rate.getRating()))
			.subscribe(nothing->
			{
				onBackPressed();
			});
	}
		
	@Override
	public void onBackPressed()
	{
		Intent intent = new Intent(this, ConsultationsListActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
	}
	
	@BindView(R.id.header) TextView header;
	@BindView(R.id.avatar) ImageView avatar;
	@BindView(R.id.title) TextView title;
	@BindView(R.id.rate) RatingBar rate;
	@BindView(R.id.feedback_text) EditText editText;
	
	private Consultation consultation;
}
