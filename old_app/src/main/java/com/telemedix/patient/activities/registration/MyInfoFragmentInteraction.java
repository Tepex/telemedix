package com.telemedix.patient.activities.registration;

import com.telemedix.patient.models.PatientInfo;

/**
 * Created by Aleksey on 01.11.2016.
 */
public interface MyInfoFragmentInteraction
{
	void onPageSecondSaveClick(PatientInfo pPatientInfo);
	void onPageOneSaveClick(PatientInfo pPatientInfo);
	void onStartProgress();
	void onStopProgress();
}
