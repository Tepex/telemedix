package com.telemedix.patient.activities.application;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.BaseButterKnifeFragment;
import com.telemedix.patient.network.Api;
import com.telemedix.patient.network.request.ConsultationMessage;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApplicationFragmentStep3 extends BaseButterKnifeFragment<ApplicationStepsCallback> {
    @BindView(R.id.application_3_duration_rgroup)
    RadioGroup durationRadioGroup;

    @OnClick(R.id.application_3_continue)
    public void onNext(View v) {
        HashMap<String, String> data = new HashMap<>();

        data.put("symptomDuration", ((RadioButton)durationRadioGroup.findViewById(durationRadioGroup.getCheckedRadioButtonId())).getText().toString());

        Api.get.setConsultationStep(mCallback.getConsultationId(), ConsultApplicationActivity.STEP_THREE, data)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o != null) {
                        mCallback.onNextStep(ConsultApplicationActivity.STEP_THREE);
                    }
                });
    }

    public ApplicationFragmentStep3() {
        mRootViewResource = R.layout.fragment_appilcation_step3;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_application_steps, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_continue:{
                onNext(null);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        ConsultationManager.instance().setDurationPos(durationRadioGroup.getCheckedRadioButtonId());
    }

    @Override
    public void onResume() {
        super.onResume();
        durationRadioGroup.check(ConsultationManager.instance().getDurationPos());
    }
}
