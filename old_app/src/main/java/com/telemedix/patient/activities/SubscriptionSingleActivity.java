package com.telemedix.patient.activities;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.widget.ImageView;
import android.widget.TextView;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.R;
import com.telemedix.patient.models.Subscription;

import butterknife.BindView;

public class SubscriptionSingleActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_subscription_single);
		subscription = getIntent().getParcelableExtra(ARG_SUBSCRIPTION);
		
		image.setImageResource(subscription.image);
		subscription_code.setText(subscription.code);
		subscription_instruction.setText(subscription.description);
		issues_date.setText(subscription.issued_at);
		includes.setText(subscription.includes);
		valid_until.setText(subscription.valid_until);
	}
	
	@BindView(R.id.backdrop) ImageView image;
	@BindView(R.id.subscription_code) TextView subscription_code;
	@BindView(R.id.subscription_instruction) TextView subscription_instruction;
	@BindView(R.id.issued_date) TextView issues_date;
	@BindView(R.id.includes) TextView includes;
	@BindView(R.id.valid_until) TextView valid_until;
	
	private Subscription subscription;
	public static final String ARG_SUBSCRIPTION = "subscription";
}
