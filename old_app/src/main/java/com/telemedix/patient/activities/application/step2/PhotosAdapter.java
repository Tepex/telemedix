package com.telemedix.patient.activities.application.step2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import com.telemedix.patient.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User 1337 on 05.11.2016.
 */

public class PhotosAdapter extends RecyclerView.Adapter<ViewHolderBase> {
    private ArrayList<String> mItems = new ArrayList<>();
    private LayoutInflater inflater;
    private RecyclerView mRecyclerView;

    public PhotosAdapter(ArrayList<String> photos) {
        if (photos != null) {
            mItems.addAll(photos);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
        inflater = LayoutInflater.from(recyclerView.getContext());
    }

    @Override
    public ViewHolderBase onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.item_step2_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolderBase holder, int position) {
        holder.init(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends ViewHolderBase<String> {
        @BindView(R.id.imageView)
        ImageView picture;
        @BindView(R.id.remove)
        ImageView remove;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            remove.setOnClickListener(v -> {
                String path = (String) v.getTag();
                File img = new File(path);
                img.delete();
                int position = mItems.indexOf(path);
                if (position < 0) return;
                mItems.remove(position);
                notifyItemRemoved(position);
                if (mItems.size() < 1) {
                    mRecyclerView.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void init(String item) {
            Glide.with(picture.getContext()).load(new File(item)).into(picture);
            remove.setTag(item);
        }
    }

    public void addPhoto(String path) {
        mItems.add(path);
        notifyItemInserted(mItems.size() - 1);
    }

    public ArrayList<String> getItems() {
        return mItems;
    }
}
