package com.telemedix.patient.activities.security;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;
import com.telemedix.patient.activities.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class InputPINActivity extends AppCompatActivity {

    private Unbinder mUnbinder;

    @BindView(R.id.pin_edit)
    TextView pin;

    @OnClick(R.id.pin_key_0)
    public void on0Click() {
        pin.setText(pin.getText() + "0");
    }

    @OnClick(R.id.pin_key_1)
    public void on1Click() {
        pin.setText(pin.getText() + "1");
    }

    @OnClick(R.id.pin_key_2)
    public void on2Click() {
        pin.setText(pin.getText() + "2");
    }

    @OnClick(R.id.pin_key_3)
    public void on3Click() {
        pin.setText(pin.getText() + "3");
    }

    @OnClick(R.id.pin_key_4)
    public void on4Click() {
        pin.setText(pin.getText() + "4");
    }

    @OnClick(R.id.pin_key_5)
    public void on5Click() {
        pin.setText(pin.getText() + "5");
    }

    @OnClick(R.id.pin_key_6)
    public void on6Click() {
        pin.setText(pin.getText() + "6");
    }

    @OnClick(R.id.pin_key_7)
    public void on7Click() {
        pin.setText(pin.getText() + "7");
    }

    @OnClick(R.id.pin_key_8)
    public void on8Click() {
        pin.setText(pin.getText() + "8");
    }

    @OnClick(R.id.pin_key_9)
    public void on9Click() {
        pin.setText(pin.getText() + "9");
    }

    @OnClick(R.id.pin_key_bckspc)
    public void onBckSpcsClick() {
        if (pin.getText().length() > 0) {
            pin.setText(pin.getText().toString().substring(0, pin.getText().length() - 1));
        }
    }

    @OnClick(R.id.pin_enter)
    public void checkPin() {
        if (Hawk.contains(Constants.PREF_USER_PIN)) {
            if (Hawk.get(Constants.PREF_USER_PIN).equals(pin.getText().toString())) {
                startHomeActivity();
            } else {
                Snackbar.make(pin, "Invalid PIN", Snackbar.LENGTH_SHORT);
                pin.setText("");
            }
        } else {
            startHomeActivity();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_pin);

        mUnbinder = ButterKnife.bind(this);
    }

    private void startHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        if (getIntent().getExtras() != null) intent.putExtras(getIntent().getExtras());
        startActivity(intent);
        finish();
    }
}
