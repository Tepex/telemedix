package com.telemedix.patient.activities.doctor;

import android.os.Bundle;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.annotation.CallSuper;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.base.MenuActivity;

import com.telemedix.patient.base.DividerItemDecoration;

import com.telemedix.patient.R;
import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.adapters.DoctorFilterListAdapter;
import com.telemedix.patient.network.DoctorManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyDoctorsActivity extends MenuActivity
{
	@Override
	protected int getLayoutResId()
	{
		return R.layout.activity_my_doctors;
	}
	
	@Override
	protected void initViews()
	{
		searchView.setHint(getString(R.string.search_hint));
		searchView.showSearch(false);
		searchView.closeSearch();
		
		listView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		listView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.list_divider), 0));
		doctorListAdapter = new DoctorFilterListAdapter(this);
		
		
		doctorListAdapter.setAdapterListener(new DoctorFilterListAdapter.AdapterListener()
		{
			@Override
			public void onDoctorSelected(Doctor doctor)
			{
				selectDoctor(doctor);
			}
			
			@Override
			public void onFilterSelected(int filter) {}
		});
		listView.setAdapter(doctorListAdapter);
		
		progress.setVisibility(View.VISIBLE);
		DoctorManager.getDoctors().subscribe(doctors->
		{
			doctorListAdapter.setList(doctors);
			progress.setVisibility(View.GONE);
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.my_doctors, menu);
		MenuItem item = menu.findItem(R.id.action_search);
		if(item != null) searchView.setMenuItem(item);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		return super.onPrepareOptionsMenu(menu);
	}
	
	@OnClick(R.id.action_up_btn)
	void closeSearch()
	{
		searchView.closeSearch();
	}
	
	@BindView(R.id.list_view) SuperRecyclerView listView;
	@BindView(R.id.search_view) MaterialSearchView searchView;
	@BindView(R.id.progress) ProgressBar progress;
	
	private DoctorFilterListAdapter doctorListAdapter;
}
