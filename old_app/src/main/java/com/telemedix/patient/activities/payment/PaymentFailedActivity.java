package com.telemedix.patient.activities.payment;

import android.content.Intent;
import android.os.Bundle;

import android.widget.TextView;

import android.support.annotation.CallSuper;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.R;
import com.telemedix.patient.activities.HomeActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class PaymentFailedActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_payment_failed);
		sum.setText(""+getIntent().getStringExtra(EXTRA_SUM));
		orderId.setText("" + getIntent().getIntExtra(EXTRA_ORDER, 0));
	}
	
	@OnClick(R.id.toCardInput)
	public void openCardInput()
	{
		onBackPressed();
	}
	
	@Override
	public void onBackPressed()
	{
		finish();
	}
	
	@OnClick(R.id.toMain)
	public void openMain()
	{
		goHome();
	}
	
	public static final String EXTRA_SUM = "extra_sum";
	public static final String EXTRA_ORDER = "extra_order";
	
	@BindView(R.id.pay_status_id) TextView orderId;
	@BindView(R.id.pay_status_sum) TextView sum;
}
