package com.telemedix.patient.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.registration.MyInfoFragmentInteraction;
import com.telemedix.patient.models.PatientInfo;
import com.telemedix.patient.network.PatientManager;
import com.telemedix.patient.network.response.IdNamePair;
import com.telemedix.patient.network.response.StringName;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MyInfoActivitySecondFragment extends Fragment {

    private static final String PATIENT_PARAM = "patient_param";

    Unbinder                  mUnbinder;
    MyInfoFragmentInteraction callback;

    @BindView(R.id.addAllergyBtn)
    Button       addAllergy;
    @BindView(R.id.myInfoSecond_allergies_layout)
    LinearLayout allergyLayout;
    @BindView(R.id.myInfoSecond_chronics_layout)
    LinearLayout chronicsLayout;

    private int allergyRows  = 0;
    private int chronicsRows = 0;
    private PatientInfo patient;

    @OnClick(R.id.addAllergyBtn)
    public void addAllergy() {
        addAllergyRow(null);
    }

    private void addChronicDisease(String text) {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EditText view = (EditText) inflater.inflate(R.layout.patient_allergy_layout, chronicsLayout, false);
        if (text != null) {
            view.setText(text);
        }
        chronicsLayout.addView(view);
        chronicsRows++;
    }

    private void addAllergyRow(String text) {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EditText view = (EditText) inflater.inflate(R.layout.patient_allergy_layout, allergyLayout, false);
        if (text != null) {
            view.setText(text);
        }
        allergyLayout.addView(view);
        allergyRows++;
    }

    @OnClick(R.id.addChronicsBtn)
    public void addChronics() {
        addChronicDisease(null);
    }

    @OnClick(R.id.myInfoSecondPageSaveBtn)
    public void saveBtn(View v) {
        List<StringName> allergies = new LinkedList<>();
        String text;

        for (int i = 0; i < allergyRows; i++) {
            text = ((EditText) allergyLayout.getChildAt(i)).getText().toString();
            if (!text.isEmpty()) {
                StringName name = new StringName(text);
                allergies.add(name);
            }
        }

        List<StringName> diseases = new LinkedList<>();
        for (int i = 0; i < chronicsRows; i++) {
            text = ((EditText) chronicsLayout.getChildAt(i)).getText().toString();
            if (!text.isEmpty()) {
                StringName name = new StringName(text);
                diseases.add(name);
            }
        }

        PatientManager.updateAllergies(allergies)
                      .flatMap(pPatientInfo -> PatientManager.updateChronics(diseases))
                      .subscribe(pPatientInfo -> {
                          callback.onPageSecondSaveClick(pPatientInfo);
                      });
    }

    public static MyInfoActivitySecondFragment newInstance(PatientInfo pPatientInfo) {
        MyInfoActivitySecondFragment mFragment = new MyInfoActivitySecondFragment();
        Bundle args = new Bundle();
        args.putSerializable(PATIENT_PARAM, pPatientInfo);
        mFragment.setArguments(args);
        return mFragment;
    }

    public MyInfoActivitySecondFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_info_secondpage, container, false);
        mUnbinder = ButterKnife.bind(this, root);

        if (patient.getAllergies() != null && patient.getAllergies().size() > 0) {
            for (IdNamePair allergy : patient.getAllergies()) {
                addAllergyRow(allergy.getName());
            }
        } else {
            addAllergyRow(null);
            addAllergyRow(null);
        }

        if (patient.getChronicDiseases() != null && patient.getChronicDiseases().size() > 0) {
            for (IdNamePair disease : patient.getChronicDiseases()) {
                addChronicDisease(disease.getName());
            }
        } else {
            addChronicDisease(null);
            addChronicDisease(null);
        }

        return root;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        callback = (MyInfoFragmentInteraction) context;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            patient = (PatientInfo) getArguments().getSerializable(PATIENT_PARAM);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}
