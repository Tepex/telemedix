package com.telemedix.patient.activities.application;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.BaseButterKnifeFragment;
import com.telemedix.patient.network.Api;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApplicationFragmentStep4 extends BaseButterKnifeFragment<ApplicationStepsCallback> {

    @BindView(R.id.application_4_taken_medicine_layout)
    ViewGroup medicineGroup;

    @OnClick(R.id.application_4_add_medicine)
    public void onAddMedicineClick() {
        addMedicineRow(null, 0);
    }

    @OnClick(R.id.application_4_continue)
    public void onNext(View v) {
        HashMap<String, ArrayList<ConsultationManager.Drug>> data = new HashMap<>();
        ArrayList<ConsultationManager.Drug> drugs = new ArrayList<>();

        for (int i = 0; i < medicineGroup.getChildCount(); i++) {
            View view = medicineGroup.getChildAt(i);
            EditText title = (EditText) view.findViewById(R.id.medicineName);
            Spinner spinner = (Spinner) view.findViewById(R.id.medicine_time);
            String titleString = title.getText().toString();
            if (TextUtils.isEmpty(titleString)) {
                continue;
            }
            ConsultationManager.Drug drug = new ConsultationManager.Drug(titleString, spinner.getSelectedItemPosition());
            drugs.add(drug);
        }

        data.put("drugsIntake", drugs);
        Api.get.setConsultationStep(mCallback.getConsultationId(), ConsultApplicationActivity.STEP_FOUR, data)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o != null) {
                        mCallback.onNextStep(ConsultApplicationActivity.STEP_FOUR);
                    }
                });
    }

    public ApplicationFragmentStep4() {
        mRootViewResource = R.layout.fragment_appilcation_step4;
    }

    @Override
    protected void initView() {
        super.initView();
        ArrayList<ConsultationManager.Drug> drugs = ConsultationManager.instance().getDrugs();
        if (drugs == null) return;
        for (int i = 0; i < drugs.size(); i++) {
            ConsultationManager.Drug drug = drugs.get(i);
            addMedicineRow(drug.name, drug.duration);
        }
    }

    private void addMedicineRow(String titleString, int time) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.application_medicine_layout, medicineGroup, false);
        medicineGroup.addView(view);
        EditText title = (EditText) view.findViewById(R.id.medicineName);
        Spinner spinner = (Spinner) view.findViewById(R.id.medicine_time);

        title.setText(titleString);
        spinner.setSelection(time);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_application_steps, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_continue: {
                onNext(null);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        ArrayList<ConsultationManager.Drug> drugs = new ArrayList<>();
        for (int i = 0; i < medicineGroup.getChildCount(); i++) {
            View view = medicineGroup.getChildAt(i);
            EditText title = (EditText) view.findViewById(R.id.medicineName);
            Spinner spinner = (Spinner) view.findViewById(R.id.medicine_time);
            String titleString = title.getText().toString();
            if (TextUtils.isEmpty(titleString)) {
                continue;
            }
            ConsultationManager.Drug drug = new ConsultationManager.Drug(titleString, spinner.getSelectedItemPosition());
            drugs.add(drug);
            title.setText(null);
            spinner.setSelection(0);
        }
        medicineGroup.removeAllViews();

        ConsultationManager.instance().setDrugs(drugs);
    }
}
