package com.telemedix.patient.activities.application;

/**
 * Created by Aleksey on 25.08.2016.
 */
public interface ApplicationStepsCallback
{
	void onNextStep(int currentStep);
	void onStartProgress();
	void onStopProgress();
	int getConsultationId();
}
