package com.telemedix.patient.activities.payment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.telemedix.patient.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CardInfoFragment extends Fragment {

    private OnCardInfoEntered mListener;
    private View root;

    @BindView(R.id.cardInfoPayBtn)
    Button pay;
    private Unbinder unBinder;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unBinder.unbind();
    }

    @OnClick(R.id.cardInfoPayBtn)
    public void payPressed(View v) {
        mListener.onCardInfoEntered();
    }

    public CardInfoFragment() {
        // Required empty public constructor
    }

    public static CardInfoFragment newInstance() {
        CardInfoFragment fragment = new CardInfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_card_info, container, false);
        unBinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCardInfoEntered) {
            mListener = (OnCardInfoEntered) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCardInfoEntered");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnCardInfoEntered {
        void onCardInfoEntered();
    }
}
