package com.telemedix.patient.activities.call;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.VideoView;

import com.telemedix.patient.R;

public class InCallActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_call);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupActionBar();

        VideoView view = (VideoView) findViewById(R.id.videoView);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.doc_interview;
        view.setVideoURI(Uri.parse(path));
        view.setOnPreparedListener(pMediaPlayer -> pMediaPlayer.setLooping(true));
        view.start();
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Иваненко А.А.");
        getSupportActionBar().setSubtitle("00:02:27");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.incall, menu);

        return super.onCreateOptionsMenu(menu);
    }
}
