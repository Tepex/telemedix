package com.telemedix.patient.activities.registration;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.telemedix.patient.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FullScreenPhotoActivity extends AppCompatActivity {

    public static final String EXTRA_PHOTO_URL = "extra_photo_url";

    @BindView(R.id.image)
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_photo);

        ButterKnife.bind(this);

        Glide.with(this)
                .load(getIntent().getStringExtra(EXTRA_PHOTO_URL))
                .crossFade()
                .into(mImageView);
    }
}
