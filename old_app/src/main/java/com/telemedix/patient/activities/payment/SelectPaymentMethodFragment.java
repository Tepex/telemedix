package com.telemedix.patient.activities.payment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.telemedix.patient.base.DividerItemDecoration;
import com.telemedix.patient.R;
import com.telemedix.patient.adapters.PaymentListAdapter;
import com.telemedix.patient.models.Payment;
import com.telemedix.patient.models.PaymentType;
import com.telemedix.patient.models.payment.CardInfo;
import com.telemedix.patient.network.PatientManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SelectPaymentMethodFragment extends Fragment {

    private OnPaymentMethodChoose mListener;

    private PaymentListAdapter paymentListAdapter;

    View root;

    @BindView(R.id.list_view)
    SuperRecyclerView list_view;
    private Unbinder unBinder;

    public SelectPaymentMethodFragment() {
        // Required empty public constructor
    }

    public static SelectPaymentMethodFragment newInstance() {
        SelectPaymentMethodFragment fragment = new SelectPaymentMethodFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_select_payment_method, container, false);
        unBinder = ButterKnife.bind(this, root);

        list_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        list_view.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity(), R.drawable.list_divider)));

        paymentListAdapter = new PaymentListAdapter(getActivity(), false);
        paymentListAdapter.setHasStableIds(true);

        List<Payment> paymentList = new ArrayList<>();

        PatientManager.getCards().subscribe(
                pCardInfos -> {

                    for (CardInfo mCardInfo : pCardInfos) {
                        paymentList.add(new Payment(String.format(getResources().getString(R.string.bank_card), mCardInfo.getPan()),
                                                    String.format(getResources().getString(R.string.bank_card_valid_until), mCardInfo.getExpiration()),
                                                    R.drawable.visa_icon,
                                                    PaymentType.EXISTING_CARD,
                                                    mCardInfo.getId()));
                    }
//
//                    paymentList.add(new Payment(String.format(getResources().getString(R.string.bank_card), " *********** 1021"),
//                                                String.format(getResources().getString(R.string.bank_card_valid_until), " 11/18"),
//                                                R.drawable.visa_icon,
//                                                PaymentType.EXISTING_CARD));
                    paymentList.add(new Payment(String.format(getResources().getString(R.string.bank_card), " Visa/MasterCard"),
                                                "",
                                                R.drawable.all_cards,
                                                PaymentType.NEW_CARD,
                                                0));
//                    paymentList.add(new Payment(getResources().getString(R.string.account) + " Google Market",
//                                                "i.ivanov@gmail.com",
//                                                R.drawable.google_play_icon,
//                                                PaymentType.GOOGLE_ACCOUNT));
//                    paymentList.add(new Payment(getResources().getString(R.string.input_promo),
//                                                "",
//                                                R.drawable.payment_promocode,
//                                                PaymentType.PROMO));
//
                    this.paymentListAdapter.setList(paymentList);
                    paymentListAdapter.setAdapterListener(payment -> mListener.onChoose(payment.type, payment.cardId));
                    list_view.setAdapter(paymentListAdapter);
                });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unBinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPaymentMethodChoose) {
            mListener = (OnPaymentMethodChoose) context;
        } else {
            throw new RuntimeException(context.toString()
                                               + " must implement OnPaymentMethodChoose");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnPaymentMethodChoose {
        void onChoose(PaymentType type, int pCardId);
    }
}
