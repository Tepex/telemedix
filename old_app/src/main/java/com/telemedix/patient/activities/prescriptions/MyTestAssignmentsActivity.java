package com.telemedix.patient.activities.prescriptions;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.telemedix.patient.R;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyTestAssignmentsActivity extends AppCompatActivity {


    @BindView(R.id.test_assignments_list)
    RecyclerView list;

    TestAssignmentsAdapter mAdapter;


    List<String> mItems; //TODO remove fake

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_test_assignments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_test_assignments_title);

        ButterKnife.bind(this);

        String[] analysisGroups = getResources().getStringArray(R.array.analysis_groups);
        mItems = Arrays.asList(analysisGroups);
        list.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new TestAssignmentsAdapter(mItems, this);
        list.setAdapter(mAdapter);
    }

}
