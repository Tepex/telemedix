package com.telemedix.patient.activities.application;

import android.annotation.SuppressLint;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.patient.activities.BaseButterKnifeFragment;
import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.R;

public class ApplicationFragmentStep8 extends BaseButterKnifeFragment<ApplicationStepsCallback>
{
	@CallSuper
	@Override
	protected void initView()
	{
		super.initView();
		if(consultation.getDoctorService() != null) cost.setText(consultation.getDoctorService().getPriceString(getActivity()));
		if(!consultation.isInstant())
		{
			date.setText(consultation.getSlot().getDateTimeString());
			captionTextView.setText(R.string.online_consultation_title);
			proceedBtn.setText(R.string.proceed_to_consultations);
		}
		else
		{
			captionTextView.setText(R.string.application_step_8_description_instant);
			proceedBtn.setText(R.string.proceed_to_consultations_instant);
		}
		if(consultation.getDoctor() != null) doctorName.setText(consultation.getDoctor().getShortName());
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mRootViewResource = R.layout.fragment_appilcation_step8;
		consultation = (Consultation)getArguments().getSerializable(ConsultApplicationActivity.EXTRA_CONSULTATION);
		setHasOptionsMenu(true);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.menu_application_steps, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == R.id.action_continue) onNext(null);
		return super.onOptionsItemSelected(item);
	}
	
	@OnClick(R.id.application_8_continue)
	public void onNext(View v)
	{
		mCallback.onNextStep(ConsultApplicationActivity.STEP_EIGHT);
	}
	
	@BindView(R.id.doctor_name) TextView doctorName;
	@BindView(R.id.application_description_caption) TextView captionTextView;
	@BindView(R.id.emergency_summmary_consultation_cost) TextView cost;
	@BindView(R.id.consultation_date) TextView date;
	@BindView(R.id.application_8_continue) Button proceedBtn;
	
	private Consultation consultation;
}
