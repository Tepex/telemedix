package com.telemedix.patient.activities.prescriptions;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telemedix.patient.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aleksey on 30.08.2016.
 */
public class ResearchesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> items;
    private Context mContext;

    public ResearchesAdapter(List<String> pItems, Context pContext) {
        items = pItems;
        mContext = pContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_research, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ItemViewHolder itemView = (ItemViewHolder) holder;
        if (position == 0) {
            itemView.newFrame.setVisibility(View.VISIBLE);
        } else {
            itemView.newFrame.setVisibility(View.INVISIBLE);
        }

        itemView.type.setText("УЗИ брюшной полости");
        itemView.date.setText("26.07.2016");
        itemView.doctor.setText("Иванова А.А");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_new_frame)
        View newFrame;
        @BindView(R.id.research_type)
        TextView type;
        @BindView(R.id.research_date)
        TextView date;
        @BindView(R.id.research_doctor)
        TextView doctor;

        public ItemViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setItems(List<String> pItems) {
        items = pItems;
        notifyDataSetChanged();
    }
}
