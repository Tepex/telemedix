package com.telemedix.patient.activities.doctor.fragments;

import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.telemedix.patient.base.DividerItemDecoration;

import com.telemedix.patient.activities.ConsultationRequestActivity;

import com.telemedix.patient.activities.doctor.DoctorSingleActivity;
import com.telemedix.patient.activities.doctor.FeedbackActivity;

import com.telemedix.patient.adapters.ScheduleAdapter;

import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.DoctorService;
import com.telemedix.patient.models.Schedule;
import com.telemedix.patient.models.Slot;

import com.telemedix.patient.network.DoctorManager;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.LocalDate;

import static com.telemedix.patient.Constants.TAG;

public class ScheduleFragment extends Fragment
{
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_doctor_schedule, container, false);
		ButterKnife.bind(this, rootView);
		doctor = (Doctor)getArguments().getSerializable(DoctorSingleActivity.ARG_DOCTOR);
		serviceId = getArguments().getInt(ARG_SERVICE_ID);
		
		scheduleList.setLayoutManager(new LinearLayoutManager(getActivity()));
		scheduleList.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity(), R.drawable.list_divider), 0));
		scheduleAdapter = new ScheduleAdapter(getActivity());
		scheduleAdapter.setAdapterListener(new ScheduleAdapter.AdapterListener()
		{
			@Override
			public void onItemSelected(Schedule schedule)
			{
				showScheduleDialog(schedule);
			}
			
			@Override
			public void onFooterSelected()
			{
				if(slots.size() == 0) return;
				
				DatePickerDialog dpd = DatePickerDialog.newInstance(
					(view, year, monthOfYear, dayOfMonth)->
					{
						LocalDate day = new LocalDate(year, monthOfYear+1, dayOfMonth);
						List<Slot> slotsForDay = new LinkedList<>();
						for(Slot slot: slots) if(day.equals(slot.getDate())) slotsForDay.add(slot);
						showScheduleDialog(new Schedule(day.toString("dd.MM"), slotsForDay));
					},
					Utils.NOW.getYear(),
					Utils.NOW.getMonthOfYear()-1,
					Utils.NOW.getDayOfMonth());
				dpd.setSelectableDays(Utils.getSeparateDays(slots));
				dpd.show(getActivity().getFragmentManager(), TAG_DATE_PICKER_DIALOG);
			}
		});
		scheduleList.setAdapter(scheduleAdapter);
		
		if(doctor.isOnline()) nowBtn.setVisibility(View.VISIBLE);
		else nowBtn.setVisibility(View.GONE);
		nowBtn.setOnClickListener(view->startConfirmConsultation(null, -1));
		
		if(serviceId != 0)
		{
			// yesterday, next month
			DoctorManager.getDoctorReceptionSlots(serviceId, Utils.NOW.minusDays(1).toString("yyyy-MM-dd"), Utils.NOW.plusMonths(1).toString("yyyy-MM-dd"))
				.subscribe(slots->
				{
					Collections.sort(slots);
					ScheduleFragment.this.slots = slots;
					
					//for(Slot slot: ScheduleFragment.this.slots) Log.d(TAG, slot.toString());
					
					List<Schedule> scheduleList = new ArrayList<>();
					if(slots.size() > 0)
					{
						scheduleList.add(Utils.createToday(this, ScheduleFragment.this.slots));
						scheduleList.add(Utils.createTomorrow(this, ScheduleFragment.this.slots));
						scheduleList.add(Utils.createAfterTomorrow(this, ScheduleFragment.this.slots));
					}
					chooseConsultTextView.setVisibility((slots.size() == 0) ? View.GONE : View.VISIBLE);
					scheduleAdapter.setList(scheduleList, slots.size() == 0);
				});
			empty.setVisibility(View.GONE);
			nonEmpty.setVisibility(View.VISIBLE);
		}
		else
		{
			empty.setVisibility(View.VISIBLE);
			nonEmpty.setVisibility(View.GONE);
		}
		
		return rootView;
	}
	
	private void showScheduleDialog(final Schedule schedule)
	{
		if(schedule.getIntervalsAsString().length == 0) return;
		new AlertDialog.Builder(getActivity())
			.setTitle(R.string.doctor_info_choose_consult_datetime)
			.setItems(schedule.getIntervalsAsString(), (di, i)->startConfirmConsultation(di, schedule.getIntervals().get(i).getId()))
			.show();
	}
	
	private void startConfirmConsultation(final DialogInterface di, final int slotId)
	{
		if(di != null) di.dismiss();
		Intent intent = new Intent(getActivity(), ConsultationRequestActivity.class);
		Bundle args = new Bundle();
		if(slotId != -1)
		{
			args.putInt(ConsultationRequestActivity.CONSULTATION_TYPE, ConsultationRequestActivity.ACTION_CONSULTATION_TYPE_SCHEDULED);
			args.putInt(ConsultationRequestActivity.SERVICE_ID, serviceId);
			args.putInt(ConsultationRequestActivity.SLOT_ID, slotId);
		}
		else
		{
			args.putInt(ConsultationRequestActivity.CONSULTATION_TYPE, ConsultationRequestActivity.ACTION_CONSULTATION_TYPE_EMERGENCY);
			args.putSerializable(DoctorSingleActivity.ARG_DOCTOR, doctor);
		}
		intent.putExtras(args);
		startActivity(intent);
	}
	
	@BindView(R.id.schedule_now_btn) Button nowBtn;
	@BindView(R.id.schedule_list) RecyclerView scheduleList;
	@BindView(R.id.list) View nonEmpty;
	@BindView(R.id.emptyListText) View empty;
	@BindView(R.id.schedule_choose_consult_datetime) TextView chooseConsultTextView; 
	
	private ScheduleAdapter scheduleAdapter;
	private Doctor doctor;
	private int serviceId;
	private List<Slot> slots;

	public static final String ARG_SERVICE_ID = "serviceId";
	public static final String TAG_DATE_PICKER_DIALOG = "datePickerDialog";
}
