package com.telemedix.patient.activities.payment;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.util.Log;

import com.telemedix.patient.App;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.activities.application.ConsultApplicationActivity;
import com.telemedix.patient.dialogs.PaymentDialog;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.PaymentType;

import com.telemedix.patient.R;
import com.telemedix.patient.BuildConfig;

import static com.telemedix.patient.Constants.TAG;

public class SelectPaymentListActivity extends BaseActivity
									   implements SelectPaymentMethodFragment.OnPaymentMethodChoose,
												  CardInfoFragment.OnCardInfoEntered,
												  PaymentDialog.DialogClickListener,
												  PaymentWebViewFragment.OnCardInfoEntered
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_select_payment_methods_list);
		getSupportFragmentManager().beginTransaction().replace(R.id.container, SelectPaymentMethodFragment.newInstance()).commit();
        
		consultation = (Consultation)getIntent().getSerializableExtra(ConsultApplicationActivity.EXTRA_CONSULTATION);
		if(BuildConfig.DEBUG) Log.d(TAG, getClass().getName()+" doctor: "+consultation.getDoctor());
		if(getIntent().getBooleanExtra(SKIP_PAYMENT_METHOD_CHOSE, false)) onChoose(PaymentType.NEW_CARD, 0);
		
		
		//if(App.getApp().isDevMode()) onSuccessClick();
		onSuccessClick();
	}
	
	@Override
	public void onChoose(final PaymentType type, final int cardId)
	{
		if(type == PaymentType.NEW_CARD) getSupportFragmentManager()
			.beginTransaction()
//			.replace(R.id.container, CardInfoFragment.newInstance())
			.replace(R.id.container, PaymentWebViewFragment.newInstance(consultation.getId()))
			.addToBackStack(null)
			.commit();
		else if(type == PaymentType.EXISTING_CARD) getSupportFragmentManager()
			.beginTransaction()
//			.replace(R.id.container, CardInfoFragment.newInstance())
			.replace(R.id.container, PaymentWebViewFragment.newInstance(consultation.getId(), cardId))
			.addToBackStack(null)
			.commit();
		else onSuccessClick();
	}
	
	@Override
	public void onCardInfoEntered()
	{
		runOnUiThread(()->PaymentDialog.getSuccessDialog(SelectPaymentListActivity.this, SelectPaymentListActivity.this).show());
	}
	
	@Override
	public void onSuccessClick()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "SelectPaymentlistActivity.onSuccessClick doctor:"+consultation.getDoctor());
		Intent intent = new Intent(this, ConsultApplicationActivity.class);
		intent.putExtra(ConsultApplicationActivity.EXTRA_CONSULTATION, consultation);
		startActivity(intent);
		finish();
	}
	
	@Override
	public void onFailClick()
	{
		finish();
	}
	
	public final static String ACTION = "action";
	public final static String SKIP_PAYMENT_METHOD_CHOSE = "skip_payment_method_chosing";
	
	public final static int ACTION_INSURANCE = 1;
	public final static int ACTION_EMERGENCY = 2;
	public final static int ACTION_ROUTINE = 3;
	
	private Consultation consultation;
}
