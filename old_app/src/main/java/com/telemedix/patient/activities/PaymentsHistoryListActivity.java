package com.telemedix.patient.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.base.MenuActivity;
import com.telemedix.patient.R;
import com.telemedix.patient.adapters.PaymentHistoryTabAdapter;
import com.telemedix.patient.fragments.PaymentHistoryListFragment;
import com.telemedix.patient.network.PaymentManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentsHistoryListActivity extends MenuActivity {

    @BindView(R.id.progressLayout)
    View progress;

    private static final String PAYMENT_HISTORY_FRAGMENT = "paymentHistoryFragment";

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_payment_history_list;
    }

    @Override
    protected void initViews() {
        progress.setVisibility(View.VISIBLE);
        PaymentManager.getPayments()
                .subscribe(pPaymentInfos -> {
                    initTabs();
                    progress.setVisibility(View.GONE);
                });
    }

    private void initTabs() {
        Bundle args = new Bundle();
        args.putInt("type", PaymentHistoryListFragment.TYPE.ALL);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.flPaymentsHistory, PaymentHistoryListFragment.
                        newInstance(args), PAYMENT_HISTORY_FRAGMENT)
                .commit();
    }
}
