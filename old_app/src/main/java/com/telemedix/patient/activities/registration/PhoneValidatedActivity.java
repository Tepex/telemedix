package com.telemedix.patient.activities.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

//import com.telemedix.mvp.tasks.user.ValidateCodeFragment;

import com.telemedix.patient.Constants;
import com.telemedix.patient.R;
import com.telemedix.patient.activities.ContactActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhoneValidatedActivity extends AppCompatActivity {

    public static final String EXTRA_USER_PHONE = "user_phone";

    @BindView(R.id.validate_phone_caption)
    TextView phoneValidated;
    @BindView(R.id.new_pin_1)
    EditText pin1;
    @BindView(R.id.new_pin_2)
    EditText pin2;

    @OnClick(R.id.skip_step)
    public void nextStep() {
        savePin();
    }

    @OnClick(R.id.pin_save_button)
    public void savePin() {
        if (!pin1.getText().toString().isEmpty()) {
            if (!pin1.getText().toString().equals(pin2.getText().toString())) {
                pin1.setText("");
                pin2.setText("");
                Snackbar.make(pin2, getResources().getString(R.string.pins_mismatch), Snackbar.LENGTH_SHORT).show();
                return;
            }

            Hawk.put(Constants.PREF_USER_PIN, pin1.getText().toString());
        }

        Intent mIntent = new Intent(this, ContactActivity.class);
        mIntent.putExtra(ContactActivity.EXTRA_NEW_CONTACT, true);
        startActivity(mIntent);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_validated);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_register);

        ButterKnife.bind(this);
/*
        phoneValidated.setText(String.format(getResources().getString(R.string.phone_validate_caption),
                getIntent().getStringExtra(ValidateCodeFragment.ARGS_USER_PHONE)));*/
    }

}
