package com.telemedix.patient.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.orhanobut.hawk.Hawk;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;
import com.telemedix.patient.activities.registration.FullScreenPhotoActivity;
import com.telemedix.patient.activities.registration.MyInfoFragmentInteraction;
import com.telemedix.patient.models.BloodType;
import com.telemedix.patient.models.PatientInfo;
import com.telemedix.patient.network.Api;
import com.telemedix.patient.network.PatientManager;
import com.telemedix.patient.utils.DateHelper;
import com.telemedix.patient.views.ButtonNumberPicker;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;


public class MyInfoActivityFragment extends Fragment {

    private final int SELECT_PHOTO = 1;

    private final static String EXTRA_NEW_CONTACT = "new_contact";
    private final static String EXTRA_PATIENT     = "patient";

    Unbinder                  mUnbinder;
    MyInfoFragmentInteraction callback;
    boolean isEdited = false;

    @BindView(R.id.myInfoSaveBtn)
    Button             save;
    @BindView(R.id.myInfoBirthText)
    TextView           birth;
    @BindView(R.id.myInfoHeight)
    ButtonNumberPicker heightPicker;
    @BindView(R.id.myInfoWeight)
    ButtonNumberPicker weightPicker;
    //    @BindView(R.id.myInfoBloodType)
//    SpinnerWithCaption bloodType;
    @BindView(R.id.myInfoSexGroup)
    RadioGroup         sexGroup;
    @BindView(R.id.myInfoSexMale)
    RadioButton        male;
    @BindView(R.id.myInfoSexFemale)
    RadioButton        female;
    @BindView(R.id.myInfoAvatarLayout)
    View               avatarLayout;
    @BindView(R.id.myInfoAvatar)
    ImageView          avatar;

    private PatientInfo patient;

    @OnClick(R.id.myInfoAvatar)
    public void openAvatarFullScreen() {
        if (patient.isValid() &&
                patient.getUser().getPhoto() != null &&
                !patient.getUser().getPhoto().isEmpty()) {
            Intent photoIntent = new Intent(getActivity(), FullScreenPhotoActivity.class);
            photoIntent.putExtra(FullScreenPhotoActivity.EXTRA_PHOTO_URL, patient.getUser().getFullPhoto());
            startActivity(photoIntent);
        }
    }

    @OnClick(R.id.upload_photo_btn)
    public void openAvatarChoser() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @OnClick(R.id.myInfoSaveBtn)
    public void openNextPage(View v) {
        callback.onStartProgress();
        Api.get.setCurrentPatientMetrics(Integer.parseInt(heightPicker.getValue()),
                                         Integer.parseInt(weightPicker.getValue()),
                                         BloodType.FOUR_MINUS,
                                         sexGroup.getCheckedRadioButtonId() == R.id.myInfoSexMale ? "male" : "female",
                                         birth.getText().toString())
               .subscribe(pPatientInfo -> {
                              callback.onStopProgress();
                              callback.onPageOneSaveClick(pPatientInfo);
                          },
                          pThrowable -> {
                              callback.onStopProgress();
                              callback.onPageOneSaveClick(patient);
                          });

    }

    @OnClick(R.id.myInfoBirthText)
    public void setBirthDate(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setCallback(pDate -> {
            birth.setText(pDate);
            setEdited(true);
        });
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    private boolean newContact;

    public static MyInfoActivityFragment newInstance(boolean newContact, PatientInfo pPatientInfo) {
        MyInfoActivityFragment mFragment = new MyInfoActivityFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_PATIENT, pPatientInfo);
        args.putBoolean(EXTRA_NEW_CONTACT, newContact);
        mFragment.setArguments(args);
        return mFragment;
    }

    public MyInfoActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            patient = (PatientInfo) getArguments().getSerializable(EXTRA_PATIENT);
            newContact = getArguments().getBoolean(EXTRA_NEW_CONTACT);
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        OnBirthDateSet mCallback;

        public void setCallback(OnBirthDateSet pCallback) {
            mCallback = pCallback;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (mCallback != null) {
                mCallback.onSet(day + "." + month + "." + year);
            }
        }
    }

    interface OnBirthDateSet {
        void onSet(String pDate);
    }

    private void setEdited(boolean value) {
        isEdited = value;
        if (value) {
            save.setText(getResources().getString(R.string.my_info_save));
        } else {
            save.setText(getResources().getString(R.string.my_info_fill_late));
        }
    }

    class EditedListener implements View.OnClickListener,
                                    RadioGroup.OnCheckedChangeListener {

        @Override
        public void onClick(final View pView) {
            setEdited(true);
        }

        @Override
        public void onCheckedChanged(final RadioGroup pRadioGroup, final int pI) {
            setEdited(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_info, container, false);
        mUnbinder = ButterKnife.bind(this, root);

//        setData();

        heightPicker.setClickListener(new EditedListener());
        weightPicker.setClickListener(new EditedListener());
//        bloodType.setClickListener(new EditedListener());
        sexGroup.setOnCheckedChangeListener(new EditedListener());

        setEdited(false);

        initViews();

        return root;
    }

    private void initViews() {
        callback.onStartProgress();
        PatientManager.getUserInfo()
                      .subscribe(pPatientInfo -> {
                          patient = pPatientInfo;

                          if (pPatientInfo.isValid()) {
                              Glide.with(getContext())
                                   .load(pPatientInfo.getUser().getPhoto())
                                   .placeholder(R.drawable.no_photo_placeholder)
                                   .into(avatar);

                              birth.setText(DateHelper.getShortDate(pPatientInfo.getUser().getBirthDate()));

                              heightPicker.setValue("" + pPatientInfo.getHeight());
                              weightPicker.setValue("" + pPatientInfo.getWeight());

                              if (pPatientInfo.getUser().getSex() != null) {
                                  if (pPatientInfo.getUser().getSex().contains("male")) {
                                      male.setChecked(true);
                                      female.setChecked(false);
                                  } else {
                                      male.setChecked(false);
                                      female.setChecked(true);
                                  }
                              }
                          }
                          callback.onStopProgress();
                      });
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    final Uri imageUri = data.getData();
                    PatientManager.uploadMyPhoto(getContext(), imageUri)
                                  .subscribe(pPatientInfo -> {
                                      patient = pPatientInfo;
                                      Glide.with(getContext())
                                           .load(imageUri)
                                           .into(avatar);
//                               Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
                                  });
                }
        }
    }

    @Override
    public void onAttachFragment(final Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        callback = (MyInfoFragmentInteraction) context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    private void setData() {
        birth.setText(Hawk.get(Constants.PREF_USER_BIRTHDAY, "01.02.1980"));
        heightPicker.setValue(Hawk.get(Constants.PREF_USER_HEIGHT, "170"));
        weightPicker.setValue(Hawk.get(Constants.PREF_USER_WEIGHT, "70"));
//        bloodType.setValue(Hawk.get(Constants.PREF_USER_BLOOD_TYPE, "I(0)"));
        if (Hawk.contains(Constants.PREF_USER_SEX)) {
            if (Hawk.get(Constants.PREF_USER_SEX, false)) {
                male.setChecked(true);
                female.setChecked(false);
            } else {
                female.setChecked(true);
                male.setChecked(false);
            }
        }
    }
}