package com.telemedix.patient.activities.doctor;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.v4.content.ContextCompat;

import android.support.v7.widget.LinearLayoutManager;

import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import com.telemedix.patient.App;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.base.DividerItemDecoration;

import com.telemedix.patient.adapters.DoctorFilterListAdapter;
import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.DoctorsPool;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import java.util.List;

import static com.telemedix.patient.Constants.TAG;

/**
 * Список докторов. Всех, по фильтру или по поиску.
 */
public class FindDoctorsActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_find_doctors);
		init(getIntent());
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode != RESULT_FILTER) return;
		if(resultCode == FilterActivity.RESULT_OK) init(data);
		else init(null);
	}
    
	@Override
	@CallSuper
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.find_doctors, menu);
		MenuItem item = menu.findItem(R.id.action_search);
		if(item != null)
		{
			searchView.setMenuItem(item);
			searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener()
			{
				@Override
				public boolean onQueryTextSubmit(final String query)
				{
					/* Backdoor for developers */
					if(Utils.processCommand(query))
					{
						Toast.makeText(FindDoctorsActivity.this, "Developer mode: "+App.getApp().isDevMode(), Toast.LENGTH_LONG).show();
						return true;
					}
					return false;
				}
				
				@Override
				public boolean onQueryTextChange(final String newText)
				{
					if(newText.isEmpty()) loadData();
					else loadData(DoctorsPool.get.searchDoctors(newText));
					return true;
				}
			});
		}
		return super.onCreateOptionsMenu(menu);
	}
    
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				finish();
				return true;
			case R.id.action_sort_price:
				if(lastSort != R.id.action_sort_price) ordering = true;
				else ordering = !ordering;
				lastSort = R.id.action_sort_price;
				DoctorsPool.get
					.getDoctorsObservable(currAction, searchString, DoctorsPool.SORT_PRICE, ordering)
					.subscribe(this::loadData);
				break;
			case R.id.action_sort_by_rating:
				if(lastSort != R.id.action_sort_by_rating) ordering = false;
				else ordering = !ordering;
				lastSort = R.id.action_sort_by_rating;
				DoctorsPool.get
					.getDoctorsObservable(currAction, searchString, DoctorsPool.SORT_RATING, ordering)
					.subscribe(this::loadData);
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	@CallSuper
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		if(menu.findItem(R.id.action_sort) != null)
		{
			menu.findItem(R.id.action_sort).setVisible(currAction != FilterActivity.EXTRA_ACTION_FIND_ALL);
			menu.findItem(R.id.action_sort).getSubMenu().getItem(0).setChecked(true);
		}
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	@CallSuper
	public void onBackPressed()
	{
		if(searchView.isSearchOpen() && currAction != FilterActivity.EXTRA_ACTION_FIND_ALL) searchView.closeSearch();
		else super.onBackPressed();
	}
	
	private void init(Intent intent)
	{
		currAction = FilterActivity.EXTRA_ACTION_FIND_ALL;
		if(intent != null)
		{
			currAction = intent.getIntExtra(FilterActivity.EXTRA_ACTION, currAction);
			searchString = intent.getStringExtra(FilterActivity.EXTRA_ACTION_ARG);
		}

		if(currAction == FilterActivity.EXTRA_ACTION_FIND_ALL) searchString = getString(R.string.all_doctors);
		if(searchString != null && !searchString.isEmpty()) getSupportActionBar().setTitle(searchString);
		
		listView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		listView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.list_divider), 1));
		
		doctorFilterListAdapter = new DoctorFilterListAdapter(this, currAction, searchString);
		doctorFilterListAdapter.setAdapterListener(new DoctorFilterListAdapter.AdapterListener()
		{
			@Override
			public void onFilterSelected(int filter)
			{
				Intent intent = new Intent(FindDoctorsActivity.this, FilterActivity.class);
				intent.putExtra(FilterActivity.EXTRA_ACTION, filter);
				startActivityForResult(intent, RESULT_FILTER);
			}
			
			@Override
			public void onDoctorSelected(Doctor doctor)
			{
				selectDoctor(doctor);
			}
		});
		listView.setAdapter(doctorFilterListAdapter);
		loadData();
	}
	
	private void loadData()
	{
		progress.setVisibility(View.VISIBLE);
		DoctorsPool.get.getDoctorsObservable(currAction, searchString).subscribe(this::loadData);
	}
	
	private void loadData(List<Doctor> list)
	{
		doctorFilterListAdapter.setList(list);
		progress.setVisibility(View.GONE);
	}
	
	private int currAction;
	private String searchString;
	private DoctorFilterListAdapter doctorFilterListAdapter;
    private int lastSort;
    private boolean ordering;
   
	private static final int RESULT_FILTER = 0x1;
	
	@BindView(R.id.list_view) SuperRecyclerView listView;
	@BindView(R.id.search_view) MaterialSearchView searchView;
	@BindView(R.id.progressLayout) View progress;
}
