package com.telemedix.patient.activities.application.step2;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by User 1337 on 05.11.2016.
 */

public abstract class ViewHolderBase<I> extends RecyclerView.ViewHolder {
    public ViewHolderBase(View itemView) {
        super(itemView);
    }

    public abstract void init(I item);
}
