package com.telemedix.patient.activities.application.step2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.telemedix.patient.R;

import com.telemedix.patient.activities.BaseButterKnifeFragment;

import com.telemedix.patient.activities.application.ApplicationStepsCallback;
import com.telemedix.patient.activities.application.ConsultApplicationActivity;
import com.telemedix.patient.activities.application.ConsultationManager;

import com.telemedix.patient.network.Api;
import com.telemedix.patient.network.request.ConsultationMessage;

import com.telemedix.patient.utils.BitmapUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApplicationFragmentStep2 extends BaseButterKnifeFragment<ApplicationStepsCallback> {
    @BindView(R.id.application_description)
    TextView description;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private PhotosAdapter adapter;

    @OnClick(R.id.application_2_continue)
    public void onNext(View v) {
        HashMap<String, String> data = new HashMap<>();
        data.put("message", description.getText().toString());
        Api.get.setConsultationStep(mCallback.getConsultationId(), ConsultApplicationActivity.STEP_TWO, new ConsultationMessage(description.getText().toString()))
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o != null) {
                        mCallback.onNextStep(ConsultApplicationActivity.STEP_TWO);
                    }
                });
    }

    @OnClick(R.id.application_photo_gallery)
    public void onGalleryClick(View v) {
        Uri uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
        Intent intent = new Intent(Intent.ACTION_PICK, uri);
        intent.setType("image/*");
        getActivity().startActivityForResult(intent, ConsultApplicationActivity.CAPTURE_GALERY_REQUEST_CODE);
    }

    @OnClick(R.id.application_photo_camera)
    public void onPhotoClick(View v) {
        if (Build.MODEL.contains("Nexus")) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri fileUri = BitmapUtils.getOutputMediaFile(BitmapUtils.MEDIA_TYPE_PICTURE); // create a file to save the image
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
            getActivity().startActivityForResult(intent, ConsultApplicationActivity.CAPTURE_PHOTO_REQUEST_CODE);
        } else {
            getActivity().startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), ConsultApplicationActivity.CAPTURE_PHOTO_REQUEST_CODE);
        }
    }

    public ApplicationFragmentStep2() {
        mRootViewResource = R.layout.fragment_appilcation_step2;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_application_steps, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_continue: {
                onNext(null);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initView() {
        super.initView();
        adapter = new PhotosAdapter(ConsultationManager.instance().getPhotos());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(adapter.getItemCount() > 0 ? View.VISIBLE : View.GONE);
    }

    public void processImage(Intent data) {
        Bitmap bitmap = null;
        if (data == null) return;

        Uri imageUri = data.getData();

        if (imageUri == null) {
            bitmap = (Bitmap) data.getExtras().get("data");
        } else {
            try {
                bitmap = BitmapUtils.getRotatedBitmap(getActivity().getContentResolver(), imageUri);
                if (android.os.Build.MODEL.contains("Nexus") && bitmap != null) {
                    ExifInterface exif = new ExifInterface(imageUri.toString());
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    bitmap = BitmapUtils.rotateBitmapNexus(bitmap, orientation);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (bitmap == null) return;
        File cachePath;
        File internalDir = new File(getActivity().getExternalCacheDir().getAbsolutePath());

        if (!internalDir.exists()) {
            internalDir.mkdirs();
        }
        cachePath = new File(internalDir, "/" + System.currentTimeMillis() + ".jpg");

        try {
            cachePath.createNewFile();
            FileOutputStream ostream = new FileOutputStream(cachePath);

            double width = ((double) bitmap.getWidth() / bitmap.getHeight()) * 240;
            double height = ((double) bitmap.getHeight() / bitmap.getWidth()) * 320;

            Bitmap resizedBitmap;
            if (width < 320) {
                resizedBitmap = Bitmap.createScaledBitmap(bitmap, 320, (int) height, false);
            } else {
                resizedBitmap = Bitmap.createScaledBitmap(bitmap, (int) width, 240, false);
            }

            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);

            ostream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String path = cachePath.getAbsolutePath();
        adapter.addPhoto(path);

        Api.get.uploadConsultationImage(mCallback.getConsultationId(), path)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
        if (adapter.getItemCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ConsultationManager.instance().setPhotos(adapter.getItems());
    }
}
