package com.telemedix.patient.activities.payment;

import android.content.Context;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;
import com.telemedix.patient.dialogs.PaymentDialog;
import com.telemedix.patient.models.payment.PaymentInfo;
import com.telemedix.patient.models.payment.PaymentInfoWithoutCardInfo;
import com.telemedix.patient.network.PatientManager;
import com.telemedix.patient.network.PaymentManager;

import com.trello.rxlifecycle2.components.support.RxFragment;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PaymentWebViewFragment extends RxFragment {

    private final static String SUCCESS_URL = "http://merchant.telemedix.com/product/purchase/success";
    private final static String FAIL_URL = "http://merchant.telemedix.com/product/purchase/error";

    //    private final static String FINISH_URL = "http://merchant.telemedix.com/api/payment/finish";
    private final static String FINISH_URL = "/api/payment/finish";

    private static final String PARAM_CONSULTATION_ID = "ConsultationId";
    private static final String PARAM_CARD_ID = "cardId";

    private OnCardInfoEntered mListener;
    private View root;

    @BindView(R.id.progressLayout)
    View progress;

    @BindView(R.id.payment_web_view)
    WebView wv;

    private Unbinder unBinder;
    private int mConsultationId;
    private int mCardId;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unBinder.unbind();
    }

    public PaymentWebViewFragment() {
        // Required empty public constructor
    }

    public static PaymentWebViewFragment newInstance(int pConsultationId) {
        PaymentWebViewFragment fragment = new PaymentWebViewFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM_CONSULTATION_ID, pConsultationId);
        fragment.setArguments(args);
        return fragment;
    }

    public static PaymentWebViewFragment newInstance(int pConsultationId, int cardId) {
        PaymentWebViewFragment fragment = new PaymentWebViewFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM_CONSULTATION_ID, pConsultationId);
        args.putInt(PARAM_CARD_ID, cardId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mConsultationId = getArguments().getInt(PARAM_CONSULTATION_ID);
            mCardId = getArguments().getInt(PARAM_CARD_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_web_view, container, false);
        unBinder = ButterKnife.bind(this, root);

        progress.setVisibility(View.VISIBLE);
        if (mCardId != 0) {
            PaymentManager.getPaymentLinkForConsultation(mConsultationId, mCardId)
                    .compose(bindToLifecycle())
                    .subscribe(this::initWebView);
        } else {
            PaymentManager.getPaymentLinkForConsultation(mConsultationId)
                    .compose(bindToLifecycle())
                    .subscribe(this::initWebView);
        }


        return root;
    }


    private void initWebView(String merchantUrl) {
//        wv.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        WebSettings settings = wv.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        wv.setWebChromeClient(new WebChromeClient());
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(final WebView view, final SslErrorHandler handler, final SslError error) {
                if (BuildConfig.DEBUG) {
                    handler.proceed();
                } else {
                    super.onReceivedSslError(view, handler, error);
                }
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.e("WV", "should override " + url);

                if (url.toString().contains(FINISH_URL)) {
                    Log.e("WV", "scess");
//                    PaymentDialog.getSuccessDialog(getActivity(),
//                                                   (PaymentDialog.DialogClickListener) getActivity())
//                                 .show();
                    checkPaymentResult(url);
                    return true;
                }

                if (url.toString().contains(FAIL_URL)) {
                    Log.e("WV", "fail");
//                    PaymentDialog.getFailedDialog(getActivity(),
//                                                   (PaymentDialog.DialogClickListener) getActivity())
//                                 .show();
                    return false;
                }

                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(final WebView view, final String url) {
                super.onPageFinished(view, url);
//                view.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                progress.setVisibility(View.GONE);
            }
        });
        String tmMerchantUrl = "http://merchant.telemedix.com/product/2/buy";

        String sberbank = "https://securepayments.sberbank.ru/payment/merchants/rbs/payment_ru.html?mdOrder=ede11651-95fc-423d-beba-4abb9d02ef62";

        wv.loadUrl(merchantUrl);
//        wv.loadUrl(sberbank);
    }

    class MyJavaScriptInterface {
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(String html) {
            // process the html as needed by the app
            Log.e("HTML", html);
        }
    }


    private void checkPaymentResult(String resultUrl) {
        progress.setVisibility(View.VISIBLE);

        new Thread(() -> {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(resultUrl)
                    .build();

            PaymentInfoWithoutCardInfo result = null;
            try {
                Response response = client.newCall(request).execute();
                String responseString = response.body().string();

                Gson gson = new Gson();
                result = gson.fromJson(responseString, PaymentInfoWithoutCardInfo.class);
                Log.e("HTML", result.toString());
                if (result.getStatus() == PaymentStatus.PAYED.getCode()) {

                    getActivity().runOnUiThread(() -> PaymentDialog.getSuccessDialog(getActivity(),
                            (PaymentDialog.DialogClickListener) getActivity())
                            .show());

                } else {
                    Intent failedPaymentIntent = new Intent(getActivity(), PaymentFailedActivity.class);
                    failedPaymentIntent.putExtra(PaymentFailedActivity.EXTRA_ORDER, result.getId());
                    failedPaymentIntent.putExtra(PaymentFailedActivity.EXTRA_SUM, result.getPrice());
                    startActivity(failedPaymentIntent);
                }
            } catch (IOException pE) {
                pE.printStackTrace();
            }

            progress.post(() -> progress.setVisibility(View.GONE));
        }).start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCardInfoEntered) {
            mListener = (OnCardInfoEntered) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCardInfoEntered");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnCardInfoEntered {
        void onCardInfoEntered();
    }
}
