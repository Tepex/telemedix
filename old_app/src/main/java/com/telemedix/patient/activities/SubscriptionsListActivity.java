package com.telemedix.patient.activities;

import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.base.DividerItemDecoration;

import com.telemedix.patient.R;
import com.telemedix.patient.adapters.SubscriptionsListAdapter;
import com.telemedix.patient.models.Subscription;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SubscriptionsListActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_subscription_list);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
		
		list_view.setLayoutManager(new LinearLayoutManager(this));
		list_view.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.list_divider)));
		
		subscriptionsListAdapter = new SubscriptionsListAdapter(this);
		subscriptionsListAdapter.setAdapterListener(new SubscriptionsListAdapter.AdapterListener()
		{
			@Override
			public void onItemSelected(Subscription subscription)
			{
				startSingleActivity(subscription);
			}
			
			@Override
			public void onItemAdd() {}
		});
		list_view.setAdapter(subscriptionsListAdapter);
		
		/* Fake list */
		List<Subscription> subscriptionsList = new ArrayList<>();
		subscriptionsList.add(new Subscription(
			"256-136-892",
			"Страховая компания\nСогаз-МЕД",
			"он-лайн консультации терапевтов (Врачи всех клиник)",
			"25.11.2016",
			"24.11.2016",
			R.drawable.subscription_1_small,
			R.drawable.subscription_1_large));
		
		subscriptionsList.add(new Subscription(
			"800 123 321",
			"Клиника\nМЕДИ",
			"педиатрия (Врачи Детской клиники №123 г.Москвы)",
			"10.11.2016",
			"10.10.2016",
			R.drawable.subscription_2_small,
			R.drawable.subscription_2_large));
		
		this.subscriptionsListAdapter.setList(subscriptionsList);
	}
	
	private void startSingleActivity(Subscription subscription)
	{
		Bundle args = new Bundle();
		args.putParcelable(SubscriptionSingleActivity.ARG_SUBSCRIPTION, subscription);
		Intent intent = new Intent(this, SubscriptionSingleActivity.class);
		intent.putExtras(args);
		startActivity(intent);
	}
	
	@BindView(R.id.list_view) SuperRecyclerView list_view;
	private SubscriptionsListAdapter subscriptionsListAdapter;
}
