package com.telemedix.patient.activities.analysis;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.telemedix.patient.R;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnalysisActivity extends AppCompatActivity {

    @BindView(R.id.analysis_list)
    RecyclerView list;

    @BindView(R.id.search_box)
    EditText searchBox;

    List<String> mItems;
    AnalysisAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.analysis_activity_title);
        ButterKnife.bind(this);
        String[] analysisGroups = getResources().getStringArray(R.array.analysis_groups);
        mItems = Arrays.asList(analysisGroups);
        list.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new AnalysisAdapter(mItems, this);
        list.setAdapter(mAdapter);

        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence pCharSequence, final int pI, final int pI1, final int pI2) {

            }

            @Override
            public void onTextChanged(final CharSequence pCharSequence, final int pI, final int pI1, final int pI2) {
                List<String> filteredList = new LinkedList<String>();
                for (String s : mItems) {
                    if (s.toLowerCase().contains(pCharSequence.toString().toLowerCase())) {
                        filteredList.add(s);
                    }
                }
                mAdapter.setItems(filteredList);
            }

            @Override
            public void afterTextChanged(final Editable pEditable) {

            }
        });
    }

}
