package com.telemedix.patient.activities.application;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by User 1337 on 09.11.2016.
 */

public class ConsultationManager {
    private static ConsultationManager mInstance;

    private String firstName;
    private String secondName;
    private String lastName;
    private String phone;
    private ArrayList<String> photos;
    private int durationPos;
    private ArrayList<Drug> drugs;
    private ArrayList<String> allergyTo;
    private TreeSet<Integer> symptoms;
    private TreeSet<Integer> status;

    public static ConsultationManager instance() {
        if (mInstance == null) {
            mInstance = new ConsultationManager();
        }
        return mInstance;
    }

    public static class Drug {
        public String name;
        public int duration;
        public Drug(String name, int duration){
            this.name = name;
            this.duration = duration;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getDurationPos() {
        return durationPos;
    }

    public void setDurationPos(int durationPos) {
        this.durationPos = durationPos;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }

    public ArrayList<Drug> getDrugs() {
        return drugs;
    }

    public void setDrugs(ArrayList<Drug> drugs) {
        this.drugs = drugs;
    }

    public ArrayList<String> getAllergyTo() {
        return allergyTo;
    }

    public void setAllergyTo(ArrayList<String> allergyTo) {
        this.allergyTo = allergyTo;
    }

    public TreeSet<Integer> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(TreeSet<Integer> symptoms) {
        this.symptoms = symptoms;
    }

    public TreeSet<Integer> getStatus() {
        return status;
    }

    public void setStatus(TreeSet<Integer> status) {
        this.status = status;
    }
}
