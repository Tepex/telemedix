package com.telemedix.patient.activities.payment;

import android.app.Activity;

import android.content.Intent;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;

import android.util.Log;

import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.bumptech.glide.Glide;

import com.telemedix.patient.activities.application.ConsultApplicationActivity;
import com.telemedix.patient.activities.consultation.ConsultationFullInfoActivity;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.Doctor;

import com.telemedix.patient.network.PaymentManager;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import com.telemedix.patient.utils.DateHelper;

import static com.telemedix.patient.Constants.TAG;

public class PaymentInfoActivity extends AppCompatActivity {

    public static final String EXTRA_PAYMENT_ID = "payment_id";

    @BindView(R.id.pay_info_date)
    TextView date;
    @BindView(R.id.pay_info_time)
    TextView time;
    @BindView(R.id.pay_info_source)
    TextView source;
    @BindView(R.id.pay_info_id)
    TextView id;
    @BindView(R.id.pay_info_sum)
    TextView sum;
    @BindView(R.id.pay_info_service)
    TextView service;

    @BindView(R.id.avatar)
    ImageView doctorAvatar;
    @BindView(R.id.doctor_name)
    TextView doctorName;
    @BindView(R.id.doctor_desc)
    TextView doctorDescription;
    @BindView(R.id.consult_date)
    TextView consultDate;
    @BindView(R.id.consult_status)
    TextView consultStatus;

    @BindView(R.id.progressLayout)
    View progress;

    @BindView(R.id.cvConsultationDetails)
    CardView cvConsultationDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        progress.setVisibility(View.VISIBLE);

        PaymentManager.getPayment(getIntent().getIntExtra(EXTRA_PAYMENT_ID, 0))
                .subscribe(result -> {
                    if (result.getPaymentInfo().isValid()) {
                        date.setText(DateHelper.getShortDate(result.getPaymentInfo().getProcessedAt()));
                        time.setText(DateHelper.getTime(result.getPaymentInfo().getProcessedAt()));
                        String cardNumber = "N/a";
                        if (result.getPaymentInfo().getCardInfo() != null) {
                            cardNumber = result.getPaymentInfo().getCardInfo().getPan();
                        }
                        source.setText(String.format(getResources().getString(R.string.bank_card), cardNumber));
                        id.setText("" + result.getPaymentInfo().getId());
                        sum.setText("" + result.getPaymentInfo().getPrice());


                        service.setText(result.getPaymentInfo().getTitle());
                        Doctor doc = result.getConsultation().getDoctor();

                        if( doc != null ) {
                            Glide.with(this)
                                 .load(doc.getPhoto())
                                 .placeholder(R.drawable.no_photo_placeholder)
                                 .into(doctorAvatar);

                            doctorName.setText(doc.getName());
                            doctorDescription.setText(/*doc.getCategory().name + ", " + */doc.getClinicString());
                        }
                        if (result.getConsultation().getSlot() != null) {
                            consultDate.setText(result.getConsultation().getSlot().getDateTime().toString("dd.MM.yyyy HH:mm"));
                        } else {
                            consultDate.setText(result.getConsultation().getCreatedAt());
                        }
                        consultStatus.setText(result.getConsultation().getStateString());

                        Activity motherActivity = this;
                        cvConsultationDetails.setOnClickListener(view -> {
                            if (result.getConsultation().getState() == Consultation.State.CREATING) {
                            	if(BuildConfig.DEBUG) Log.d(TAG, "PaymentInfoActivity start ConsultApplicationActivity. ");
                                Intent intent = new Intent(motherActivity, ConsultApplicationActivity.class);
                                intent.putExtra(ConsultApplicationActivity.EXTRA_CONSULTATION, result.getConsultation());
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Intent intent = new Intent(motherActivity, ConsultationFullInfoActivity.class);
                                intent.putExtra(ConsultationFullInfoActivity.EXTRA_CONSULTATION_ID, result.getConsultation().getId());
                                startActivity(intent);
                                finish();
                            }
                        });
                        progress.setVisibility(View.GONE);
                    }
                });
    }
}
