package com.telemedix.patient.activities.application;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;

import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.telemedix.patient.R;

import com.telemedix.patient.activities.BaseButterKnifeFragment;

import com.telemedix.patient.utils.Locales;
import com.telemedix.patient.utils.PhoneLocale;

import com.telemedix.patient.models.PatientInfo;
import com.telemedix.patient.network.PatientManager;

import java.util.Arrays;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class ApplicationFragmentStep1 extends BaseButterKnifeFragment<ApplicationStepsCallback>
{

    @OnClick(R.id.application_1_continue)
    public void onNext(View v) {
        mCallback.onNextStep(ConsultApplicationActivity.STEP_ONE);
    }

    public ApplicationFragmentStep1() {
        mRootViewResource = R.layout.fragment_appilcation_step1;
    }

    @BindView(R.id.application_1_first_name)
    TextView firstName;
    @BindView(R.id.application_1_father_name)
    TextView secondName;
    @BindView(R.id.application_1_last_name)
    TextView lastName;
    @BindView(R.id.application_1_phone)
    AppCompatEditText phone;
    @BindView(R.id.countryCode)
    Spinner countryCode;

    private PhoneLocale phoneLocale;

    @Override
    protected void initView() {
        super.initView();

        mCallback.onStartProgress();
        PatientManager.getUserInfo().subscribe(this::setData);

        ArrayAdapter<PhoneLocale> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, Locales.localies);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCode.setAdapter(adapter);

        int pos = Arrays.asList(Locales.localies).indexOf(new PhoneLocale(0, Locale.getDefault().getCountry()));

        if (pos > -1) {
            countryCode.setSelection(pos);
            phoneLocale = adapter.getItem(pos);
        } else {
            countryCode.setSelection(0);
            phoneLocale = adapter.getItem(0);
        }

        countryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phoneLocale = (PhoneLocale) parent.getAdapter().getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
    }

    private void setData(PatientInfo pInfo) {
        if (pInfo.isValid()) {
            ConsultationManager manager = ConsultationManager.instance();
            if (TextUtils.isEmpty(manager.getFirstName())) {
                firstName.setText(pInfo.getUser().getFirstName());
            } else {
                firstName.setText(manager.getFirstName());
            }

            if (TextUtils.isEmpty(manager.getSecondName())) {
                secondName.setText(pInfo.getUser().getPatronymic());
            } else {
                secondName.setText(manager.getSecondName());
            }

            if (TextUtils.isEmpty(manager.getLastName())) {
                lastName.setText(pInfo.getUser().getLastName());
            } else {
                lastName.setText(manager.getLastName());
            }

            if (TextUtils.isEmpty(manager.getPhone())) {
                phone.setText(pInfo.getUser().getPhone());
            } else {
                phone.setText(manager.getPhone());
            }
            saveData();
        } else {
            restoreData();
        }

        mCallback.onStopProgress();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_application_steps, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_continue: {
                onNext(null);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        saveData();
    }

    private void saveData() {
        ConsultationManager manager = ConsultationManager.instance();
        manager.setFirstName(firstName.getText().toString());
        manager.setSecondName(secondName.getText().toString());
        manager.setLastName(lastName.getText().toString());
        manager.setPhone(phone.getText().toString());
    }

    private void restoreData() {
        ConsultationManager manager = ConsultationManager.instance();
        firstName.setText(manager.getFirstName());
        secondName.setText(manager.getSecondName());
        lastName.setText(manager.getLastName());
        phone.setText(manager.getPhone());
    }
}
