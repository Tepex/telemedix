package com.telemedix.patient.activities;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.fragments.MyPrescriptionsFragment;
import com.telemedix.patient.R;

public class MyPrescriptionsActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_my_prescriptions);
		if(savedInstanceState == null) getSupportFragmentManager()
			.beginTransaction()
			.add(R.id.main_content, MyPrescriptionsFragment.newInstance())
			.commit();
	}
}
