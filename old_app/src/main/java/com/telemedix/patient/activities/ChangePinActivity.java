package com.telemedix.patient.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePinActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.til_old_pin)
    TextInputLayout textInputLayoutOldPin;

    @BindView(R.id.old_pin)
    EditText oldPin;
    @BindView(R.id.new_pin)
    EditText newPin;
    @BindView(R.id.confirm_pin)
    EditText confirmPin;

    @OnClick(R.id.save_btn)
    public void savePin() {
        if (Hawk.contains(Constants.PREF_USER_PIN) && !oldPin.getText().toString().equals(Hawk.get(Constants.PREF_USER_PIN, ""))) {
            Snackbar.make(confirmPin, R.string.pin_invalid, Snackbar.LENGTH_SHORT).show();
//            oldPin.setText("");
            return;
        }

        if (!newPin.getText().toString().equals(confirmPin.getText().toString())) {
            Snackbar.make(confirmPin, R.string.pins_mismatch, Snackbar.LENGTH_SHORT).show();
            return;
        }

        Hawk.put(Constants.PREF_USER_PIN, confirmPin.getText().toString());
        Toast.makeText(this, R.string.pin_code_set, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);
        ButterKnife.bind(this);
        if (!Hawk.contains(Constants.PREF_USER_PIN))
        {
            //Changing title, removing "set old pin" layout
            textInputLayoutOldPin.setVisibility(View.GONE);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
