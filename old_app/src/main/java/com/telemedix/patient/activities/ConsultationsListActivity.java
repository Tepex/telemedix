package com.telemedix.patient.activities;

import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.eowise.recyclerview.stickyheaders.StickyHeadersItemDecoration;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.base.MenuActivity;

import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.activities.consultation.ConsultationFullInfoActivity;
//import com.telemedix.patient.activities.registration.RegisterActivity;
import com.telemedix.patient.view.PhoneActivity;

import com.telemedix.patient.adapters.ConsultationListAdapter;
import com.telemedix.patient.dialogs.SignUpRequiredDialog;

import com.telemedix.patient.models.Consultation;

import com.telemedix.patient.network.ConsultationApiManager;

import com.telemedix.patient.utils.DateHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;

public class ConsultationsListActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_consultations_list);
		list_view.setLayoutManager(new LinearLayoutManager(this));
		consultationListAdapter = new ConsultationListAdapter(this);
		consultationListAdapter.setHasStableIds(true);
		progress.setVisibility(View.VISIBLE);
		ConsultationApiManager.getPatientConsultations().subscribe(this::initViews);
		
		checkSigningIn();
	}
	
	@Override
	public void onBackPressed()
	{
		goHome();
	}
	
	protected boolean checkSigningIn()
	{
		if(Utils.isUserLoggedIn()) return true;
		else
		{
			SignUpRequiredDialog.getRequiredSignupForHistoryDialog(this, new SignUpRequiredDialog.DialogClickListener()
			{
				@Override
				public void onRegisterClick()
				{
					Intent intent = new Intent(ConsultationsListActivity.this, PhoneActivity.class);
					startActivity(intent);
				}
				
				@Override
				public void onRegisterCancelClick() {}
			}).show();
			return false;
		}
	}
	
	private void initViews(List<Consultation> pConsultations)
	{
        /*
        Fake list
        */
        
		List<Consultation> consultationList = new ArrayList<>();
		/*
		consultationList.add(new ConsultationFake(
			0,
			"Запланированные он-лайн консультации",
			Consultation.ViewType.LABEL,
			"Он-лайн консультация",
			"Пупкина А.А, Педиатр",
			"https://www.colourbox.com/preview/4661529-attractive-portrait-of-confident-male-doctor.jpg",
			"Клиника \"СМ-Доктор\"",
			"2016-09-05 14:00",
			true));
		
		for(Consultation cons: pConsultations) consultationList.add(convertToFake(cons));
		*/

//        consultationList.add(new ConsultationFake(
//                "Запланированные он-лайн консультации",
//                ConsultationListItem.ViewType.CONSULTATION,
//                "Он-лайн консультация",
//                "Петрова А.А, Педиатр",
//                "https://thumbs.dreamstime.com/z/closeup-beautiful-face-woman-clean-skin-young-fresh-isolated-white-45129409.jpg",
//                "Клиника \"СМ-Доктор\"",
//                "2016-09-05 14:00",
//                true
//        ));
//
//        consultationList.add(new ConsultationFake(
//                "Запланированные он-лайн консультации",
//                ConsultationListItem.ViewType.CONSULTATION,
//                "Он-лайн консультация",
//                "Пупкина А.А, Педиатр",
//                "https://www.colourbox.com/preview/4661529-attractive-portrait-of-confident-male-doctor.jpg",
//                "Клиника \"СМ-Доктор\"",
//                "2016-09-05 14:00",
//                true
//        ));
//
//        consultationList.add(new ConsultationFake(
//                "Запланированные визиты в клинику",
//                ConsultationListItem.ViewType.LABEL,
//                "Он-лайн консультация",
//                "Пупкина А.А, Педиатр",
//                "https://www.colourbox.com/preview/4661529-attractive-portrait-of-confident-male-doctor.jpg",
//                "Клиника \"СМ-Доктор\"",
//                "2016-09-05 14:00",
//                true
//        ));
//
//        consultationList.add(new ConsultationFake(
//                "Запланированные визиты в клинику",
//                ConsultationListItem.ViewType.CONSULTATION,
//                "Визит в клинику",
//                "Степанов А.А., Терапевт",
//                "https://thumbs.dreamstime.com/z/doctor-9853320.jpg",
//                "ГКБ №99 г. Москвы",
//                "2016-09-06 14:00",
//                true
//        ));
//
//        consultationList.add(new ConsultationFake(
//                "Недавние консультации",
//                ConsultationListItem.ViewType.LABEL,
//                "Он-лайн консультация",
//                "Пупкина А.А, Педиатр",
//                "https://www.colourbox.com/preview/4661529-attractive-portrait-of-confident-male-doctor.jpg",
//                "Клиника \"СМ-Доктор\"",
//                "2016-09-05 14:00",
//                true
//        ));
//
//        consultationList.add(new ConsultationFake(
//                "Недавние консультации",
//                ConsultationListItem.ViewType.CONSULTATION,
//                "Он-лайн консультация",
//                "Иваненко А.А., Терапевт",
//                "https://thumbs.dreamstime.com/z/closeup-beautiful-face-woman-clean-skin-young-fresh-isolated-white-45129409.jpg",
//                "Клиника МЕДСИ",
//                "2016-07-25 14:00",
//                false
//        ));
//
//        consultationList.add(new ConsultationFake(
//                "Недавние консультации",
//                ConsultationListItem.ViewType.CONSULTATION,
//                "Визит в клинику",
//                "Иванова Е.И., Терапевт",
//                "https://thumbs.dreamstime.com/z/successful-female-doctor-holding-something-11074259.jpg",
//                "Клиника МЕДСИ",
//                "2016-07-26 16:00",
//                false
//        ));
//
//        consultationList.add(new ConsultationFake(
//                "Недавние консультации",
//                ConsultationListItem.ViewType.CONSULTATION,
//                "Визит в клинику",
//                "Пупкина Е.И., Терапевт",
//                "https://thumbs.dreamstime.com/z/successful-female-doctor-holding-something-11074259.jpg",
//                "Клиника МЕДСИ",
//                "2016-07-26 18:00",
//                false
//        ));


		consultationList.add(null/*footer*/);
		this.consultationListAdapter.setList(consultationList);
		consultationListAdapter.setAdapterListener(new ConsultationListAdapter.AdapterListener()
		{
			@Override
			public void onItemClicked(Consultation consultation)
			{
				selectConsultation(consultation);
			}
			
			@Override
			public void onFooterClicked()
			{
				//Toast.makeText(ConsultationsListActivity.this, "Footer clicked", Toast.LENGTH_SHORT).show();
			}
		});
		
		list_view.setAdapter(consultationListAdapter);
		progress.setVisibility(View.GONE);
		
		if(pConsultations.size() <= 0)
		{ 
			list_view.setVisibility(View.INVISIBLE);
			voidList.setVisibility(View.VISIBLE);
		}
		else
		{
			list_view.setVisibility(View.VISIBLE);
			voidList.setVisibility(View.GONE);
		}
	}
	
	private void selectConsultation(Consultation consultation)
	{
		Intent consultationIntent = new Intent(this, ConsultationFullInfoActivity.class);
		consultationIntent.putExtra(ConsultationFullInfoActivity.EXTRA_CONSULTATION_ID, consultation.getId());
		startActivity(consultationIntent);
		//Toast.makeText(this, "Consultation selected", Toast.LENGTH_SHORT).show();
	}
	
	@BindView(R.id.list_view) SuperRecyclerView list_view;
	@BindView(R.id.progressLayout) View progress;
	@BindView(R.id.void_list_layout) View voidList;
	
	private ConsultationListAdapter     consultationListAdapter;
	private StickyHeadersItemDecoration top;
}
