package com.telemedix.patient.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.base.MenuActivity;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecurityActivity extends MenuActivity {

    @BindView(R.id.tvSetPinCode)
    protected TextView tvSetPinCode;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_security;
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Hawk.contains(Constants.PREF_USER_PIN)) tvSetPinCode.setText(getResources().getText(R.string.change_pin_code));
    }

    @OnClick(R.id.change_phone_btn)
    void startChangePhoneActivity() {
        Intent intent = new Intent(this, ChangePhoneNumberActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.change_pin_btn)
    void startChangePinActivity() {
        Intent intent = new Intent(this, ChangePinActivity.class);
        startActivity(intent);
    }
}
