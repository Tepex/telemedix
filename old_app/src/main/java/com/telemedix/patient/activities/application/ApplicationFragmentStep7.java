package com.telemedix.patient.activities.application;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.BaseButterKnifeFragment;
import com.telemedix.patient.network.Api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApplicationFragmentStep7 extends BaseButterKnifeFragment<ApplicationStepsCallback> {
    @BindView(R.id.healthContainer)
    ViewGroup healthContainer;

    @OnClick(R.id.application_7_continue)
    public void onNext(View v) {
        HashMap<String, ArrayList<String>> data = new HashMap<>();
        ArrayList<String> healthStatuses = new ArrayList<>();
        for (int i = 0; i < healthContainer.getChildCount(); i++) {
            CheckBox view = (CheckBox) healthContainer.getChildAt(i);
            if (view.isChecked()) {
                healthStatuses.add(view.getText().toString());
            }
        }
        data.put("healthStatuses", healthStatuses);

        Api.get.setConsultationStep(mCallback.getConsultationId(), ConsultApplicationActivity.STEP_SEVEN, data)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o != null) {
                        mCallback.onNextStep(ConsultApplicationActivity.STEP_SEVEN);
                    }
                });
    }

    public ApplicationFragmentStep7() {
        mRootViewResource = R.layout.fragment_appilcation_step7;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void initView() {
        super.initView();
        TreeSet<Integer> status = ConsultationManager.instance().getStatus();
        if (status == null) return;

        for (Integer pos : status) {
            ((CheckBox) healthContainer.getChildAt(pos)).setChecked(true);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_application_steps, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_continue: {
                onNext(null);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        TreeSet<Integer> status = new TreeSet<>();
        for (int i = 0; i < healthContainer.getChildCount(); i++) {
            CheckBox view = (CheckBox) healthContainer.getChildAt(i);
            if (view.isChecked()) {
                status.add(i);
            }
        }

        ConsultationManager.instance().setStatus(status);
    }
}
