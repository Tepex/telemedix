package com.telemedix.patient.activities.clinic;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;
import com.telemedix.patient.models.Clinic;
import com.telemedix.patient.network.ClinicsManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ClinicActivity extends AppCompatActivity {

    public static final String EXTRA_CLINIC_ID = "extra_clinic_id";

    @BindView(R.id.clinic_address)
    TextView clinicAddress;
    @BindView(R.id.clinic_name)
    TextView clinicName;
    @BindView(R.id.icon)
    ImageView clinicIcon;

    @BindView(R.id.progressLayout)
    View progress;

    @OnClick(R.id.location_indicator)
    public void onLocationClick() {
        startActivity(Utils.viewOnMap(clinicAddress.getText().toString()));
    }

    @OnClick(R.id.clinic_info_delete)
    public void onDeleteClinic() {
        Snackbar.make(clinicIcon, "Not implemented yet", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        progress.setVisibility(View.VISIBLE);
        ClinicsManager.getClinicById(getIntent().getIntExtra(EXTRA_CLINIC_ID, -1))
                .subscribe(this::initViews);
    }

    private void initViews(Clinic pClinic) {
        clinicAddress.setText(pClinic.getAddress());
        clinicName.setText(pClinic.getName());
        Glide.with(this)
                .load(pClinic.getIcon())
                .into(clinicIcon);

        progress.setVisibility(View.GONE);
    }
}
