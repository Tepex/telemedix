package com.telemedix.patient.activities.consultation;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.v7.app.AppCompatActivity;

import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.dialogs.ConsultationDialog;
import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.network.ConsultationApiManager;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import static com.telemedix.patient.Constants.TAG;

public class ConsultationFullInfoActivity extends BaseActivity
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_consultation_full_info_activity);
		progress.setVisibility(View.VISIBLE);
		ConsultationApiManager
			.getConsultation(getIntent().getIntExtra(EXTRA_CONSULTATION_ID, -1))
			.subscribe(consultation->
			{
				ConsultationFullInfoActivity.this.consultation = consultation;
				if(consultation.isValid())
				{
					if(consultation.getDoctor() != null) doctorName.setText(consultation.getDoctor().getName());
					if(consultation.getSlot() != null) date.setText(consultation.getSlot().getDateString());
					getSupportActionBar().setTitle(getString(R.string.consultation_header, consultation.getId()));
					goal.setText(consultation.getMessage());
				}
				progress.setVisibility(View.GONE);
			});
	}
	
	@OnClick(R.id.btn_cancel)
	public void onCancel()
	{
		ConsultationDialog.getCancelConsultationDialog(this, new ConsultationDialog.DialogClickListener()
		{
			@Override
			public void onApprove()
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "Approve cancel consultation "+consultation.getId());
			}
			
			@Override
			public void onCancel() {}
		}).show();
	}
	
	@OnClick(R.id.btn_edit)
	public void onEdit() {}
	
	@OnClick(R.id.btn_move)
	public void onMove() {}
	
	@BindView(R.id.progressLayout) View progress;
	@BindView(R.id.consultation_info_doctor_name) TextView doctorName;
	@BindView(R.id.consultation_info_date) TextView date;
	@BindView(R.id.consultation_info_goal) TextView goal;
	@BindView(R.id.consultation_sickness_duration) TextView duration;
	@BindView(R.id.consultation_info_medicines) TextView medicines;
	@BindView(R.id.consultation_info_allergies) TextView allergies;
	@BindView(R.id.consultation_info_symptoms) TextView symptoms;
	@BindView(R.id.consultation_info_health_condition) TextView healthCondition;
	
	public final static String EXTRA_CONSULTATION_ID = "extra_consultation_id";
	
	private Consultation consultation;
}
