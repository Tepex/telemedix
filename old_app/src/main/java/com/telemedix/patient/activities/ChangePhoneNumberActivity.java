package com.telemedix.patient.activities;

import android.os.Bundle;
import android.view.MenuItem;

import android.support.annotation.CallSuper;

import com.telemedix.patient.base.BaseActivity;
import com.telemedix.patient.R;

public class ChangePhoneNumberActivity extends BaseActivity
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_change_phone_number);
	}
}
