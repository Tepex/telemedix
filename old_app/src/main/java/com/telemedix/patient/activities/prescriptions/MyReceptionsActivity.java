package com.telemedix.patient.activities.prescriptions;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.analysis.AnalysisAdapter;
import com.telemedix.patient.network.PatientManager;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyReceptionsActivity extends AppCompatActivity {

    @BindView(R.id.analysis_list)
    RecyclerView list;
    @BindView(R.id.progressLayout)
    View progress;
    @BindView(R.id.void_list_layout)
    View voidList;

    ReceiptAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_receptions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_receipt_title);

        ButterKnife.bind(this);

        list.setLayoutManager(new LinearLayoutManager(this));

        progress.setVisibility(View.VISIBLE);
        PatientManager.getRecipes()
                .subscribe(pRecipes -> {
                    mAdapter = new ReceiptAdapter(pRecipes, this);
                    list.setAdapter(mAdapter);
                    progress.setVisibility(View.GONE);

                    if (pRecipes.size() <= 0) {
                        voidList.setVisibility(View.VISIBLE);
                        list.setVisibility(View.GONE);
                    } else {
                        voidList.setVisibility(View.GONE);
                        list.setVisibility(View.VISIBLE);
                    }
                });
    }

}
