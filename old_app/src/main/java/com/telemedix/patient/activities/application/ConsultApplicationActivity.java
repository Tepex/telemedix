package com.telemedix.patient.activities.application;

import android.app.Activity;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.v7.app.AppCompatActivity;

import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.activities.application.step2.ApplicationFragmentStep2;
import com.telemedix.patient.activities.BaseButterKnifeFragment;
import com.telemedix.patient.activities.call.ConversationV2Activity;
import com.telemedix.patient.activities.doctor.FeedbackActivity;
import com.telemedix.patient.activities.HomeActivity;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.network.ConsultationApiManager;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import static com.telemedix.patient.Constants.TAG;

public class ConsultApplicationActivity extends BaseActivity implements ApplicationStepsCallback
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_application);
		consultation = (Consultation)getIntent().getSerializableExtra(EXTRA_CONSULTATION);
		if(BuildConfig.DEBUG) Log.d(TAG, "ConsultApplicationActivity.onCreate consultation.doctor:"+consultation.getDoctor());
		getSupportActionBar().setTitle(R.string.activity_contact_title);
		getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.fragment_container, new ApplicationFragmentStep1())
			.commit();
	}
	
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(final MenuItem item)
	{
		boolean popResult = false;
		if(item.getItemId() == android.R.id.home) popResult = getSupportFragmentManager().popBackStackImmediate();
		if(!popResult) return super.onOptionsItemSelected(item);
		else return true;
	}
	
	@Override
	public void onNextStep(int currentStep)
	{
		BaseButterKnifeFragment nextFrag = null;
		String nextToolbarTitle = getResources().getString(R.string.consultation_application_title_1);
		
		
		if(currentStep == STEP_TWO) currentStep = STEP_SEVEN;
			
			
		switch(currentStep)
		{
			case STEP_ONE:
				nextFrag = new ApplicationFragmentStep2();
				nextToolbarTitle = getResources().getString(R.string.consultation_application_title_2);
				break;
			case STEP_TWO:
				nextFrag = new ApplicationFragmentStep3();
				nextToolbarTitle = getResources().getString(R.string.consultation_application_title_3);
				break;
			case STEP_THREE:
				nextFrag = new ApplicationFragmentStep4();
				nextToolbarTitle = getResources().getString(R.string.consultation_application_title_4);
				break;
			case STEP_FOUR:
				nextFrag = new ApplicationFragmentStep5();
				nextToolbarTitle = getResources().getString(R.string.consultation_application_title_5);
				break;
			case STEP_FIVE:
				nextFrag = new ApplicationFragmentStep6();
				nextToolbarTitle = getResources().getString(R.string.consultation_application_title_6);
				break;
			case STEP_SIX:
				nextFrag = new ApplicationFragmentStep7();
				nextToolbarTitle = getResources().getString(R.string.consultation_application_title_7);
				break;
			case STEP_SEVEN:
				nextFrag = new ApplicationFragmentStep8();
				Bundle args = new Bundle();
				args.putSerializable(EXTRA_CONSULTATION, consultation);
				nextFrag.setArguments(args);
				if(consultation.isInstant()) nextToolbarTitle = getString(R.string.urgent_consultation);
				else nextToolbarTitle = getString(R.string.online_consultation);
				break;
			case STEP_EIGHT:
				finishCreation();
				break;
			default:
				finish();
		}
		
		if(nextFrag != null)
		{
			getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.fragment_container, nextFrag, nextFrag.getClass().getName())
				.addToBackStack(nextFrag.getClass().getName())
				.commit();
		}
		getSupportActionBar().setTitle(nextToolbarTitle);
	}
	
	@Override
	public void onStartProgress()
	{
		progress.setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onStopProgress()
	{
		progress.setVisibility(View.GONE);
	}
	
	@Override
	@CallSuper
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(data == null || resultCode != Activity.RESULT_OK) return;
		if(requestCode == CAPTURE_PHOTO_REQUEST_CODE || requestCode == CAPTURE_GALERY_REQUEST_CODE)
		{
			ApplicationFragmentStep2 f = (ApplicationFragmentStep2)getSupportFragmentManager().findFragmentByTag(ApplicationFragmentStep2.class.getName());
			if(f != null) f.processImage(data);
		}
	}
	
	@Override
	public int getConsultationId()
	{
		return consultation.getId();
	}
	
	
	private void finishCreation()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "ConsultApplicationActivity.finishCreation("+consultation.getId()+")");
		onStartProgress();
		ConsultationApiManager.finishCreation(consultation.getId())
			.subscribe(consultation->
			{
				onStopProgress();
				
				if(!ConsultApplicationActivity.this.consultation.isInstant()) goHome();
				else
				{
					if(BuildConfig.DEBUG) Log.d(TAG, "ConsultApplicationActivity.consultation.isInstant() is true. doctor: "+ConsultApplicationActivity.this.consultation.getDoctor());
					Intent intent = new Intent(ConsultApplicationActivity.this, ConversationV2Activity.class);
					intent.putExtra(EXTRA_CONSULTATION, ConsultApplicationActivity.this.consultation);
					startActivity(intent);
					finish();
				}
			});
	}
	
	public static final int CAPTURE_PHOTO_REQUEST_CODE = 100;
	public static final int CAPTURE_GALERY_REQUEST_CODE = 101;
	
	public static final int STEP_ONE = 1;
	public static final int STEP_TWO = 2;
	public static final int STEP_THREE = 3;
	public static final int STEP_FOUR = 4;
	public static final int STEP_FIVE = 5;
	public static final int STEP_SIX = 6;
	public static final int STEP_SEVEN = 7;
	public static final int STEP_EIGHT = 8;
	
	public static final String EXTRA_CONSULTATION = "extra_consultation";
	
	private Consultation consultation;
    
	@BindView(R.id.progressLayout) View progress;
}
