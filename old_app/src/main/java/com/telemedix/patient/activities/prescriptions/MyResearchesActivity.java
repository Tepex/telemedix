package com.telemedix.patient.activities.prescriptions;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.telemedix.patient.R;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyResearchesActivity extends AppCompatActivity {

    @BindView(R.id.researches_list)
    RecyclerView list;

    ResearchesAdapter mAdapter;
    List<String> mItems; //TODO remove fake

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_resesarches);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.activity_test_assignments_title);

        ButterKnife.bind(this);

        String[] analysisGroups = getResources().getStringArray(R.array.analysis_groups);
        mItems = Arrays.asList(analysisGroups);
        list.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ResearchesAdapter(mItems, this);
        list.setAdapter(mAdapter);
    }

}
