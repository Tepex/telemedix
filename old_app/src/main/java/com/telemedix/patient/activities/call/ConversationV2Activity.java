package com.telemedix.patient.activities.call;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import android.content.pm.PackageManager;

import android.Manifest;

import android.media.AudioManager;

import android.os.Bundle;
import android.os.Handler;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;

import android.util.Log;

import android.view.View;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.activities.application.ConsultApplicationActivity;
import com.telemedix.patient.activities.doctor.FeedbackActivity;

import com.telemedix.patient.dialogs.IncomingCallDialog;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.TwilioSession;

import com.telemedix.patient.network.ConsultationApiManager;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import com.twilio.common.AccessManager;

import com.twilio.video.AudioTrack;
import com.twilio.video.CameraCapturer;
import com.twilio.video.CameraCapturer.CameraSource;
import com.twilio.video.ConnectOptions;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalMedia;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.Media;
import com.twilio.video.Participant;
import com.twilio.video.Room;
import com.twilio.video.TwilioException;
import com.twilio.video.VideoClient;
import com.twilio.video.VideoTrack;
import com.twilio.video.VideoView;

import java.util.concurrent.TimeUnit;

import java.util.Map;

import org.joda.time.Period;

import io.reactivex.android.schedulers.AndroidSchedulers;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

import io.reactivex.schedulers.Schedulers;

import static com.telemedix.patient.Constants.TAG;

/**
 * 1. check camers permissions
 * 2. init()
 * 3. startInstant
 * 3. startSession()
 */
public class ConversationV2Activity extends BaseActivity implements Room.Listener
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_video);
		
		/* Enable changing the volume using the up/down keys during a conversation */
		setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
		/* Needed for setting/abandoning audio focus during call */
		audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
		
		/* Check camera and microphone permissions. Needed in Android M. */
		int resultCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
		int resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
		if(resultCamera == PackageManager.PERMISSION_GRANTED && resultMic == PackageManager.PERMISSION_GRANTED) init();
		else
		{
			if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
				ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO))
			{
				Toast.makeText(this, R.string.permissions_needed, Toast.LENGTH_LONG).show();
				return;
			}
			else
			{
				/* Все готово для отрисовки UI */
				ActivityCompat.requestPermissions(
				this,
				new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
				CAMERA_MIC_PERMISSION_REQUEST_CODE);
			}
		}
	}
	
	@Override
	@CallSuper
	protected void onResume()
	{
		super.onResume();
		LocalBroadcastManager.getInstance(this).registerReceiver(cancelReceiver, new IntentFilter(INTENT_CANCEL_CONSULTATION));
	}
	
	@Override
	@CallSuper
	protected void onPause()
	{
		LocalBroadcastManager.getInstance(this).unregisterReceiver(cancelReceiver);
		super.onPause();
	}
	
	@Override
	@CallSuper
	protected void onStop()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Conversation onStop");
		stopSession(false);
		super.onStop();
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		/* Не мой request */
		if(requestCode != CAMERA_MIC_PERMISSION_REQUEST_CODE) return;
		
		boolean cameraAndMicPermissionGranted = true;
		for(int grantResult: grantResults) cameraAndMicPermissionGranted &= grantResult == PackageManager.PERMISSION_GRANTED;
		if(!cameraAndMicPermissionGranted)
		{
			Toast.makeText(this, R.string.permissions_needed, Toast.LENGTH_LONG).show();
			return;
		}
		init();
	}
	
	private void init()
	{
		consultation = (Consultation)getIntent().getSerializableExtra(ConsultApplicationActivity.EXTRA_CONSULTATION);
		if(BuildConfig.DEBUG) Log.d(TAG, "init() consultation: "+consultation);
		TwilioSession session = (TwilioSession)getIntent().getSerializableExtra(EXTRA_CONSULTATION_RESPONSE);
		if(BuildConfig.DEBUG) Log.d(TAG, "start consultation. doctor: "+consultation.getDoctorShortName(getString(R.string.emergency_info_doctor_name)));
		
		/* Сброс будильника */
		if(BuildConfig.DEBUG) Log.d(TAG, "Canceling alarm and pending intent… id: "+consultation.getId());
		PendingIntent broadcast = PendingIntent.getBroadcast(this, consultation.getId(), getIntent(), PendingIntent.FLAG_NO_CREATE);
		if(broadcast != null)
		{
			((AlarmManager)getSystemService(Context.ALARM_SERVICE)).cancel(broadcast);
			broadcast.cancel();
			if(BuildConfig.DEBUG) Log.d(TAG, "Alarm broadcast canceled");
		}

		/* Ждем подключения. Отображение дилога */
		showWaitDialog(true);
		
		localMedia = LocalMedia.create(this);
		// Share your microphone
		localAudioTrack = localMedia.addAudioTrack(true);
		// Share your camera
		cameraCapturer = new CameraCapturer(this, CameraSource.FRONT_CAMERA);
		localVideoTrack = localMedia.addVideoTrack(true, cameraCapturer);
		primaryVideoView.setMirror(true);
		//localVideoTrack.addRenderer(primaryVideoView);
		localVideoView = primaryVideoView;
		
		if(BuildConfig.DEBUG) Log.d(TAG, "start request Twilio session. Thread: "+Thread.currentThread().getName()+" ("+Thread.currentThread().getId()+")");
		// retrieve access token from server or local, if session != null 
		ConsultationApiManager.startInstantConsultation(session, consultation.getId()).subscribe(s->
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "Start session. Thread: "+Thread.currentThread().getName()+" ("+Thread.currentThread().getId()+")");
			
			if(s.getVideoToken() != null && s.getVideoToken().length() > 0)
			{
				if(session == null)
				{
					if(BuildConfig.DEBUG) Log.d(TAG, "Instant consultation with unknown doctor. Getting Consultation with doctor…");
					ConsultationApiManager.getConsultation(consultation.getId()).subscribe(cons->
					{
						consultation = cons;
						if(BuildConfig.DEBUG) Log.d(TAG, "Instant doctor is: "+cons.getDoctor().getName());
						waitDialog.setMessage(consultation.getDoctorShortName(getString(R.string.emergency_info_doctor_name)));
						startSession(s);
					});
				}
				else startSession(s);
			}
			else runOnUiThread(()->
			{
				showWaitDialog(false);
				Toast.makeText(ConversationV2Activity.this, R.string.error_retrieving_access_token, Toast.LENGTH_SHORT).show();
			});
		});
	}
	
	private void startSession(final TwilioSession session)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "start video client on: "+session);
		videoClient = new VideoClient(this, session.getVideoToken());
		
		ConnectOptions connectOptions = new ConnectOptions.Builder()
			.roomName(session.getRoom())
			.localMedia(localMedia)
			.build();

		room = videoClient.connect(connectOptions, ConversationV2Activity.this);
	}
	
	private void stopSession(boolean showConfirmation)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "stopSession. showConfirmation:"+showConfirmation);
		if(showConfirmation)
		{
			if(dialog != null) dialog.dismiss();
			showConfirmDialog(
				getString(R.string.emergency_dialog_title),
				getString(R.string.consultation_finish),
				getString(R.string.consultation_finish_btn),
				getString(R.string.consultation_continue_btn),
				false,
				new ConfirmDialogListener()
			{
				@Override
				public void onConfirmed()
				{
					if(BuildConfig.DEBUG) Log.d(TAG, "stopSession. confirmed");
					closeResources();
					finishConsultation();
				}
			
				@Override
				public void onDeclined()
				{
					if(BuildConfig.DEBUG) Log.d(TAG, "stopSession declined");
					backPressed = false;
				}
			});
		}
		else closeResources();
	}
	
	private void closeResources()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "stopSession. closeResources");
		
		/* Release local media when no longer needed */
		if(room != null)
		{
			room.disconnect();
			room = null;
		}
		if(cameraCapturer != null)
		{
			cameraCapturer.stopCapture();
			cameraCapturer = null;
		}
		if(localMedia != null)
		{
			localMedia.release();
			localMedia = null;
		}
	}
	
	@Override
	public void onConnected(Room room)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Room.onConnected() "+room.getName());
		setTitle(room.getName());
		/* Осматриваемся в комнате */
		for(Map.Entry<String, Participant> entry: room.getParticipants().entrySet())
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "Somebody here!");
			startVideoSessionWithDoctor(entry.getValue());
			return;
		}
	}
	
	@Override
	public void onConnectFailure(Room room, TwilioException e)
	{
		if(BuildConfig.DEBUG) Log.e(TAG, "Room.onConnectionFailure", e);
		
		// @TODO добавить уведомление пользователю и попробовать переподключиться
		
		setConnectionStatus(false);
	}
			
	@Override
	public void onDisconnected(Room room, TwilioException e)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Room listener: Disconnected from "+room.getName());
		setConnectionStatus(false);
	}
			
	@Override
	public void onParticipantConnected(Room room, final Participant participant)
	{
		startVideoSessionWithDoctor(participant);
	}
			
	@Override
	public void onParticipantDisconnected(Room room, Participant participant)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Participant "+participant.getIdentity()+" left.");
		stopSession(false);
		setConnectionStatus(false);
		doctorOffline();
	}

	private void startVideoSessionWithDoctor(final Participant participant)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "start video session "+participant.getIdentity());
		/* This app only displays video for one additional participant per Room */
		if(thumbnailVideoView.getVisibility() == View.VISIBLE)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "Multiple participants are not currently support in this UI");
			Snackbar
				.make(disconnectActionFab, "Multiple participants are not currently support in this UI", Snackbar.LENGTH_LONG)
				.setAction("Action", null).show();
			return;
		}
		
		/* Start listening for participant media events */
		participant.getMedia().setListener(new Media.Listener()
		{
			/** Доктор вошел в комнату */
			@Override
			public void onVideoTrackAdded(Media media, VideoTrack videoTrack)
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "onVideoTrackAdded. consultation.isInstant():"+consultation.isInstant());
				/* Set primary view as renderer for participant video track */
				primaryVideoView.setMirror(false);
				videoTrack.addRenderer(primaryVideoView);
				
				showWaitDialog(false);
		
				doctorInfoTextView.setText(consultation.getDoctorName(getString(R.string.emergency_info_doctor_name)));
				
				/* Возникает лаг учета времени консультации! */
				setConnectionStatus(true);				
			}
			
			@Override
			public void onVideoTrackRemoved(Media media, VideoTrack videoTrack)
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "onVideoTrackRemoved");
				videoTrack.removeRenderer(primaryVideoView);
			}
			
			@Override
			public void onAudioTrackAdded(Media media, AudioTrack audioTrack) {}
			@Override
			public void onAudioTrackRemoved(Media media, AudioTrack audioTrack)	{}
			@Override
			public void onAudioTrackEnabled(Media media, AudioTrack audioTrack) {}
			@Override
			public void onAudioTrackDisabled(Media media, AudioTrack audioTrack) {}
			@Override
			public void onVideoTrackEnabled(Media media, VideoTrack videoTrack) {}
			@Override
			public void onVideoTrackDisabled(Media media, VideoTrack videoTrack) {}
		});
		
		showIncomeDialog();
	}
	
	@OnClick(R.id.connect_action_fab)
	void connectionClick(View view)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "connectActionFab.onClick");
		//connectActionFab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_call_white_24px));
		stopSession(true);
	}
	
	@OnClick(R.id.switch_camera_action_fab)
	void switchCamera(View view)
	{
		if(cameraCapturer == null) return;
		cameraCapturer.switchCamera();
		localVideoView.setMirror(cameraCapturer.getCameraSource() == CameraSource.FRONT_CAMERA);
	}
	
	@OnClick(R.id.local_video_action_fab)
	void localVideo(View view)
	{
		/* Enable/disable the local video track */
		if(localVideoTrack == null) return;
		boolean enable = !localVideoTrack.isEnabled();
		localVideoTrack.enable(enable);
		int icon;
		if(enable)
		{
			icon = R.drawable.ic_videocam_green_24px;
			switchCameraActionFab.show();
		}
		else
		{
			icon = R.drawable.ic_videocam_off_red_24px;
			switchCameraActionFab.hide();
		}
		localVideoActionFab.setImageDrawable(ContextCompat.getDrawable(this, icon));
	}
	
	@OnClick(R.id.mute_action_fab)
	void mute(View view)
	{
		/* Enable/disable the local audio track */
		if(localAudioTrack == null) return;
		boolean enable = !localAudioTrack.isEnabled();
		localAudioTrack.enable(enable);
		int icon = enable ? R.drawable.ic_mic_green_24px : R.drawable.ic_mic_off_red_24px;
		muteActionFab.setImageDrawable(ContextCompat.getDrawable(this, icon));
	}
	
	private void setConnectionStatus(boolean isConnected)
	{
		setAudioFocus(isConnected);
		if(isConnected)
		{
			disconnectActionFab.show();
			/* Stop rendering local video track in primary view and move it to thumbnail view */ 
			localVideoTrack.removeRenderer(primaryVideoView);
			thumbnailVideoView.setVisibility(View.VISIBLE);
			localVideoTrack.addRenderer(thumbnailVideoView);
			localVideoView = thumbnailVideoView;
			Timer.get.start(timerTextView);
		}
		/* Кнопки могут быть уже обнулены ButterKnife */
		else if(!isFinishing())
		{
			disconnectActionFab.hide();
			/* Show local video in primary view */
			thumbnailVideoView.setVisibility(View.GONE);
			localVideoTrack.removeRenderer(thumbnailVideoView);
			primaryVideoView.setMirror(true);
			localVideoTrack.addRenderer(primaryVideoView);
			localVideoView = primaryVideoView;
			Timer.get.stop();
		}
	}
	
	private void setAudioFocus(boolean focus)
	{
		if(focus)
		{
			previousAudioMode = audioManager.getMode();
			// Request audio focus before making any device switch.
			audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
			/*
             * Use MODE_IN_COMMUNICATION as the default audio mode. It is required
             * to be in this mode when playout and/or recording starts for the best
             * possible VoIP performance. Some devices have difficulties with
             * speaker mode if this is not set.
			 */
			audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
			audioManager.setSpeakerphoneOn(true);
		}
		else
		{
			audioManager.setMode(previousAudioMode);
			audioManager.abandonAudioFocus(null);
		}
	}
	
	@Override
	@CallSuper
	public void onBackPressed()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "back pressed");
		backPressed = true;
		stopSession(true);
	}
	
	@Override
	@CallSuper
	public void cancelConsultationByDoctor()
	{
		showWaitDialog(false);
		super.cancelConsultationByDoctor();
	}
	
	/**
	 * Диалог ожидания входа доктора в комнату.
	 */
	private void showWaitDialog(boolean show)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Show wait dialog "+show);
		if(!show)
		{
			if(waitDialog != null)
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "Dismiss wait dialog");
				waitDialog.dismiss();
				waitDialog = null;
			}
			return;
		}
		
		waitDialog = new ProgressDialog(this);
		waitDialog.setTitle(R.string.doctor_waiting);
		waitDialog.setMessage(consultation.getDoctorShortName(getString(R.string.emergency_info_doctor_name)));
		waitDialog.setCancelable(true);
		waitDialog.setIndeterminate(true);
		waitDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		waitDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.action_cancel_consultation), (dialog, which)->waitDialog.cancel());
		waitDialog.setCanceledOnTouchOutside(false);
		waitDialog.setOnCancelListener(this::cancelConsultation);
		waitDialog.show();
	}
	
	private void showIncomeDialog()
	{
		if(consultation.isInstant()) return;
		
		IncomingCallDialog.showDialog(this, consultation.getDoctor(), new IncomingCallDialog.DialogClickListener()
		{
			@Override
			public void withVideo()	{}
					
			@Override
			public void withAudio()
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "IncomingCallDialog.withAudio");
				localVideo(null);
			}
					
			@Override
			public void onCancel()
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "IncomingCallDialog.onCancel");
				cancelConsultation(null);
			}
		});
	}
	
	/**
	 * 
	 * Финальный диалог после завершения сессии.
	 */
	private void byeBye()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "bye bye!");
	}
	
	/**
	 * Консультация завершена пациентом. OK. Уведомление сервера. Диалог с отчетом. Переход на «морду».
	 * Вызывается из диалога подтверждения выхода.
	 */
	private void finishConsultation()
	{
		ConsultationApiManager.finishConsultation(consultation.getId())
			.subscribe(nothing->
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "Finish consultation. Bye bye!");
				showConclusion();
			});
	}
	
	private void showConclusion()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "show conclusion");
		startActivity(new Intent(this, FeedbackActivity.class)
			.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK)
			.putExtra(EXTRA_ELAPSED_CONSULTATION_TIME, Timer.get.getElapsedMillis())
			.putExtra(EXTRA_CONSULTATION, consultation)
			);
		finish();
	}
	
	/**
	 * Консультация отменена. Уведомление сервера. Закрытие Activity. Переход на «морду».
	 */
	private void cancelConsultation(DialogInterface di)
	{
		ConsultationApiManager.cancelConsultation(consultation.getId())
			.subscribe(nothing->{if(BuildConfig.DEBUG) Log.d(TAG, "Cancel consultation. Bye bye!");});
		goHome();
	}
	
	/**
	 * Доктор вышел из комнаты.
	 */
	private void doctorOffline()
	{
		if(BuildConfig.DEBUG) Log.w(TAG, "Doctor offline!");
		/*
		
		Врач покинул комнату
		Запрос на сервер - WTF?
		
		пока-что так :(
		*/
		showConclusion();
		
	}
	
	private static final int CAMERA_MIC_PERMISSION_REQUEST_CODE = 1;
	
	//public final static String EXTRA_CALLEE_ID = "callee";
	public final static String EXTRA_CONSULTATION_RESPONSE = "consultation_response";
	/* You must provide a Twilio Access Token to connect to the Video service */
	private static final String TWILIO_ACCESS_TOKEN = "TWILIO_ACCESS_TOKEN";
	
	public static final String INTENT_CANCEL_CONSULTATION = "cancel_consultation";
	
	private Consultation consultation;
	private VideoClient videoClient;
	private Room room;
	private int previousAudioMode;
	private ProgressDialog waitDialog;
	/** Флаг клика на кнопку BACK */
	private boolean backPressed;
	
	private CameraCapturer cameraCapturer;
	private LocalMedia localMedia;
	private LocalAudioTrack localAudioTrack;
	private LocalVideoTrack localVideoTrack;
	private VideoView localVideoView;
	private AudioManager audioManager;
	
	/* A VideoView receives frames from a local or remote video track and renders them to an associated view. */
	@BindView(R.id.primary_video_view) VideoView primaryVideoView;
	@BindView(R.id.thumbnail_video_view) VideoView thumbnailVideoView;
	/* Android application UI elements */
	@BindView(R.id.tv_twilio_patient_fio) TextView doctorInfoTextView;
	@BindView(R.id.tv_twilio_timer) TextView timerTextView;
	
	@BindView(R.id.connect_action_fab) FloatingActionButton disconnectActionFab;
	@BindView(R.id.switch_camera_action_fab) FloatingActionButton switchCameraActionFab;
	@BindView(R.id.local_video_action_fab) FloatingActionButton localVideoActionFab;
	@BindView(R.id.mute_action_fab) FloatingActionButton muteActionFab;
	
	private BroadcastReceiver cancelReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			showWaitDialog(false);
		}
	};
	
	private static enum Timer
	{
		get;
		
		void start(final TextView textView)
		{
			timer = Observable.interval(0, 1, TimeUnit.SECONDS)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribeWith(getObserver(textView));
		}
		
		void stop()
		{
			if(timer != null && !timer.isDisposed())
			{
				timer.dispose();
				timer = null;
			}
		}
		
		long getElapsedMillis()
		{
			return elapsed;
		}
		
		private DisposableObserver<Long> getObserver(TextView textView)
		{
			return new DisposableObserver<Long>()
			{
				@Override
				public void onNext(Long t)
				{
					elapsed = t * 1000;
					Period period = new Period(elapsed);
					String str = "";
					if(period.getHours() > 0) str += period.getHours()+":";
					int min = period.getMinutes();
					if(min < 10) str += "0";
					str += min+":";
					int sec = period.getSeconds();
					if(sec < 10) str += "0";
					str += sec;
					textView.setText(str);
				}
					
				@Override
				public void onComplete() {}
					
				@Override
				public void onError(Throwable e) {}
			};
		}
			
		private Disposable timer;
		private long elapsed;
	}
}
