package com.telemedix.patient.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.pm.PackageManager;

import android.graphics.Bitmap;

import android.Manifest;

import android.net.Uri;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.design.widget.NavigationView;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;

import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.base.MenuActivity;

import com.telemedix.patient.activities.application.ConsultApplicationActivity;

import com.telemedix.patient.activities.call.ConversationV2Activity;
import com.telemedix.patient.activities.call.InCallActivity;
import com.telemedix.patient.activities.call.voip.ClientActivity;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;

import com.telemedix.patient.dialogs.EmergencyCallDialog;
import com.telemedix.patient.dialogs.RegistrationFinishedDialog;
import com.telemedix.patient.dialogs.SignUpRequiredDialog;

import com.telemedix.patient.fragments.HomeFragment;
import com.telemedix.patient.fragments.IncomingCallFragment;

import com.telemedix.patient.gcm.RegistrationIntentService;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.TwilioSession;

import com.telemedix.patient.network.ConsultationApiManager;
import com.telemedix.patient.network.PatientManager;

import static com.telemedix.patient.Constants.TAG;

public class HomeActivity extends MenuActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(savedInstanceState == null) getSupportFragmentManager()
			.beginTransaction()
			.add(R.id.main_content, HomeFragment.newInstance())
			.commit();
	}
	
	@Override
	@CallSuper
	protected void onResume()
	{
		super.onResume();

		Class cl = (Class)getIntent().getSerializableExtra(EXTRA_ACTIVITY_START_CLASS);
		if(cl != null)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "redirect: "+cl.getSimpleName());
			startActivity(new Intent(this, cl).putExtras(getIntent()));
			finish();
			return;
		}
		
		if(getIntent().getIntExtra("DONE", 0) == 0)
		{
			switch(getIntent().getIntExtra(INTENT_ACTION, -1))
			{
				case ACTION_INCOMING_CALL:
					openIncomingCall();
					break;
				case ACTION_SUCCESSFULL_REGISTRATION:
					RegistrationFinishedDialog.getDialog(this).show();
					break;
			}
		}
		getIntent().putExtra("DONE", 1);
	}
	
	@Override
	@CallSuper
	protected void onStart()
	{
		super.onStart();
		if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
			ActivityCompat.requestPermissions(
				this,
				new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.RECEIVE_SMS, Manifest.permission.CALL_PHONE},
				MIC_PERMISSION_REQUEST_CODE);

//        Api.get.getSymptoms()
//               .subscribe();
//
//        Api.get.getDurations()
//               .subscribe();
//
//        Api.get.getHealthStatuses()
//               .subscribe();
	}
	
	@Override
	@CallSuper
	public void onBackPressed()
	{
		if(drawer_layout.isDrawerOpen(GravityCompat.START)) drawer_layout.closeDrawer(GravityCompat.START);
		else super.onBackPressed();
	}

	@Override
	protected int getLayoutResId()
	{
		return R.layout.activity_home;
	}
	
	private void startSubscriptionsActivity()
	{
		if(checkSigningIn()) startActivity(new Intent(this, SubscriptionsListActivity.class));
	}

	@Override
	protected void initViews() {}
	
	public static final int ACTION_INCOMING_CALL = 1;
	public static final int ACTION_OUTGOING_CALL = 2;
	public static final int ACTION_SUCCESSFULL_REGISTRATION = 3;
	
	public static final String INTENT_ACTION = "action";
	private static final int MIC_PERMISSION_REQUEST_CODE = 1;
}
