package com.telemedix.patient.activities;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;

import android.support.v7.app.AppCompatActivity;

import android.util.Log;

import android.view.View;

import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.Doctor;

import com.telemedix.patient.network.ConsultationApiManager;
import com.telemedix.patient.network.PatientManager;
import com.telemedix.patient.network.PaymentManager;

import com.telemedix.patient.activities.application.ConsultApplicationActivity;
import com.telemedix.patient.activities.doctor.DoctorSingleActivity;

import com.telemedix.patient.activities.payment.SelectPaymentListActivity;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import static com.telemedix.patient.Constants.TAG;

public class ConsultationRequestActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_emergency_help);
		progress.setVisibility(View.VISIBLE);
		int consultationType = getIntent().getIntExtra(CONSULTATION_TYPE, ACTION_CONSULTATION_TYPE_EMERGENCY);
		if(consultationType == ACTION_CONSULTATION_TYPE_SCHEDULED)
		{
			title.setText(getString(R.string.scheduled_consultation_request_title));
			getSupportActionBar().setTitle(R.string.online_consultation);
			int serviceId = getIntent().getIntExtra(SERVICE_ID, 0);
			int slotId = getIntent().getIntExtra(SLOT_ID, 0);
			ConsultationApiManager.requestScheduledConsultation(serviceId, slotId).subscribe(this::initViews);
		}
		else
		{
			title.setText(getString(R.string.urgent_advice_info));
			getSupportActionBar().setTitle(R.string.urgent_consultation);
			
			doctor = (Doctor)getIntent().getSerializableExtra(DoctorSingleActivity.ARG_DOCTOR);
			/* консультация от дежурного врача */
			if(doctor == null) ConsultationApiManager.requestInstantConsultation().subscribe(this::initViews);
			/* консультация от выбранного врача */
			else ConsultationApiManager.requestInstantDoctorConsultation(doctor.getId()).subscribe(this::initViews);
		}
	}
	
	@OnClick(R.id.emergencyConsultBtnCancel)
	public void cancel(View v)
	{
		finish();
	}
	
	@OnClick(R.id.paymentBtn)
	public void openPaymentMethodActivity(View v)
	{
		progress.setVisibility(View.VISIBLE);
		PatientManager.getCards().subscribe(pCardInfos->
		{
			PaymentManager.flushCache();
			ConsultationApiManager.flushConsultations();
			
			Intent intent = new Intent(this, SelectPaymentListActivity.class);
			if(pCardInfos != null && pCardInfos.size() > 0) intent.putExtra(ConsultApplicationActivity.EXTRA_CONSULTATION, consultation);
			else
			{
				intent.putExtra(ConsultApplicationActivity.EXTRA_CONSULTATION, consultation);
				intent.putExtra(SelectPaymentListActivity.SKIP_PAYMENT_METHOD_CHOSE, true);
			}
			progress.setVisibility(View.GONE);
			startActivity(intent);
			finish();
		});
	}
	
	private void initViews(Consultation consultation)
	{
		this.consultation = consultation;
		if(consultation.isValid())
		{
			if(!consultation.isInstant())
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "ConsultationRequestActivity.doctor: "+consultation.getDoctor());
				dateHint.setText(getString(R.string.scheduled_consultation_wait_time_caption));
				waitingTime.setText(consultation.getSlot().getDateTimeString());
			}
			else
			{
				dateHint.setText(getResources().getString(R.string.emergency_info_wait_time_caption));
				waitingTime.setText(getString(R.string.emergency_consultation_duration, 10));
			}
			String name = getString(R.string.emergency_info_doctor_name);
			if(consultation.getDoctor() != null) name = consultation.getDoctor().getShortName();
			doctorName.setText(name);
			cost.setText(getString(R.string.price_format, consultation.getDoctorService().getPrice()));
			duration.setText(R.string.time_30minutes);
		}
		else
		{
			Toast.makeText(this, "Connection error, try again later", Toast.LENGTH_SHORT).show();
			finish();
		}
		progress.setVisibility(View.GONE);
	}
	
	@BindView(R.id.progressLayout) View progress;
	@BindView(R.id.consultation_request_title) TextView title;
	@BindView(R.id.consultation_request_doctor_name) TextView doctorName;
	@BindView(R.id.consultation_request_date_hint) TextView dateHint;
	@BindView(R.id.emergency_waiting_time) TextView waitingTime;
	@BindView(R.id.emergency_cost) TextView cost;
	@BindView(R.id.emergency_duration) TextView duration;
	
	public static final String SERVICE_ID = "serviceId";
	public static final String SLOT_ID = "slotId";
	public static final String CONSULTATION_TYPE = "consultation_type";
	
	public static final int ACTION_CONSULTATION_TYPE_EMERGENCY = 1;
	public static final int ACTION_CONSULTATION_TYPE_SCHEDULED = 2;
	
	private Consultation consultation;
	private Doctor doctor; 
}
