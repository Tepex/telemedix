package com.telemedix.patient.activities.payment;

/**
 * Created by Aleksey on 27.10.2016.
 */

public enum PaymentStatus {
    NEW(0),
    PAYED(2),
    REVERSED(3),
    REFUNDED(4),
    EXPIRED(5),
    FAILED(6),
    PROCESSING(7);

    private int mCode;

    PaymentStatus(int statusCode) {
        mCode = statusCode;
    }

    public int getCode() {
        return mCode;
    }

    public PaymentStatus getByCode(int pCode) {
        for (PaymentStatus ps : PaymentStatus.values()) {
            if (ps.getCode() == pCode) {
                return ps;
            }
        }

        return FAILED;
    }
}
