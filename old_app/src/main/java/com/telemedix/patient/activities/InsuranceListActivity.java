package com.telemedix.patient.activities;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import com.telemedix.patient.base.MenuActivity;
import com.telemedix.patient.base.DividerItemDecoration;
import com.telemedix.patient.R;
import com.telemedix.patient.adapters.InsuranceListAdapter;
import com.telemedix.patient.models.Insurance;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class InsuranceListActivity extends MenuActivity {

    @BindView(R.id.list_view)
    SuperRecyclerView list_view;

    private InsuranceListAdapter insuranceListAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_insurance;
    }

    @Override
    protected void initViews() {
        list_view.setLayoutManager(new LinearLayoutManager(this));
        list_view.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.list_divider)));
        insuranceListAdapter = new InsuranceListAdapter(this);
        insuranceListAdapter.setHasStableIds(true);

        /*
        Fake list
         */
        List<Insurance> insuranceList = new ArrayList<>();
        insuranceList.add(new Insurance(
                "Полис ОМС",
                R.drawable.insurance_1_small
        ));
        insuranceList.add(new Insurance(
                "Полис ДМС",
                R.drawable.insurance_2_small
        ));

        this.insuranceListAdapter.setList(insuranceList);
        insuranceListAdapter.setAdapterListener(new InsuranceListAdapter.AdapterListener() {
            public void onItemClicked(Insurance insurance) {
                selectInsurance(insurance);
            }
        });

        list_view.setAdapter(insuranceListAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void selectInsurance(Insurance insurance) {
//        Bundle args = new Bundle();
//        args.putParcelable("insurance", insurance);
//
//        Intent intent = new Intent(this, SelectPaymentListActivity.class);
//        intent.putExtras(args);
//
//        startActivity(intent);
    }
}
