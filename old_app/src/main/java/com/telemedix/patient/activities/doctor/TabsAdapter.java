package com.telemedix.patient.activities.doctor;

import android.content.Context;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import android.util.Log;

import com.telemedix.patient.activities.doctor.fragments.ScheduleFragment;

import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.DoctorService;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import java.util.ArrayList;
import java.util.List;

import static com.telemedix.patient.Constants.TAG;

public class TabsAdapter extends FragmentStatePagerAdapter
{
	public TabsAdapter(Context context, Doctor doctor, FragmentManager fm)
	{
		super(fm);
		this.doctor = doctor;
		for(DoctorService service: doctor.getServices()) tabs.add(new TabWrapper(service));
	}
	
	@Override
	public Fragment getItem(int pos)
	{
		TabWrapper wrapper = tabs.get(pos);
		ScheduleFragment fragment = wrapper.getFragment();
		if(fragment == null)
		{
			fragment = new ScheduleFragment();
			Bundle args = new Bundle();
			args.putSerializable(DoctorSingleActivity.ARG_DOCTOR, doctor);
			args.putInt(ScheduleFragment.ARG_SERVICE_ID, wrapper.getServiceId());
			fragment.setArguments(args);
			wrapper.setFragment(fragment);
		}
		return fragment;
	}
	
	@Override
	public int getCount()
	{
		return tabs.size();
	}
	
	@Override
	public CharSequence getPageTitle(int pos)
	{
		return tabs.get(pos).getName();
	}
	
	private List<TabWrapper> tabs = new ArrayList<>();
	private Doctor doctor;
	
	private static class TabWrapper
	{
		public TabWrapper(DoctorService service)
		{
			this.service = service;
		}
		
		public String getName()
		{
			return service.getClinicService().getService().getName();
		}
		
		public int getServiceId()
		{
			return service.getId();
		}
		
		public ScheduleFragment getFragment()
		{
			return fragment;
		}
		
		public void setFragment(ScheduleFragment fragment)
		{
			this.fragment = fragment;
		}
		
		private DoctorService service;
		private ScheduleFragment fragment;
	}
}
