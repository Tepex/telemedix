package com.telemedix.patient.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.application.ApplicationStepsCallback;
import com.trello.rxlifecycle2.components.support.RxFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Aleksey on 25.08.2016.
 */
public class BaseButterKnifeFragment<CB> extends RxFragment {

    private Unbinder mUnbinder;
    protected CB mCallback;

    protected int mRootViewResource = 0;
    
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        if (mRootViewResource == 0) {
            throw new IllegalArgumentException("Specify mRootViewResource! (R.layout.<your_layout>");
        }
        View root = inflater.inflate(mRootViewResource, container, false);
        mUnbinder = ButterKnife.bind(this, root);

        initView();

        return root;
    }

    protected void initView() {}

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        try {
            mCallback = (CB) context;
        } catch (Exception e) {
            throw new IllegalArgumentException("Root activity should implement necesserary callback" );
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

}
