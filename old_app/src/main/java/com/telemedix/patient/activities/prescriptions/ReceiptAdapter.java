package com.telemedix.patient.activities.prescriptions;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telemedix.patient.R;
import com.telemedix.patient.dialogs.CustomDialog;
import com.telemedix.patient.dialogs.DrugSchemeDialog;
import com.telemedix.patient.network.request.Recipe;
import com.telemedix.patient.utils.DateHelper;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Aleksey on 30.08.2016.
 */
public class ReceiptAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Recipe> items;
    private Context mContext;

    public ReceiptAdapter(List<Recipe> pItems, Context pContext) {
        items = pItems;
        mContext = pContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.receipt_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ItemViewHolder itemView = (ItemViewHolder) holder;
        Recipe item = items.get(position);

        if (position == 0) {
            itemView.newFrame.setVisibility(View.VISIBLE);
        } else {
            itemView.newFrame.setVisibility(View.INVISIBLE);
        }

        itemView.drugName.setText(item.getName());
        itemView.drugDose.setText(item.getDosage());
        itemView.drugForm.setText(item.getDosageForm());
        itemView.drugDate.setText(item.getCreatedAt());
        itemView.scheme = item.getReceiveCircuit() + ", " + item.getDuration() + " дней";
        itemView.doctor.setText("Иванова А.А");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.receipt_item_new_frame)
        View newFrame;
        @BindView(R.id.receipt_drug_name)
        TextView drugName;
        @BindView(R.id.receipt_drug_dose)
        TextView drugDose;
        @BindView(R.id.receipt_drug_form)
        TextView drugForm;
        @BindView(R.id.receipt_drug_date)
        TextView drugDate;
        @BindView(R.id.receipt_drug_doctor)
        TextView doctor;

        String scheme;

        @OnClick(R.id.receipt_button_scheme)
        public void openScheme(View v) {
            DrugSchemeDialog.getDialog(mContext, scheme).show();
        }

        @OnClick(R.id.receipt_button_order)
        public void orderReceipt(View v) {
            CustomDialog.getDialog(mContext, "Заказ лекарств", "Заказан препарат: " + drugName.getText().toString());
        }

        public ItemViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setItems(List<Recipe> pItems) {
        items = pItems;
        notifyDataSetChanged();
    }
}
