package com.telemedix.patient.activities.registration;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.telemedix.patient.base.MenuActivity;
import com.telemedix.patient.R;
import com.telemedix.patient.activities.MyInfoActivityFragment;
import com.telemedix.patient.activities.MyInfoActivitySecondFragment;
import com.telemedix.patient.models.PatientInfo;
import com.telemedix.patient.network.PatientManager;

import butterknife.BindView;


public class MyInfoActivity extends MenuActivity
        implements  MyInfoFragmentInteraction {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    public static final String EXTRA_NEW_CONTACT = "extra_new_contact";

    private boolean newContact = false;

    @BindView(R.id.progressLayout)
    View progress;

    private PatientInfo patient;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_info;
    }

    @Override
    protected void initViews() {
        getSupportActionBar().setTitle(R.string.title_activity_my_info);
        if (getIntent().hasExtra(EXTRA_NEW_CONTACT)) {
            newContact = getIntent().getBooleanExtra(EXTRA_NEW_CONTACT, false);
        }

        progress.setVisibility(View.VISIBLE);
        PatientManager.flushAndGetInfo()
                      .subscribe(pPatientInfo -> {
                          MyInfoActivityFragment frag1 = MyInfoActivityFragment.newInstance(newContact, pPatientInfo);
                          FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                          ft.replace(R.id.container, frag1);
                          ft.commit();
                          requestPermission();

                          progress.setVisibility(View.GONE);
                      });
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

//            // Should we show an explanation?
//            if (shouldShowRequestPermissionRationale(
//                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                // Explain to the user why we need to read the contacts
//            }

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant

            return;
        }
    }

    @Override
    public void onPageOneSaveClick(PatientInfo pPatient) {
        patient = pPatient;
        MyInfoActivitySecondFragment frag2 = MyInfoActivitySecondFragment.newInstance(patient);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, frag2);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onPageSecondSaveClick(PatientInfo pPatientInfo) {
        patient = pPatientInfo;

        if (newContact) {
            startHomeActivityForSuccesfullRegistration();
            finish();
        }
    }

    @Override
    public void onStartProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStopProgress() {
        progress.setVisibility(View.GONE);
    }
}
