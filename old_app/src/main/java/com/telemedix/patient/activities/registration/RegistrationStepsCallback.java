package com.telemedix.patient.activities.registration;

/**
 * Created by Aleksey on 25.08.2016.
 */
public interface RegistrationStepsCallback {
    void onNextStep(int currentStep);
}
