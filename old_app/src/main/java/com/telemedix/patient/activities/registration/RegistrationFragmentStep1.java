package com.telemedix.patient.activities.registration;

import android.view.View;

import com.telemedix.patient.R;
import com.telemedix.patient.activities.BaseButterKnifeFragment;
import com.telemedix.patient.activities.application.ConsultApplicationActivity;

import butterknife.OnClick;

public class RegistrationFragmentStep1 extends BaseButterKnifeFragment<RegistrationStepsCallback> {

    @OnClick(R.id.application_1_continue)
    public void onNext(View v) {
        mCallback.onNextStep(ConsultApplicationActivity.STEP_ONE);
    }

    public RegistrationFragmentStep1() {
        mRootViewResource = R.layout.fragment_appilcation_step1;
    }
}
