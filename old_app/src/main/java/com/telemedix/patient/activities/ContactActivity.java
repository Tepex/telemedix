package com.telemedix.patient.activities;

import android.content.Intent;

import android.os.Handler;

import android.support.v4.widget.NestedScrollView;

import android.util.Log;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;

import com.telemedix.patient.base.MenuActivity;

import com.telemedix.patient.activities.registration.MyInfoActivity;
import com.telemedix.patient.network.Api;
import com.telemedix.patient.network.PatientManager;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import static com.telemedix.patient.Constants.TAG;

public class ContactActivity extends MenuActivity
{
	@Override
	protected int getLayoutResId()
	{
		return R.layout.activity_contact;
	}
	
	@Override
	protected void initViews()
	{
		newContact = getIntent().getBooleanExtra(EXTRA_NEW_CONTACT, false);
		
		//new Handler().postDelayed(()->scrollview.scrollTo(0, 0), 100);
		
		progress.setVisibility(View.VISIBLE);
		PatientManager.flushAndGetInfo().subscribe(pPatientInfo->
		{
			firstName.setText(pPatientInfo.getUser().getFirstName());
			middleName.setText(pPatientInfo.getUser().getPatronymic());
			lastName.setText(pPatientInfo.getUser().getLastName());
			
			country.setText(pPatientInfo.getAddress().getCountry().getTitle());
			city.setText(pPatientInfo.getAddress().getCity());
			address.setText(pPatientInfo.getAddress().getAddress());
			email.setText(pPatientInfo.getUser().getEmail());
			
			emergencyFirstName.setText(pPatientInfo.getClosestRelative().getFirstName());
			emergencyLastName.setText(pPatientInfo.getClosestRelative().getLastName());
			emergencyPhone.setText(pPatientInfo.getClosestRelative().getPhone());
			
			progress.setVisibility(View.GONE);
		},
		thr->
		{
			if(BuildConfig.DEBUG) Log.e(TAG, "Error getting user info", thr);
			progress.setVisibility(View.GONE);
		});
	}
	@OnClick(R.id.save_btn)
	public void saveBtn()
	{
		progress.setVisibility(View.VISIBLE);
		Api.get.setCurrentUserInfo(
			firstName.getText().toString(),
			middleName.getText().toString(),
			lastName.getText().toString(),
			email.getText().toString(),
			emergencyFirstName.getText().toString(),
			emergencyLastName.getText().toString(),
			emergencyPhone.getText().toString(),
			1,
			city.getText().toString(),
			address.getText().toString()
		)
		.subscribe(pPatientInfo->
		{
			progress.setVisibility(View.GONE);
			if(newContact)
			{
				Intent intent = new Intent(ContactActivity.this, MyInfoActivity.class);
				intent.putExtra(EXTRA_NEW_CONTACT, true);
				startActivity(intent);
			}
			finish();
		},
		thr->
		{
			progress.setVisibility(View.GONE);
			Toast.makeText(this, "Ошибка сохранения информации. Убедитесь, что все(!) поля заполнены.", Toast.LENGTH_LONG).show();
		});
	}
	
	@OnClick(R.id.skip_step)
	public void skipStep()
	{
		saveBtn();
	}
	
	@BindView(R.id.scrollview) NestedScrollView scrollview;
	@BindView(R.id.first_name_input) EditText firstName;
	@BindView(R.id.middle_name_input) EditText middleName;
	@BindView(R.id.last_name_input) EditText lastName;
	@BindView(R.id.country_input) EditText country;
	@BindView(R.id.city_input) EditText city;
	@BindView(R.id.address_input) EditText address;
	@BindView(R.id.email_input) EditText email;
	@BindView(R.id.emergency_first_name_input) EditText emergencyFirstName;
	@BindView(R.id.emergency_last_name_input) EditText emergencyLastName;
	@BindView(R.id.emergency_phone_input) EditText emergencyPhone;
    @BindView(R.id.progressLayout) View progress;
    @BindView(R.id.save_btn) Button save;
    
    private boolean newContact = false;
}
