package com.telemedix.patient.activities;

import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;

import android.support.annotation.CallSuper;

import android.support.design.widget.Snackbar;

import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.App;

import com.telemedix.patient.base.BaseActivity;

import com.telemedix.patient.activities.security.InputPINActivity;

import com.telemedix.patient.network.AppManager;
import com.telemedix.patient.network.ConsultationApiManager;

import com.telemedix.patient.utils.PhoneData;

import com.telemedix.patient.gcm.GcmPreferences;
import com.telemedix.patient.gcm.RegistrationIntentService;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;

import static com.telemedix.patient.Constants.TAG;

public class MainActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_main);
		if(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS && !Hawk.contains(GcmPreferences.TOKEN))
			startService(new Intent(this, RegistrationIntentService.class));
		
		if(Hawk.contains(Constants.PREF_CONSULTATION_ID))
		{
			ConsultationApiManager.getConsultation(Hawk.get(Constants.PREF_CONSULTATION_ID)).subscribe(consultation->
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "Has unfinished consultaton: "+consultation);
			});
		}
		if(BuildConfig.DEBUG) Log.d(TAG, "User PIN:"+Hawk.get(Constants.PREF_USER_PIN));
		if(!Hawk.contains(Constants.PREF_USER_PIN)) goHome();
		else
		{
			Intent intent = new Intent(this, InputPINActivity.class);
			if(getIntent().getExtras() != null) intent.putExtras(getIntent().getExtras());
			startActivity(intent);
			finish();
		}
	}
}
