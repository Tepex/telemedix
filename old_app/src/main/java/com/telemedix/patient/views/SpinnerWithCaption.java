package com.telemedix.patient.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.telemedix.patient.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aleksey on 18.08.2016.
 */
public class SpinnerWithCaption extends FrameLayout {

    View root;
    OnClickListener callback;
    boolean isCalled = false;
    String[] entries = null;


    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.clinic_name)
    TextView caption;

    public SpinnerWithCaption(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SpinnerWithCaption, 0, 0);
            String title = a.getString(R.styleable.SpinnerWithCaption_spc_title);
            final int entriesId = a.getResourceId(R.styleable.SpinnerWithCaption_spc_entries, 0);
            if (entriesId != 0) {
                entries = getResources().getStringArray(entriesId);
            }

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            root = inflater.inflate(R.layout.spinner_with_text_layout, this, true);

            ButterKnife.bind(this, root);

            if (title != null && !title.isEmpty()) {
                caption.setText(title);
            }

            if (entriesId != 0) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, entries);
                spinner.setAdapter(adapter);
            }
        }


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> pAdapterView, final View pView, final int pI, final long pL) {
                if (isCalled && callback != null) {
                    callback.onClick(spinner);
                }
                isCalled = true;
            }

            @Override
            public void onNothingSelected(final AdapterView<?> pAdapterView) {

            }
        });
    }

    public String getValue() {
        return spinner.getSelectedItem().toString();
    }

    public void setValue(String value) {
        int i = 0;
        for (i = 0; i < entries.length; i++) {
            if (entries[i].equals(value)) {
                break;
            }
        }
        spinner.setSelection(i);
    }

    public void setClickListener(OnClickListener pClickListener) {
        callback = pClickListener;
    }
}
