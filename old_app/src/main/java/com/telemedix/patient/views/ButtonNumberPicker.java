package com.telemedix.patient.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.shawnlin.numberpicker.NumberPicker;
import com.telemedix.patient.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Aleksey on 11.08.2016.
 */
public class ButtonNumberPicker extends FrameLayout {

    View root;
    String[] mDisplayValues;
    OnClickListener clickCallback;

    @BindView(R.id.picker)
    NumberPicker picker;
    @BindView(R.id.up_btn)
    Button up;
    @BindView(R.id.down_btn)
    Button down;
    @BindView(R.id.clinic_name)
    TextView text;

    @OnClick(R.id.up_btn)
    public void upValue(View v) {
        picker.setValue(picker.getValue() - 1);
        if (clickCallback != null) {
            clickCallback.onClick(v);
        }
    }

    @OnClick(R.id.down_btn)
    public void downValue(View v) {
        picker.setValue(picker.getValue() + 1);
        if (clickCallback != null) {
            clickCallback.onClick(v);
        }
    }

    public String getValue() {
        return mDisplayValues[picker.getValue()];
    }

    public void setValue(String value) {
        int i = 0;
        for (i = 0; i < mDisplayValues.length; i++) {
            if (mDisplayValues[i].equals(value)) {
                picker.setValue(i);
                break;
            }
        }
    }

    public void setText(String pText) {
        text.setText(pText);
    }

    public ButtonNumberPicker(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ButtonsNumberPicker, 0, 0);
        String title = a.getString(R.styleable.ButtonsNumberPicker_np_title);
        int max = a.getInt(R.styleable.ButtonsNumberPicker_np_max, 100);
        int min = a.getInt(R.styleable.ButtonsNumberPicker_np_min, 0);
        int value = a.getInt(R.styleable.ButtonsNumberPicker_np_value, 50);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        root = inflater.inflate(R.layout.buttons_number_picker, this, true);

        if (isInEditMode()) {
            return;
        }

        ButterKnife.bind(this, root);

        if (title != null) {
            text.setText(title);
        }

        mDisplayValues = getDisplayValues(min, max);
        picker.setDisplayedValues(mDisplayValues);

        picker.setMinValue(0);
        picker.setMaxValue(mDisplayValues.length - 1);
        picker.setValue(value);

        picker.setOnValueChangedListener(new android.widget.NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(final android.widget.NumberPicker pNumberPicker, final int pI, final int pI1) {
                if (clickCallback != null) {
                    clickCallback.onClick(picker);
                }
            }
        });


    }

    private String[] getDisplayValues(int minimumInclusive, int maximumInclusive) {

        ArrayList<String> result = new ArrayList<String>();

        for (int i = maximumInclusive; i >= minimumInclusive; i--) {
            result.add(Integer.toString(i));
        }

        return result.toArray(new String[0]);
    }

    public void setClickListener(OnClickListener pClickListener) {
        clickCallback = pClickListener;
    }
}
