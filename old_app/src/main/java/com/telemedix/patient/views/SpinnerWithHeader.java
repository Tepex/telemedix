package com.telemedix.patient.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.telemedix.patient.R;

import java.util.List;

/**
 * Created by Aleksey on 26.10.2016.
 */

public class SpinnerWithHeader extends Spinner {

    private String header = "";

    public SpinnerWithHeader(final Context context) {
        super(context);
    }

    public SpinnerWithHeader(final Context context, final int mode) {
        super(context, mode);
    }

    public SpinnerWithHeader(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SpinnerWithHeader, 0, 0);
            header = a.getString(R.styleable.SpinnerWithHeader_sph_header);
        }
    }

    @Override
    public void setAdapter(final SpinnerAdapter adapter) {
        super.setAdapter(adapter);
    }

    public void setEntries(Context pContext, List<String> pEntries) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(pContext, android.R.layout.simple_spinner_item, pEntries) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;

                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    tv.setVisibility(View.GONE);
                    v = tv;
                } else {
                    // Pass convertView as null to prevent reuse of special case views
                    v = super.getDropDownView(position, null, parent);
                }

                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        setAdapter(dataAdapter);
    }
}
