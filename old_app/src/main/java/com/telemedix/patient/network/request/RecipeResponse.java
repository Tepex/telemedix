package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aleksey on 24.11.2016.
 */

public class RecipeResponse implements Serializable {
    @SerializedName("id")
    private String       id;
    @SerializedName("recipes")
    private List<Recipe> recipes = new LinkedList<>();

    public String getId() {
        return id;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }
}
