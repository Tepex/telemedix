package com.telemedix.patient.network;

import android.util.Log;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;

import com.telemedix.patient.network.response.AppRegisterResponse;
import com.telemedix.patient.gcm.GcmPreferences;
import com.telemedix.patient.utils.PhoneData;

import io.reactivex.Observable;

import static com.telemedix.patient.Constants.TAG;

public class AppManager
{
	public static Observable<AppRegisterResponse> registerApp(String imei, String gcmToken)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "AppManager.registerApp. Token:"+gcmToken);
		return Api.get.registerApp(PhoneData.get.getUUID(), imei, gcmToken).onErrorReturn(pThrowable->new AppRegisterResponse());
	}
}
