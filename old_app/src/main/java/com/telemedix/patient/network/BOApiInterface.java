package com.telemedix.patient.network;

import com.telemedix.patient.models.BaseModel;
import com.telemedix.patient.models.Clinic;
import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.DoctorService;
import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.Slot;
import com.telemedix.patient.models.TwilioSession;
import com.telemedix.patient.models.PatientInfo;

import com.telemedix.patient.models.payment.CardInfo;
import com.telemedix.patient.models.payment.PaymentInfo;
import com.telemedix.patient.models.payment.PaymentInfoWithoutCardInfo;

import com.telemedix.patient.network.request.AllergiesPostRequest;
import com.telemedix.patient.network.request.ChronicDiseasesPostRequest;
import com.telemedix.patient.network.request.ConfirmPhoneRequestBody;
import com.telemedix.patient.network.request.ConsultationRequest;
import com.telemedix.patient.network.request.EditMetricsRequest;
import com.telemedix.patient.network.request.EditUserInfoRequest;
import com.telemedix.patient.network.request.FeedbackRequest;
import com.telemedix.patient.network.request.InstantConsultationRequest;
import com.telemedix.patient.network.request.Recipe;
import com.telemedix.patient.network.request.RecipeResponse;
import com.telemedix.patient.network.request.RegisterRequest;

import com.telemedix.patient.network.response.AppRegisterResponse;
import com.telemedix.patient.network.response.BaseResponse;
import com.telemedix.patient.network.response.City;
import com.telemedix.patient.network.response.ConfirmConsultationResponse;
import com.telemedix.patient.network.response.ConfirmPhoneResponse;
import com.telemedix.patient.network.response.DoctorCity;
import com.telemedix.patient.network.response.IdNamePair;
import com.telemedix.patient.network.response.RegisterResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

import io.reactivex.Observable;

/**
 * Created by Aleksey on 13.09.2016.
 */

public interface BOApiInterface
{
	// Тестовый запрос. Инициирует отправку нотифиации о начале онлайн-консультации.
	@GET("p/t/patient/notification/consultation_start")
	Observable<Void> sendConsultationNotification();

	///////////////////////////////////APPLICATION\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @POST ("app/patient/install/{appid}/{imei}/{gcm}")
    Observable<AppRegisterResponse> registerApp(@Path("appid") String appId, @Path("imei") String imei, @Path("gcm") String gcm);

    @POST("app/mainpage/buttons")
    Observable<Void> getMainpageLayout();

    //////////////////////////////////DOCTORS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    @GET("doctor/doctor/{doctorId}")
    Observable<Doctor> getDoctor(@Path("doctorId") String doctorId);

    @GET("doctor/doctors")
    Observable<List<Doctor>> getDoctors(@Query("_format") String format);

    @GET("doctor/services/{docId}")
    Observable<List<DoctorService>> getServicesByDoctor(@Path("docId") String doctorId);

    @GET("doctor/service/reception_slots/{service}")
    Observable<List<Slot>> getReceptionSlots(@Path("service") int serviceId, @Query("date") String date);

    @GET("doctor/cities")
    Observable<List<DoctorCity>> getDoctorsCities();

    ///////////////////////////////////PATIENT\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    @POST("patient/phone")
    Observable<RegisterResponse> registerUserPhone(@Body RegisterRequest pRequest);

    @PATCH("patient/contacts/{id}")
    Observable<PatientInfo> editPatientInfo(@Path("id") String patientId, @Body EditUserInfoRequest pEditUserInfoRequest);

    @GET("patient/patient/{id}")
    Observable<PatientInfo> getPatientById(@Path("id") String patientId);

    @PUT("patient/metrics/{id}")
    Observable<PatientInfo> editPatientMetrics(@Path("id") String id, @Body EditMetricsRequest pEditMetricsRequest);

    @GET("patient/confirm_phone/{phone}")
    Observable<ConfirmPhoneResponse> confirmPhone(@Path("phone") String phone, @Query("code") String code);

    @Multipart
    @POST("patient/photo/upload/{id}")
    Observable<PatientInfo> uploadPhoto(@Path("id") String id, @Part MultipartBody.Part photo);

    @POST("patient/chronic_diseases/{patientId}")
    Observable<PatientInfo> updateChronicDiseases(@Path("patientId") String patientId, @Body ChronicDiseasesPostRequest pChronicDiseasesPostRequest);

    @POST("patient/allergies/{patientId}")
    Observable<PatientInfo> updateAllergies(@Path("patientId") String patientId, @Body AllergiesPostRequest pAllergiesPostRequest);

    @GET("patient/recipes/{patient}")
    Observable<RecipeResponse> getRecipes(@Path("patient") String pid);

    
    
	////////////////////////////////CONSULTATION\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	/** Создание плановой консультации */
	@POST("consultation")
	Observable<Consultation> requestConsultation(@Body ConsultationRequest pConsultationRequest);
	
	/** Пошаговое создание онсультации (срочной и плановой) */
	@POST("consultation/wizard/{consultationId}/{step}")
	Observable<Object> setConsultationStep(@Path("consultationId") int consultationId, @Path("step") int step, @Body Object body);
	
	/** Загрузка картинки для консультации */
	@Multipart
	@POST("consultation/file/{consultationId}")
	Observable<Object> uploadConsultationImage(@Path("consultationId") int consultationId, @Part MultipartBody.Part pic, @Part("name") RequestBody name);

	/** Завершение создания плановой и срочной консультации */
	@GET("consultation/{cid}/create/finish")
	Observable<Consultation> consultationFinish(@Path("cid") int consultationId);
	
	/** Подтверждение плановой консультации в ответ на нотификацию */
	@GET("consultation/{consultationId}/start_time/delay")
	Observable<ConfirmConsultationResponse> confirmConsultation(@Path("consultationId") int consultationId);
	
	/** Получение плановой консультации */
	@GET("consultation/{cid}")
	Observable<Consultation> getConsultation(@Path("cid") int consultationId);

	/** Отмена консультации */
	@GET("consultation/{cid}/cancel")
	Observable<Void> cancelConsultation(@Path("cid") int consultationId);
	
	/** Завершение консультации */
	@POST("consultation/{cid}/finish")
	Observable<Void> finishConsultation(@Path("cid") int consultationId);
	
	/** Смена времени плановой консультации */
	@GET("consultation/{cid}/change_slot/{slot}")
	Observable<Consultation> changeSlotConsultation(@Path("cid") int consultationId, @Path("slot") int slotId);


	
	
	/** Создание срочной консультации с дежурным врачом */
	@POST("instant/consultation")
	Observable<Consultation> requestInstantConsultation(@Body InstantConsultationRequest pConsultationRequest);
	
	/** Создание срочной консультации с известным врачом */
	@POST("instant/consultation/{doctorId}")
	Observable<Consultation> requestInstantDoctorConsultation(@Path("doctorId") String doctorId, @Body InstantConsultationRequest pConsultationRequest);

	/** Начало срочной консультации */
	@GET("instant/consultation/{cid}/start")
	Observable<TwilioSession> startInstantConsultation(@Path("cid") int consultationId);

	
	/** Создание отзыва о враче по результатам консультации */
	@POST("consultation/comment/{cid}")
	Observable<Void> consultationFeedback(@Path("cid") int consultationId, @Body FeedbackRequest request);
	
    
    /** Список плановых консультаций пациента */
    @GET("patient/consultations/{patientId}")
    Observable<List<Consultation>> getPatientConsultations(@Path("patientId") String patientId);

    /** Список срочных консультаций пациента */
    @GET("patient/instant/consultations/{patientId}")
    Observable<List<Consultation>> getInstantPatientConsultations(@Path("patientId") String patientId);

    
    
    
    

    @GET("durations")
    Observable<List<IdNamePair>> getDurations();

    @GET("health_statuses")
    Observable<List<IdNamePair>> getHealthStatuses();

    @GET("symptoms")
    Observable<List<IdNamePair>> getSymptoms();

    /////////////////////////////////////////CLINICS\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @GET("clinics")
    Observable<List<Clinic>> getClinics();

    @GET("clinic/{clinic_id}")
    Observable<Clinic> getClinicById(@Path("clinic_id") int id);

    //////////////////////////////////////PAYMENT\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @GET("payments/{user}")
    Observable<List<PaymentInfo>> getUserPayments(@Path("user") String userId);

    @GET("payment/{payment}")
    Observable<PaymentInfo> getPaymentById(@Path("payment") String id);


    @GET("consultation/{id}/pay/request?_format=json")
    Observable<PaymentInfoWithoutCardInfo> requestPaymentLink(@Path("id") int consultationId);

    @GET("consultation/{id}/pay/request?_format=json")
    Observable<PaymentInfoWithoutCardInfo> requestPaymentLink(@Path("id") int consultationId, @Query("cardId") int cardId);

    @GET("user/cards/{user}")
    Observable<List<CardInfo>> getUserCards(@Path("user") String userId);
}

