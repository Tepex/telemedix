package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 03.11.2016.
 */

public class City implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("country")
    private Country country;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Country getCountry() {
        return country;
    }
}
