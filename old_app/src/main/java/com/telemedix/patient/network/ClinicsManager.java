package com.telemedix.patient.network;

import com.telemedix.patient.models.Clinic;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Aleksey on 25.10.2016.
 */

public class ClinicsManager {

    public static Observable<List<Clinic>> getClinics() {
        return Api.get.getClinics();
    }

    public static Observable<Clinic> getClinicById(int id) {
        if (id == -1) {
            return Observable.just(new Clinic());
        }
        return Api.get.getClinicById(id);
    }
}
