package com.telemedix.patient.network;

import android.content.Context;

import android.net.Uri;

import android.util.Log;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.PatientInfo;

import com.telemedix.patient.models.payment.CardInfo;

import com.telemedix.patient.network.request.AllergiesPostRequest;
import com.telemedix.patient.network.request.ChronicDiseasesPostRequest;
import com.telemedix.patient.network.request.Recipe;
import com.telemedix.patient.network.request.RecipeResponse;

import com.telemedix.patient.network.response.StringName;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;
import com.telemedix.patient.Utils;

import java.util.List;

import io.reactivex.Observable;

import static com.telemedix.patient.Constants.TAG;

public class PatientManager
{
	public static Observable<List<CardInfo>> getCards()
	{
		if(userCards != null) return Observable.just(userCards);
		else return Api.get.getUserCards(Hawk.get(Constants.PREF_USER_ID)).doOnNext(pCardInfos->userCards = pCardInfos);
	}
	
	public static Observable<PatientInfo> getUserInfo()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "getUserInfo. local:"+user);
		if(user == null || !user.isValid())
		{
			return Api.get.getCurrentUser()
				.doOnNext(pPatientInfo->
				{
					if(pPatientInfo.isValid())
					{
						Hawk.put(Constants.PREF_USER_ID, pPatientInfo.getUser().getId());
						if(pPatientInfo.getUser() != null && pPatientInfo.getUser().getPhone() != null)
							Hawk.put(Constants.PREF_USER_PHONE, pPatientInfo.getUser().getPhone());
						else Hawk.delete(Constants.PREF_USER_PHONE);
						user = pPatientInfo;
					}
				});
		}
		else return Observable.just(user);
	}
	
	public static void flushPatient()
	{
		user = null;
		Hawk.delete(Constants.PREF_USER_ID);
	}
	
	public static Observable<PatientInfo> flushAndGetInfo()
	{
		flushPatient();
		return getUserInfo();
	}
	
	public static Observable<PatientInfo> uploadMyPhoto(Context context, Uri image)
	{
		return uploadPhoto(Hawk.get(Constants.PREF_PATIENT_ID), Utils.getRealPathFromURI(context, image));
	}
	
	public static Observable<PatientInfo> uploadPhoto(String id, String path)
	{
		return Api.get.uploadPhoto(id, path).flatMap(pPatientInfo->flushAndGetInfo());
	}
	
	public static Observable<PatientInfo> updateChronics(List<StringName> chronics)
	{
		ChronicDiseasesPostRequest mPostRequest = new ChronicDiseasesPostRequest();
		mPostRequest.setDiseases(chronics);
		return Api.get.updateChronics(mPostRequest);
	}
	
	public static Observable<PatientInfo> updateAllergies(List<StringName> allergies)
	{
		if(allergies.size() > 0)
		{
			AllergiesPostRequest mPostRequest = new AllergiesPostRequest();
			mPostRequest.setAllergies(allergies);
			return Api.get.updateAllergies(mPostRequest);
		}
		else return getUserInfo();
	}
	
	public static Observable<List<Recipe>> getRecipes()
	{
		return Api.get.getRecipes(Hawk.get(Constants.PREF_PATIENT_ID)).map(RecipeResponse::getRecipes);
	}
	
	private static PatientInfo user = null;
	private static List<CardInfo> userCards = null;
}
