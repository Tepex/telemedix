package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConfirmConsultationResponse implements Serializable
{
	public boolean isSuccess()
	{
		return success;
	}
	
	public int getDelay()
	{
		return delay;
	}
	
	@SerializedName("success")
	private boolean success;
	@SerializedName("delay")
	private int delay;
}
