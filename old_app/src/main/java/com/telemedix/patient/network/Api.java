package com.telemedix.patient.network;

import android.content.Context;

import android.net.Uri;

import android.util.Log;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;
import com.telemedix.patient.Utils;

import com.telemedix.patient.models.BaseModel;
import com.telemedix.patient.models.BloodType;
import com.telemedix.patient.models.Clinic;
import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.DoctorService;
import com.telemedix.patient.models.Durations;
import com.telemedix.patient.models.GCMRegister;
import com.telemedix.patient.models.HealthStatuses;
import com.telemedix.patient.models.PatientInfo;

import com.telemedix.patient.models.payment.CardInfo;
import com.telemedix.patient.models.payment.PaymentInfo;
import com.telemedix.patient.models.payment.PaymentInfoWithoutCardInfo;

import com.telemedix.patient.models.Slot;
import com.telemedix.patient.models.Symptoms;
import com.telemedix.patient.models.TwilioSession;
import com.telemedix.patient.models.User;

import com.telemedix.patient.network.request.AddressRequest;
import com.telemedix.patient.network.request.AllergiesPostRequest;
import com.telemedix.patient.network.request.ChronicDiseasesPostRequest;
import com.telemedix.patient.network.request.ConsultationRequest;
import com.telemedix.patient.network.request.EditMetricsRequest;
import com.telemedix.patient.network.request.EditUserInfoRequest;
import com.telemedix.patient.network.request.FeedbackRequest;
import com.telemedix.patient.network.request.InstantConsultationRequest;
import com.telemedix.patient.network.request.Recipe;
import com.telemedix.patient.network.request.RecipeResponse;
import com.telemedix.patient.network.request.RegisterRequest;

import com.telemedix.patient.network.response.AppRegisterResponse;
import com.telemedix.patient.network.response.ConfirmConsultationResponse;
import com.telemedix.patient.network.response.ConfirmPhoneResponse;
import com.telemedix.patient.network.response.DoctorCity;
import com.telemedix.patient.network.response.RegisterResponse;

import com.telemedix.patient.utils.CacheManager;
import com.telemedix.patient.utils.PhoneData;

import java.io.File;
import java.io.IOException;

import java.util.concurrent.TimeUnit;

import java.util.LinkedList;
import java.util.List;

import okhttp3.logging.HttpLoggingInterceptor;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import retrofit2.adapter.rxjava2.HttpException;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.converter.gson.GsonConverterFactory;

import retrofit2.Retrofit;

import io.reactivex.android.schedulers.AndroidSchedulers;

import io.reactivex.Observable;

import io.reactivex.schedulers.Schedulers;

import timber.log.Timber;

import static com.telemedix.patient.Constants.TAG;

/**
 * Created by Aleksey on 13.09.2016.
 */
public enum Api {

    get;

    private final BOApiInterface boService;

    Api() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging)
                .addInterceptor(
                        chain -> {
                            Request original = chain.request();
                            Request.Builder requestBuilder = original.newBuilder()
                                    .method(original.method(), original.body());
                            requestBuilder
                                    .header("Content-Type", "application/json")
                                    .header("AppId", PhoneData.get.getUUID());

                            Request request = requestBuilder.build();
                            Response response = chain.proceed(request);

                            return response;
                        }
                );

        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        Retrofit retrofitBackoffice = new Retrofit.Builder()
                .baseUrl(Constants.BACKOFFICE_API_HOST)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        boService = retrofitBackoffice.create(BOApiInterface.class);
    }
    
	Observable<AppRegisterResponse> registerApp(String appId, String imei, String gcm)
	{
		return boService.registerApp(appId, imei, gcm)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}
   
    
	Observable<Void> sendConsultationNotification()
	{
		return boService.sendConsultationNotification()
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}

	Observable<ConfirmConsultationResponse> confirmConsultation(int consultationId)
	{
		return boService.confirmConsultation(consultationId)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}

    /*
    public Observable<AppRegisterResponse> registerGCM(GCMRegister pGCMRegister) {
        return boService.registerApp(PhoneData.get.getUUID(), pGCMRegister.getImei(), pGCMRegister.getGcmToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable ->
                {
                	if(BuildConfig.DEBUG) Log.e(TAG, "Register error!");
                	return new AppRegisterResponse();
                })
                .unsubscribeOn(Schedulers.io());
    }
    */
    
    Observable<List<Doctor>> getDoctors() {
        return boService.getDoctors("json")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    Observable<Doctor> getDoctor(String doctorId) {
        return boService.getDoctor(doctorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    Observable<List<DoctorService>> getDoctorServices(String doctorId) {
        return boService.getServicesByDoctor(doctorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    Observable<List<Slot>> getDoctorReceptionSlots(int serviceId, String from, String to) {
        String date = "";
        if (from != null || to != null) {
            date += "[";
        }
        if (from != null) {
            date += "\">" + from + "\"";
        }

        if (from != null && to != null) {
            date += ",";
        }

        if (to != null) {
            date += "\"<" + to + "\"";
        }
        if (!date.isEmpty()) {
            date += "]";
        }
        return boService.getReceptionSlots(serviceId, date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

	Observable<PatientInfo> getCurrentUser()
	{
		return getPatientById(Hawk.get(Constants.PREF_PATIENT_ID));
	}
	
	Observable<PatientInfo> getPatientById(String id)
	{
		return boService.getPatientById(id)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.onErrorReturn(pThrowable->
			{
				try
				{
					Log.e(TAG, pThrowable.getMessage());
				}
				catch (Exception ignored)
				{
				}
				return PatientInfo.getFailedPatientInfo();
			})
			.unsubscribeOn(Schedulers.io());
	}

    public Observable<PatientInfo> setCurrentUserInfo(String pFirstName, String pMiddleName,
                                                      String pLastName, String pEmail,
                                                      String relativeFirstName,
                                                      String relativeLastName, String relativePhone,
                                                      int countryId, String city, String address) {
        if (pFirstName == null || pFirstName.isEmpty()) {
            pFirstName = null;
        }
        if (pMiddleName == null || pMiddleName.isEmpty()) {
            pMiddleName = null;
        }
        if (pLastName == null || pLastName.isEmpty()) {
            pLastName = null;
        }
        if (pEmail == null || pEmail.isEmpty()) {
            pEmail = null;
        }
        if (relativeFirstName == null || relativeFirstName.isEmpty()) {
            relativeFirstName = null;
        }
        if (relativeLastName == null || relativeLastName.isEmpty()) {
            relativeLastName = null;
        }
        if (relativePhone == null || relativePhone.isEmpty()) {
            relativePhone = null;
        }
        if (city == null || city.isEmpty()) {
            city = null;
        }
        if (city == null || city.isEmpty()) {
            city = null;
        }

        return setPatientInfo(Hawk.get(Constants.PREF_PATIENT_ID),
                pFirstName, pMiddleName, pLastName, pEmail,
                relativeFirstName, relativeLastName, relativePhone,
                countryId, city, address);
    }

    Observable<PatientInfo> updateChronics(ChronicDiseasesPostRequest pChronicDiseasesPostRequest) {
        return boService.updateChronicDiseases(Hawk.get(Constants.PREF_PATIENT_ID), pChronicDiseasesPostRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> {
                    Log.e(TAG, pThrowable.getMessage());
                    return PatientInfo.getFailedPatientInfo();
                })
                .unsubscribeOn(Schedulers.io());
    }

    Observable<PatientInfo> updateAllergies(AllergiesPostRequest pAllergiesPostRequest) {
        return boService.updateAllergies(Hawk.get(Constants.PREF_PATIENT_ID), pAllergiesPostRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> {
                    Log.e(TAG, pThrowable.getMessage());
                    return PatientInfo.getFailedPatientInfo();
                })
                .unsubscribeOn(Schedulers.io());
    }

    public Observable<PatientInfo> setPatientInfo(String id, String pFirstName, String pMiddleName,
                                                  String pLastName, String pEmail,
                                                  String relativeFirstName,
                                                  String relativeLastName, String relativePhone,
                                                  int countryId, String city, String address) {

        EditUserInfoRequest request = new EditUserInfoRequest(
                new PatientInfo.ClosestRelative(relativeFirstName, relativeLastName, relativePhone),
                new AddressRequest(countryId, city, address),
                new User(pFirstName, pLastName, pMiddleName, pEmail));

        return boService.editPatientInfo(id, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> PatientInfo.getFailedPatientInfo())
                .unsubscribeOn(Schedulers.io());
    }

    public Observable<PatientInfo> setCurrentPatientMetrics(int pHeight, int pWeight,
                                                            BloodType pBloodType, String pSex,
                                                            String pBirthdate) {
        return setPatientMetrics(Hawk.get(Constants.PREF_PATIENT_ID), pHeight, pWeight,
                pBloodType, pSex, pBirthdate);
    }

    public Observable<PatientInfo> setPatientMetrics(String id, int pHeight, int pWeight,
                                                     BloodType pBloodType, String pSex,
                                                     String pBirthdate) {

        EditMetricsRequest request = new EditMetricsRequest(
                pHeight, pWeight, pBloodType, pSex, pBirthdate);

        return boService.editPatientMetrics(id, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> PatientInfo.getFailedPatientInfo())
                .unsubscribeOn(Schedulers.io());
    }

    public Observable<PatientInfo> uploadMyPhoto(Context context, Uri image) {
        return uploadPhoto(Hawk.get(Constants.PREF_USER_ID), Utils.getRealPathFromURI(context, image));
    }

    Observable<PatientInfo> uploadPhoto(String id, String photoPath) {
        File file = new File(photoPath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("user[photo]", file.getName(), requestFile);

        return boService.uploadPhoto(id, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> {
                    Log.e(TAG, "UploadPhoto: " + pThrowable.getMessage());
                    return PatientInfo.getFailedPatientInfo();
                })
                .unsubscribeOn(Schedulers.io());
    }


    
    
    /**
     * TODO: error handling http://bytes.babbel.com/en/articles/2016-03-16-retrofit2-rxjava-error-handling.html
     */
	Observable<RegisterResponse> register(String phone)
	{
		return boService.registerUserPhone(new RegisterRequest(phone))
			.subscribeOn(Schedulers.io())
//			.delay(10, TimeUnit.SECONDS)
			.observeOn(AndroidSchedulers.mainThread())
			.onErrorReturn(throwable->
			{
				if(throwable instanceof HttpException)
				{
					HttpException he = (HttpException)throwable;
					if(he.code() == 500) return RegisterResponse.getServerError(he.message());
					try
					{
						return new RegisterResponse(he.response().errorBody().string());
					}
					catch(IOException e)
					{
						Timber.e(e, "register phone error");
						return null;
					}
				}
				Timber.e("Network error");
				return null;
			})
			.unsubscribeOn(Schedulers.io());
	}
	
	Observable<ConfirmPhoneResponse> confirmPhone(String phone, String code)
	{
		return boService.confirmPhone(phone, code)
			.subscribeOn(Schedulers.io())
//			.delay(10, TimeUnit.SECONDS)
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}

    public Observable<Symptoms> getSymptoms() {
        if (CacheManager.instance().symptoms != null) {
            return Observable.just(CacheManager.instance().symptoms);
        } else {
            return boService.getSymptoms()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .onErrorReturn(pThrowable -> new LinkedList<>())
                    .map(pIdNamePairs -> {
                        Symptoms res = new Symptoms();
                        res.setItems(pIdNamePairs);
                        return res;
                    })
                    .doOnNext(pSymptoms -> CacheManager.instance().symptoms = pSymptoms);
        }
    }

    public Observable<Durations> getDurations() {
        if (CacheManager.instance().durations != null) {
            return Observable.just(CacheManager.instance().durations);
        } else {
            return boService.getDurations()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .onErrorReturn(pThrowable -> new LinkedList<>())
                    .map(pIdNamePairs -> {
                        Durations res = new Durations();
                        res.setItems(pIdNamePairs);
                        return res;
                    })
                    .doOnNext(pDurations -> CacheManager.instance().durations = pDurations);
        }
    }

    public Observable<HealthStatuses> getHealthStatuses() {
        if (CacheManager.instance().healthStatuses != null) {
            return Observable.just(CacheManager.instance().healthStatuses);
        } else {
            return boService.getHealthStatuses()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .onErrorReturn(pThrowable -> new LinkedList<>())
                    .map(pIdNamePairs -> {
                        HealthStatuses res = new HealthStatuses();
                        res.setItems(pIdNamePairs);
                        return res;
                    })
                    .doOnNext(pHealthStatuses -> CacheManager.instance().healthStatuses = pHealthStatuses);
        }
    }

    Observable<List<Clinic>> getClinics() {
        if (CacheManager.instance().clinics != null && CacheManager.instance().clinics.size() > 0) {
            return Observable.just(CacheManager.instance().clinics);
        } else {
            return boService.getClinics()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .onErrorReturn(pThrowable -> {
                        Log.e(TAG, pThrowable.getMessage());
                        return new LinkedList<>();
                    })
                    .doOnNext(pClinics -> CacheManager.instance().clinics = pClinics);
        }
    }

    Observable<Clinic> getClinicById(int id) {
        return boService.getClinicById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .onErrorReturn(pThrowable -> {
                    Log.e(TAG, pThrowable.getMessage());
                    return new Clinic();
                });

    }

    Observable<PaymentInfo> getPaymentById(String id) {
        return boService.getPaymentById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .onErrorReturn(pThrowable -> PaymentInfo.getInvalidPaymentInfo());
    }

    Observable<List<PaymentInfo>> getUserPayments(String userId) {
        return boService.getUserPayments(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> {
                    Log.e(TAG, pThrowable.getMessage());
                    return new LinkedList();
                })
                .unsubscribeOn(Schedulers.io());

    }

    Observable<PaymentInfoWithoutCardInfo> requestPaymentLink(int consultationId, int cardId) {
        return boService.requestPaymentLink(consultationId, cardId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .onErrorReturn(pThrowable -> new PaymentInfoWithoutCardInfo());
    }

    Observable<PaymentInfoWithoutCardInfo> requestPaymentLink(int consultationId) {
        return boService.requestPaymentLink(consultationId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .onErrorReturn(pThrowable -> {
                            Log.e(TAG, pThrowable.getMessage());
                            return new PaymentInfoWithoutCardInfo();
                        });
    }

    Observable<Consultation> getConsultation(int consultationId) {
        return boService.getConsultation(consultationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .onErrorReturn(pThrowable -> new Consultation());
    }


    Observable<List<Consultation>> getInstantPatientConsultations(String patientId) {
        return boService.getInstantPatientConsultations(patientId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .onErrorReturn(pThrowable -> new LinkedList<>());
    }

    Observable<List<Consultation>> getPatientConsultations(String patientId) {
        return boService.getPatientConsultations(patientId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .onErrorReturn(pThrowable -> new LinkedList<>());
    }

	Observable<Consultation> requestInstantConsultation()
	{
		return boService.requestInstantConsultation(new InstantConsultationRequest(Hawk.get(Constants.PREF_PATIENT_ID)))
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}
	
	Observable<Consultation> requestInstantDoctorConsultation(String doctorId)
	{
		return boService.requestInstantDoctorConsultation(doctorId, new InstantConsultationRequest(Hawk.get(Constants.PREF_PATIENT_ID)))
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}


    Observable<Consultation> requestScheduledConsultation(int serviceId, int slotId) {
        return boService.requestConsultation(new ConsultationRequest(Hawk.get(Constants.PREF_PATIENT_ID), serviceId, slotId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

    public Observable<List<CardInfo>> getUserCards(String userId) {
        return boService.getUserCards(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> new LinkedList())
                .unsubscribeOn(Schedulers.io());

    }

    Observable<List<DoctorCity>> getDoctorsCities() {
        return boService.getDoctorsCities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> new LinkedList<>())
                .unsubscribeOn(Schedulers.io());
    }

    Observable<Consultation> finishConsultationCreation(int cid) {
        return boService.consultationFinish(cid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> {
                    Log.e(TAG, pThrowable.getMessage());
                    return Consultation.createInvalidConsultation();
                })
                .unsubscribeOn(Schedulers.io());
    }


	Observable<TwilioSession> startInstantConsultation(TwilioSession session, int cid)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "startInstantConsultation(). session: "+session);
		if(session != null)
		{
			return Observable.just(session)
				.subscribeOn(Schedulers.io())
				.observeOn(Schedulers.io())
				.unsubscribeOn(Schedulers.io());
		}
		return boService.startInstantConsultation(cid)
			.subscribeOn(Schedulers.io())
			.observeOn(Schedulers.io())
			.onErrorReturn(t->
			{
				Log.e(TAG, t.getMessage(), t);
				return new TwilioSession();
			})
			.unsubscribeOn(Schedulers.io());
	}
	
	Observable<Void> finishConsultation(int cid)
	{
		return boService.finishConsultation(cid)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}
	
	Observable<Void> consultationFeedback(int cid, FeedbackRequest request)
	{
		return boService.consultationFeedback(cid, request)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}

	
	Observable<Void> cancelConsultation(int cid)
	{
		return boService.cancelConsultation(cid)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.unsubscribeOn(Schedulers.io());
	}
	
	

    Observable<RecipeResponse> getRecipes(String patient) {
        return boService.getRecipes(patient)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(pThrowable -> {
                    Log.e(TAG, pThrowable.getMessage());
                    return new RecipeResponse();
                })
                .unsubscribeOn(Schedulers.io());
    }

    public Observable<Object> setConsultationStep(int consultationId, int step, Object data) {
        return boService.setConsultationStep(consultationId, step, data)
                .onErrorReturn(throwable -> {
                    throwable.printStackTrace();
                    return null;
                });
    }

    public Observable<Object> uploadConsultationImage(int consultationId, String path) {
        File image = new File(path);
        RequestBody pic = RequestBody.create(MediaType.parse("multipart/form-data"), image);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "" + path.hashCode());

        MultipartBody.Part picPart =
                MultipartBody.Part.createFormData("file", image.getName(), pic);
        return boService.uploadConsultationImage(consultationId, picPart, name)
                .onErrorReturn(throwable -> {
                    throwable.printStackTrace();
                    return null;
                });
    }
}
