package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import com.telemedix.patient.models.PatientInfo;
import com.telemedix.patient.models.User;

import java.io.Serializable;

/**
 * Created by Aleksey on 06.10.2016.
 */
public class EditUserInfoRequest implements Serializable
{
	public EditUserInfoRequest(PatientInfo.ClosestRelative closestRelative, AddressRequest addressRequest, User user)
	{
		this.closestRelative = closestRelative;
		this.addressRequest = addressRequest;
		this.user = user;
	}
	
	@SerializedName("closestRelative")
	private PatientInfo.ClosestRelative closestRelative;
	@SerializedName("address")
	private AddressRequest addressRequest;
	@SerializedName("user")
	private User user;
}
