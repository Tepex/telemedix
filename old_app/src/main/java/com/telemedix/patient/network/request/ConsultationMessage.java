package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User 1337 on 24.11.2016.
 */

public class ConsultationMessage {
    @SerializedName("message")
    public String message;

    public ConsultationMessage(String message){
        this.message = message;
    }
}
