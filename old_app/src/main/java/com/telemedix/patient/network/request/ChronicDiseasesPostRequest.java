package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.network.response.StringId;
import com.telemedix.patient.network.response.StringName;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aleksey on 01.11.2016.
 */

public class ChronicDiseasesPostRequest  {
    @SerializedName("chronicDiseases")
    private List<StringName> diseases = new LinkedList<>();


    public List<StringName> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<StringName> pDiseases) {
        diseases = pDiseases;
    }
}
