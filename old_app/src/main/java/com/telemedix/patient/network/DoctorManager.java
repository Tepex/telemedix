package com.telemedix.patient.network;

import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.DoctorService;
import com.telemedix.patient.models.Slot;
import com.telemedix.patient.models.Service;
import com.telemedix.patient.models.Specialization;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;

public class DoctorManager
{
	public static Observable<List<Doctor>> getDoctors()
	{
		return Api.get.getDoctors().onErrorReturn(pThrowable->new LinkedList<>());
	}

	public static Observable<Doctor> getDoctor(String id)
	{
		return Api.get.getDoctor(id).onErrorReturn(pThrowable->new Doctor(null));
	}
	
	public static Observable<List<DoctorService>> getDoctorServices(String doctorId)
	{
		return Api.get.getDoctorServices(doctorId).onErrorReturn(pThrowable->new LinkedList<>());
	}
	
	public static Observable<List<Slot>> getDoctorReceptionSlots(int serviceId, String from, String to)
	{
		return Api.get.getDoctorReceptionSlots(serviceId, from, to).onErrorReturn(pThrowable->new LinkedList<>());
	}
}
