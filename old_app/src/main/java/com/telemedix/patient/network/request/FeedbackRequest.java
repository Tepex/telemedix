package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FeedbackRequest implements Serializable
{
	public FeedbackRequest(String msg, int rate)
	{
		this.msg = msg;
		this.rate = rate;
	}
	
	@SerializedName("message")
	private String msg;
	@SerializedName("rate")
	private int rate;
}
