package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddressRequest implements Serializable
{
	public AddressRequest(int pCountryId, String pCity, String pAddress)
	{
		countryId = pCountryId;
		city = pCity;
		address = pAddress;
	}
	
	public int getCountryId()
	{
		return countryId;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	@SerializedName("country")
	private int countryId;
	@SerializedName("city")
	private String city;
	@SerializedName("address")
	private String address;
}
