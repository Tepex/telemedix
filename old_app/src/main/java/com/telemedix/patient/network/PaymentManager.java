package com.telemedix.patient.network;


import com.orhanobut.hawk.Hawk;
import com.telemedix.patient.Constants;
import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.payment.PaymentInfo;
import com.telemedix.patient.models.payment.PaymentInfoWithConsultation;
import com.telemedix.patient.models.payment.PaymentInfoWithoutCardInfo;
import com.telemedix.patient.utils.CacheManager;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Aleksey on 21.10.2016.
 */

public class PaymentManager {

    private PaymentManager() {
    }

    private static PaymentInfoWithConsultation sPaymentInfoWithConsultation;

    private static PaymentInfo storePaymentInfo(PaymentInfo pPaymentInfo) {
        sPaymentInfoWithConsultation = new PaymentInfoWithConsultation(pPaymentInfo, null);
        return pPaymentInfo;
    }

    private static PaymentInfoWithConsultation getPaymentWithConsultation(Consultation pConsultation) {
        sPaymentInfoWithConsultation.setConsultation(pConsultation);
        return sPaymentInfoWithConsultation;
    }

    public static Observable<PaymentInfoWithConsultation> getPayment(int pId) {
        return Api.get.getPaymentById("" + pId)
                      .map(PaymentManager::storePaymentInfo)
                      .flatMap(pPaymentInfo -> ConsultationApiManager.getConsultation(pPaymentInfo.getConsultation().getId()))
                      .map(PaymentManager::getPaymentWithConsultation);
    }

    public static void flushCache() {
        if (CacheManager.instance().payments != null) {
            CacheManager.instance().payments.clear();
        }
    }

    public static Observable<List<PaymentInfo>> getPayments() {
        if (CacheManager.instance().payments != null && CacheManager.instance().payments.size() > 0) {
            return Observable.just(CacheManager.instance().payments);
        } else {
            return Api.get.getUserPayments(Hawk.get(Constants.PREF_USER_ID))
                          .doOnNext(pPaymentInfos -> CacheManager.instance().payments = pPaymentInfos);
        }
    }

    public static Observable<String> getPaymentLinkForConsultation(int id) {
        return Api.get.requestPaymentLink(id)
                      .map(PaymentInfoWithoutCardInfo::getPaymentUrl);
    }

    public static Observable<String> getPaymentLinkForConsultation(int id, int cardId) {
        return Api.get.requestPaymentLink(id, cardId)
                      .map(PaymentInfoWithoutCardInfo::getPaymentUrl);
    }
}
