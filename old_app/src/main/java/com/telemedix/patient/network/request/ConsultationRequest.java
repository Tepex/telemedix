package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConsultationRequest implements Serializable
{
	public ConsultationRequest(String patientId, int serviceId, int slotId)
	{
		this.patientId = patientId;
		this.serviceId = serviceId;
		this.slotId = slotId;
	}
	
	@SerializedName("patient")
	private String patientId;
	@SerializedName("service")
	private int serviceId;
	@SerializedName("receptionSlot")
	private int slotId;
}
