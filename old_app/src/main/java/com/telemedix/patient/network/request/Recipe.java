package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 24.11.2016.
 */

public class Recipe implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("dosage")
    private String dosage;
    @SerializedName("dosageForm")
    private String dosageForm;
    @SerializedName("receiveCircuit")
    private String receiveCircuit;
    @SerializedName("duration")
    private int duration;
    @SerializedName("createdAt")
    private String createdAt;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDosage() {
        return dosage;
    }

    public String getDosageForm() {
        return dosageForm;
    }

    public String getReceiveCircuit() {
        return receiveCircuit;
    }

    public int getDuration() {
        return duration;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
