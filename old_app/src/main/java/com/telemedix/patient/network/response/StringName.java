package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 13.10.2016.
 */

public class StringName implements Serializable {
    @SerializedName("name")
    public String name;

    public StringName() {}

    public StringName(String pName) {
        name = pName;
    }
}
