package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.models.BloodType;

import java.io.Serializable;

/**
 * Created by Aleksey on 06.10.2016.
 */

public class EditMetricsRequest implements Serializable {
    @SerializedName("height")
    private int height;
    @SerializedName("weight")
    private int weight;
    @SerializedName("bloodType")
    private String bloodType;
    @SerializedName("user")
    private UserMetrics userMetrics;

    public EditMetricsRequest(int pHeight, int pWeight, BloodType pBloodType, String pSex, String pBirthDate) {
        this.height = pHeight;
        this.weight = pWeight;
        this.bloodType = pBloodType.text;
        this.userMetrics = new UserMetrics(pSex, pBirthDate);
    }
}
