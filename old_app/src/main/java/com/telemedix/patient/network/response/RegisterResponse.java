package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;

import java.util.Properties;

import timber.log.Timber;

public class RegisterResponse implements Serializable
{
	public static RegisterResponse getServerError(String error)
	{
		RegisterResponse ret = new RegisterResponse();
		ret.error = error;
		return ret;
	}
	
	private RegisterResponse() {}
	
	/**
	 * TODO: Будет общая модель ошибочных ответов. 
	 */
	public RegisterResponse(String json)
	{
		int start = json.indexOf("errors\":[\"")+10;
		int end = json.indexOf("\"]}}}}}");
		Properties p = new Properties();
		try
		{
			p.load(new StringReader("key="+json.substring(start, end)));
			error = p.getProperty("key");
		}
		catch(IOException e)
		{
			Timber.e(e, "My fucking exception");
			error = "broken error";
		}
	}
	
	public boolean success()
	{
		return result;
	}
	
	public String getError()
	{
		return error;
	}
	
	@SerializedName("result")
	private boolean result;
	
	private transient String error; 
}
