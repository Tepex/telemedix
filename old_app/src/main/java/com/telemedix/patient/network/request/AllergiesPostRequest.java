package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.network.response.StringName;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aleksey on 01.11.2016.
 */

public class AllergiesPostRequest {
    @SerializedName("allergies")
    private List<StringName> allergies = new LinkedList<>();

    public List<StringName> getAllergies() {
        return allergies;
    }

    public void setAllergies(List<StringName> pAllergies) {
        allergies = pAllergies;
    }
}
