package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 24.10.2016.
 */

public class ConfirmPhoneRequestBody implements Serializable {
    @SerializedName("code")
    private String code;

    public ConfirmPhoneRequestBody(String pCode) {
        code = pCode;
    }

    public String getCode() {
        return code;
    }
}
