package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 13.10.2016.
 */

public class StringId implements Serializable {
    @SerializedName("id")
    public String id;
}
