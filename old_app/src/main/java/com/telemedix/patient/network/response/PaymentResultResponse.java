package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 21.10.2016.
 */

public class PaymentResultResponse implements Serializable {
    @SerializedName("success")
    private boolean success;
    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("orderStatus")
    private int orderStatus;
    @SerializedName("orderId")
    private int orderId = 0;

    public boolean isSuccess() {
        return success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public int getOrderId() {
        return orderId;
    }

    @Override
    public String toString() {
        if (success) {
            return "orderStatus: " + orderStatus + ", orderId: " + orderId;
        } else {
            return "errorMessage: " + errorMessage + ", orderId: " + orderId;
        }
    }
}
