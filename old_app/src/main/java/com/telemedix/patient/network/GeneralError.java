package com.telemedix.patient.network;

/**
 * Created by Aleksey on 31.10.2016.
 */

public class GeneralError extends Exception {
    public GeneralError(final String message) {
        super(message);
    }

    public GeneralError(final String message, final Throwable cause) {
        super(message, cause);
    }
}
