package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 03.11.2016.
 */

public class DoctorCity implements Serializable {
    @SerializedName("city")
    private String city;

    public String getCity() {
        return city;
    }
}
