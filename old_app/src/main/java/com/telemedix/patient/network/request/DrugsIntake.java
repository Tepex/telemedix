package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 13.10.2016.
 */

public class DrugsIntake implements Serializable {
    @SerializedName("name")
    private String name;
    @SerializedName("duration")
    private int duration;
    @SerializedName("consultation")
    private String consultation;

    public DrugsIntake(String pName, int pDuration, String pConsultation) {
        this.name = pName;
        this.duration = pDuration;
        this.consultation = pConsultation;
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public String getConsultation() {
        return consultation;
    }
}
