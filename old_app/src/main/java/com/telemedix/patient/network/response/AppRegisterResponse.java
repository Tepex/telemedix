package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppRegisterResponse implements Serializable
{
	public String getPatientId()
	{
		return patientId;
	}
	
	public boolean isNtSend()
	{
		return ntSend;
	}

	@SerializedName("patientId")
	private String patientId;
	@SerializedName("ntSend")
	private boolean ntSend;
}
