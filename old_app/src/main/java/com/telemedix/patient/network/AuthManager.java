package com.telemedix.patient.network;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.Constants;

import com.telemedix.patient.network.response.ConfirmPhoneResponse;
import com.telemedix.patient.network.response.RegisterResponse;

import io.reactivex.Observable;

import timber.log.Timber;

/**
 * Created by Aleksey on 31.10.2016.
 */
public class AuthManager
{
	public static Observable<RegisterResponse> registerPhone(String phone)
	{
		return Api.get.register(phone);
	}
	/*
	public static Observable<Boolean> registerPhone(String phone)
	{
		return Api.get.register(phone)
			.map(RegisterResponse::success)
			.doOnNext(pBoolean->
			{
				if(!pBoolean) throw Exceptions.propagate(new GeneralError("Phone registration failed"));
			});
	}
	*/
	
	public static Observable<Boolean> confirmPhone(String phone, String code)
	{
		return Api.get.confirmPhone(phone, code)
			.doOnNext(response->
			{
				if(response.success()) Hawk.put(Constants.PREF_PATIENT_ID, response.getPatientId());
			})
			.map(ConfirmPhoneResponse::success);
	}
}
