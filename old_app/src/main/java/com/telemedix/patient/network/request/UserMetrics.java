package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 06.10.2016.
 */

public class UserMetrics implements Serializable {
    @SerializedName("sex")
    private String sex;
    @SerializedName("birthDate")
    private String birthDate;

    public UserMetrics(String pSex, String pBirthDate) {
        this.sex = pSex;
        this.birthDate = pBirthDate;
    }
}
