package com.telemedix.patient.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;

import java.util.Properties;

import timber.log.Timber;

/**
 * TODO: Будет общая модель ошибочных ответов. 
	 
	 Ответ Гоши на неверный код:
	
--> GET http://backoffice.telemedix.com/api/patient/confirm_phone/79152265280?code=1112 http/1.1
D  --> END GET
D  <-- 200 OK http://backoffice.telemedix.com/api/patient/confirm_phone/79152265280?code=1112 (376ms)
D  Server: nginx/1.10.2
D  Date: Thu, 09 Feb 2017 15:17:55 GMT
D  Content-Type: application/json
D  Content-Length: 16
D  Connection: keep-alive
D  X-Powered-By: PHP/7.0.14
D  Cache-Control: private, must-revalidate
D  pragma: no-cache
D  expires: -1
D  {"result":false}
D  <-- END HTTP (16-byte body)
*/
public class ConfirmPhoneResponse implements Serializable
{
	public boolean success()
	{
		return result;
	}
	
	public String getPatientId()
	{
		return patientId;
	}
	
	@SerializedName("result")
	private boolean result;
	@SerializedName("patient_id")
	private String patientId;
}
