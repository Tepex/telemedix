package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InstantConsultationRequest implements Serializable
{
	public InstantConsultationRequest(String patientId)
	{
		this.patientId = patientId;
	}
	
	@SerializedName("patient")
	private String patientId;
}
