package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;
import com.telemedix.patient.network.response.RegisterResponse;

/**
 * Created by Aleksey on 06.10.2016.
 */

public class RegisterRequest {
    @SerializedName("user")
    private UserPhoneRegister user;

    public RegisterRequest(String phone) {
        this.user = new UserPhoneRegister(phone);
    }
}
