package com.telemedix.patient.network.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aleksey on 06.10.2016.
 */

public class UserPhoneRegister implements Serializable {
    @SerializedName("phone")
    private String phone;

    public UserPhoneRegister(String phone) {
        this.phone = phone;
    }
}
