package com.telemedix.patient.network;

import android.util.Log;
import android.util.SparseIntArray;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.TwilioSession;

import com.telemedix.patient.network.request.FeedbackRequest;
import com.telemedix.patient.network.response.ConfirmConsultationResponse;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;

import static com.telemedix.patient.Constants.TAG;

/**
 * Created by Aleksey on 24.10.2016.
 */

public class ConsultationApiManager
{
	public static Observable<Consultation> getConsultation(int id)
	{
		return Api.get.getConsultation(id);
	}
	
	public static Observable<Consultation> requestScheduledConsultation(int serviceId, int slotId)
	{
		return Api.get.requestScheduledConsultation(serviceId, slotId).onErrorReturn(pThrowable->Consultation.createInvalidConsultation());
	}
	
	public static Observable<Consultation> requestInstantConsultation()
	{
		return Api.get.requestInstantConsultation().onErrorReturn(pThrowable->Consultation.createInvalidConsultation());
	}
	
	public static Observable<Consultation> requestInstantDoctorConsultation(String doctorId)
	{
		return Api.get.requestInstantDoctorConsultation(doctorId).onErrorReturn(pThrowable->Consultation.createInvalidConsultation());
	}
	
	public static Observable<Consultation> finishCreation(int cid)
	{
		return Api.get.finishConsultationCreation(cid);
	}
	
	/**
	 * Возвращает сессию без запроса если она не null в левом потоке.
	 */
	public static Observable<TwilioSession> startInstantConsultation(TwilioSession session, int cid)
	{
		return Api.get.startInstantConsultation(session, cid);
	}
    
	public static Observable<Void> sendConsultationNotification()
	{
		return Api.get.sendConsultationNotification().onErrorReturn(t->null);
	}
	
	/** Нормальное завершение консультации пациентом. */
	public static Observable<Void> finishConsultation(int cid)
	{
		return Api.get.finishConsultation(cid).onErrorReturn(t->null);
	}
	
	/** Отмена консультации. */
	public static Observable<Void> cancelConsultation(int cid)
	{
		return Api.get.cancelConsultation(cid).onErrorReturn(t->null);
	}
	
	/**
	 * Подтверждение о скором начале консультации. В ответе приходит интервал времени в секундах через который
	 * необходимо начать видео сессию.
	 */
	public static Observable<ConfirmConsultationResponse> confirmConsultation(int consultationId)
	{
		return Api.get.confirmConsultation(consultationId).onErrorReturn(pThrowable->new ConfirmConsultationResponse());
	}
	
	public static Observable<List<Consultation>> getPatientConsultations()
	{
		if(consultationList != null && consultationList.size() > 0) return Observable.just(consultationList);
		else return Api.get.getPatientConsultations(Hawk.get(Constants.PREF_PATIENT_ID)).onErrorReturn(pThrowable->new LinkedList<>());
	}
	
	public static Observable<Void> consultationFeedback(int cid, String msg, int rate)
	{
		return Api.get.consultationFeedback(cid, new FeedbackRequest (msg, rate)).onErrorReturn(t->null);
	}
	
	
	public static void flushConsultations()
	{
		if(consultationList != null) consultationList.clear();
	}
	
	private static List<Consultation> consultationList;
	
	public static SparseIntArray STATES = new SparseIntArray(5);
	static
	{
		STATES.append(Consultation.State.CREATING.ordinal(), R.string.consultation_error_creating);
		STATES.append(Consultation.State.QUEUE.ordinal(), R.string.consultation_error_queue);
		STATES.append(Consultation.State.STARTING.ordinal(), R.string.consultation_error_starting);
		STATES.append(Consultation.State.FINISHED.ordinal(), R.string.consultation_error_finished);
		STATES.append(Consultation.State.CANCELED.ordinal(), R.string.consultation_error_canceled);
		STATES.append(Consultation.State.TIMEOUT.ordinal(), R.string.consultation_error_timeout);
	}
}
