package com.telemedix.patient.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BitmapUtils {
    public static final int MEDIA_TYPE_PICTURE = 1;

    public static Uri getOutputMediaFile(int type) {
        if (Environment.getExternalStorageState() != null) {
            // this works for Android 2.2 and above
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "telemedix");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File mediaFile;
            if (type == MEDIA_TYPE_PICTURE) {
                mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                        "IMG_" + timeStamp + ".jpg");
            } else {
                return null;
            }

            return Uri.fromFile(mediaFile);
        }

        return null;
    }

    public static Bitmap getCircleAvatar(Bitmap bitmapimg) {
        if (bitmapimg == null) return null;
        Bitmap dstBmp;

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmapimg.getWidth(), bitmapimg.getHeight());

        if (bitmapimg.getWidth() >= bitmapimg.getHeight()) {

            dstBmp = Bitmap.createBitmap(
                    bitmapimg,
                    bitmapimg.getWidth() / 2 - bitmapimg.getHeight() / 2,
                    0,
                    bitmapimg.getHeight(),
                    bitmapimg.getHeight()
            );

        } else {

            dstBmp = Bitmap.createBitmap(
                    bitmapimg,
                    0,
                    bitmapimg.getHeight() / 2 - bitmapimg.getWidth() / 2,
                    bitmapimg.getWidth(),
                    bitmapimg.getWidth()
            );
        }

        Bitmap output = Bitmap.createBitmap(dstBmp.getWidth(),
                dstBmp.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle((float) dstBmp.getWidth() / 2.0f, (float) dstBmp.getHeight() / 2.0f, (float) dstBmp.getWidth() / 2.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(dstBmp, rect, rect, paint);
        return output;
    }

    public static Bitmap getRotatedBitmap(ContentResolver contentResolver, Uri uri) {
        Bitmap bitmap = null;
        final String[] CONTENT_ORIENTATION = new String[]{
                MediaStore.Images.ImageColumns.ORIENTATION
        };
        try {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(contentResolver, uri);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (bitmap == null) return null;

        Cursor cursor = null;

        int orientation;
        try {
            cursor = contentResolver.query(uri, CONTENT_ORIENTATION, null, null, null);
            if (cursor == null || !cursor.moveToFirst()) {
                orientation = 0;
            }
            orientation = cursor.getInt(0);
        } catch (RuntimeException ignored) {
            // If the orientation column doesn't exist, assume no rotation.
            orientation = 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(orientation);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);
        return rotatedBitmap;
    }

    public static Bitmap rotateBitmapNexus(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }
}
