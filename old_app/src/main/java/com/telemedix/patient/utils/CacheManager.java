package com.telemedix.patient.utils;

import com.telemedix.patient.models.Clinic;
import com.telemedix.patient.models.Durations;
import com.telemedix.patient.models.HealthStatuses;
import com.telemedix.patient.models.Symptoms;
import com.telemedix.patient.models.payment.PaymentInfo;

import java.util.List;

/**
 * Created by Aleksey on 13.10.2016.
 */

public class CacheManager {

    private static CacheManager instance;

    private CacheManager() {}

    public static CacheManager instance() {
        if (instance == null) {
            instance = new CacheManager();
        }
        return instance;
    }

    public Symptoms       symptoms;
    public Durations      durations;
    public HealthStatuses healthStatuses;

    public List<Clinic> clinics;

    public List<PaymentInfo> payments;
}
