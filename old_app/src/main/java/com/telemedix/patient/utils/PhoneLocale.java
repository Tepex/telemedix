package com.telemedix.patient.utils;

public class PhoneLocale implements Comparable<PhoneLocale>
{
	public PhoneLocale(int countryCode, String locale)
	{
		this.countryCode = countryCode;
		this.locale = locale;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;
		PhoneLocale that = (PhoneLocale)o;
		if(locale == null || that.locale == null) return false;
		return locale.equals(that.locale);
	}
    
	@Override
	public int hashCode()
	{
		return locale != null ? locale.hashCode() : 0;
	}
	
	@Override
	public String toString()
	{
		return locale;
	}
	
	public String getCode()
	{
		return ""+countryCode;
	}
	
	@Override
	public int compareTo(PhoneLocale other)
	{
		return locale.compareTo(other.locale);
	}
	
	private final int countryCode;
	private final String locale;
}
