package com.telemedix.patient.utils;

import android.content.Context;

import com.telemedix.patient.R;
import com.telemedix.patient.models.Slot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.joda.time.LocalDate;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class DateHelper {

    public static Calendar today() {
        Calendar c = Calendar.getInstance();
        return c;
    }

    public static Calendar tomorrow() {
        Calendar c = today();
        c.add(Calendar.DAY_OF_YEAR, 1);
        return c;
    }

    public static Calendar afterTomorrow() {
        Calendar c = today();
        c.add(Calendar.DAY_OF_YEAR, 2);
        return c;
    }

    public static boolean compareDatesByDay(Date d1, Date d2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", new Locale("ru"));
        return sdf.format(d1).equals(sdf.format(d2));
    }

    /*
    public static Date getFromServerString(String date) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("ru"));
        try {
            return serverFormat.parse(date);
        } catch (ParseException pE) {
            pE.printStackTrace();
            return new Date();
        }
    }
    */

    /*
    public static String getFormatedTime(Date date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd\nMMMM\nyyyy\nHH-mm", new Locale("ru"));
        return simpleDateFormat.format(date);
    }
    */

    public static String getShortDate(String dateTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("ru"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd.MM.yyyy", new Locale("ru"));
        if (dateTime == null || dateTime.isEmpty()) {
            return "";
        }
        try {
            return outputFormat.format(simpleDateFormat.parse(dateTime));
        } catch (ParseException pE) {
            pE.printStackTrace();
            return "Invalid date";
        }
    }

    public static String getTime(String dateTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("ru"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm", new Locale("ru"));
        try {
            return outputFormat.format(simpleDateFormat.parse(dateTime));
        } catch (ParseException pE) {
            pE.printStackTrace();
            return "Invalid date";
        }
    }

    /*
    public static String getLongDate(String dateTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("ru"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", new Locale("ru"));
        try {
            return outputFormat.format(simpleDateFormat.parse(dateTime));
        } catch (ParseException | NullPointerException pE) {
            pE.printStackTrace();
            return "Invalid date";
        }
    }
    */

    public static String getFormatedTimeForConsultationList(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", new Locale("ru"));
        return simpleDateFormat.format(date);
    }

    public static String convertTimeToServer(String date) {
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd.MM.yyyy", new Locale("ru"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("ru"));
        try {
            return outputFormat.format(simpleDateFormat.parse(date));
        } catch (ParseException pE) {
            pE.printStackTrace();
            return "Invalid date";
        }
    }

    public static String convertServerTimeToChat(String dateTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", new Locale("ru"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", new Locale("ru"));
        try {
            return outputFormat.format(simpleDateFormat.parse(dateTime));
        } catch (ParseException pE) {
            pE.printStackTrace();
            return "Invalid date";
        }
    }
}
