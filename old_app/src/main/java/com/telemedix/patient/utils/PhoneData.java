package com.telemedix.patient.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.orhanobut.hawk.Hawk;
import com.telemedix.patient.Constants;

import java.util.UUID;

/**
 * Created by Aleksey on 13.09.2016.
 */
public enum PhoneData
{
	get;
	
	public String getIMEI(Context pContext)
	{
		TelephonyManager telephonyManager = (TelephonyManager)pContext.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}
	
	public String getUUID()
	{
		if (Hawk.contains(Constants.PREF_APP_ID)) return Hawk.get(Constants.PREF_APP_ID);
		else
		{
			String appId = UUID.randomUUID().toString();
			Hawk.put(Constants.PREF_APP_ID, appId);
			return appId;
		}
	}
}
