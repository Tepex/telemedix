package com.telemedix.patient.base;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import android.support.design.widget.Snackbar;

import android.support.v4.content.LocalBroadcastManager;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;

import android.support.v7.widget.Toolbar;

import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.telemedix.patient.activities.application.ConsultApplicationActivity;
import com.telemedix.patient.activities.call.ConversationV2Activity;
import com.telemedix.patient.activities.doctor.DoctorSingleActivity;
import com.telemedix.patient.activities.HomeActivity;

import com.telemedix.patient.dialogs.ConsultationNotifyDialog;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.TwilioSession;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.R;

import java.util.concurrent.TimeoutException;

import retrofit2.Response;

import static com.telemedix.patient.Constants.TAG;

/**
 * Created by baneizalfe on 5/13/16.
 */
public class BaseActivity extends AppCompatActivity
{
	@CallSuper
	protected void onCreate(@Nullable Bundle savedInstanceState, @LayoutRes int layout)
	{
		super.onCreate(savedInstanceState);
		setContentView(layout);
		unbinder = ButterKnife.bind(this);
		if(toolbar != null)
		{
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);		
		}
	}
	
	@Override
	@CallSuper
	protected void onStart()
	{
		super.onStart();
		if(BuildConfig.DEBUG) Log.d(TAG, "start activity: "+getClass().getSimpleName());
		
		int delay = getIntent().getIntExtra(EXTRA_CONSULTATION_DELAY, -1);
		if(delay != -1)
		{
			Consultation consultation = (Consultation)getIntent().getSerializableExtra(ConsultApplicationActivity.EXTRA_CONSULTATION);
			TwilioSession twilioSession = (TwilioSession)getIntent().getSerializableExtra(ConversationV2Activity.EXTRA_CONSULTATION_RESPONSE);
			consultationNotifyDialog(consultation, twilioSession, delay);
		}
	}
	
	@Override
	@CallSuper
	protected void onDestroy()
	{
		dismissProgressDialog();
		if(dialog != null) dialog.dismiss();
		if(unbinder != null) unbinder.unbind();
		super.onDestroy();
	}

	protected boolean checkHomeButton(MenuItem item)
	{
		if(item.getItemId() != android.R.id.home) return onOptionsItemSelected(item);
		finish();
		return true;
	}
	
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(final MenuItem item)
	{
		if(item.getItemId() == android.R.id.home)
		{
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void showSnackbar(String message)
	{
		showSnackbar(findViewById(android.R.id.content), message);
	}
	
	public void showSnackbar(View view, String message)
	{
		Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
	}
	
	public void showSnackbar(View view, @StringRes int msgId)
	{
		Snackbar.make(view, msgId, Snackbar.LENGTH_LONG).show();
	}
	
	public void handleError(Response<?> response, boolean snack)
	{
//        GenericError restError = ErrorUtils.parseError(response);
//        String errorMessage = (restError != null && restError.message != null) ? restError.message : getString(R.string.common_error);
//        if (snack)
//            showSnackbar(errorMessage);
//        else
//            showAlertDialog(errorMessage);
	}

	public void handleError(Response<?> response)
	{
		handleError(response, false);
	}
	
	public void handleError(Throwable t)
	{
		if(t instanceof TimeoutException) showSnackbar(getString(R.string.network_error));
		else showSnackbar(getString(R.string.common_error));
	}
	
	public void showProgressDialog()
	{
		showProgressDialog(getString(R.string.please_wait));
	}
	
	public void showProgressDialog(String message)
	{
		dismissProgressDialog();
		progressDialog = new ProgressDialog(this);
		progressDialog.setIndeterminate(true);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setMessage(message);
		progressDialog.show();
	}
	
	public void dismissProgressDialog()
	{
		if(progressDialog != null) progressDialog.dismiss();
	}
	
	public void showAlertDialog(String message)
	{
		showAlertDialog(null, message, getString(R.string.ok), true, null);
	}
	
	public void showAlertDialog(String title, String message)
	{
		showAlertDialog(title, message, getString(R.string.ok), true, null);
	}
	
	public void showAlertDialog(String title, String message, String positive, boolean cancelable, final AlertDialogListener listener)
	{
		if(dialog != null) dialog.dismiss();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if(title != null) builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(positive, (dialogInterface, i)->{if(listener != null) listener.onConfirmed();});
		builder.setCancelable(cancelable);
		dialog = builder.show();
		dialog.setCanceledOnTouchOutside(false);
	}
	
	public void showConfirmDialog(String title, String message, String positive, String negative, boolean cancelable, final ConfirmDialogListener listener)
	{
		if(dialog != null) dialog.dismiss();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if(title != null) builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(positive, (dialogInterface, i) -> {if(listener != null) listener.onConfirmed();});
		builder.setNegativeButton(negative, (dialogInterface, i) -> {if(listener != null) listener.onDeclined();});
		builder.setCancelable(cancelable);
		dialog = builder.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	public void showConfirmDialog(String title, String message, ConfirmDialogListener listener)
	{
		showConfirmDialog(title, message, getString(R.string.ok), getString(R.string.cancel), true, listener);
	}
	
	public void showConfirmDialog(String message, String positive, String negative, ConfirmDialogListener listener)
	{
		showConfirmDialog(null, message, positive, negative, true, listener);
	}
	
	public void selectDoctor(Doctor doctor)
	{
		Intent intent = new Intent(this, DoctorSingleActivity.class);
		Bundle args=  new Bundle();
		args.putSerializable(DoctorSingleActivity.ARG_DOCTOR, doctor);
		intent.putExtras(args);
		startActivity(intent);
	}
	
	protected void goHome()
	{
		goHomeAndStartActivity(null);
	}
	
	protected void goHomeAndStartActivity(Intent newIntent)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "goHome() from "+getClass().getSimpleName()+" and start new Activity: "+newIntent);
		Intent intent = new Intent(this, HomeActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		if(newIntent != null) intent.putExtras(newIntent);
		startActivity(intent);
		finish();
	}
	
	public void consultationNotifyDialog(final Consultation consultation, final TwilioSession twilioSession, final int delay)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Slot consultation receiver. Consultation: "+consultation+" "+delay+" sec");
		if(notifyDialog != null) notifyDialog.dismiss();
		if(consultation.getState() == Consultation.State.QUEUE ||
			consultation.getState() == Consultation.State.STARTING ||
			consultation.getId() == DEBUG_CONSULTATION_ID)
		{
			notifyDialog = ConsultationNotifyDialog.showDialog(this, delay);
			if(BuildConfig.DEBUG) Log.d(TAG, "Start wait for "+delay+" sec");
			long millis = System.currentTimeMillis();
			// Для тестирования и отладки ставим будильник на 30 сек. 
			if(delay == 0) millis += 30*1000;
			else millis += delay * 1000;
					
			if(BuildConfig.DEBUG) Log.d(TAG, "Twilio will start after "+delay+" sec for "+consultation);
		
			Intent intent = new Intent(this, ConversationV2Activity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(ConsultApplicationActivity.EXTRA_CONSULTATION, consultation);
			intent.putExtra(ConversationV2Activity.EXTRA_CONSULTATION_RESPONSE, twilioSession);
			PendingIntent pendingIntent = PendingIntent.getActivity(this, consultation.getId(), intent, PendingIntent.FLAG_ONE_SHOT);
			((AlarmManager)getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, millis, pendingIntent);
			if(BuildConfig.DEBUG) Log.d(TAG, "set pending intent for consultation: "+consultation.getId()+", delay: "+delay+" sec.");
		}
	}
	
	public void cancelConsultationByDoctor()
	{
		if(notifyDialog != null) notifyDialog.cancel();
		showAlertDialog(getString(R.string.consultation_canceled_by_doctor));
	}
	
	public interface ConfirmDialogListener
	{
		void onConfirmed();
		void onDeclined();
	}
	
	public interface AlertDialogListener
	{
		void onConfirmed();
	}
	
	@Nullable
	@BindView(R.id.toolbar) Toolbar toolbar;
	
	protected AppCompatDialog dialog;
	protected AlertDialog notifyDialog;
	protected ProgressDialog progressDialog;
	protected Unbinder unbinder;
	
	public static final String EXTRA_CONSULTATION = "extra_consultation";
	public static final String EXTRA_CONSULTATION_ID = "extra_consultation_id";
	public static final String EXTRA_CONSULTATION_DELAY = "extra_consultation_delay";
	public static final String EXTRA_ELAPSED_CONSULTATION_TIME = "extra_elapsed_consultation_time";
	public static final String EXTRA_ACTIVITY_START_CLASS = "extra_activity_start_class";

	public static final String EXTRA_NEW_CONTACT = "extra_new_contact";

	public static final int DEBUG_CONSULTATION_ID = 6;
}
