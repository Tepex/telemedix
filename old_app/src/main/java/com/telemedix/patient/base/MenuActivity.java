package com.telemedix.patient.base;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.PackageManager;

import android.graphics.Bitmap;

import android.Manifest;

import android.net.Uri;

import android.os.Bundle;

import android.preference.PreferenceManager;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

import android.support.design.widget.NavigationView;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.Thing;

import com.orhanobut.hawk.Hawk;

import com.telemedix.patient.App;

import com.telemedix.patient.activities.application.ConsultApplicationActivity;
import com.telemedix.patient.activities.doctor.MyDoctorsActivity;

import com.telemedix.patient.activities.call.ConversationV2Activity;
import com.telemedix.patient.activities.call.InCallActivity;
import com.telemedix.patient.activities.call.voip.ClientActivity;

import com.telemedix.patient.activities.ContactActivity;
import com.telemedix.patient.activities.HomeActivity;
import com.telemedix.patient.activities.InsuranceListActivity;
import com.telemedix.patient.activities.MyClinicsActivity;
import com.telemedix.patient.activities.PaymentsHistoryListActivity;
import com.telemedix.patient.activities.PaymentsListActivity;
import com.telemedix.patient.activities.SecurityActivity;

import com.telemedix.patient.activities.registration.MyInfoActivity;
//import com.telemedix.patient.activities.registration.RegisterActivity;
import com.telemedix.patient.view.PhoneActivity;

import com.telemedix.patient.dialogs.EmergencyCallDialog;
import com.telemedix.patient.dialogs.SignUpRequiredDialog;

import com.telemedix.patient.fragments.IncomingCallFragment;
import com.telemedix.patient.gcm.GcmPreferences;

import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.models.Doctor;
import com.telemedix.patient.models.PatientInfo;
import com.telemedix.patient.models.TwilioSession;

import com.telemedix.patient.network.AppManager;
import com.telemedix.patient.network.ConsultationApiManager;
import com.telemedix.patient.network.PatientManager;

import com.telemedix.patient.BuildConfig;
import com.telemedix.patient.Constants;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;
import com.telemedix.patient.utils.PhoneData;

import static com.telemedix.patient.Constants.TAG;

/**
 * Created by User 1337 on 31.10.2016.
 */
public abstract class MenuActivity extends BaseActivity implements IncomingCallFragment.OnCallInteractionListener
{
	@Override
	@CallSuper
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, getLayoutResId());
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
		
		nav_view.setNavigationItemSelectedListener(selectionListener);
		if(nav_view.getMenu().findItem(R.id.support_title) != null) nav_view
			.getMenu()
			.findItem(R.id.support_title)
			.setTitle(String.format(getString(R.string.label_support_version), ""+BuildConfig.VERSION_CODE));
			
		View headerView = nav_view.getHeaderView(0);
		
		initViews();
		
		if(this instanceof HomeActivity) nav_view.getMenu().findItem(R.id.action_home_screen).setChecked(true);
		else if(this instanceof PaymentsListActivity) nav_view.getMenu().findItem(R.id.action_payment_methods).setChecked(true);
		else if(this instanceof PaymentsHistoryListActivity) nav_view.getMenu().findItem(R.id.action_payment_history).setChecked(true);
		else if(this instanceof MyDoctorsActivity) nav_view.getMenu().findItem(R.id.action_my_doctors).setChecked(true);
		else if(this instanceof ContactActivity) nav_view.getMenu().findItem(R.id.action_contact_data).setChecked(true);
		else if(this instanceof MyClinicsActivity) nav_view.getMenu().findItem(R.id.action_my_clinics).setChecked(true);
		else if(this instanceof SecurityActivity) nav_view.getMenu().findItem(R.id.action_safety).setChecked(true);
		else if(this instanceof InsuranceListActivity) nav_view.getMenu().findItem(R.id.action_my_documents).setChecked(true);
		else if(this instanceof MyInfoActivity) nav_view.getMenu().findItem(R.id.action_my_information).setChecked(true);
	}
	
	private void updateSideBar(PatientInfo pPatientInfo)
	{
		View headerView = nav_view.getHeaderView(0);
		if(pPatientInfo.isValid())
		{
			final ImageView nav_avatar = (ImageView) headerView.findViewById(R.id.nav_avatar);
			Glide.with(this)
				.load(pPatientInfo.getUser().getPhoto())
				.asBitmap()
				.placeholder(R.drawable.avatar_bg)
				.centerCrop()
				.into(new BitmapImageViewTarget(nav_avatar)
				{
					@Override
					protected void setResource(Bitmap resource)
					{
						RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
						circularBitmapDrawable.setCircular(true);
						nav_avatar.setImageDrawable(circularBitmapDrawable);
					}
				});
			((TextView)headerView.findViewById(R.id.nav_username)).setText(pPatientInfo.getUser().getLastName()+"\n"+
				pPatientInfo.getUser().getFirstName()+" "+pPatientInfo.getUser().getPatronymic());
			((TextView)headerView.findViewById(R.id.nav_email)).setText(pPatientInfo.getUser().getEmail());
		}
	}
	
	@Override
	@CallSuper
	protected void onStart()
	{
		super.onStart();
		nav_view.getMenu().findItem(R.id.action_my_test).setVisible(App.getApp().isDevMode());
		nav_view.getMenu().findItem(R.id.action_logout).setVisible(App.getApp().isDevMode());
	}
	
	@Override
	@CallSuper
	protected void onPostResume()
	{
		PatientManager.getUserInfo().subscribe(this::updateSideBar);
		super.onPostResume();
	}
	
	@Override
	@CallSuper
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == android.R.id.home)
		{
			drawer_layout.openDrawer(GravityCompat.START);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onVideo()
	{
		/*
		callFragment = InCallFragment.newInstance();
		getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.callFragmentContainer, callFragment)
			.commit();
		*/
		
		closeIncomingCall();
		startActivity(new Intent(this, InCallActivity.class));
	}
	
	@Override
	public void onHangUp()
	{
		closeIncomingCall();
	}
	
	@Override
	public void onTake()
	{
		/*
		callFragment = InCallFragment.newInstance();
		getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.callFragmentContainer, callFragment)
			.commit();
		*/
		
		closeIncomingCall();
		startActivity(new Intent(this, InCallActivity.class));
	}
	
	protected void handleEmergencyCall()
	{
		EmergencyCallDialog.getDialog(this, new EmergencyCallDialog.DialogClickListener()
		{
			@Override
			public void onEmergencyCallClick()
			{
				try
				{
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:112"));
					/*
					callIntent.putExtra("simSlot", 0);
					callIntent.putExtra("com.android.phone.extra.slot", 0);
					*/
					if(ActivityCompat.checkSelfPermission(MenuActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
					{
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
						return;
					}
					MenuActivity.this.startActivity(callIntent);
				}
				catch(ActivityNotFoundException activityException)
				{
					if(BuildConfig.DEBUG) Log.e("Calling a Phone Number", "Call failed", activityException);
				}
			}
			
			@Override
			public void onEmergencyCancelClick() {}
		}).show();
	}
	
	protected boolean checkSigningIn()
	{
		if(Utils.isUserLoggedIn()) return true;
		else
		{
			SignUpRequiredDialog.getRequiredAccessDialog(this, new SignUpRequiredDialog.DialogClickListener()
			{
				@Override
				public void onRegisterClick()
				{
					Intent intent = new Intent(MenuActivity.this, PhoneActivity.class);
					startActivity(intent);
				}
				
				@Override
				public void onRegisterCancelClick() {}
			}).show();
			return false;
		}
	}
	
	protected void startHomeActivity()
	{
		if(this instanceof HomeActivity) return;
		Intent intent = new Intent(this, HomeActivity.class);
		startActivity(intent);
	}
	
	protected void startHomeActivityForSuccesfullRegistration()
	{
		if(this instanceof HomeActivity) return;
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra(HomeActivity.INTENT_ACTION, HomeActivity.ACTION_SUCCESSFULL_REGISTRATION);
		startActivity(intent);
	}
	
	protected void startPaymentMethodsActivity()
	{
		if(this instanceof PaymentsListActivity) return;
		if(checkSigningIn())
		{
			Intent intent = new Intent(this, PaymentsListActivity.class);
			startActivity(intent);
		}
	}
	
	protected void startPaymentHistoryActivity()
	{
		if(this instanceof PaymentsHistoryListActivity) return;
		if(checkSigningIn())
		{
			Intent intent = new Intent(this, PaymentsHistoryListActivity.class);
			startActivity(intent);
		}
	}
	
	protected void startMyDoctorsActivity()
	{
		if(this instanceof MyDoctorsActivity) return;
		if(checkSigningIn())
		{
			Intent intent = new Intent(this, MyDoctorsActivity.class);
			startActivity(intent);
		}
	}
	
	protected void startContactActivity()
	{
		if(this instanceof ContactActivity) return;
		if (checkSigningIn())
		{
			Intent intent = new Intent(this, ContactActivity.class);
			startActivity(intent);
		}
	}
	
	protected void startMyClinicsActivity()
	{
		if(this instanceof MyClinicsActivity) return;
		if(checkSigningIn())
		{
			Intent intent = new Intent(this, MyClinicsActivity.class);
			startActivity(intent);
		}
	}
	
	protected void startSecurityActivity()
	{
		if(this instanceof SecurityActivity) return;
		if(checkSigningIn())
		{
			Intent intent = new Intent(this, SecurityActivity.class);
			startActivity(intent);
		}
	}
	
	protected void startInsuranceActivity()
	{
		if(this instanceof InsuranceListActivity) return;
		if(checkSigningIn())
		{
			Intent intent = new Intent(this, InsuranceListActivity.class);
			startActivity(intent);
		}
	}
	
	protected void startMyInfoActivity()
	{
		if(this instanceof MyInfoActivity) return;
		if(checkSigningIn())
		{
			Intent intent = new Intent(this, MyInfoActivity.class);
			startActivity(intent);
		}
	}
	
	protected void openIncomingCall()
	{
		callFragment = IncomingCallFragment.newInstance();
		getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.callFragmentContainer, callFragment)
			.commit();
	}
	
	protected void openTwilioActivity(Consultation consultation, TwilioSession response)
	{
		Intent intent = new Intent(this, ConversationV2Activity.class);
		if(consultation != null) intent.putExtra(ConsultApplicationActivity.EXTRA_CONSULTATION, consultation);
		if(response != null) intent.putExtra(ConversationV2Activity.EXTRA_CONSULTATION_RESPONSE, response);
		startActivity(intent);
	}
	
	protected void openVOIPActivity()
	{
		startActivity(new Intent(this, ClientActivity.class));
	}
    
	private void myTest()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "My test");
		ConsultationApiManager.sendConsultationNotification()
			.subscribe(nothing->
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "notification sent");
			});	
	}
	
	private void logout()
	{
		
	}
	
	private void closeIncomingCall()
	{
		if(callFragment != null)
		{
			getSupportFragmentManager()
				.beginTransaction()
				.remove(callFragment)
				.commit();
		}
	}
	
	/**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
	public Action getIndexApiAction()
	{
		Thing object = new Thing.Builder()
			.setName("Menu Page") // TODO: Define a title for the content shown.
			// TODO: Make sure this auto-generated URL is correct.
			.setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
			.build();
		return new Action.Builder(Action.TYPE_VIEW)
			.setObject(object)
			.setActionStatus(Action.STATUS_TYPE_COMPLETED)
			.build();
	}
	
	protected abstract int getLayoutResId();
	protected abstract void initViews();
	
	@BindView(R.id.drawer_layout)
	protected DrawerLayout drawer_layout;
	@BindView(R.id.nav_view)
	protected NavigationView nav_view;
	protected Fragment callFragment = null;
	
	private NavigationView.OnNavigationItemSelectedListener selectionListener = item ->
	{
		for(int i = 0; i < nav_view.getMenu().size(); ++i)
		{
			nav_view.getMenu().getItem(i).setChecked(false);
			if(nav_view.getMenu().getItem(i).getSubMenu() != null)
				for(int j = 0; j < nav_view.getMenu().getItem(i).getSubMenu().size(); j++)
					nav_view.getMenu().getItem(i).getSubMenu().getItem(j).setChecked(false);
		}
		nav_view.invalidate();
		
		switch(item.getItemId())
		{
			case R.id.action_emergency_call:
				handleEmergencyCall();
				break;
			case R.id.action_home_screen:
				startHomeActivity();
				break;
			case R.id.action_payment_methods:
				startPaymentMethodsActivity();
				break;
			case R.id.action_payment_history:
				startPaymentHistoryActivity();
				break;
			case R.id.action_my_doctors:
				startMyDoctorsActivity();
				break;
			case R.id.action_contact_data:
				startContactActivity();
				break;
			case R.id.action_my_clinics:
				startMyClinicsActivity();
				break;
			case R.id.action_safety:
				startSecurityActivity();
				break;
			case R.id.action_my_documents:
				startInsuranceActivity();
				break;
			case R.id.action_my_information:
				startMyInfoActivity();
				break;
			/*
			case R.id.action_test_call_fake:
				openIncomingCall();
				break;
			case R.id.action_test_call_twilio:
				openTwilioActivity(null, null);
				break;
			case R.id.action_test_call_VOIP:
				openVOIPActivity();
				break;
			*/
			case R.id.action_my_test:
				myTest();
			case R.id.action_logout:
				logout();
			default:
				item.setChecked(true);
		}
		drawer_layout.closeDrawers();
		return true;
	};
}
