package com.telemedix.patient;

import com.telemedix.core.utils.Config;

import com.telemedix.patient.utils.PhoneData;

public class Constants
{
	public static Config getConfig(boolean isDebug)
	{
		if(config == null)
		{
			config = new Config();
			config.UUID = PhoneData.get.getUUID();
			config.API_HOST = BACKOFFICE_API_HOST;
			config.IS_DEBUG = isDebug;
		}
		return config;
	}
	
	public static final String TAG = "Telemedix";
	public final static String PREF_USER_BIRTHDAY = "pref_user_birthday";
	public final static String PREF_USER_HEIGHT = "pref_user_height";
	public final static String PREF_USER_WEIGHT = "pref_user_weight";
	public final static String PREF_USER_SEX = "pref_user_sex";
	public final static String PREF_USER_BLOOD_TYPE = "pref_user_blood_type";
	public final static String PREF_USER_AVATAR = "pref_user_avatar";
	
	public final static String PREF_USER_PIN = "pref_user_pin";
	public final static String PREF_USER_ID  = "pref_user_id";
	public final static String PREF_PATIENT_ID  = "pref_patient_id";
	public final static String PREF_APP_ID  = "pref_app_id";
	public final static String PREF_USER_PHONE  = "pref_user_phone";
	
	public final static String PREF_USER_CS_ID = "cs_id";
	
	public static final String PREF_CONSULTATION_ID = "consultation_id";
	public static final int NO_CONSULTATION = -1;
	
	//public static final String HOST = "http://188.166.124.155:4567/";
	public static final String TWILIO_ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzQ2YWQwYjg3ZGQ1ZGFkOTFhYTNiMWEyODE0ZTU3YmY0LTE0Njg0OTExNTAiLCJpc3MiOiJTSzQ2YWQwYjg3ZGQ1ZGFkOTFhYTNiMWEyODE0ZTU3YmY0Iiwic3ViIjoiQUNlNTQxYzcwYjk2ODQwOTVkYWI5NzU4ZGJlMzUxMjI2OSIsImV4cCI6MTQ2ODQ5NDc1MCwiZ3JhbnRzIjp7ImlkZW50aXR5IjoicXVpY2tzdGFydCIsInJ0YyI6eyJjb25maWd1cmF0aW9uX3Byb2ZpbGVfc2lkIjoiVlMxNTQ0MzE2OWVmZDdiZDZiZDNiMWJkODJjNzE4YTUzOSJ9fX0.5d2wQgF75Ii1XNG0sVEYlShVpnf5IUkpcqSwWOaDitQ";
	
	//public static final String CS_API_HOST = "http://188.166.124.155:4567/";
	public static final String BACKOFFICE_HOST = "http://backoffice.telemedix.com";
	public static final String BACKOFFICE_API_HOST = BACKOFFICE_HOST+"/api/";
	
	private static Config config;
}
