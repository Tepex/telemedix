package com.telemedix.patient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telemedix.patient.R;
import com.telemedix.patient.models.Insurance;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class InsuranceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;

    private Context context;
    private List<Insurance> list;
    private AdapterListener adapterListener;

    public InsuranceListAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon)
        ImageView icon;

        @BindView(R.id.clinic_name)
        TextView text;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class FooterHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.container)
        View container;

        @BindView(R.id.title)
        TextView title;

        public FooterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size()) return TYPE_FOOTER;
        return TYPE_ITEM;
    }

    public void setList(List<Insurance> consultations) {
        this.list = consultations;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_FOOTER) {
            return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer_add_item, parent, false));
        }

        return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.insurance_list_item, null, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemHolder) {
            final Insurance insurance = list.get(position);
            ((ItemHolder) holder).text.setText(insurance.text);
            ((ItemHolder) holder).icon.setImageResource(insurance.icon);

            if (adapterListener != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        adapterListener.onItemClicked(insurance);
                    }
                });
            }
        }

        if (holder instanceof FooterHolder) {
            ((FooterHolder) holder).title.setText("Добавить полис страхования");
        }
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) return 0;
        return list.size() + 1;
    }

    public interface AdapterListener {
        void onItemClicked(Insurance insurance);
    }

    public void setAdapterListener(AdapterListener listener) {
        this.adapterListener = listener;
    }
}
