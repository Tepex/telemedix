package com.telemedix.patient.adapters;

import android.content.Context;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.joda.time.Period;

import com.telemedix.patient.R;
import com.telemedix.patient.Utils;
import com.telemedix.patient.models.Consultation;
import com.telemedix.patient.utils.DateHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConsultationListAdapter extends RecyclerView.Adapter<ConsultationListAdapter.ItemHolder>
{
	public ConsultationListAdapter(Context context)
	{
		this.context = context;
		this.list = new ArrayList<>();
	}
	
	@Override
	public int getItemCount()
	{
		return list.size();
	}
	
	@Override
	public int getItemViewType(final int position)
	{
		if(position == list.size() - 1) return FOOTER_VIEW;
		else if(list.get(position).getViewType() == Consultation.ViewType.LABEL) return SECTION_VIEW;
		else return super.getItemViewType(position);
	}
	
	@Override
	public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		if(viewType == FOOTER_VIEW)
			return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.consult_history_btn, null, false));
		else if(viewType == SECTION_VIEW)
			return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.consult_history_section_item, null, false));
		else
			return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.consultation_list_item, null, false));
	}
	
	@Override
	public void onBindViewHolder(ItemHolder holder, int position)
	{
		int type = getItemViewType(position);
		if(type == FOOTER_VIEW) holder.itemView.setOnClickListener(view->adapterListener.onFooterClicked());
		else if(type == SECTION_VIEW) holder.sectionCaption.setText("name: "+list.get(position).getName());
		else
		{
			final Consultation consultation = list.get(position);
			holder.name.setText(consultation.getName());
			holder.place.setText(consultation.getDoctor().getClinicString());
			if(consultation.getDoctor().getPhoto() != null)
			{
				Glide.with(context)
					.load(consultation.getDoctor().getPhoto())
					.placeholder(R.drawable.no_photo_placeholder)
					.crossFade()
					.centerCrop()
					.into(holder.avatar);
			}
			if(consultation.getSlot() != null)
			{
				if(consultation.isPending())
				{
					Period period = new Period(Utils.NOW, consultation.getSlot().getDateTime());
					holder.time.setText(context.getString(R.string.countdown_format, period.getDays(), period.getHours(), period.getMinutes()));
					holder.note.setText(consultation.getSlot().getDateTime().toString("dd MMMM HH:mm"));
				}
				else
				{
					String dateTimeStr = consultation.getSlot().getDateTime().toString("dd")+"\n"+
						consultation.getSlot().getDateTime().toString("MMMM")+"\n"+
						consultation.getSlot().getDateTime().toString("yyyy")+"\n"+
						consultation.getSlot().getDateTime().toString("HH:mm");
					holder.time.setText(dateTimeStr);
					holder.note.setText(consultation.getViewType().toString());
				}
			}
			if(adapterListener != null) holder.itemView.setOnClickListener(view->adapterListener.onItemClicked(consultation));
		}
	}
	
	public void setList(List<Consultation> consultations)
	{
		this.list = consultations;
		notifyDataSetChanged();
	}
	
	public void setAdapterListener(AdapterListener listener)
	{
		this.adapterListener = listener;
	}
    
	public interface AdapterListener
	{
		void onItemClicked(Consultation consultation);
		void onFooterClicked();
	}
	
	public static class ItemHolder extends RecyclerView.ViewHolder
	{
		public ItemHolder(View itemView)
		{
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
		
		@Nullable @BindView(R.id.time) TextView time;
		@Nullable @BindView(R.id.avatar) ImageView avatar;
		@Nullable @BindView(R.id.name) TextView name;
		@Nullable @BindView(R.id.place) TextView place;
		@Nullable @BindView(R.id.note) TextView note;
		@Nullable @BindView(R.id.consult_history_section_item) TextView sectionCaption;
	}
	
	public static class FooterHolder extends RecyclerView.ViewHolder
	{
		public FooterHolder(final View itemView)
		{
			super(itemView);
		}
	}
	
	private Context context;
	private List<Consultation> list;
	private AdapterListener adapterListener;
	
	private final static int FOOTER_VIEW = -999;
	private final static int SECTION_VIEW = -900;
}
