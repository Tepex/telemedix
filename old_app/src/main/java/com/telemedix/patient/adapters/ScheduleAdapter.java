package com.telemedix.patient.adapters;

import android.content.Context;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.telemedix.patient.models.Schedule;
import com.telemedix.patient.R;

import java.util.ArrayList;
import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	public ScheduleAdapter(Context context)
	{
		this.list = new ArrayList<>();
	}
	
	public void setList(List<Schedule> list, boolean isEmpty)
	{
		this.list = list;
		this.isEmpty = isEmpty;
		notifyDataSetChanged();
	}
	
	/*
	public List<Schedule> getList()
	{
		return list;
	}
	*/
	
	public void setAdapterListener(AdapterListener listener)
	{
		this.adapterListener = listener;
	}
	
	@Override
	public int getItemViewType(int position)
	{
		if(position == list.size() && !isEmpty) return TYPE_FOOTER;
		return TYPE_ITEM;
	}
	
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		if(viewType == TYPE_FOOTER && !isEmpty)
			return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_schedule_list_footer, parent, false));
		return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_schedule_list_item, parent, false));
	}
	
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
	{
		if(holder instanceof ItemHolder)
		{
			final Schedule schedule = list.get(position);
			((ItemHolder)holder).bind(schedule, position);
			if(adapterListener != null)
			{
				holder.itemView.setOnClickListener(view->
				{
					if(schedule.getIntervals() != null && schedule.getIntervals().size() > 0) adapterListener.onItemSelected(schedule);
				});
			}
		}
		else if((holder instanceof FooterHolder) && (adapterListener != null))
			holder.itemView.setOnClickListener(view->adapterListener.onFooterSelected());
	}
	
	@Override
	public int getItemCount()
	{
		int size = list.size();
		if(!isEmpty) ++size; // Если слоты пусты — не показывать последний элемент, который запускает календарь
		return size;
	}

	public class ItemHolder extends RecyclerView.ViewHolder
	{
		public ItemHolder(View itemView)
		{
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
		
		public void bind(Schedule schedule, int position)
		{
			title.setText(schedule.getTitle());
			boolean isEmptySlots = (schedule.getIntervals() == null || schedule.getIntervals().size() == 0);
			if(schedule.getStartTime().equals(schedule.getEndTime()) || isEmptySlots)
			{
				description.setText(schedule.getStartTime());
				arrowImage.setVisibility(isEmptySlots ? View.GONE : View.VISIBLE);
			}
			else description.setText(String.format("%s — %s", schedule.getStartTime(), schedule.getEndTime()));
		}
		
		@BindView(R.id.title) TextView title;
		@BindView(R.id.description) TextView description;
		@BindView(R.id.arrow) ImageView arrowImage;
	}
	
	public class FooterHolder extends RecyclerView.ViewHolder
	{
		public FooterHolder(View itemView)
		{
			super(itemView);
		}
	}
	
	public interface AdapterListener
	{
		void onItemSelected(Schedule schedule);
		void onFooterSelected();
	}
	
	private List<Schedule> list;
	private boolean isEmpty = true;
	private AdapterListener adapterListener;
	
	private static final int TYPE_ITEM = 0;
	private static final int TYPE_FOOTER = 1;
}
