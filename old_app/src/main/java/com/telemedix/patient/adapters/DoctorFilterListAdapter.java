package com.telemedix.patient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.telemedix.patient.R;
import com.telemedix.patient.activities.doctor.FilterActivity;
import com.telemedix.patient.models.Doctor;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropTransformation;

public class DoctorFilterListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	public DoctorFilterListAdapter(Context context)
	{
		this.context = context;
		this.isFilterable = false;
	}
	
	public DoctorFilterListAdapter(Context context, int currAction, String searchString)
	{
		this.context = context;
		this.currAction = currAction;
		this.searchString = searchString;
		this.isFilterable = true;
	}
	
	@Override
	public int getItemViewType(int position)
	{
		/* Для фильтруемого списка (isFilterable) первый элемент — особенный: отдельное View с кнопками фильтра */
		if(position == 0 && isFilterable) return TYPE_HEADER;
		return TYPE_ITEM;
	}
	
	@Override
	public int getItemCount()
	{
		int size = list.size();
		/* Для фильтруемого списка (isFilterable) первый элемент — особенный: отдельное View с кнопками фильтра */
		if(isFilterable && size > 0) ++size;
		return size;
	}
	
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		if(viewType == TYPE_HEADER)
			return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.find_doctor_filter, null, false));
		return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_list_item, null, false));
	}
	
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder adapterHolder, int position)
	{
		if(adapterHolder instanceof HeaderHolder)
		{
			final HeaderHolder holder = (HeaderHolder)adapterHolder;
			switch(currAction)
			{
				case FilterActivity.EXTRA_ACTION_FIND_BY_SPEC:
					holder.filterSpecialisationBtn.setText(searchString);
					break;
				case FilterActivity.EXTRA_ACTION_FIND_BY_CITY:
					holder.filterCityBtn.setText(searchString);
					break;
				case FilterActivity.EXTRA_ACTION_FIND_BY_CLINIC:
					//holder.filter_clinic_btn.setText(searchString);
					break;
			}
			if(adapterListener != null)
			{
				holder.filterSpecialisationBtn.setOnClickListener(view->adapterListener.onFilterSelected(FilterActivity.EXTRA_ACTION_FIND_BY_SPEC));
				holder.filterCityBtn.setOnClickListener(view->adapterListener.onFilterSelected(FilterActivity.EXTRA_ACTION_FIND_BY_CITY));
				holder.filterClinicBtn.setOnClickListener(view->adapterListener.onFilterSelected(FilterActivity.EXTRA_ACTION_FIND_BY_CLINIC));
			}
		}
		else if (adapterHolder instanceof ItemHolder)
		{
			/* Первый элемент списка — заголовок */
			if(isFilterable) --position;
			final Doctor doctor = list.get(position);
			final ItemHolder holder = (ItemHolder) adapterHolder;
			
			String title = doctor.getName();
			if(doctor.getCategory() != null) title += ", "+doctor.getCategory();
			holder.title.setText(title);
			
			String subtitle = "";
			if(doctor.getClinicString() != null) subtitle += doctor.getClinicString();
			subtitle += ", "+doctor.getPriceString(context);
			holder.subtitle.setText(subtitle);
			
			if(doctor.getPhoto() != null)
			{
				Glide.with(context).load(doctor.getPhoto())
					.crossFade()
					//.centerCrop
					.bitmapTransform(new CropTransformation(
						context,
						holder.avatar.getWidth(),
						holder.avatar.getHeight(),
						//Utils.dpToPx(mContext, 350),
						//holder.getImageView().getHeight(),
						CropTransformation.CropType.TOP))
					.into(holder.avatar);
			}
			
			holder.actionIndicator.setImageResource((doctor.isVideoEnabled()) ? R.drawable.videocam : R.drawable.audiocall);
			holder.actionIndicator.setEnabled(doctor.isOnline());
			
			if(doctor.isOnline())
			{
				holder.online.setVisibility(View.VISIBLE);
				holder.offlineInfo.setVisibility(View.GONE);
			}
			else
			{
				holder.online.setVisibility(View.GONE);
				holder.offlineInfo.setVisibility(View.VISIBLE);
			}
			
			if(adapterListener != null) holder.itemView.setOnClickListener(view->adapterListener.onDoctorSelected(doctor));
			holder.rate.setRating(doctor.getRating());
		}
	}
	
	public void setList(List<Doctor> list)
	{
		this.list = list;
		notifyDataSetChanged();
	}
	
	public void setAdapterListener(AdapterListener listener)
	{
		adapterListener = listener;
	}
	
	private static final int TYPE_HEADER = 1;
	private static final int TYPE_ITEM = 0;
	
	private Context context;
	private List<Doctor> list = new ArrayList<>();
	private AdapterListener adapterListener;
	private String searchString;
	private int currAction;
	private boolean isFilterable;
	
	public interface AdapterListener
	{
		void onFilterSelected(int filter);
		void onDoctorSelected(Doctor doctor);
	}
	
	static class HeaderHolder extends RecyclerView.ViewHolder
	{
		public HeaderHolder(View itemView)
		{
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
		
		@BindView(R.id.filter_specialisation_btn) Button filterSpecialisationBtn;
		@BindView(R.id.filter_city_btn) Button filterCityBtn;
		@BindView(R.id.filter_clinic_btn) Button filterClinicBtn;
	}
	
	static class ItemHolder extends RecyclerView.ViewHolder
	{
		public ItemHolder(View itemView)
		{
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
		
		@BindView(R.id.title) TextView title;
		@BindView(R.id.subtitle) TextView subtitle;
		@BindView(R.id.avatar) ImageView avatar;
		@BindView(R.id.online) TextView online;
		@BindView(R.id.offline_info) TextView offlineInfo;
		@BindView(R.id.action_indicator) ImageView actionIndicator;
		@BindView(R.id.rate) RatingBar rate;
    }
}
