package com.telemedix.patient.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eowise.recyclerview.stickyheaders.StickyHeadersAdapter;
import com.telemedix.patient.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class HeaderAdapter implements StickyHeadersAdapter<HeaderAdapter.ViewHolder> {

    private List<String> items;

    public HeaderAdapter(List<String> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder headerViewHolder, int position) {
        if (items.size() > 0 && position < items.size()) {
            headerViewHolder.header_text.setText(items.get(position));

        } else {
            headerViewHolder.itemView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public long getHeaderId(int position) {
        if (items.size() > 0 && position < items.size()) return items.get(position).hashCode();
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.header_text)
        TextView header_text;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}