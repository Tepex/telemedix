package com.telemedix.patient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telemedix.patient.R;
import com.telemedix.patient.models.Payment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class PaymentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;

    private Context context;
    private List<Payment> list;
    private boolean showsAddPaymentButton;
    private AdapterListener adapterListener;

    public PaymentListAdapter(Context context, boolean showsAddPaymentButton) {
        this.context = context;
        this.list = new ArrayList<>();
        this.showsAddPaymentButton = showsAddPaymentButton;
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.container)
        View container;

        @BindView(R.id.icon)
        ImageView icon;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.subtitle)
        TextView subtitle;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class FooterHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.container)
        View container;

        @BindView(R.id.title)
        TextView title;

        public FooterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size()) return TYPE_FOOTER;
        return TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_FOOTER) {
            return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer_add_item, parent, false));
        }

        return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemHolder) {

            final Payment payment = list.get(position);

            String title = payment.title;
            ((ItemHolder) holder).title.setText(title);

            String subtitle = payment.subtitle;
            ((ItemHolder) holder).subtitle.setText(subtitle);

            if (payment.icon != 0) {
                ((ItemHolder) holder).icon.setImageResource(payment.icon);

                if (showsAddPaymentButton) {
                    if (payment.icon == R.drawable.visa_icon) {
                        ((ItemHolder) holder).container.setBackgroundColor(context.getResources().getColor(R.color.payment_credit_card));
                    } else if (payment.icon == R.drawable.google_play_icon) {
                        ((ItemHolder) holder).container.setBackgroundColor(context.getResources().getColor(R.color.payment_google_play));
                    }
                }
            }
            if (adapterListener != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        adapterListener.onItemSelected(payment);
                    }
                });
            }

        }

        if (holder instanceof FooterHolder) {
            ((FooterHolder) holder).title.setText(context.getString(R.string.add_payment_method));
            ((FooterHolder) holder).container.setBackgroundColor(context.getResources().getColor(R.color.payment_new));
        }
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) return 0;
        return list.size() + (showsAddPaymentButton ? 1 : 0);
    }

    public void setList(List<Payment> newList) {
        this.list = newList;
        notifyDataSetChanged();
    }

    public interface AdapterListener {
        void onItemSelected(Payment payment);
    }

    public void setAdapterListener(AdapterListener adapterListener) {
        this.adapterListener = adapterListener;
    }
}
