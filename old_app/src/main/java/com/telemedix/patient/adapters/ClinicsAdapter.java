package com.telemedix.patient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.telemedix.patient.R;
import com.telemedix.patient.Utils;
import com.telemedix.patient.models.Clinic;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class ClinicsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;

    private Context context;
    private List<Clinic> list;
    private AdapterListener adapterListener;

    public ClinicsAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon)
        ImageView icon;

        @BindView(R.id.clinic_name)
        TextView clinicName;
        @BindView(R.id.clinic_address)
        TextView address;

        @OnClick(R.id.location_indicator)
        public void openLocation() {
            address.getContext().startActivity(Utils.viewOnMap(address.getText().toString()));
        }

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class FooterHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.container)
        View container;

        @BindView(R.id.title)
        TextView title;

        public FooterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setList(List<Clinic> pClinics) {
        this.list = pClinics;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.clinic_list_item, null, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemHolder) {
            final Clinic clinic = list.get(position);
            ((ItemHolder) holder).clinicName.setText(clinic.getName());
            ((ItemHolder) holder).address.setText(clinic.getAddress());
            Glide.with(context)
                    .load(clinic.getIcon())
                    .into(((ItemHolder) holder).icon);

            if (adapterListener != null) {
                holder.itemView.setOnClickListener(view -> adapterListener.onItemClicked(clinic));
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface AdapterListener {
        void onItemClicked(Clinic pClinic);
    }

    public void setAdapterListener(AdapterListener listener) {
        this.adapterListener = listener;
    }
}
