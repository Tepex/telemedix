package com.telemedix.patient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telemedix.patient.R;
import com.telemedix.patient.models.Subscription;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class SubscriptionsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;

    private Context context;
    private List<Subscription> list;

    private String subscription_code;
    private String subscription_include;
    private String subscription_valid_until;

    private AdapterListener adapterListener;

    public SubscriptionsListAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
        this.subscription_code = context.getString(R.string.subscription_code);
        this.subscription_include = context.getString(R.string.subscription_include);
        this.subscription_valid_until = context.getString(R.string.subscription_valid_until);
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon)
        ImageView icon;

        @BindView(R.id.description)
        TextView description;

        @BindView(R.id.valid_until)
        TextView valid_until;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class FooterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;

        public FooterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size()) return TYPE_FOOTER;
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_FOOTER)
            return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer_add_item, parent, false));

        return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.subscription_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemHolder) {
            final Subscription subscription = list.get(position);

            ((ItemHolder) holder).description.setText(subscription.description);
            ((ItemHolder) holder).valid_until.setText(String.format(subscription_valid_until, subscription.valid_until));
            ((ItemHolder) holder).icon.setImageResource(subscription.icon);

            if (adapterListener != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        adapterListener.onItemSelected(subscription);
                    }
                });
            }
        }

        if (holder instanceof FooterHolder) {

            ((FooterHolder) holder).title.setText(context.getString(R.string.add_subscription));

            if (adapterListener != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        adapterListener.onItemAdd();
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) return 0;
        return list.size() + 1;
    }

    public void setList(List<Subscription> newList) {
        this.list = newList;
        notifyDataSetChanged();
    }

    public interface AdapterListener {
        void onItemSelected(Subscription subscription);

        void onItemAdd();
    }

    public void setAdapterListener(AdapterListener adapterListener) {
        this.adapterListener = adapterListener;
    }
}
