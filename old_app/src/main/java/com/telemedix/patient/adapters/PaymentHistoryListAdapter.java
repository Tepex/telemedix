package com.telemedix.patient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telemedix.patient.R;
import com.telemedix.patient.models.payment.PaymentInfo;
import com.telemedix.patient.utils.DateHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by baneizalfe on 7/6/16.
 */
public class PaymentHistoryListAdapter extends RecyclerView.Adapter<PaymentHistoryListAdapter.ItemHolder> {

    private Context context;
    private List<PaymentInfo> list;
    private String priceFormat;

    private AdapterListener adapterListener;

    public PaymentHistoryListAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
        this.priceFormat = context.getString(R.string.price_format);
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon)
        ImageView icon;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.type)
        TextView type;

        @BindView(R.id.amount)
        TextView amount;

        @BindView(R.id.description)
        TextView description;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_history_list_item, null, false));
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {

        final PaymentInfo payment = list.get(position);

        holder.icon.setImageResource(R.drawable.visa_icon);
        holder.date.setText(payment.getProcessedAt());
        if (payment.getCardInfo() != null) {
            holder.type.setText(String.format(context.getResources().getString(R.string.bank_card), "\n" + payment.getCardInfo().getPan()));
        }
        holder.amount.setText(payment.getPrice() + " " + context.getResources().getString(R.string.rubles_short));
        holder.description.setText(payment.getTitle());

        if (adapterListener != null) {
            holder.itemView.setOnClickListener(view -> adapterListener.onItemClicked(payment));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<PaymentInfo> newList) {
        this.list = newList;
        notifyDataSetChanged();
    }

    public interface AdapterListener {
        void onItemClicked(PaymentInfo payment);
    }

    public void setAdapterListener(AdapterListener listener) {
        this.adapterListener = listener;
    }
}
