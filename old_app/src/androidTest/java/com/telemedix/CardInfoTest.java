package com.telemedix;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import android.util.Pair;

import com.orhanobut.hawk.Hawk;
import com.telemedix.patient.Constants;
import com.telemedix.patient.network.Api;
import com.telemedix.patient.utils.CacheManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
@RunWith(AndroidJUnit4.class)
@SmallTest
public class CardInfoTest {

    public static final String TEST_UUID = "6A0DCD15-B571-B2D7-F55D-35BCA96DFC41";


    @Before
    public void createRequest() {

    }

    @Test
    public void getCardInfoTest() throws InterruptedException {
        // Set up the Parcelable object to send and receive.
        Api.get.getUserCards(TEST_UUID)
                .toBlocking()
                .subscribe(cCardInfo -> {
                    assertEquals(1, cCardInfo.size());
                    assertEquals(cCardInfo.get(0).getCardholder(), "Alexey makarkin");
                    assertEquals(cCardInfo.get(0).getPan(), "427638**9303");
                    assertEquals(cCardInfo.get(0).getExpiration(), "12/17");
                });
    }
}
