package com.telemedix.core.model;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import java.util.UUID;

import org.junit.After;
import org.junit.Before;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.HttpException;

public class BaseTest
{
	@Before
	public void setUp() throws Exception
	{
		repository = Repository.getInstance(HOST, TIMEOUT, true);
		api = repository.getApi();
		repository.setApiKey(API_KEY);
		
		compositeDisposable.add(api.registerApp(APP_ID, IMEI, FCM_TOKEN)
			.subscribe(response->
			{
				patientId = response.getPatientId();
				apiKey = response.getApiKey();
				LOG.debug("patientId {}", patientId);
				LOG.debug("apikey: {}", apiKey);
				repository.setApiKey(apiKey);
			},
			error->
				{
					if(error instanceof HttpException)
					{
						HttpException he = (HttpException)error;
						if(he.code() == 500) LOG.error("API. registerApp serverError {}: {}", he.code(), he.message(), error);
					}
					else LOG.error("registerApp error", error);
				}));
	}
	
	@After
	public void tearDown()
	{
		compositeDisposable.clear();
	}
	
    private Repository repository;
	protected Api api;
	protected String patientId;
	protected String apiKey;
	protected CompositeDisposable compositeDisposable = new CompositeDisposable();
	
	protected static final String APP_ID = UUID.randomUUID().toString();
	protected static final String IMEI = "my_unit_test_IMEI";
	protected static final String FCM_TOKEN = "fake_token";
	//protected static final String FCM_TOKEN = "foLytMF8wGI:APA91bFQe1aUS9lo8z5yyHqSj_57bTMwpBVXw5DECgKpY8SNqAs0Q9etQGY-bqw0z4KLHRpBf0HcKzpVNDxulKn_DEt0ALQ3fiEmy9U94aVM6Oc4mkXUCp0PF-bp6oOQEB9C8SFbmBrH";
	
	
	private static final String HOST = "http://dev.backoffice.telemedix.com/api/p/";
	//private static final String HOST = "http://10.1.11.170:8080/backoffice.telemedix.com/web/api/";
	
	protected static final String PATIENT_ID = "0F65B92E-C340-866B-61EB-7BF43CABB4E9";  
	protected static final String API_KEY = "E7ED0018-0584-44CC-7873-752D3A57C28B";
	
	private static final int TIMEOUT = 60;
	
	private static final Logger LOG = LoggerFactory.getLogger(BaseTest.class);
}
