package com.telemedix.core.model;

import com.telemedix.core.model.request.RegisterRequest;
import com.telemedix.core.model.response.RegisterResponse;
import com.telemedix.core.model.response.ValidateCodeResponse;

import io.reactivex.observers.TestObserver;

import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.HttpException;

public class RegisterPhoneTest extends BaseTest
{
	@Test
	public void testRegisterPhoneError() throws Exception
	{
		TestObserver<RegisterResponse> observer = TestObserver.create();
		api.registerUserPhone(new RegisterRequest(PHONE_NUMBER_INVALID)).subscribe(observer);
		observer.assertNotComplete();
		observer.assertError(thr->
		{
			if(!(thr instanceof HttpException)) return false;
			HttpException he = (HttpException)thr;
			if(he.code() != 400) return false;
			//LOG.debug("error {}", he.message(), he);
			return true;
		});
	}
	
	@Test
	public void testRegisterPhone() throws Exception
	{
		TestObserver<RegisterResponse> observer = TestObserver.create();
		api.registerUserPhone(new RegisterRequest(PHONE_NUMBER)).subscribe(observer);
		observer.assertNoErrors();
		observer.assertComplete();
		//observer.assertValueCount(1);
		observer.assertValue(RegisterResponse.createSuccessResponse());
    }
    
    /**
    @Test
	public void test_confirm_phone() throws Exception
	{
		TestObserver<ValidateCodeResponse> observer = TestObserver.create();
		api.confirmPhone(PHONE_NUMBER, SMS_CODE).subscribe(observer);
		observer.assertNoErrors();
		observer.assertComplete();
		//observer.assertValueCount(1);
		observer.assertValue(ValidateCodeResponse.createSuccessResponse(PATIENT_ID));
	}
	*/

	
	private static final String PHONE_NUMBER = "79152265280";
	private static final String SMS_CODE = "1234";
	//private static final String PHONE_NUMBER = "79255898040";
	private static final String PHONE_NUMBER_INVALID = "000";
	
	private static final Logger LOG = LoggerFactory.getLogger(RegisterPhoneTest.class);
}
