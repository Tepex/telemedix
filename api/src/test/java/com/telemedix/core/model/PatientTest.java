package com.telemedix.core.model;

import com.google.gson.Gson;

import io.reactivex.observers.TestObserver;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.HttpException;

/**
 * Добавление массивов allergies и chronics через сайт:
 * - в правом верхнем углу выставить body format: JSON
 * - в поле "Content" запроса:
 * {"chronicDiseases": [{"name": "astma"}, {"name": "wefwefwe"}]}
 */
public class PatientTest extends BaseTest
{
	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		InputStream in = PatientTest.class.getClassLoader().getResourceAsStream(FILE_CONTACTS);
		if(in == null) throw new FileNotFoundException("Testin data file "+FILE_CONTACTS+" not found in the <resources> directory.");
		try(InputStreamReader reader = new InputStreamReader(in, "UTF-8"))
		{
			contactsModel = gson.fromJson(reader, PatientContactsModel.class);
		}
		catch(IOException e)
		{
			LOG.error("Error reading file {}", FILE_CONTACTS, e);
			throw e;
		}
		
		in = PatientTest.class.getClassLoader().getResourceAsStream(FILE_METRICS);
		if(in == null) throw new FileNotFoundException("Testin data file "+FILE_METRICS+" not found in the <resources> directory.");
		try(InputStreamReader reader = new InputStreamReader(in, "UTF-8"))
		{
			metricsModel = gson.fromJson(reader, PatientMetricsModel.class);
		}
		catch(IOException e)
		{
			LOG.error("Error reading file {}", FILE_CONTACTS, e);
			throw e;
		}
		
		/*
		in = PatientTest.class.getClassLoader().getResourceAsStream(FILE_ALLERGIES);
		if(in == null) throw new FileNotFoundException("Testin data file "+FILE_ALLERGIES+" not found in the <resources> directory.");
		try(InputStreamReader reader = new InputStreamReader(in, "UTF-8"))
		{
			allergiesModel = gson.fromJson(reader, PatientAllergiesModel.class);
		}
		catch(IOException e)
		{
			LOG.error("Error reading file {}", FILE_ALLERGIES, e);
			throw e;
		}

		in = PatientTest.class.getClassLoader().getResourceAsStream(FILE_CHRONICS);
		if(in == null) throw new FileNotFoundException("Testin data file "+FILE_CHRONICS+" not found in the <resources> directory.");
		try(InputStreamReader reader = new InputStreamReader(in, "UTF-8"))
		{
			chronicDiseasesModel = gson.fromJson(reader, PatientChronicDiseasesModel.class);
		}
		catch(IOException e)
		{
			LOG.error("Error reading file {}", FILE_CHRONICS, e);
			throw e;
		}
		*/
	}

	/*
	@Test
	public void test_get_patient_contacts_data() throws Exception
	{
		TestObserver<PatientContactsModel> observer = TestObserver.create();
		api.getPatientContacts(SCOPE_CONTACTS).subscribe(observer);
		observer.assertNoErrors();
		observer.assertComplete();
		observer.assertValue(contactsModel);
	}
	*/
	
	/**
	 @TODO: Добавить некорректный запрос. Например: HTTP: 400
	 
	 {"children":{"user":{"children":{"phone":{"errors":["Телефон '"000"' не верен."]}}}}}
	*/
	@Test
	public void test_save_patient_contacts_data() throws Exception
	{
		TestObserver<PatientContactsModel> observer = TestObserver.create();
		api.savePatientContacts(contactsModel).subscribe(observer);
		observer.assertNoErrors();
		observer.assertComplete();
		observer.assertValue(contactsModel);
	}

	@Test
	public void test_save_patient_metrics() throws Exception
	{
		TestObserver<PatientMetricsModel> observer = TestObserver.create();
		api.savePatientMetrics(metricsModel).subscribe(observer);
		observer.assertNoErrors();
		observer.assertComplete();
		observer.assertValue(metricsModel);
	}

	/*
	@Test
	public void test_save_patient_allergies() throws Exception
	{
		TestObserver<PatientAllergiesModel> observer = TestObserver.create();
		api.savePatientAllergies(allergiesModel).subscribe(observer);
		observer.assertNoErrors();
		observer.assertComplete();
		observer.assertValue(allergiesModel);
	}
	
	@Test
	public void test_save_patient_chronic_diseases() throws Exception
	{
		TestObserver<PatientChronicDiseasesModel> observer = TestObserver.create();
		api.savePatientChronicDiseases(chronicDiseasesModel).subscribe(observer);
		observer.assertNoErrors();
		observer.assertComplete();
		observer.assertValue(chronicDiseasesModel);
	}
	*/
	
	@After
	public void tearDown()
	{
		super.tearDown();
		
	}
	
	private Gson gson = new Gson();
	private PatientContactsModel contactsModel;
	private PatientMetricsModel metricsModel;
	/*
	private PatientAllergiesModel allergiesModel;
	private PatientChronicDiseasesModel chronicDiseasesModel;
	*/

	private static final Logger LOG = LoggerFactory.getLogger(PatientTest.class);
	
	
	/*
	
	
	private static final PatientContactsModel CONTACTS_MODEL = PatientContactsModel.newBuilder()
		.setId(PATIENT_ID)
		.setUser(User.newBuilder()
			.setId(PATIENT_ID)
			.setFirstName("cvxcvxcv")
			.setLastName("ergertger")
			.setPatronymic("erververv")
			.setEmail("eee@sdfsdf")
			.build())
		.setAddress(Address.newBuilder()
			.setCountry(new Address.Country(1, "Россия"))
			.setCity("ddvv")
			.setAddress("cab fdbdf")
			.build())
		.setClosestRelative(ClosestRelative.newBuilder()
			.setId(35)
			.setFirstName("dfadg")
			.setLastName("dfgdfgf")
			.setPhone("cvvc")
			.build())
		.build();

	private static final PatientMetricsModel METRICS_MODEL = PatientMetricsModel.newBuilder()
		.setId(PATIENT_ID)
		.setHeight(140)
		.setWeight(55)
		.setUser(User.newBuilder()
			.setId(PATIENT_ID)
			.setSex(false)
			.setBirthDate("1988-04-22T00:00:00+0400")
			.setPhoto("/api/p/patient/photo")
			.build())
		.build();

	private static final PatientAllergiesChronicsModel ALLERGIES_CHRONICS_MODEL = PatientAllergiesChronicsModel.newBuilder()
		.setId(PATIENT_ID)
		.addAllergy(new Entity(22, "Шерсть"))
		.addChronicDisease(new Entity(16, "Unit test chronic desease"))
		.build();
		*/
		
	public static final String FILE_CONTACTS = "contacts.json";
	public static final String FILE_METRICS = "metrics.json";
	public static final String FILE_ALLERGIES = "allergies.json";
	public static final String FILE_CHRONICS = "chronics.json";
}
