package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.DoctorsInterceptor;
import com.telemedix.core.model.HomeModel;
import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.pojo.Entity;
import com.telemedix.core.view.HomeView;

import io.reactivex.Observable;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;

import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.mockito.junit.MockitoJUnitRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest
{
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		presenter = new HomePresenter();
		presenter.onCreate(new HomeModel(), new DoctorsInterceptor(new ArrayList<Entity>()));
		presenter.bindView(view);
	}
	
	@After
	public void tearDown()
	{
		presenter.unbindView();
		presenter.onDestroy();
	}
	
	@Test
	public void testHome()
	{
		assertThat(2 * 2).isEqualTo(4);
	}
	
	@Mock HomeView view;
	
	private HomePresenter presenter;
	
	private static final Logger LOG = LoggerFactory.getLogger(HomePresenterTest.class);
}
