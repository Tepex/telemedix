package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.ChangePinCodeModel;

import com.telemedix.core.model.Model.State;

import com.telemedix.core.view.ChangePinCodeView;

import io.reactivex.Observable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** http://joel-costigliola.github.io/assertj/core-8/api/index.html */
import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChangePinCodePresenterTest
{
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		
		presenter = new ChangePinCodePresenter(null);
		presenter.onCreate(new ChangePinCodeModel(), interceptor);
		presenter.bindView(view);
	}
	
	@After
	public void tearDown()
	{
		presenter.unbindView();
		presenter.onDestroy();
	}
	
	@Test
	public void test_change_pin_code()
	{
		assertThat(1+1).isEqualTo(2);
		/*
		presenter.onPhoneLocaleChanged(PHONE_LOCALE);
		presenter.onPhoneNumberChanged(PHONE_NUMBER);
		presenter.registerPhone();
		*/
		/*
		verify(interceptor).registerPhone(CreatePinCodeModel.create(...));
		verify(view).setState(State.SUCCESS);
		*/
	}
	
	@Mock ChangePinCodeView view;
	@Mock RegisterInterceptor interceptor;
	
	private ChangePinCodePresenter presenter;
	
	private static final Logger LOG = LoggerFactory.getLogger(ChangePinCodePresenterTest.class);
}
