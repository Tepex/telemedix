package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.Interceptor;

import com.telemedix.core.model.InputPinCodeModel;

import com.telemedix.core.model.Model.State;

import com.telemedix.core.view.InputPinCodeView;

import io.reactivex.Observable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InputPinCodePresenterTest
{
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		presenter = new InputPinCodePresenter();
		presenter.onCreate(new InputPinCodeModel(), interceptor);
		presenter.bindView(view);
	}
	
	@After
	public void tearDown()
	{
		presenter.unbindView();
		presenter.onDestroy();
	}
	
	@Test
	public void testPinCode()
	{
		assertThat(2 * 2).isEqualTo(4);
	}
	
	@Mock InputPinCodeView view;
	@Mock Interceptor interceptor;
	
	private InputPinCodePresenter presenter;
	
	private static final Logger LOG = LoggerFactory.getLogger(InputPinCodePresenterTest.class);
}
