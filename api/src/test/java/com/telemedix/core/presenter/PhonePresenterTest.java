package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.PhoneModel;

import com.telemedix.core.model.pojo.PhoneLocale;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.response.RegisterResponse;

import com.telemedix.core.view.PhoneView;

import io.reactivex.Observable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** http://joel-costigliola.github.io/assertj/core-8/api/index.html */
import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhonePresenterTest
{
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		
		PhoneModel model = PhoneModel.create(PHONE_LOCALE, PHONE_NUMBER);
		when(interceptor.registerPhone(model)).thenReturn(Observable.just(RegisterResponse.createSuccessResponse()));
		/* Добавить корявый телефон для тестирования ошибки */
		
		
		/* Инициализация презентера */
		presenter = new PhonePresenter();
		presenter.onCreate(new PhoneModel(), interceptor);
		presenter.bindView(view);
	}
	
	@After
	public void tearDown()
	{
		presenter.unbindView();
		presenter.onDestroy();
	}
	
	@Test
	public void test_incomplete_phone_number_and_phone_number_normalization()
	{
		presenter.onPhoneLocaleChanged(PHONE_LOCALE);
		presenter.onPhoneNumberChanged("(495) 333-45-1");
		assertThat(presenter.getModel().getPhoneNumberWithCode()).isEqualTo("7495333451");
		verify(view).setState(State.NEW);
	}
	
	@Test
	public void test_valid_phone_number()
	{
		presenter.onPhoneLocaleChanged(PHONE_LOCALE);
		presenter.onPhoneNumberChanged("(915) 226-52-80");
		assertThat(presenter.getModel().getPhoneNumberWithCode()).isEqualTo("79152265280");
		verify(view).setState(State.READY);
	}
	
	@Test
	public void test_register_phone_answer()
	{
		presenter.onPhoneLocaleChanged(PHONE_LOCALE);
		presenter.onPhoneNumberChanged(PHONE_NUMBER);
		presenter.registerPhone();
		verify(interceptor).registerPhone(PhoneModel.create(PHONE_LOCALE, PHONE_NUMBER));
		verify(view).setState(State.SUCCESS);
	}
	
	@Mock PhoneView view;
	@Mock RegisterInterceptor interceptor;
	
	private PhonePresenter presenter;
	
	private static final PhoneLocale PHONE_LOCALE = new PhoneLocale(7, "RU");
	private static final String PHONE_NUMBER = "9152265280";
	
	private static final Logger LOG = LoggerFactory.getLogger(PhonePresenterTest.class);
}


/**
Assert:

AbstractListAssert<?,List<?>,Object,ObjectAssert<Object>>	asList() Verifies that the actual value is an instance of List, and returns a list assertion, to allow chaining of list-specific assertions from this call.
AbstractCharSequenceAssert<?,String>	asString() Verifies that the actual value is an instance of String, and returns a String assertion, to allow chaining of String-specific assertions from this call.
S	doesNotHaveSameClassAs(Object other) Verifies that the actual value does not have the same class as the given object.
S	hasSameClassAs(Object other) Verifies that the actual value has the same class as the given object.
S	hasToString(String expectedToString) Verifies that actual actual.toString() is equal to the given String.
S	isEqualTo(Object expected) Verifies that the actual value is equal to the given one.
S	isExactlyInstanceOf(Class<?> type) Verifies that the actual value is exactly an instance of the given type.
S	isIn(Iterable<?> values) Verifies that the actual value is present in the given values.
S	isIn(Object... values) Verifies that the actual value is present in the given array of values.
S	isInstanceOf(Class<?> type) Verifies that the actual value is an instance of the given type.
S	isInstanceOfAny(Class<?>... types) Verifies that the actual value is an instance of any of the given types.
<T> S	isInstanceOfSatisfying(Class<T> type, Consumer<T> requirements) Verifies that the actual value is an instance of the given type satisfying the given requirements expressed as a Consumer.
S	isNotEqualTo(Object other) Verifies that the actual value is not equal to the given one.
S	isNotExactlyInstanceOf(Class<?> type) Verifies that the actual value is not exactly an instance of given type.
S	isNotIn(Iterable<?> values) Verifies that the actual value is not present in the given values.
S	isNotIn(Object... values) Verifies that the actual value is not present in the given array of values.
S	isNotInstanceOf(Class<?> type) Verifies that the actual value is not an instance of the given type.
S	isNotInstanceOfAny(Class<?>... types) Verifies that the actual value is not an instance of any of the given types.
S	isNotNull() Verifies that the actual value is not null.
S	isNotOfAnyClassIn(Class<?>... types) Verifies that the actual value type is not in given types.
S	isNotSameAs(Object other) Verifies that the actual value is not the same as the given one, ie using == comparison.
void	isNull() Verifies that the actual value is null.
S	isOfAnyClassIn(Class<?>... types) Verifies that the actual value type is in given types.
S	isSameAs(Object expected) Verifies that the actual value is the same as the given one, ie using == comparison.
S	usingComparator(Comparator<? super A> customComparator) Use given custom comparator instead of relying on actual type A equals method for incoming assertion checks.
S	usingDefaultComparator() Revert to standard comparison for incoming assertion checks.
S	withRepresentation(Representation representation) Use the given Representation to describe/represent values in AssertJ error messages.
S	withThreadDumpOnError() In case of assertion error, the thread dump will be printed on System.err.
*/
