package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.ValidateCodeModel;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.response.ValidateCodeResponse;

import com.telemedix.core.view.ValidateCodeView;

import io.reactivex.Observable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** http://joel-costigliola.github.io/assertj/core-8/api/index.html */
import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValidateCodePresenterTest
{
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		
		ValidateCodeModel model = ValidateCodeModel.create(PHONE_NUMBER, VALID_SMS_CODE, PATIENT_ID);
		when(interceptor.confirmPhone(model)).thenReturn(Observable.just(ValidateCodeResponse.createSuccessResponse(PATIENT_ID)));
		
		/* Инициализация презентера */
		presenter = new ValidateCodePresenter();
		presenter.onCreate(model, interceptor);
		presenter.bindView(view);
	}
	
	@After
	public void tearDown()
	{
		presenter.unbindView();
		presenter.onDestroy();
	}
	
	@Test
	public void test_incomplete_sms_code()
	{
		presenter.onSmsCodeChanged("12");
		verify(view).setState(State.NEW);
	}
	
	@Test
	public void test_received_valid_sms_code()
	{
		assertThat(presenter.onReceiveSms(VALID_SMS_RECEIVED)).isEqualTo(true);
		assertThat(presenter.getModel().getSmsCode()).isEqualTo(VALID_SMS_CODE1);
		verify(view).setSmsCode(VALID_SMS_CODE1);
		
	}

	@Test
	public void test_valid_user_sms_code()
	{
		presenter.onSmsCodeChanged(VALID_SMS_CODE);
		assertThat(presenter.getModel().getSmsCode()).isEqualTo(VALID_SMS_CODE);
		verify(view).setState(State.LOADING);
		verify(interceptor).confirmPhone(ValidateCodeModel.create(PHONE_NUMBER, VALID_SMS_CODE, PATIENT_ID));
		//verify(view).savePatientInfo(ValidateCodeModel.create(PHONE_NUMBER, VALID_SMS_CODE, PATIENT_ID));
		verify(view).setState(State.SUCCESS);
	}
		
	@Mock ValidateCodeView view;
	@Mock RegisterInterceptor interceptor;
	
	private ValidateCodePresenter presenter;
	
	private static final String PHONE_NUMBER = "+79152265280";
	private static final String VALID_SMS_CODE = "1234";
	private static final String VALID_SMS_CODE1 = "5432";
	private static final String VALID_SMS_RECEIVED = "Telemedix: "+VALID_SMS_CODE1;
	private static final String PATIENT_ID = "FAKE PATIENT ID";
	
	private static final Logger LOG = LoggerFactory.getLogger(ValidateCodePresenterTest.class);
}
