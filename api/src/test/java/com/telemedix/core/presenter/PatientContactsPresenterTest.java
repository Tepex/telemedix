package com.telemedix.core.presenter;

import com.google.gson.Gson;

import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.PatientContactsModel;

import com.telemedix.core.view.PatientContactsView;

import io.reactivex.Observable;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;

import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.mockito.junit.MockitoJUnitRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.telemedix.core.model.PatientTest.FILE_CONTACTS;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PatientContactsPresenterTest
{
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		
		Gson gson = new Gson();
		InputStream in = getClass().getClassLoader().getResourceAsStream(FILE_CONTACTS);
		if(in == null) throw new FileNotFoundException("Testin data file "+FILE_CONTACTS+" not found in the <resources> directory.");
		try(InputStreamReader reader = new InputStreamReader(in, "UTF-8"))
		{
			model = gson.fromJson(reader, PatientContactsModel.class);
		}
		catch(IOException e)
		{
			LOG.error("Error reading file {}", FILE_CONTACTS, e);
			throw e;
		}
		when(interceptor.getPatientContacts()).thenReturn(Observable.just(model));
		
		/* Инициализация презентера */
		presenter = new PatientContactsPresenter();
		presenter.onCreate(new PatientContactsModel(), interceptor);
		presenter.bindView(view);
	}
	
	@After
	public void tearDown()
	{
		presenter.unbindView();
		presenter.onDestroy();
	}
	
	@Test
	public void test_get_data()
	{
		/*
		presenter.getData();
		verify(view).setState(State.LOADING);
		verify(interceptor).getPatientContacts();
		assertThat(presenter.getModel()).isEqualTo(model);
		verify(view).setState(State.READY);
		*/
		assertEquals(4, 2 + 2);
	}
	
	@Mock PatientContactsView view;
	@Mock PatientInterceptor interceptor;
	
	private PatientContactsPresenter presenter;
	private PatientContactsModel model;
	
	private static final Logger LOG = LoggerFactory.getLogger(PatientContactsPresenterTest.class);
}
