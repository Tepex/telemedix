package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.AppInterceptor;

import com.telemedix.core.model.AppModel;

import com.telemedix.core.model.Model.State;

import com.telemedix.core.view.AppView;

import io.reactivex.Observable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** http://joel-costigliola.github.io/assertj/core-8/api/index.html */
import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppPresenterTest
{
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		
		model = new AppModel(API_KEY, PATIENT_ID, true);
		when(interceptor.registerApp(model.getAppId(), model.getImei(), TOKEN)).thenReturn(Observable.just(model));
		
		
		/* Инициализация презентера */
		presenter = new AppPresenter();
		presenter.onCreate(model, interceptor);
		presenter.bindView(view);
	}
	
	@After
	public void tearDown()
	{
		presenter.unbindView();
		presenter.onDestroy();
	}
	
	
	@Test
	public void test_register_app()
	{
		presenter.registerApp(TOKEN);
		
		verify(interceptor).registerApp(model.getAppId(), model.getImei(), TOKEN);
		assertThat(presenter.getModel().getPatientId()).isEqualTo(PATIENT_ID);
		verify(view).setState(State.LOADING);
	}
	
	@Mock AppView view;
	@Mock AppInterceptor interceptor;
	private AppModel model;
	private AppPresenter presenter;
	
	private static final String API_KEY = "my_test_API_key";
	private static final String PATIENT_ID = "patient_id";
	private static final String TOKEN = "FCM_token";
	
	private static final Logger LOG = LoggerFactory.getLogger(AppPresenterTest.class);
}
