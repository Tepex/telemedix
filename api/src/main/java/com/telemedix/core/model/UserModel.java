package com.telemedix.core.model;

import com.telemedix.core.model.pojo.Address;
import com.telemedix.core.model.pojo.BloodType;
import com.telemedix.core.model.pojo.ClosestRelative;
import com.telemedix.core.model.pojo.Entity;
import com.telemedix.core.model.pojo.User;

import java.util.Objects;
import java.util.Set;

public class UserModel extends Model
{
	public String getId()
	{
		return id;
	}
	
	public Integer getHeight()
	{
		return height;
	}
	
	public Integer getWeight()
	{
		return weight;
	}
	
	public BloodType getBloodType()
	{
		return bloodTypeEnum;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public Address getAddress()
	{
		return address;
	}
	
	public ClosestRelative getClosestRelative()
	{
		return closestRelative;
	}
	
	public Set<Entity> getAllergies()
	{
		return allergies;
	}
	
	public Set<Entity> getChronicDiseases()
	{
		return chronicDiseases;
	}
	
	public PatientContactsModel getPatientContactsModel()
	{
		return PatientContactsModel.newBuilder()
			.setUser(getUser())
			.setAddress(getAddress())
			.setClosestRelative(getClosestRelative())
			.build();
	}
	
	public PatientMetricsModel getPatientMetricsModel()
	{
		Integer height = null;
		Integer weight = null;
		if(getHeight() != null) height = getHeight();
		if(getWeight() != null) weight = getWeight();
		return PatientMetricsModel.newBuilder()
			.setHeight(height)
			.setWeight(weight)
			.setBloodTypeEnum(getBloodType())
			.setUser(getUser())
			.build();
	}
	
	public PatientAllergiesChronicDiseasesModel getAllergiesChronicDiseasesModel()
	{
		return new PatientAllergiesChronicDiseasesModel(getAllergies(), getChronicDiseases());
	}
	
	public void updateContacts(PatientContactsModel contacts)
	{
		if(contacts.getUser() != null)
		{
			if(user == null) user = contacts.getUser();
			else user.update(contacts.getUser(), true);
		}
		address = contacts.getAddress();
		closestRelative = contacts.getClosestRelative();
		contactsUpdated = true;
	}
	
	public void updateMetrics(PatientMetricsModel metrics)
	{
		if(metrics.getUser() != null)
		{
			if(user == null) user = metrics.getUser();
			else user.update(metrics.getUser(), false);
		}
		if(metrics.getHeight() != null) height = metrics.getHeight();
		if(metrics.getWeight() != null) weight = metrics.getWeight();
		if(metrics.getBloodType() != null) bloodTypeEnum = metrics.getBloodType();
		metricsUpdated = true;
	}
	
	public void updateAllergiesChronicDiseases(PatientAllergiesChronicDiseasesModel model)
	{
		if(model.getAllergies() != null)
		{
			allergies = model.getAllergies();
			allergiesChronicDiseasesUpdated = true;
		}
		if(model.getChronicDiseases() != null)
		{
			chronicDiseases = model.getChronicDiseases();
			allergiesChronicDiseasesUpdated = true;
		}
	}
	
	public boolean isContactsUpdated()
	{
		return contactsUpdated;
	}
	
	public boolean isMetricsUpdated()
	{
		return metricsUpdated;
	}
	
	public boolean isAllergiesChronicDiseasesUpdated()
	{
		return allergiesChronicDiseasesUpdated;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(address, closestRelative);
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof PatientContactsModel)) return false;
		/*
		PatientContactsModel other = (PatientContactsModel)obj;
		return (Objects.equals(user, other.user) &&
			Objects.equals(address, other.address) &&
			Objects.equals(closestRelative, other.closestRelative));
			*/
		return true;
	}
	
	private String id;
	private Integer height;
	private Integer weight;
	private String bloodType;
	private User user;
	private Address address;
	private ClosestRelative closestRelative;
	private Set<Entity> allergies;
	private Set<Entity> chronicDiseases;
	
	private transient BloodType bloodTypeEnum;
	private transient boolean contactsUpdated;
	private transient boolean metricsUpdated;
	private transient boolean allergiesChronicDiseasesUpdated;
}
