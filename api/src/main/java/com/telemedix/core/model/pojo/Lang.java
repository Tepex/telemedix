package com.telemedix.core.model.pojo;

public class Lang extends Entity
{
	public String getLabel()
	{
		return label;
	}
	
	public String getLocale()
	{
		return locale;
	}
	
	@Override
	public String toString()
	{
		return "[id: "+getId()+", label: "+getLabel()+", locale: "+getLocale()+"]";
	}
	
	private String label;
	private String locale;
}
