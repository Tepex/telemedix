package com.telemedix.core.model;

import java.util.Objects;

public class ChangePinCodeModel extends Model
{
	public static Builder newBuilder()
	{
		return new ChangePinCodeModel().new Builder();
	}

	public String getCurrentPinCode()
	{
		return currentPinCode;
	}
	
	public String getPinCode1()
	{
		return pinCode1;
	}
	
	public String getPinCode2()
	{
		return pinCode2;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(getCurrentPinCode(), getPinCode1(), getPinCode2());
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof ChangePinCodeModel)) return false;
		if(this == obj) return true;
		ChangePinCodeModel other = (ChangePinCodeModel)obj;
		return (Objects.equals(getCurrentPinCode(), other.getCurrentPinCode()) &&
			Objects.equals(getPinCode1(), other.getPinCode1()) &&
			Objects.equals(getPinCode2(), other.getPinCode2()));
	}
	
	private String currentPinCode;
	private String pinCode1;
	private String pinCode2;
	
	public class Builder
	{
		private Builder() {}
		
		public Builder setCurrentPinCode(String pin)
		{
			currentPinCode = pin;
			return this;
		}
		
		public Builder setNewPinCode1(String pin)
		{
			pinCode1 = pin;
			return this;
		}
		
		public Builder setNewPinCode2(String pin)
		{
			pinCode2 = pin;
			return this;
		}
		
		public ChangePinCodeModel build()
		{
			ChangePinCodeModel.this.hashCode = 0;
			return ChangePinCodeModel.this;
		}
	}
}
