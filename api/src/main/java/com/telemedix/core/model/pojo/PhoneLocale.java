package com.telemedix.core.model.pojo;

import java.util.Objects;

public class PhoneLocale implements Comparable<PhoneLocale>
{
	public PhoneLocale(int countryCode, String locale)
	{
		this.countryCode = countryCode;
		this.locale = locale;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof PhoneLocale)) return false;
		if(obj == this) return true;
		PhoneLocale other = (PhoneLocale)obj;
		return (Objects.equals(locale, other.locale) && countryCode == other.countryCode);
	}
    
	@Override
	public int hashCode()
	{
		/* Просто вернуть countryCode нельзя, т.к., например, Россия и Казахстан разные
		страны, но с одинаковым кодом */
		return Objects.hashCode(locale);
	}
	
	@Override
	public String toString()
	{
		return locale;
	}
	
	public String getCode()
	{
		return ""+countryCode;
	}
	
	public int getCountryCode()
	{
		return countryCode;
	}
	
	@Override
	public int compareTo(PhoneLocale other)
	{
		return locale.compareTo(other.locale);
	}
	
	private final int countryCode;
	private final String locale;
}
