package com.telemedix.core.model.pojo;

import com.telemedix.core.Utils;

import java.util.Objects;

import org.joda.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class User
{
	private User() {}
	
	public static Builder newBuilder()
	{
		return new User().new Builder();
	}
	public String getId()
	{
		return id;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public String getPatronymic()
	{
		return patronymic;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	
	
	
	public String getSex()
	{
		return sex;
	}
	
	public String getPhoto()
	{
		//return "/api/p/patient/photo";
		//if(photo == null) return "/api/p/patient/photo";
		return photo;
	}
	
	public String getPhoto(int width, int height)
	{
		return getPhoto()+"/"+width+"/"+height;
	}
	
	public String getBirthDateAsString()
	{
		return birthDate;
	}
	
	public LocalDate getBirthday()
	{
		if(birthday == null && birthDate != null) birthday = Utils.parseDate(getBirthDateAsString());
		return birthday;
	}
	
	public String getFormattedBirthday()
	{
		LocalDate date = getBirthday();
		if(date == null) return null;
		return Utils.DATE_FORMATTER.print(date);
	}
	
	public void update(User other, boolean isContacts)
	{
		if(isContacts)
		{	
			firstName = other.getFirstName();
			lastName = other.getLastName();
			patronymic = other.getPatronymic();
			email = other.getEmail();
		}
		else
		{
			sex = other.getSex();
			birthDate = other.getBirthDateAsString();
			birthday = other.getBirthday();
			photo = other.getPhoto();
		}
	}
	
	/* Для тестирования. Обнуление неиспользуемых полей чтобы корректно работал метод equals() */
	public void dirtyHackFlushExcess(boolean isContacts)
	{
		if(isContacts)
		{
			sex = null;
			birthDate = null;
			birthday = null;
		}
		else
		{
			firstName = null;
			lastName = null;
			patronymic = null;
			email = null;
		}
	}
	
	/** 
	 * Грязный хак для опеделения юзера со всеми нулевыми полями. 
	 * Нужно для корректной работы hashCode при инциализации. 
	 */
	public boolean isEmpty()
	{
		return (firstName == null && lastName == null && patronymic == null && email == null && sex == null && birthday == null);
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(getFirstName(), getLastName(), getPatronymic(), getEmail(), getSex(), getBirthday());
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof User)) return false;
		if(obj == this) return true;
		User other = (User)obj;
		return (Objects.equals(getFirstName(), other.getFirstName()) &&
			Objects.equals(getLastName(), other.getLastName()) &&
			Objects.equals(getPatronymic(), other.getPatronymic()) &&
			Objects.equals(getEmail(), other.getEmail()) &&
			Objects.equals(getSex(), other.getSex()) &&
			Objects.equals(getBirthday(), other.getBirthday()));
	}
	
	@Override
	public String toString()
	{
		return "\n[\n\tid: "+id+",\n\tfirstName: "+firstName+",\n\tlastName: "+lastName+",\n\tmiddleName: "+patronymic+",\n\temail: "+email+",\n\tsex: "+sex+",\n\tbirthDate: "+birthDate+"\n\tbirtday: "+getBirthday()+"\n\tphoto: "+getPhoto()+"\n]";
	}

	private String id;
	private String firstName;
	private String lastName;
	private String patronymic;
	private String email;
	private String sex;
	private String birthDate;
	private String photo;
	
	private transient LocalDate birthday;
	private transient int hashCode = 0;
	
	public static final String MALE = "male";
	public static final String FEMALE = "female";
	
	private static final Logger LOG = LoggerFactory.getLogger(User.class);
	
	public class Builder
	{
		private Builder() {}
		
		public Builder setFirstName(String firstName)
		{
			User.this.firstName = firstName;
			return this;
		}
		
		public Builder setLastName(String lastName)
		{
			User.this.lastName = lastName;
			return this;
		}

		public Builder setPatronymic(String patronymic)
		{
			User.this.patronymic = patronymic;
			return this;
		}

		public Builder setEmail(String email)
		{
			User.this.email = email;
			return this;
		}
		
		public Builder setBirthday(String birthDate)
		{
			User.this.birthDate = birthDate;
			return this;
		}
		
		public Builder setSex(String sex)
		{
			User.this.sex = sex;
			return this;
		}
		
		public Builder setPhoto(String photo)
		{
			//User.this.photo = photo;
			return this;
		}
		
		public User build()
		{
			User.this.hashCode = 0;
			return User.this;
		}
	}	
}
