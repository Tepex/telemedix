package com.telemedix.core.model.pojo;

import com.telemedix.core.Utils;

import java.util.List;
import java.util.Objects;

import org.joda.time.DateTime;

public class Doctor
{
	public String getId()
	{
		return id;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public boolean isVideoEnabled()
	{
		return consultationChannel.contains("video");
	}
	
	public float getRating()
	{
		return rating;
	}
	
	public boolean isOnline()
	{
		return Objects.equals(sessionStatus, SS_ONLINE);
	}

	public DateTime getClosestConsultation()
	{
		if(closestConsultationDateTime == null) closestConsultationDateTime = Utils.parseDateTime(closestConsultation);
		return closestConsultationDateTime;
	}
	
	public Entity getSpecialization()
	{
		return specialization;
	}
    
	public Clinic getClinic()
	{
		return clinic;
	}
	
	public List<DoctorService> getServices()
	{
		return services;
	}
	
	/* Convenience methods */
	public String getShortName()
	{
		if(shortName == null)
		{
			if(user == null) shortName = "";
			else
			{
				StringBuilder sb = new StringBuilder();
				if(user.getLastName() != null) sb.append(user.getLastName());
				if(user.getFirstName() != null) sb.append(' ').append(user.getFirstName().substring(0, 1).toUpperCase()).append('.');
				if(user.getPatronymic() != null) sb.append(' ').append(user.getPatronymic().substring(0, 1).toUpperCase()).append('.');
				shortName = sb.toString();
			}
		}
		return shortName;
	}
	
	public int getPrice()
	{
		if(price == NO_PRICE)
		{
			for(DoctorService service: services)
				if(service.isOnlineConsultation())
				{
					price = service.getPrice();
					break;
				}
		}
		return price;
	}
	
	private String id;
	private String city;
	private String consultationChannel;
	private Float rating;
	private String sessionStatus;
	/** ISO date-time */
	private String closestConsultation;
	private List<Lang> communicationLanguages;
	private User user;
	private Entity specialization;
	private Clinic clinic;
	private List<DoctorService> services;
	
	private transient DateTime closestConsultationDateTime;
	private transient String shortName;
	private transient int price = NO_PRICE;
	
	public static final String SS_ONLINE = "online";
	public static final String SS_OFFLINE = "offline";
	
	public static final int NO_PRICE = -1;
}
