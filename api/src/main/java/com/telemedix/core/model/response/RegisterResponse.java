package com.telemedix.core.model.response;

import java.io.IOException;
import java.io.StringReader;

import java.util.Properties;

import retrofit2.HttpException;

public class RegisterResponse
{
	/** For Unit test */
	public static RegisterResponse createSuccessResponse()
	{
		RegisterResponse response = new RegisterResponse();
		response.result = true;
		return response;
	}
	
	/**
	 * TODO: Будет общая модель ошибочных ответов. 
	 */
	public static String createError(HttpException he)
	{
		String json;
		try
		{
			json = he.response().errorBody().string();
		}
		catch(IOException e)
		{
			return "Can't parse error body: "+e.getMessage();
		}
		int start = json.indexOf("errors\":[\"")+10;
		int end = json.indexOf("\"]}}}}}");
		Properties p = new Properties();
		try
		{
			p.load(new StringReader("key="+json.substring(start, end)));
			return p.getProperty("key");
		}
		catch(IOException e)
		{
			return "broken error";
		}
	}
	
	public boolean success()
	{
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof RegisterResponse)) return false;
		if(obj == this) return true;
		RegisterResponse other = (RegisterResponse)obj;
		return (this.result == other.result);
	}
	
	private boolean result;
}
