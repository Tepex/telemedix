package com.telemedix.core.model;

import com.telemedix.core.model.pojo.Locales;
import com.telemedix.core.model.pojo.PhoneLocale;

import java.util.Objects;

/**
 * POJO
 */
public class PhoneModel extends Model
{
	public static PhoneModel create(PhoneLocale phoneLocale, String phoneNumber)
	{
		PhoneModel model = new PhoneModel(); 
		model.phoneLocale = phoneLocale;
		model.phoneNumber = phoneNumber;
		return model;
	}
	
	public static PhoneModel create(String fullPhoneNumber)
	{
		// Грязный хак!
		int i = fullPhoneNumber.length() - 10;
		String pn = fullPhoneNumber.substring(i);
		String code = fullPhoneNumber.substring(0, i);
		PhoneLocale locale;
		try
		{
			locale = Locales.findByCode(Integer.parseInt(code));
		}
		catch(Exception e)
		{
			return null;
		}
		return create(locale, pn);
	}
	
	public PhoneLocale getPhoneLocale()
	{
		return phoneLocale;
	}
	
	public void setPhoneLocale(PhoneLocale phoneLocale)
	{
		this.phoneLocale = phoneLocale;
	}
	
	public String getPhoneNumber()
	{
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumberWithCode()
	{
		return phoneLocale.getCode()+phoneNumber;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = getPhoneNumberWithCode().hashCode();
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof PhoneModel)) return false;
		if(obj == this) return true;
		PhoneModel other = (PhoneModel)obj;
		return (Objects.equals(phoneLocale, other.phoneLocale) &&
			Objects.equals(phoneLocale, other.phoneLocale));
	}
	
	private PhoneLocale phoneLocale;
	private String phoneNumber;
}
