package com.telemedix.core.model.pojo;

import java.util.Objects;

public class Address
{
	private Address() {}
	
	public static Builder newBuilder()
	{
		return new Address().new Builder();
	}
	
	/*
	public Country getCountry()
	{
		return country;
	}
	*/
	
	public String getCity()
	{
		return city;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(city, address);
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof Address)) return false;
		Address other = (Address)obj;
		return (Objects.equals(country, other.country) &&
			Objects.equals(city, other.city) &&
			Objects.equals(address, other.address));
	}
	
	/** 
	 * Грязный хак для опеделения юзера со всеми нулевыми полями. 
	 * Нужно для корректной работы hashCode при инциализации. 
	 */
	public boolean isEmpty()
	{
		return (country == null && city == null && address == null);
	}
	
	//private Country country;
	//private int country = 1;
	/* Мега грязный хак!!! */
	private Object country;
	private String city;
	private String address;
	
	private transient int hashCode;
	
	public class Builder
	{
		private Builder() {}
		
		public Builder setCountry(Country country)
		{
			Address.this.country = Integer.valueOf(1);
			return this;
		}
		
		public Builder setCity(String city)
		{
			Address.this.city = city;
			return this;
		}
		
		public Builder setAddress(String address)
		{
			Address.this.address = address;
			return this;
		}
		
		public Address build()
		{
			Address.this.hashCode = 0;
			return Address.this;
		}
	}
	
	public static class Country
	{
		public Country(int id, String title)
		{
			this.id = id;
			this.title = title;
		}
		
		public int getId()
		{
			return id;
		}
		
		public String getTitle()
		{
			return title;
		}
		
		@Override
		public boolean equals(Object obj)
		{
			if(obj == null || !(obj instanceof Country)) return false;
			Country other = (Country)obj;
			return Objects.equals(title, other.title);
		}
		
		@Override
		public int hashCode()
		{
			return Objects.hashCode(title);
		}
		
		private int id;
		private final String title;
	}
}
