package com.telemedix.core.model.pojo;

public class DoctorService extends Entity
{
	public ClinicService getClinicService()
	{
		return service;
	}
	
	public int getPrice()
	{
		return price;
	}
	
	/* Convenience methods */
	
	@Override
	public String getName()
	{
		if(service != null && service.getService() != null) return service.getService().getName();
		return null;
	}
	
	public boolean isOnlineConsultation()
	{
		if(service != null && service.getService() != null) return service.getService().isOnlineConsultation();
		return false;
	}
	
	private ClinicService service;
	private int price;
	
	public static class ClinicService
	{
		public Service getService()
		{
			return service;
		}
		
		private Service service;
	}
	
	public static class Service extends Entity
	{
		public boolean isOnlineConsultation()
		{
			return isOnlineConsultation;
		}
		
		private boolean isOnlineConsultation;
	}
}
