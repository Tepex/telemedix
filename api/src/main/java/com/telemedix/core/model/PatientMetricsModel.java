package com.telemedix.core.model;

import com.telemedix.core.model.pojo.BloodType;
import com.telemedix.core.model.pojo.User;

import java.util.Objects;

public class PatientMetricsModel extends Model
{
	public PatientMetricsModel() {}
	
	public static Builder newBuilder()
	{
		return new PatientMetricsModel().new Builder();
	}
	
	public String getId()
	{
		return id;
	}
	
	public Integer getHeight()
	{
		return height;
	}
	
	public String getHeightStr()
	{
		if(heightStr == null)
		{
			if(height == null) heightStr = "";
			else heightStr = height.toString();
		}
		return heightStr;
	}
	
	public Integer getWeight()
	{
		return weight;
	}
	
	public String getWeightStr()
	{
		if(weightStr == null)
		{
			if(weight == null) weightStr = "";
			else weightStr = weight.toString();
		}
		return weightStr;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public BloodType getBloodType()
	{
		if(bloodType == null) bloodTypeEnum = BloodType.NONE;
		if(bloodTypeEnum == null) bloodTypeEnum = BloodType.getBloodType(bloodType);
		return bloodTypeEnum;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(getHeight(), getWeight(), getBloodType(), getUser());
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof PatientMetricsModel)) return false;
		if(this == obj) return true;
		PatientMetricsModel other = (PatientMetricsModel)obj;
		
		other.user.dirtyHackFlushExcess(false);
//		LOG.debug("checking user: {}, {}", getUser(), other.getUser());
		return (Objects.equals(getHeight(), other.getHeight()) &&
			Objects.equals(getWeight(), other.getWeight()) &&
			Objects.equals(getBloodType(), other.getBloodType()) &&
			Objects.equals(getUser(), other.getUser()));
	}

	@Override
	public String toString()
	{
		return "[hashCdoe: "+hashCode()+", User: "+getUser()+", height: "+getHeight()+", weight: "+getWeight()+", bloodType: "+getBloodType()+"]";
	}

	private String id;
	private Integer height;
	private Integer weight;
	private String bloodType;
	private User user;
	
	private transient BloodType bloodTypeEnum;
	private transient String heightStr;
	private transient String weightStr;
	
	public class Builder
	{
		private Builder() {}
		
		public Builder setHeight(Integer height)
		{
			PatientMetricsModel.this.height = height;
			if(height != null) heightStr = height.toString();
			return this;
		}
		
		public Builder setHeightStr(String heightStr)
		{
			PatientMetricsModel.this.heightStr = heightStr;
			if(heightStr == null) PatientMetricsModel.this.height = null;
			else
			{
				try
				{
					PatientMetricsModel.this.height = Integer.valueOf(heightStr);
				}
				catch(NumberFormatException e)
				{
					PatientMetricsModel.this.height = null;
				}
			}
			return this;
		}

		public Builder setWeight(Integer weight)
		{
			PatientMetricsModel.this.weight = weight;
			if(weight != null) weightStr = weight.toString();
			return this;
		}
		
		public Builder setWeightStr(String weightStr)
		{
			PatientMetricsModel.this.weightStr = weightStr;
			if(weightStr == null) PatientMetricsModel.this.weight = null;
			else
			{
				try
				{
					PatientMetricsModel.this.weight = Integer.valueOf(weightStr);
				}
				catch(NumberFormatException e)
				{
					PatientMetricsModel.this.weight = null;
				}
			}
			return this;
		}
		
		public Builder setBloodTypeEnum(BloodType bloodTypeEnum)
		{
			if(bloodTypeEnum == BloodType.NONE || bloodTypeEnum == null) PatientMetricsModel.this.bloodType = null;
			else PatientMetricsModel.this.bloodType = bloodTypeEnum.toString();
			PatientMetricsModel.this.bloodTypeEnum = bloodTypeEnum;
			return this;
		}

		public Builder setUser(User user)
		{
			if(user != null && user.isEmpty()) user = null;
			PatientMetricsModel.this.user = user;
			return this;
		}
		
		public PatientMetricsModel build()
		{
			PatientMetricsModel.this.hashCode = 0;
			return PatientMetricsModel.this;
		}
	}
}
