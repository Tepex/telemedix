package com.telemedix.core.model.pojo;

public class Clinic extends Entity
{
	public String getAddress()
	{
		return address;
	}
	
	@Override
	public String toString()
	{
		return "[id: "+getId()+", name: "+getName()+", address: "+getAddress()+"]";
	}
	
	private String address;
}
