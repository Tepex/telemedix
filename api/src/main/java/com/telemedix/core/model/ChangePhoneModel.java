package com.telemedix.core.model;

import com.telemedix.core.model.pojo.Locales;
import com.telemedix.core.model.pojo.PhoneLocale;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

public class ChangePhoneModel extends Model
{
	public ChangePhoneModel()
	{
		int pos = Arrays.binarySearch(Locales.localies, new PhoneLocale(0, Locale.getDefault().getCountry()));
		if(pos == -1) pos = 0;
		defaultLocale = Locales.localies[pos];
		setState(State.NEW);
	}
	
	public static Builder newBuilder()
	{
		return new ChangePhoneModel().new Builder();
	}
	
	public PhoneLocale getOldPhoneLocale()
	{
		if(oldPhoneLocale == null) oldPhoneLocale = defaultLocale;
		return oldPhoneLocale;
	}
	
	public PhoneLocale getNewPhoneLocale()
	{
		if(newPhoneLocale == null) newPhoneLocale = defaultLocale;
		return newPhoneLocale;
	}
	
	public String getOldPhoneNumber()
	{
		return oldPhoneNumber;
	}
	
	public String getNewPhoneNumber()
	{
		return newPhoneNumber;
	}
	
	public String getOldPhoneNumberWithCode()
	{
		return oldPhoneLocale.getCode()+oldPhoneNumber;
	}
	
	public String getNewPhoneNumberWithCode()
	{
		return newPhoneLocale.getCode()+newPhoneNumber;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(getOldPhoneLocale(), getNewPhoneLocale(), getOldPhoneNumber(), getNewPhoneNumber());
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof ChangePhoneModel)) return false;
		if(this == obj) return true;
		ChangePhoneModel other = (ChangePhoneModel)obj;
		return (Objects.equals(getOldPhoneLocale(), other.getOldPhoneLocale()) &&
			Objects.equals(getNewPhoneLocale(), other.getNewPhoneLocale()) &&
			Objects.equals(getOldPhoneNumber(), other.getOldPhoneNumber()) &&
			Objects.equals(getNewPhoneNumber(), other.getNewPhoneNumber()));
	}
	
	private PhoneLocale oldPhoneLocale;
	private PhoneLocale newPhoneLocale;
	private String oldPhoneNumber;
	private String newPhoneNumber;
	
	private transient PhoneLocale defaultLocale;
	
	public class Builder
	{
		private Builder() {}
		
		public Builder setOldPhoneLocale(PhoneLocale phoneLocale)
		{
			oldPhoneLocale = phoneLocale;
			return this;
		}
		
		public Builder setNewPhoneLocale(PhoneLocale phoneLocale)
		{
			newPhoneLocale = phoneLocale;
			return this;
		}
		
		public Builder setOldPhoneNumber(String number)
		{
			oldPhoneNumber = number;
			return this;
		}
		
		public Builder setNewPhoneNumber(String number)
		{
			newPhoneNumber = number;
			return this;
		}

		public ChangePhoneModel build()
		{
			ChangePhoneModel.this.hashCode = 0;
			return ChangePhoneModel.this;
		}
	}
}
