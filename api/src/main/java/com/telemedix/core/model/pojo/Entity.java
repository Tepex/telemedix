package com.telemedix.core.model.pojo;

import java.io.Serializable;
import java.util.Objects;

public class Entity implements Serializable
{
	public Entity() {}
	
	public Entity(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	@Override
	public int hashCode()
	{
		if(id == null) return getName().hashCode();
		return id;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof Entity)) return false;
		if(this == obj) return true;
		Entity other = (Entity)obj;
		if(id == null) return Objects.equals(getName(), other.getName());
		return (getId() == other.getId());
	}
	
	@Override
	public String toString()
	{
		return "[id: "+getId()+", name: "+getName()+"]";
	}
	
	/** Can be NULL */
	protected Integer id;
	protected String name;
	protected transient int hashCode;
	
	private static final long serialVersionUID = 5089949668185268834L;
}
