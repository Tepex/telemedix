package com.telemedix.core.model;

import com.telemedix.core.model.pojo.Doctor;
import com.telemedix.core.model.pojo.Entity;

import com.telemedix.core.model.request.RegisterRequest;
import com.telemedix.core.model.response.RegisterResponse;
import com.telemedix.core.model.response.Response;
import com.telemedix.core.model.response.ValidateCodeResponse;

import io.reactivex.Observable;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.Part;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface Api
{
	/* Регистрация по SMS */
	
	@POST ("app/install/{appid}/{imei}/{token}")
	Observable<AppModel> registerApp(@Path("appid") String appId, @Path("imei") String imei, @Path("token") String token);
	
//	@POST("patient/phone")
	@POST("registration/register/phone")
	Observable<RegisterResponse> registerUserPhone(@Body RegisterRequest request);

	@PATCH("security/change/phone")
	Observable<RegisterResponse> changeUserPhone(@Body RegisterRequest request);

//	@GET("patient/confirm_phone/{phone}")
	@GET("registration/confirm_phone/{phone}")
	Observable<ValidateCodeResponse> confirmPhone(@Path("phone") String phone, @Query("code") String code);
	
	@DELETE("security/delete/profile")
	Observable<Response> deleteProfile();
	
	/* Данные пациента */
	
	@GET("patient/")
	Observable<UserModel> getPatientInfo(@Query("_scope") String scope);

	@GET("patient/")
	Observable<PatientContactsModel> getPatientContacts(@Query("_scope") String scope);

	@GET("patient/")
	Observable<PatientMetricsModel> getPatientMetrics(@Query("_scope") String scope);
	
	@GET("patient/")
	Observable<PatientAllergiesChronicDiseasesModel> getPatientAllergiesChronicDiseases(@Query("_scope") String scope);
	
	/*  Регистрация пациента */
	
	@PATCH("registration/contacts")
	Observable<PatientContactsModel> savePatientContacts(@Body PatientContactsModel request);
	
	@PATCH("registration/metrics")
	Observable<PatientMetricsModel> savePatientMetrics(@Body PatientMetricsModel request);

	@PATCH("registration/allergies_chronic_diseases")
	Observable<PatientAllergiesChronicDiseasesModel> savePatientAllergiesChronicDiseases(@Body PatientAllergiesChronicDiseasesModel request);
	
	@Multipart
	@POST("registration/photo/upload")
	Observable<UserModel> uploadPhoto(@Part MultipartBody.Part photo);
	
	/* Список докторов */
	
	@GET("doctor/")
	Observable<List<Doctor>> getDoctors(@Query("specialization") Integer spec, @Query("clinic") Integer clinic, @Query("city") String city);
	
	@GET("catalog/doctor/specializations")
	Observable<List<Entity>> getSpecializations();
}
