package com.telemedix.core.model.pojo;

import java.util.Objects;

public class ClosestRelative extends Entity
{
	private ClosestRelative() {}
	
	public static Builder newBuilder()
	{
		return new ClosestRelative().new Builder();
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public String getPhone()
	{
		return phone;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(firstName, lastName, phone);
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof ClosestRelative)) return false;
		ClosestRelative other = (ClosestRelative)obj;
		return (Objects.equals(getFirstName(), other.getFirstName()) &&
			Objects.equals(getLastName(), other.getLastName()) &&
			Objects.equals(getPhone(), other.getPhone()));
	}
	
	@Override
	public String toString()
	{
		return "[id: "+getId()+", firstName: "+getFirstName()+", lastName: "+getLastName()+", phone: "+getPhone()+"]";
	}
	
	/** 
	 * Грязный хак для опеделения юзера со всеми нулевыми полями. 
	 * Нужно для корректной работы hashCode при инциализации. 
	 */
	public boolean isEmpty()
	{
		return (getFirstName() == null && getLastName() == null && getPhone() == null);
	}
	
	private String firstName;
	private String lastName;
	private String phone;
	
	public class Builder
	{
		private Builder() {}
		
		public Builder setFirstName(String firstName)
		{
			ClosestRelative.this.firstName = firstName;
			return this;
		}
		
		public Builder setLastName(String lastName)
		{
			ClosestRelative.this.lastName = lastName;
			return this;
		}

		public Builder setPhone(String phone)
		{
			ClosestRelative.this.phone = phone;
			return this;
		}
		
		public ClosestRelative build()
		{
			ClosestRelative.this.hashCode = 0;
			return ClosestRelative.this;
		}
	}
}
