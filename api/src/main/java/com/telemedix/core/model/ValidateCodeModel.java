package com.telemedix.core.model;

import com.telemedix.core.model.response.ValidateCodeResponse;

import java.util.Objects;

/**
 * POJO
 */
public class ValidateCodeModel extends Model
{
	public static ValidateCodeModel create(String phoneNumber, String smsCode, String patientId)
	{
		ValidateCodeModel model = new ValidateCodeModel();
		model.phoneNumber = phoneNumber;
		model.smsCode = smsCode;
		model.patientId = patientId;
		return model;
	}
	
	public String getPhoneNumber()
	{
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	
	public String getSmsCode()
	{
		return smsCode;
	}
	
	public void setSmsCode(String smsCode)
	{
		this.smsCode = smsCode;
	}
	
	public void update(ValidateCodeResponse response)
	{
		patientId = response.getPatientId();
	}
	
	public String getPatientId()
	{
		return patientId;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(phoneNumber, smsCode, patientId);
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof ValidateCodeModel)) return false;
		if(obj == this) return true;
		ValidateCodeModel other = (ValidateCodeModel)obj;
		return (Objects.equals(phoneNumber, other.phoneNumber) &&
			Objects.equals(smsCode, other.smsCode) &&
			Objects.equals(patientId, other.patientId));
	}
	
	@Override
	public String toString()
	{
		return "[phoneNumber: "+phoneNumber+", smsCode: "+smsCode+", patientId: "+patientId+"]";
	}

	/** Request */
	private String phoneNumber;
	private String smsCode;
	/** Response */
	private String patientId;
}
