package com.telemedix.core.model;

import java.util.Objects;

/**
 * POJO
 */
public class AppModel extends Model
{
	public AppModel(String appId, String imei)
	{
		this.appId = appId;
		this.imei = imei;
	}
	
	/* For Unit test */
	public AppModel(String apiKey, String patientId, boolean nothing)
	{
		this("my_unit_test_app_id", "my_unit_test_imei");
		this.apikey = apiKey;
		this.patientId = patientId;
	}
	
	public String getAppId()
	{
		return appId;
	}
	
	public String getImei()
	{
		return imei;
	}

	public String getApiKey()
	{
		return apikey;
	}
	
	public String getPatientId()
	{
		return patientId;
	}
	
	public String getToken()
	{
		return token;
	}
	
	public void setToken(String token)
	{
		this.token = token;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(apikey, patientId);
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof AppModel)) return false;
		if(obj == this) return true;
		AppModel other = (AppModel)obj;
		return (Objects.equals(apikey, other.apikey) &&
			Objects.equals(patientId, other.patientId));
	}
	
	@Override
	public String toString()
	{
		return "[apikey: "+apikey+", patientId: "+patientId+", token: "+token+"]";
	}

	private String apikey;
	private String patientId;
	private boolean ntSend;
	
	private final transient String appId;
	private final transient String imei;
	private transient String token;
}
