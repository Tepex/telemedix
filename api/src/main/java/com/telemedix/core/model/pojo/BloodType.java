package com.telemedix.core.model.pojo;

public enum BloodType
{
	NONE(""),
	I_RH_NEG("I(-)"),
	I_RH_POS("I(+)"),
	II_RH_NEG("II(-)"),
	II_RH_POS("II(+)"),
	III_RH_NEG("III(-)"),
	III_RH_POS("III(+)"),
	IV_RH_NEG("IV(-)"),
	IV_RH_POS("IV(+)");
	
	BloodType(String view)
	{
		this.view = view;
	}
	
	@Override
	public String toString()
	{
		return view;
	}
	
	public static BloodType getBloodType(String s)
	{
		for(BloodType item: BloodType.values()) if(item.toString().equals(s)) return item;
		return null;
	}
	
	private String view;
}
