package com.telemedix.core.model;

import com.telemedix.core.model.pojo.Doctor;
import com.telemedix.core.model.pojo.Entity;
import com.telemedix.core.model.pojo.Scope;

import java.util.List;

public class DoctorsListModel extends Model
{
	public DoctorsListModel()
	{
	}
	
	public DoctorsListModel(List<Doctor> doctorsList, List<Entity> doctorsSpec, Scope scope)
	{
		this.doctorsList = doctorsList;
		this.doctorsSpec = doctorsSpec;
		this.scope = scope;
	}
	
	public List<Doctor> getDoctorsList()
	{
		return doctorsList;
	}
	
	public List<Entity> getDoctorsSpec()
	{
		return doctorsSpec;
	}
	
	public Scope getScope()
	{
		return scope;
	}
	
	private List<Doctor> doctorsList;
	private List<Entity> doctorsSpec;
	private Scope scope;
}
