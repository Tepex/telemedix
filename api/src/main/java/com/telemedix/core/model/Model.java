package com.telemedix.core.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POJO
 */
public class Model
{
	public State getState()
	{
		return state;
	}
	
	public void setState(State state)
	{
		this.state = state;
	}
	
	protected transient int hashCode;
	protected transient String toStringCache;
	
	private transient State state;
	
	protected static final Logger LOG = LoggerFactory.getLogger(Model.class);
	
	public static enum State
	{
		NEW, READY, LOADING, SUCCESS, ERROR;
	
		public static State getErrorState(String error)
		{
			ERROR.error = error;
			return ERROR;
		}
		
		public static State getErrorState(long errorCode)
		{
			ERROR.errorCode = errorCode;
			return ERROR;
		}
	
		@Override
		public String toString()
		{
			String ret = "["+name();
			if(this == ERROR)
			{
				if(error != null) ret += ", error:"+error;
				else if(errorCode != 0) ret += ", code: 0x"+Long.toHexString(errorCode);
			}
			ret += "]";
			return ret;
		}
	
		public String getError()
		{
			return error;
		}
		
		public long getErrorCode()
		{
			return errorCode;
		}
	
		private String error;
		/** Флаги ошибок (задается установкой битов */
		private long errorCode;
	}
}
