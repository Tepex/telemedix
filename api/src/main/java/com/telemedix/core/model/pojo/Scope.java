package com.telemedix.core.model.pojo;

import java.io.Serializable;

/** Критерий поиска врачей */
public class Scope implements Serializable
{
	public Scope(Type type, Entity entity)
	{
		this.type = type;
		this.entity = entity;
	}
	
	public Type getType()
	{
		return type;
	}
	
	public Entity getEntity()
	{
		return entity;
	}
	
	
	@Override
	public String toString()
	{
		return "[type: "+getType().name()+", entity: "+getEntity()+"]";
	}
	
	private Type type;
	private Entity entity;
	
	private static final long serialVersionUID = 6831330070767665756L;
	
	public static enum Type
	{
		SPEC, CLINIC, CITY, NAME;
	}
}
