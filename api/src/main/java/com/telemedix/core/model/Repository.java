package com.telemedix.core.model;

import java.util.concurrent.TimeUnit;

import okhttp3.logging.HttpLoggingInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Выполняет роль интерактора.
 */
public class Repository
{
	public static Repository getInstance(String host, int timeout, boolean isDebug)
	{
		if(INSTANCE == null) INSTANCE = new Repository(host, timeout, isDebug);
		return INSTANCE;
	}
	
	private Repository(String host, int timeout, boolean isDebug)
	{
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		if(isDebug) logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		else logging.setLevel(HttpLoggingInterceptor.Level.NONE);
		
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		httpClient
			.addInterceptor(logging)
			.addInterceptor(chain->
			{
				Request original = chain.request();
				Request request = original
					.newBuilder()
					.method(original.method(), original.body())
					.header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
//					.header(APP_ID, appId)
					.header(API_KEY, apiKey)
					.build();
				Response response = chain.proceed(request);
				return response;
			});
			
		httpClient.connectTimeout(timeout, TimeUnit.SECONDS);
		Retrofit retrofitBackoffice = new Retrofit.Builder()
			.baseUrl(host)
			.client(httpClient.build())
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.build();
		api = retrofitBackoffice.create(Api.class);
	}
	
	public Api getApi()
	{
		return api;
	}
	
	public void setApiKey(String apiKey)
	{
		this.apiKey = apiKey;
		LOG.debug("Repository.setApiKey {}", apiKey);
	}
		
	private final Api api;
//	private final String appId;
	private String apiKey = "";
	
	private static Repository INSTANCE;
	
	private static final Logger LOG = LoggerFactory.getLogger(Repository.class);
	
	private static final String CONTENT_TYPE = "Content-Type";
	private static final String CONTENT_TYPE_VALUE = "application/json";
	
	public static final String CONTENT_TYPE_MULTIPART = "multipart/form-data";
//	private static final String APP_ID = "AppId";
	public static final String API_KEY = "apikey";
}
