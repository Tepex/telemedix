package com.telemedix.core.model.response;

public class ValidateCodeResponse
{
	public static ValidateCodeResponse createSuccessResponse(String patientId)
	{
		ValidateCodeResponse response = new ValidateCodeResponse();
		response.success = true;
		response.patient_id = patientId;
		return response;
	}
	
	public boolean isSuccess()
	{
		return success;
	}
	
	public String getPatientId()
	{
		return patient_id;
	}
	
	private boolean success;
	private String patient_id;
}
