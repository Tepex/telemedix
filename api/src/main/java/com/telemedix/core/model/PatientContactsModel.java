package com.telemedix.core.model;

import com.telemedix.core.model.pojo.Address;
import com.telemedix.core.model.pojo.ClosestRelative;
import com.telemedix.core.model.pojo.User;

import java.util.Objects;

public class PatientContactsModel extends Model
{
	public PatientContactsModel() {}
	
	public static Builder newBuilder()
	{
		return new PatientContactsModel().new Builder();
	}
	
	public String getUserId()
	{
		return id;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public Address getAddress()
	{
		return address;
	}
	
	public ClosestRelative getClosestRelative()
	{
		return closestRelative;
	}
	
	@Override
	public int hashCode()
	{
		if(hashCode == 0) hashCode = Objects.hash(getUser(), getAddress(), getClosestRelative());
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof PatientContactsModel)) return false;
		if(this == obj) return true;
		PatientContactsModel other = (PatientContactsModel)obj;
		// грязный хак
		other.user.dirtyHackFlushExcess(true);
		
		return (Objects.equals(getUser(), other.getUser()) &&
			Objects.equals(getAddress(), other.getAddress()) &&
			Objects.equals(getClosestRelative(), other.getClosestRelative()));
	}
	
	@Override
	public String toString()
	{
		return "[id: "+id+", User: "+user+", address: "+address+", closestRelative: "+closestRelative+"]";
	}
	
	private String id;
	private User user;
	private Address address;
	private ClosestRelative closestRelative;
	
	public class Builder
	{
		private Builder() {}
		
		public Builder setUser(User user)
		{
			if(user != null && user.isEmpty()) user = null;
			PatientContactsModel.this.user = user;
			return this;
		}
		
		public Builder setAddress(Address address)
		{
			if(address != null && address.isEmpty()) address = null;
			PatientContactsModel.this.address = address;
			return this;
		}

		public Builder setClosestRelative(ClosestRelative closestRelative)
		{
			if(closestRelative != null && closestRelative.isEmpty()) closestRelative = null;
			PatientContactsModel.this.closestRelative = closestRelative;
			return this;
		}
		
		public PatientContactsModel build()
		{
			PatientContactsModel.this.hashCode = 0;
			return PatientContactsModel.this;
		}
	}
}
