package com.telemedix.core.model;

import com.telemedix.core.model.pojo.Entity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PatientAllergiesChronicDiseasesModel extends Model
{
	public PatientAllergiesChronicDiseasesModel() {}
	
	public static Builder newBuilder()
	{
		return new PatientAllergiesChronicDiseasesModel().new Builder();
	}
	
	public PatientAllergiesChronicDiseasesModel(Set<Entity> allergiesSet, Set<Entity> chronicDiseasesSet)
	{
		this.allergiesSet = allergiesSet;
		this.chronicDiseasesSet = chronicDiseasesSet;
		if(allergiesSet != null) allergies = allergiesSet.toArray(new Entity[0]);
		if(chronicDiseasesSet != null) chronicDiseases = chronicDiseasesSet.toArray(new Entity[0]);
	}
	
	public Set<Entity> getAllergies()
	{
		if(allergies == null) return null;
		if(allergiesSet == null) allergiesSet = new HashSet<Entity>(Arrays.asList(allergies));
		return allergiesSet;
	}
	
	public Set<Entity> getChronicDiseases()
	{
		if(chronicDiseases == null) return null;
		if(chronicDiseasesSet == null) chronicDiseasesSet = new HashSet<Entity>(Arrays.asList(chronicDiseases));
		return chronicDiseasesSet;
	}
	/*

	@Override
	public int hashCode()
	{
		return Arrays.hashCode(getChronicDiseases());
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof PatientChronicDiseasesModel)) return false;
		if(obj == this) return true;
		PatientChronicDiseasesModel other = (PatientChronicDiseasesModel)obj;
		return Objects.deepEquals(getChronicDiseases(), other.getChronicDiseases());
	}
	*/
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("Allergies: [");
		for(int i = 0; i < allergies.length; ++i)
		{
			sb.append(allergies[i]);
			if(i < (allergies.length-1)) sb.append(", ");
		}
		sb.append(']');
		sb.append("\nChronic: [");
		for(int i = 0; i < chronicDiseases.length; ++i)
		{
			sb.append(chronicDiseases[i]);
			if(i < (chronicDiseases.length-1)) sb.append(", ");
		}
		sb.append(']');
		return sb.toString();
	}

	private Entity[] allergies;
	private Entity[] chronicDiseases;
	
	private transient Set<Entity> allergiesSet;
	private transient Set<Entity> chronicDiseasesSet;
	
	public class Builder
	{
		private Builder() {}
		
		public Builder setAllergies(Set<Entity> allergiesSet)
		{
			if(allergiesSet == null) PatientAllergiesChronicDiseasesModel.this.allergies = null;
			else PatientAllergiesChronicDiseasesModel.this.allergies = allergiesSet.toArray(new Entity[0]);
			PatientAllergiesChronicDiseasesModel.this.allergiesSet = allergiesSet;
			return this;
		}
		
		public Builder setChronicDiseases(Set<Entity> chronicDiseasesSet)
		{
			if(chronicDiseasesSet == null) PatientAllergiesChronicDiseasesModel.this.chronicDiseases = null;
			else PatientAllergiesChronicDiseasesModel.this.chronicDiseases = chronicDiseasesSet.toArray(new Entity[0]);
			PatientAllergiesChronicDiseasesModel.this.chronicDiseasesSet = chronicDiseasesSet;
			return this;
		}

		public PatientAllergiesChronicDiseasesModel build()
		{
			PatientAllergiesChronicDiseasesModel.this.hashCode = 0;
			return PatientAllergiesChronicDiseasesModel.this;
		}
	}
}
