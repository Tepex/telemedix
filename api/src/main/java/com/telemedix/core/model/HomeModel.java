package com.telemedix.core.model;

import com.telemedix.core.model.pojo.Entity;

import java.util.List;

public class HomeModel extends Model
{
	public HomeModel() {}
	
	public HomeModel(List<Entity> specList)
	{
		this.specList = specList;
	}
	
	public List<Entity> getDoctorsSpec()
	{
		return specList;
	}
	
	private List<Entity> specList;
}