package com.telemedix.core.view;

import com.telemedix.core.model.pojo.PhoneLocale;

public interface PhoneView extends View
{
	void setPhoneNumber(String number);
	void setCode(PhoneLocale item);
	void registerPhone();
}
