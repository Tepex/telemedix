package com.telemedix.core.view;

import com.telemedix.core.model.pojo.Entity;

import java.util.Set;

public interface PatientAllergiesChronicDiseasesView extends View
{
	void nextStep();
	void updateNextButton(boolean isChanged);
	
	Set<Entity> getAllergies();
	Set<Entity> getChronicDiseases();
}
