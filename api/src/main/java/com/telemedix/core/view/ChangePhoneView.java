package com.telemedix.core.view;

import com.telemedix.core.model.pojo.PhoneLocale;

public interface ChangePhoneView extends View, HasNextButton
{
	PhoneLocale getOldPhoneLocale();
	String getOldPhoneNumber();
	PhoneLocale getNewPhoneLocale();
	String getNewPhoneNumber();
}
