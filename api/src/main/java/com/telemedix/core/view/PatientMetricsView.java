package com.telemedix.core.view;

import com.telemedix.core.model.pojo.BloodType;

public interface PatientMetricsView extends View, HasNextButton
{
	String getBirthday();
	String getHeight();
	String getWeight();
	BloodType getBloodType();
	String getSex();
}
