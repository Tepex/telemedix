package com.telemedix.core.view;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.presenter.Presenter;

public interface View<P extends Presenter>
{
	P getPresenter();
	
	void setState(State state);
	void onError(State state);
	void onNew();
	void onReady();
	void onSuccess();
	
	void showModel();
	void serverError(int code);
	void networkError(Throwable thr);
	
	void finishView();
}
