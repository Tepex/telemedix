package com.telemedix.core.view;

public interface SecurityView extends View
{
	void changePhone();
	void changePin();
	void delAccount();
}
