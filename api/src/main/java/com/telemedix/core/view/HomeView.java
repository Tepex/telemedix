package com.telemedix.core.view;

import com.telemedix.core.model.pojo.Entity;

import io.reactivex.Observable;

import java.util.List;

public interface HomeView extends View
{
	void startConsultationsListActiviy();
	void startMyPerescriptionsActivity();
	void startRegisterActivity();
	void startSupport();
	void startDoctorsList(Entity entity);
}
