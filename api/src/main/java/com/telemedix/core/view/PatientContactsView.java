package com.telemedix.core.view;

public interface PatientContactsView extends View, HasNextButton
{
	String getFirstName();
	String getMiddleName();
	String getLastName();
	String getEmail();
	
	String getCity();
	String getAddress();
	
	String getClosestRelativeFirstName();
	String getClosestRelativeLastName();
	String getClosestRelativePhone();
}
