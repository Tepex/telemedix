package com.telemedix.core.view;

import com.telemedix.core.model.pojo.Doctor;

public interface DoctorsListView extends View
{
	void selectDoctor(Doctor doctor);
}
