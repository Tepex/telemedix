package com.telemedix.core.view;

public interface ChangePinCodeView extends View, HasNextButton
{
	String getCurrentPinCode();
	String getNewPinCode1();
	String getNewPinCode2();
}
