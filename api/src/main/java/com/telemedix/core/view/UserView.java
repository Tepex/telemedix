package com.telemedix.core.view;

import com.telemedix.core.model.pojo.User;

public interface UserView extends View
{
	void updateMenuHeader(User user);
}
