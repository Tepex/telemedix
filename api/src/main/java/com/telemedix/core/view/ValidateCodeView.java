package com.telemedix.core.view;

public interface ValidateCodeView extends View
{
	void onChangeButtonClick();
	void onSendAgainClick();
	void setSmsCode(String code);
}
