package com.telemedix.core.view;

public interface AppView extends View
{
	void registerApp(String token);
}
