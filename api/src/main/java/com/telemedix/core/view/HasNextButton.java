package com.telemedix.core.view;

public interface HasNextButton
{
	void nextStep();
	void updateNextButton(boolean isChanged);
}
