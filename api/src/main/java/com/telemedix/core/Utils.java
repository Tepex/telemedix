package com.telemedix.core;

import com.telemedix.core.model.pojo.Entity;

import java.util.regex.Pattern;

import java.util.Set;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

public class Utils
{
	public static LocalDate parseDate(String date)
	{
		LocalDate ret = null;
		try
		{
			ret = DATE_FORMATTER.parseLocalDate(date);
		}
		catch(Exception e)
		{
			int i = date.indexOf('+');
			if(i != -1) date = date.substring(0, i);
			date += "Z";
			try
			{
				ret = ISO_FORMATTER.parseLocalDate(date);
			}
			catch(Exception e1)
			{
			}		
		}
		return ret;
	}
	
	public static DateTime parseDateTime(String dateTime)
	{
		if(dateTime == null) return null;
		return ISO_FORMATTER.parseDateTime(dateTime);
	}
	
	public static boolean isDateValid(String date, boolean canBeEmpty)
	{
		if(canBeEmpty && isEmpty(date)) return true;
		try
		{
			parseDate(date);
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	public static boolean isSetsEqual(Set<Entity> set1, Set<Entity> set2)
	{
		if(set1 == null && set2 == null) return true;
		if(set1 == null || set2 == null) return false; 
		if(set1.size() != set2.size()) return false;
		return set1.containsAll(set2);
	}
	
	public static boolean isPhoneNumberValid(String phoneNumber, boolean canBeEmpty)
	{
		if(canBeEmpty && isEmpty(phoneNumber)) return true;
		return PHONE_NUMBER_PATTERN.matcher(phoneNumber).matches();
	}
	
	public static boolean isEmailValid(String email, boolean canBeEmpty)
	{
		if(canBeEmpty && isEmpty(email)) return true;
		return EMAIL_PATTERN.matcher(email).matches();
	}
	
	public static boolean isEmpty(String s)
	{
		return (s == null || s.trim().isEmpty());
	}
	
	public static final DateTimeFormatter ISO_FORMATTER = ISODateTimeFormat.dateTimeNoMillis();
	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("dd.MM.yyyy");
	
	public static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("^\\d{10}$");
	public static final Pattern EMAIL_PATTERN = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

}
