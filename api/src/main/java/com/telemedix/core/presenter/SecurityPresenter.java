package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.SecurityModel;

import com.telemedix.core.view.SecurityView;

public class SecurityPresenter extends Presenter<SecurityView, SecurityModel, RegisterInterceptor>
{
	public void delAccount()
	{
		showLoading();
		addDisposable(getInterceptor().deleteProfile().subscribe(response->success()));
	}
}
