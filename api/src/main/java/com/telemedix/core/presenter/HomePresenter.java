package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.DoctorsInterceptor;
import com.telemedix.core.model.HomeModel;
import com.telemedix.core.view.HomeView;

public class HomePresenter extends Presenter<HomeView, HomeModel, DoctorsInterceptor>
{
	@Override
	protected void initModel()
	{
		showLoading();
		addDisposable(getInterceptor().getDoctorsSpec().subscribe(response->setModel(response)));
	}
}
