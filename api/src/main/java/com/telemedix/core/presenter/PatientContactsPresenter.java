package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.pojo.Address;
import com.telemedix.core.model.pojo.ClosestRelative;
import com.telemedix.core.model.pojo.User;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.PatientContactsModel;

import com.telemedix.core.Utils;
import com.telemedix.core.view.PatientContactsView;

import java.util.Objects;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatientContactsPresenter extends Presenter<PatientContactsView, PatientContactsModel, PatientInterceptor>
{
	@Override
	protected void initModel()
	{
		showLoading();
		/*
		 * Получение данных из вне (сервер или наш кеш в Interceptor).
		 * Сохраняем локально и оповещаем View.
		 */
		addDisposable(getInterceptor().getPatientContacts().subscribe(response->setModel(response)));
	}
	
	/**
	 * Сохранение данных формы на сервере. Обновление кеша происходит в Interceptor
	 * @TODO: доделать валидацию
	 */
	public void saveData()
	{
		long err = validateUserInput();
		if(err != ERR_NONE)
		{
			error(err);
			return;
		}
		
		commitStageModel();
		if(isUserChangeModel())
		{
			LOG.debug("Saving contacts model: {}", getModel());
			showLoading();
			addDisposable(getInterceptor().savePatientContacts(getModel()).subscribe(response->success()));
		}
		else success();
	}
	
	@Override
	protected PatientContactsModel buildModelFromView()
	{
		return PatientContactsModel.newBuilder()
			.setUser(User.newBuilder()
				.setFirstName(getView().getFirstName())
				.setPatronymic(getView().getMiddleName())
				.setLastName(getView().getLastName())
				.setEmail(getView().getEmail())
				.build())
			.setAddress(Address.newBuilder()
				.setCountry(new Address.Country(1, "Россия")) // Грязный хак!!!
				.setCity(getView().getCity())
				.setAddress(getView().getAddress())
				.build())
			.setClosestRelative(ClosestRelative.newBuilder()
				.setFirstName(getView().getClosestRelativeFirstName())
				.setLastName(getView().getClosestRelativeLastName())
				.setPhone(getView().getClosestRelativePhone())
				.build())
			.build();
	}
	
	@Override
	protected long validateUserInput()
	{
		long err = ERR_NONE;
		if(getModel().getUser() != null && !Utils.isEmailValid(getModel().getUser().getEmail(), true)) err |= ERR_EMAIL;
		
		String phone = null;
		if(getModel().getClosestRelative() != null) phone = getModel().getClosestRelative().getPhone();
		/*
		if(phone != null) phone = phone.replaceAll("\\D", "");
		if(!Utils.isPhoneNumberValid(phone, true)) err |= ERR_PHONE;
		*/
		if(phone != null)
		{
			Matcher matcher = RELATIVE_PHONE_PATTERN.matcher(phone); 
			if(!matcher.matches()) err |= ERR_PHONE;
		}
		return err;
	}
	
	public static final long ERR_EMAIL = 0x1;
	public static final long ERR_PHONE = 0x2;
	
	private static final Pattern RELATIVE_PHONE_PATTERN = Pattern.compile("\\+?\\d{10,11}");
}
