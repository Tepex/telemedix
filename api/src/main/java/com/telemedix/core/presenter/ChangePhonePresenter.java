package com.telemedix.core.presenter;

import com.telemedix.core.Utils;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.Model.State;

import com.telemedix.core.model.ChangePhoneModel;
import com.telemedix.core.model.PhoneModel;

import com.telemedix.core.model.pojo.PhoneLocale;

import com.telemedix.core.view.ChangePhoneView;

import java.util.Objects;

public class ChangePhonePresenter extends Presenter<ChangePhoneView, ChangePhoneModel, RegisterInterceptor>
{
	public ChangePhonePresenter(String currentPhoneNumber)
	{
		this.currentPhoneNumber = currentPhoneNumber;
	}
	
	public void changePhone()
	{
		commitStageModel();
		if(isUserChangeModel())
		{
			LOG.debug("Changing phone number: {}", getModel());
			showLoading();
			addDisposable(getInterceptor().changePhone(getModel()).subscribe(response->success()));
		}
		else success();
	}
	
	@Override
	protected ChangePhoneModel buildModelFromView()
	{
		return ChangePhoneModel.newBuilder()
			.setOldPhoneLocale(getView().getOldPhoneLocale())
			.setNewPhoneLocale(getView().getNewPhoneLocale())
			.setOldPhoneNumber(getView().getOldPhoneNumber())
			.setNewPhoneNumber(getView().getNewPhoneNumber())
			.build();
	}
	
	@Override
	protected long validateUserInput()
	{
		long err = ERR_NONE;
		String oldNumber = getModel().getOldPhoneNumber();
		String newNumber = getModel().getNewPhoneNumber();
		if(oldNumber == null) err |= ERR_OLD_EMPTY;
		if(newNumber == null) err |= ERR_NEW_EMPTY;
		
		if(oldNumber != null)
		{
			if(!Utils.isPhoneNumberValid(oldNumber, false)) err |= ERR_OLD_INVALID;
			else if(!getModel().getOldPhoneNumberWithCode().equals(currentPhoneNumber)) err |= ERR_OLD_NOT_MATCH;
		}
		if(newNumber != null && !Utils.isPhoneNumberValid(newNumber, false)) err |= ERR_NEW_INVALID;
		
		if(oldNumber != null && newNumber != null &&
			Objects.equals(getModel().getOldPhoneLocale(), getModel().getNewPhoneLocale()) &&
			Objects.equals(getModel().getOldPhoneNumber(), getModel().getNewPhoneNumber())) err |= ERR_EQUALS;
		return err;
	}
	
	@Override
	public boolean copyOnWrite()
	{
		super.copyOnWrite();
		long err = validateUserInput();
		if(err == ERR_NONE) ready();
		else error(err);
		return true;
	}
	
	private String currentPhoneNumber;
	
	public static final long ERR_OLD_EMPTY		= 0x01;
	public static final long ERR_NEW_EMPTY		= 0x02;
	public static final long ERR_OLD_INVALID	= 0x04;
	public static final long ERR_NEW_INVALID	= 0x08;
	public static final long ERR_OLD_NOT_MATCH	= 0x10;
	public static final long ERR_EQUALS			= 0x20;
}
