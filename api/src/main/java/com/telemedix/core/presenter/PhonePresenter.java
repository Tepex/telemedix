package com.telemedix.core.presenter;

import com.telemedix.core.Utils;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.Model.State;

import com.telemedix.core.model.pojo.PhoneLocale;

import com.telemedix.core.model.PhoneModel;

import com.telemedix.core.model.response.RegisterResponse;

import com.telemedix.core.view.PhoneView;

import io.reactivex.disposables.Disposable;

import java.io.IOException;

import java.net.UnknownHostException;

import retrofit2.HttpException;

/**
 * <ul>
 * 	<li>Изменение кода страны</li>
 * 	<li>Изменение номера телефона</li>
 * 	<li>Валидация номера</li>
 * 	<li>Нормализация номера</li>
 * </ul>
 */
public class PhonePresenter extends Presenter<PhoneView, PhoneModel, RegisterInterceptor>
{
	@Override
	protected void initModel() {}

	/* PhoneNumber View */
	
	/** Смена телефонного кода страны */
	public void onPhoneLocaleChanged(PhoneLocale phoneLocale)
	{
		getModel().setPhoneLocale(phoneLocale);
	}
	
	/** Смена телефонного номера */
	public void onPhoneNumberChanged(String text)
	{
		if(text == null) text = "";
		String phoneNumber = text.replaceAll("\\D", "");
		getModel().setPhoneNumber(phoneNumber);
		if(Utils.isPhoneNumberValid(phoneNumber, false)) ready();
		else newState();
	}
	
	public void registerPhone()
	{
		showLoading();
		addDisposable(getInterceptor().registerPhone(getModel()).subscribe(response->
		{
			if(response.success())
			{
				success();
				getModel().setState(State.READY);
			}
			else error("Undefined error");
		},
		error->
		{
			ready();
			if(error instanceof HttpException)
			{
				HttpException he = (HttpException)error;
				if(he.code() == 500) error(he.message());
				else if(he.code() == 400) error(RegisterResponse.createError(he));
				else serverError(he.code());
			}
			else networkError(error);
		}));
	}
	
	@Override
	protected PhoneModel buildModelFromView()
	{
		return null;
	}
}
