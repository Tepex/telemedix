package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.DoctorsInterceptor;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.DoctorsListModel;
import com.telemedix.core.model.pojo.Scope;

import com.telemedix.core.view.DoctorsListView;

public class DoctorsListPresenter extends Presenter<DoctorsListView, DoctorsListModel, DoctorsInterceptor>
{
	public void loadDoctorsList(final Scope scope)
	{
		showLoading();
		addDisposable(getInterceptor().getDoctors(scope).subscribe(response->
		{
			origin = response.getDoctorsList();
			setModel(response);
		}));
	}
	
	public void setFilter(String search)
	{
		this.search = search;
		addDisposable(Observable.fromIterable(origin)
			.filter()
			.subscribe(list->setModel()));
	}
	
	private List<Doctor> origin;
	private String search;
}
