package com.telemedix.core.presenter;

import java.util.concurrent.atomic.AtomicLong;

import java.util.HashMap;
import java.util.Map;

public class PresenterCache
{
	public static PresenterCache getInstance(long seed)
	{
		if(INSTANCE == null) INSTANCE = new PresenterCache(seed);
		return INSTANCE;
	}
	
	private PresenterCache(long seed)
	{
		nextId = new AtomicLong(seed);
	}
	
	public long generateId()
	{
		return nextId.getAndIncrement();
	}
	
	@SuppressWarnings("unchecked")
	public <P extends Presenter> P get(long id)
	{
		return (P)map.get(id);
	}
	
	public void put(long id, Presenter presenter)
	{
		map.put(id, presenter);
	}
	
	public void remove(long id)
	{
		map.remove(id);
	}
	
	public long getId()
	{
		return nextId.get();
	}
	
	@Override
	public String toString()
	{
		return "[size="+map.size()+", nextId="+nextId.get()+", "+super.toString()+"]";
	}
	
	private static PresenterCache INSTANCE;
	
	private Map<Long, Presenter> map = new HashMap<>();
	private AtomicLong nextId;
}
