package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.AppInterceptor;
import com.telemedix.core.model.AppModel;
import com.telemedix.core.model.Model.State;
import com.telemedix.core.view.AppView;

import java.util.concurrent.TimeUnit;

import retrofit2.HttpException;

public class AppPresenter extends Presenter<AppView, AppModel, AppInterceptor>
{
	/** Приход токена от Firebase Cloud Messaging */
	public void registerApp(final String token)
	{
		showLoading();
		addDisposable(getInterceptor().registerApp(getModel().getAppId(), getModel().getImei(), token)
			.subscribe(response->
			{
				/** @TODO: переделать в copyOnWrite */
				response.setToken(token);
				setModel(response);
				LOG.debug("AppPresenter.registerApp OK: {}", getModel());
			},
			error->
			{
				if(error instanceof HttpException)
				{
					HttpException he = (HttpException)error;
					if(he.code() == 500) LOG.warn("API. registerApp serverError 500: {}", he.message());
					serverError(he.code());
				}
				else networkError(error);
			}));
	}
}
