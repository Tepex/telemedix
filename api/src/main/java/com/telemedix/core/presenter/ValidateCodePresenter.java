package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.RegisterInterceptor;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.PhoneModel;
import com.telemedix.core.model.ValidateCodeModel;

import com.telemedix.core.model.response.RegisterResponse;

import com.telemedix.core.view.ValidateCodeView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.HttpException;

public class ValidateCodePresenter extends Presenter<ValidateCodeView, ValidateCodeModel, RegisterInterceptor>
{
	@Override
	protected void initModel() {}

	/* ValidateCode View */
	
	/** Смена СМС-кода пользователем */
	public void onSmsCodeChanged(String smsCode)
	{
		getModel().setSmsCode(smsCode);
		if(SMS_CODE_PATTERN.matcher(smsCode).matches()) confirmPhone();
		else newState();
	}
	
	public boolean onReceiveSms(String sms)
	{
		Matcher matcher = SMS_PATTERN.matcher(sms);
		if(matcher.matches())
		{
			String code = matcher.group(1);
			getModel().setSmsCode(code);
			if(getView() != null) getView().setSmsCode(code);
			return true;
		}
		return false;
	}
	
	public void resendSms()
	{
		showLoading();
		PhoneModel phoneModel = PhoneModel.create(getModel().getPhoneNumber());
		addDisposable(getInterceptor().registerPhone(phoneModel).subscribe(response->
		{
			if(response.success())
			{
				ready();
			}
			else error("Undefined error");
		},
		error->
		{
			ready();
			if(error instanceof HttpException)
			{
				HttpException he = (HttpException)error;
				if(he.code() == 500) error(he.message());
				else if(he.code() == 400) error(RegisterResponse.createError(he));
				else serverError(he.code());
			}
			else networkError(error);
		}));
	}
	
	public void confirmPhone()
	{
		showLoading();
		addDisposable(getInterceptor().confirmPhone(getModel()).subscribe(response->
		{
			if(response.isSuccess())
			{
				getModel().update(response);
				success();
			}
			else error("Invalid code!");
		},
		error->networkError(error)));
	}
		
	public static final Pattern SMS_CODE_PATTERN = Pattern.compile("^\\d{4}$");
	public static final Pattern SMS_PATTERN = Pattern.compile("^Telemedix:\\s(\\d{4})$");
}
