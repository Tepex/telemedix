package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.Interceptor;

import com.telemedix.core.model.Model;
import com.telemedix.core.model.PatientMetricsModel;
import com.telemedix.core.model.Model.State;

import com.telemedix.core.view.View;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Вся логика сосредоточена здесь. Не должен ничего знать об Android-компонентах.  
 */
public class Presenter<V extends View, M extends Model, I extends Interceptor>
{
	public void onCreate(M m, I i)
	{
		interceptor = i;
		model = m;
		initModel();
	}
	
	public M getModel()
	{
		if(stageModel != null) return stageModel;
		return model;
	}
	
	protected void setModel(M m)
	{
		model = m;
		stageModel = null;
//		model.setState(State.NEW);
//		ready();
		newState();
	}
	
	public boolean isUserChangeModel()
	{
		return (stageModel != null);
	}
	
	protected void commitStageModel()
	{
		if(stageModel != null) model = stageModel;
		model.setState(State.READY);
	}
	
	public I getInterceptor()
	{
		return interceptor;
	}
	
	public void bindView(V view)
	{
		this.view = view;
		view.setState(getModel().getState());
	}
	
	public void unbindView()
	{
		/* Сохранить состояние модели (для onConfigurationChanged) */
		if(isUserChangeModel()) copyOnWrite();
		view = null;
	}
	
	public V getView()
	{
		return view;
	}
	
	public void onDestroy()
	{
		compositeDisposable.clear();
	}
	
	protected void showLoading()
	{
		getModel().setState(State.LOADING);
		if(view != null) view.setState(State.LOADING);
	}
	
	protected void newState()
	{
		getModel().setState(State.NEW);
		if(getView() != null) getView().setState(State.NEW);
	}
	
	protected void success()
	{
		getModel().setState(State.SUCCESS);
		if(view != null) view.setState(State.SUCCESS);
	}
	
	protected void ready()
	{
		getModel().setState(State.READY);
		if(view != null) view.setState(State.READY);
	}
	
	protected void error(String err)
	{
		getModel().setState(State.getErrorState(err));
		if(view != null) view.setState(getModel().getState());
	}
	
	protected void error(long code)
	{
		if(code == ERR_NONE) return;
		getModel().setState(State.getErrorState(code));
		if(view != null) view.setState(getModel().getState());
	}
	
	protected void serverError(int code)
	{
		getModel().setState(State.NEW);
		if(view != null)
		{
			view.setState(getModel().getState());
			view.serverError(code);
		}
	}
	
	protected void networkError(Throwable error)
	{
		getModel().setState(State.READY);
		if(view != null)
		{
			view.setState(getModel().getState());
			view.networkError(error);
		}
	}
	
	protected void addDisposable(Disposable disposable)
	{
		compositeDisposable.add(disposable);
	}
	
	/**  Инициализация данных. */
	protected void initModel() {}
	
	protected M buildModelFromView()
	{
		return null;
	}
	
	protected long validateUserInput()
	{
		return ERR_NONE;
	}
	
	/**
	 * Создание копии модели при изменениях во View
	 *
	 * @return возвращает true, если данные реально измененились.
	 */
	public boolean copyOnWrite()
	{
		boolean changed = (stageModel != null);
		LOG.debug("Presenter.copyOnWrite {}. changed: {}", getClass().getSimpleName(), changed);
		stageModel = buildModelFromView();
//		LOG.debug("{}. model.hash: {}, stage.hash: {}", getClass().getSimpleName(), model.hashCode(), (stageModel == null ? null : stageModel.hashCode()));
		if(stageModel == null) return false;
		
		changed = (stageModel.hashCode() != model.hashCode());
		if(!changed) stageModel = null;
		else stageModel.setState(State.NEW);
		LOG.debug("{}. return changed: {}", getClass().getSimpleName(), changed);
		
		return changed;
	}
	
	private V view;
	private M model;
	/** Промежуточная модель для View. Для отслеживания изменений между View и Model */
	private M stageModel;
	private I interceptor;
	
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	
	public static final long ERR_NONE = 0x0;
	
	protected static final Logger LOG = LoggerFactory.getLogger(Presenter.class);
}
