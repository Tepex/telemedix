package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.Interceptor;

import com.telemedix.core.model.Model.State;

import com.telemedix.core.model.ChangePinCodeModel;

import com.telemedix.core.view.ChangePinCodeView;

import java.util.Objects;

public class ChangePinCodePresenter extends Presenter<ChangePinCodeView, ChangePinCodeModel, Interceptor>
{
	public ChangePinCodePresenter(String currentPinCode)
	{
		this.currentPinCode = currentPinCode;
	}
	
	@Override
	protected ChangePinCodeModel buildModelFromView()
	{
		return ChangePinCodeModel.newBuilder()
			.setCurrentPinCode(getView().getCurrentPinCode())
			.setNewPinCode1(getView().getNewPinCode1())
			.setNewPinCode2(getView().getNewPinCode2())
			.build();
	}
	
	@Override
	protected long validateUserInput()
	{
		long err = ERR_NONE;
		/* Проверяем, что если у пользователя уже имеется ПИН-код, то он правильно его ввел */
		if(currentPinCode != null && !Objects.equals(currentPinCode, getModel().getCurrentPinCode())) err |= ERR_INVALID_CURRENT_PIN_CODE;
		
		/* Проверяем, что новый ПИН-код равен новому ПИН-коду, введенному повторно 
		(в том числе оба могут быть null, тогда ПИН-код сбрасывается) */
		if(!Objects.equals(getModel().getPinCode1(), getModel().getPinCode2())) err |= ERR_EQUALS;
		return err;
	}
	
	@Override
	public boolean copyOnWrite()
	{
		super.copyOnWrite();
		long err = validateUserInput();
		if(err == ERR_NONE) ready();
		else error(err);
		return true;
	}
	
	private String currentPinCode;
	
	public static final long ERR_INVALID_CURRENT_PIN_CODE	= 0x01;
	public static final long ERR_EQUALS						= 0x02;
}
