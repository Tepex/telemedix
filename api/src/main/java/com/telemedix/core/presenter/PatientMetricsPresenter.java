package com.telemedix.core.presenter;

import com.telemedix.core.Utils;

import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.PatientMetricsModel;

import com.telemedix.core.model.pojo.User;

import com.telemedix.core.view.PatientMetricsView;

import java.util.Objects;

public class PatientMetricsPresenter extends Presenter<PatientMetricsView, PatientMetricsModel, PatientInterceptor>
{
	@Override
	protected void initModel()
	{
		showLoading();
		addDisposable(getInterceptor().getPatientMetrics().subscribe(response->setModel(response)));
	}
	
	/**
	 * Сохранение данных формы на сервере. Обновление кеша происходит в Interceptor
	 * @TODO: доделать валидацию
	 */
	public void saveData()
	{
		long err = validateUserInput();
		if(err != ERR_NONE)
		{
			error(err);
			return;
		}
		
		commitStageModel();
		if(isUserChangeModel())
		{
			LOG.debug("Saving metrics model: {}", getModel());
			showLoading();
			addDisposable(getInterceptor().savePatientMetrics(getModel()).subscribe(response->success()));
		}
		else success();
	}
	
	public void uploadPhoto(String path)
	{
		showLoading();
		addDisposable(getInterceptor().uploadPhoto(path).subscribe(response->ready()));
	}
	
	@Override
	protected PatientMetricsModel buildModelFromView()
	{
		return PatientMetricsModel.newBuilder()
			.setUser(User.newBuilder()
				.setBirthday(getView().getBirthday())
				.setSex(getView().getSex())
				.build())
			.setHeightStr(getView().getHeight())
			.setWeightStr(getView().getWeight())
			.setBloodTypeEnum(getView().getBloodType())
			.build();
	}
	
	@Override
	protected long validateUserInput()
	{
		long err = ERR_NONE;
		LOG.debug("metrics user: {}", (getModel().getUser() == null ? "null" : getModel().getUser().getBirthDateAsString()));
		if(getModel().getUser() != null && !Utils.isDateValid(getModel().getUser().getBirthDateAsString(), true)) err |= ERR_BIRTHDAY;
		if(getView().getHeight() != null)
		{
			try
			{
				int height = Integer.parseInt(getView().getHeight());
				if(height < MIN_HEIGHT || height > MAX_HEIGHT) err |= ERR_HEIGHT;
			}
			catch(NumberFormatException e)
			{
				err |= ERR_HEIGHT;
			}
		}
		if(getView().getWeight() != null)
		{
			try
			{
				int weight = Integer.parseInt(getView().getWeight());
				if(weight < MIN_WEIGHT || weight > MAX_WEIGHT) err |= ERR_WEIGHT;
			}
			catch(NumberFormatException e)
			{
				err |= ERR_WEIGHT;
			}
		}
		return err;
	}
	
	public static final int MIN_HEIGHT = 50;
	public static final int MAX_HEIGHT = 250;
	public static final int MIN_WEIGHT = 3;
	public static final int MAX_WEIGHT = 250;
	
	public static final long ERR_BIRTHDAY	= 0x1;
	public static final long ERR_HEIGHT		= 0x2;
	public static final long ERR_WEIGHT		= 0x4;
}
