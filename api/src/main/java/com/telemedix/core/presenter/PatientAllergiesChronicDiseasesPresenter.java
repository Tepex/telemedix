package com.telemedix.core.presenter;

import com.telemedix.core.Utils;

import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.Model.State;
import com.telemedix.core.model.PatientAllergiesChronicDiseasesModel;

import com.telemedix.core.view.PatientAllergiesChronicDiseasesView;

public class PatientAllergiesChronicDiseasesPresenter extends Presenter<PatientAllergiesChronicDiseasesView, PatientAllergiesChronicDiseasesModel, PatientInterceptor>
{
	@Override
	protected void initModel()
	{
		showLoading();
		addDisposable(getInterceptor().getPatientAllergiesChronicDiseases().subscribe(response->setModel(response)));
	}
	
	/**
	 * Сохранение данных формы на сервере. Обновление кеша происходит в Interceptor
	 * @TODO: доделать валидацию
	 */
	public void saveData()
	{
		commitStageModel();
		if(isUserChangeModel())
		{
			LOG.debug("Saving allergies chronic model: {}", getModel());
			showLoading();
			addDisposable(getInterceptor().savePatientAllergiesChronicDiseases(getModel()).subscribe(response->success()));
		}
		else success();
	}

	@Override
	protected PatientAllergiesChronicDiseasesModel buildModelFromView()
	{
		return PatientAllergiesChronicDiseasesModel.newBuilder()
			.setAllergies(getView().getAllergies())
			.setChronicDiseases(getView().getChronicDiseases())
			.build();
	}
}
