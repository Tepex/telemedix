package com.telemedix.core.presenter;

import com.telemedix.core.interceptor.PatientInterceptor;

import com.telemedix.core.model.UserModel;

import com.telemedix.core.view.UserView;

public class UserPresenter extends Presenter<UserView, UserModel, PatientInterceptor>
{
	@Override
	protected void initModel()
	{
		addDisposable(getInterceptor().getPatientInfo().subscribe(response->setModel(response)));
	}
}
