package com.telemedix.core.interceptor;

import com.telemedix.core.model.ChangePhoneModel;
import com.telemedix.core.model.PhoneModel;
import com.telemedix.core.model.ValidateCodeModel;

import com.telemedix.core.model.request.RegisterRequest;

import com.telemedix.core.model.response.RegisterResponse;
import com.telemedix.core.model.response.Response;
import com.telemedix.core.model.response.ValidateCodeResponse;

import io.reactivex.Observable;

public class RegisterInterceptor extends Interceptor
{
	public Observable<RegisterResponse> registerPhone(PhoneModel model)
	{
		return api.registerUserPhone(new RegisterRequest(model.getPhoneNumberWithCode()))
			.subscribeOn(subscribeOn)
			.observeOn(observeOn);
	}
	
	public Observable<RegisterResponse> changePhone(ChangePhoneModel model)
	{
		return api.changeUserPhone(new RegisterRequest(model.getNewPhoneNumberWithCode()))
			.subscribeOn(subscribeOn)
			.observeOn(observeOn);
	}
	
	public Observable<ValidateCodeResponse> confirmPhone(ValidateCodeModel model)
	{
		return api.confirmPhone(model.getPhoneNumber(), model.getSmsCode())
			.subscribeOn(subscribeOn)
			.observeOn(observeOn);
	}
	
	public Observable<Response> deleteProfile()
	{
		return api.deleteProfile()
			.subscribeOn(subscribeOn)
			.observeOn(observeOn);
	}
}
