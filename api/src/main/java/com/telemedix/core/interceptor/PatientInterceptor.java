package com.telemedix.core.interceptor;

import com.telemedix.core.model.PatientAllergiesChronicDiseasesModel;
import com.telemedix.core.model.PatientContactsModel;
import com.telemedix.core.model.UserModel;
import com.telemedix.core.model.PatientMetricsModel;
import com.telemedix.core.model.Repository;

import io.reactivex.Observable;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;

public class PatientInterceptor extends Interceptor
{
	public Observable<UserModel> getPatientInfo()
	{
		if(userModel.getState() != null) return Observable.just(userModel);
		else return api.getPatientInfo(SCOPE_PROFILE)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.doOnNext(response->{userModel = response;});
	}
	
	
	
	/**
	 */
	public Observable<PatientContactsModel> getPatientContacts()
	{
		if(userModel.isContactsUpdated())
		{
			LOG.debug("return contact data from cache");
			return Observable.just(userModel.getPatientContactsModel());
		}
		else return api.getPatientContacts(SCOPE_CONTACTS)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.doOnNext(response->userModel.updateContacts(response));
	}
	
	public Observable<PatientContactsModel> savePatientContacts(PatientContactsModel request)
	{
		return api.savePatientContacts(request)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.doOnNext(response->userModel.updateContacts(response));
	}
	
	
	public Observable<PatientMetricsModel> getPatientMetrics()
	{
		if(userModel.isMetricsUpdated()) return Observable.just(userModel.getPatientMetricsModel());
		else return api.getPatientMetrics(SCOPE_METRICS)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.doOnNext(response->userModel.updateMetrics(response));
	}
	
	public Observable<PatientMetricsModel> savePatientMetrics(PatientMetricsModel request)
	{
		return api.savePatientMetrics(request)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.doOnNext(response->userModel.updateMetrics(response));
	}
	
	
	
	public Observable<PatientAllergiesChronicDiseasesModel> getPatientAllergiesChronicDiseases()
	{
		if(userModel.isAllergiesChronicDiseasesUpdated()) return Observable.just(userModel.getAllergiesChronicDiseasesModel());
		else return api.getPatientAllergiesChronicDiseases(SCOPE_ALLERGIES_CHRONICS)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.doOnNext(response->userModel.updateAllergiesChronicDiseases(response));
	}
	
	public Observable<PatientAllergiesChronicDiseasesModel> savePatientAllergiesChronicDiseases(PatientAllergiesChronicDiseasesModel request)
	{
		return api.savePatientAllergiesChronicDiseases(request)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.doOnNext(response->userModel.updateAllergiesChronicDiseases(response));
	}
	
	
	public Observable<UserModel> uploadPhoto(String path)
	{
		LOG.debug("Start uploading photo: {}", path);
		File file = new File(path);
		RequestBody requestFile = RequestBody.create(MediaType.parse(Repository.CONTENT_TYPE_MULTIPART), file);
		MultipartBody.Part body = MultipartBody.Part.createFormData(USER_PHOTO_NAME, file.getName(), requestFile);
		return api.uploadPhoto(body)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.onErrorReturn(e->
			{
				LOG.error("Upload photo error", e);
				return null;
			});
	}
	
	public UserModel getCachedPatientInfo()
	{
		return userModel;
	}
	
	public void clearCache()
	{
		userModel = new UserModel();
	}
	
	
	private UserModel userModel = new UserModel();
	
	/** запрос только данных пользователя без его консультаций, назначений и пр. */
	public static final String SCOPE_PROFILE = "profile";
	public static final String SCOPE_CONTACTS = "contacts";
	public static final String SCOPE_METRICS = "metrics";
	/** У Гоши тут ошибка — одна «l» в слове «allergies» */
	public static final String SCOPE_ALLERGIES_CHRONICS = "alergies_chronics";
	
	private static final String USER_PHOTO_NAME = "user[photo]";
}
