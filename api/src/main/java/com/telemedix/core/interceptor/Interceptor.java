package com.telemedix.core.interceptor;

import com.telemedix.core.model.Api;
import io.reactivex.Scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Interceptor
{
	public void setApi(Api api)
	{
		this.api = api;
	}
	
	public void setSubscribeOn(Scheduler subscribeOn)
	{
		this.subscribeOn = subscribeOn;
	}
	
	public void setObserveOn(Scheduler observeOn)
	{
		this.observeOn = observeOn;
	}
	
	protected Api api;
	protected Scheduler subscribeOn;
	protected Scheduler observeOn;
	
	protected static final Logger LOG = LoggerFactory.getLogger(Interceptor.class);
}
