package com.telemedix.core.interceptor;

import com.telemedix.core.model.AppModel;

import io.reactivex.Observable;

public class AppInterceptor extends Interceptor
{
	public Observable<AppModel> registerApp(String appId, String imei, String token)
	{
		return api.registerApp(appId, imei, token)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn);
//			.onErrorReturn(err->new AppRegisterResponse());
	}
}
