package com.telemedix.core.interceptor;

import com.telemedix.core.model.pojo.Doctor;
import com.telemedix.core.model.pojo.Entity;
import com.telemedix.core.model.pojo.Scope;

import com.telemedix.core.model.DoctorsListModel;
import com.telemedix.core.model.HomeModel;

import io.reactivex.Observable;

import java.util.List;

public class DoctorsInterceptor extends Interceptor
{
	public DoctorsInterceptor(List<Entity> specializations)
	{
		this.specializations = specializations;
	}
	
	public Observable<DoctorsListModel> getDoctors(final Scope scope)
	{
		/* Решили докторов не кешировать */
		/*
		if(doctorsListCache != null) return Observable.just(doctorsListCache);
		else return api.getDoctors()
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.doOnNext(response->{patientInfo = response;});
		*/
		Integer spec = null;
		Integer clinic = null;
		String city = null;
		if(scope != null)
		{
			if(scope.getType() == Scope.Type.SPEC && scope.getEntity().getId() != 0) spec = scope.getEntity().getId();
			else if(scope.getType() == Scope.Type.CLINIC) clinic = spec = scope.getEntity().getId();
			else if(scope.getType() == Scope.Type.CITY) city = scope.getEntity().getName();
		}
		return api.getDoctors(spec, clinic, city)
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.map(list->new DoctorsListModel(list, specializations, scope));
	}
	
	public Observable<HomeModel> getDoctorsSpec()
	{
		if(specializations != null) return Observable.just(new HomeModel(specializations));
		else return api.getSpecializations()
			.subscribeOn(subscribeOn)
			.observeOn(observeOn)
			.map(list->
			{
				specializations = list;
				return new HomeModel(list);
			});
	}
	
	private List<Doctor> doctorsListCache;
	private List<Entity> specializations;
}
