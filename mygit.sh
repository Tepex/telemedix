#!/bin/bash
#
# $ ./mygit.sh [-i] patch <messsage>
# -ip — increment patch explicitly

ANDROID_MANIFEST="app/src/main/AndroidManifest.xml"

is_increment=false
for i in "$@" ; do
	if [[ $i == "-ip" ]] ; then
		is_increment=true
        break
    fi
done

chunk=$(head -n 5 ${ANDROID_MANIFEST})
#echo "$chunk"
re='android:versionName="([0-9]+)\.([0-9]+)\.([0-9]+)-([0-9]+)">'

if [[ $chunk =~ $re ]]; then
	major="${BASH_REMATCH[1]}"
	minor="${BASH_REMATCH[2]}"
	patch="${BASH_REMATCH[3]}"
	build="${BASH_REMATCH[4]}"

	#TODO: записать в манифест новую версию
	
	if [ "$is_increment" == true ] ; then ((patch++))
	fi
	
#	echo "major: $major"
#	echo "minor: $minor"
#	echo "patch: $patch"
#	echo "build: $patch"
	tag="v${major}.${minor}.${patch}"
else echo "versionName not found!"
fi

if [ "$1" == "patch" ]; then
	if [ -z "$2" ]; then
		echo "No message for commit!"
		exit 1
	fi
	git add .
	git commit -m "$2"
	git tag $tag -m "Patch v${major}.${minor}.${patch}-${build}"
	git status
else echo "command not found!"
fi
